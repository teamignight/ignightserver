#clean build
gradle clean 

#create WAR
gradle createWar

#upload if exists
if [ -f 'build/dist/IgNight.war' ]; then
    echo "Uploading WAR to DEV..."
    aws s3 cp build/dist/IgNight.war s3://dev.resources.ignight/artifacts/
else
    echo "WAR file missing in build/dist/"
fi
