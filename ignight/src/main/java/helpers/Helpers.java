package helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.ignight.dna.DNA;
import com.ignight.dna.elements.Atmosphere;
import com.ignight.dna.elements.Music;

public class Helpers {
	public static Integer getAverage(ArrayList<Integer> dataList) {

		Integer sumOfData = 0;

		Iterator<Integer> dataIter = dataList.iterator();
		while (dataIter.hasNext()) {
			Integer currentDataValue = dataIter.next();
			sumOfData += currentDataValue;
		}
		return (int) Math.round((sumOfData / (dataList.size())));
	}

	public static HashMap<String, Double> getMeanAndVariance(ArrayList<Double> dataList) {

		Double mK = 0.0;
		Double qK = 0.0;
		Double sumOfData = 0.0;
		int count = 1;

		Iterator<Double> dataIter = dataList.iterator();
		while (dataIter.hasNext()) {
			Double currentDataValue = dataIter.next();
			sumOfData += currentDataValue;
			if (count == 1) {
				mK = currentDataValue;
			} else {
				Double oldMk = mK;
				mK += (currentDataValue - mK) / count;
				qK += ((count - 1) * Math.pow(currentDataValue - oldMk, 2)) / count;
			}
			count++;
		}

		count--;
		HashMap<String, Double> statisticsOutputMap = new HashMap<String, Double>();
		statisticsOutputMap.put("Mean", sumOfData / count);
		statisticsOutputMap.put("Stdev", Math.sqrt(qK / (count - 1)));

		return statisticsOutputMap;
	}

	public static Integer getMode(ArrayList<Integer> dataList) {
		Integer maxIndex = 0;
		Integer maxValue = 0;

		Map<Integer, Integer> frequencies = new HashMap<Integer, Integer>();

		Iterator<Integer> dataListIterator = dataList.iterator();

		while (dataListIterator.hasNext()) {
			Integer val = dataListIterator.next();
			if (frequencies.containsKey(val)) {
				frequencies.put(val, frequencies.get(val) + 1);
			} else {
				frequencies.put(val, 0);
			}
		}

		for (Map.Entry<Integer, Integer> entry : frequencies.entrySet()) {

			if (entry.getValue() > maxValue) {
				maxIndex = entry.getKey();
				maxValue = entry.getValue();
			}
		}
		return maxIndex;
	}

	public static String getAgeBucketMode(ArrayList<Integer> dataList) {
		ArrayList<String> ageRangeBuckets = new ArrayList<String>(Arrays.asList("18-20", "21-25", "26-30", "31-35",
				"36-40", "41+"));
		Integer modeIndex = getMode(dataList);
		return ageRangeBuckets.get(modeIndex);
	}

	public static Long getMusicBucketMode(ArrayList<Integer> dataList) {
		Integer modeIndex = getMode(dataList);
		return Music.values()[modeIndex].getId();
	}

	public static Long getAtmosphereBucketMode(ArrayList<Integer> dataList) {
		Integer modeIndex = getMode(dataList);
		return Atmosphere.values()[modeIndex].getId();
	}

	public static void addToCrowd(Long age, DNA dna, ConcurrentHashMap<Long, Long> ageBucketCountMap,
			ConcurrentHashMap<Long, Long> musicCountMap, ConcurrentHashMap<Long, Long> atmosphereCountMap,
			ConcurrentHashMap<Long, Long> spendingCountMap) {
		Long ageCount = ageBucketCountMap.get(age);
		if (ageCount == null)
			ageCount = 0l;
		ageBucketCountMap.put(age, ++ageCount);

		Set<Long> musicPrefsAlreadyAdded = new HashSet<Long>();
		for (int i = 0; i < dna.musicPrefs.size(); i++) {
			long music = dna.musicPrefs.get(i);

			Long musicCount = musicCountMap.get(music);
			if (musicCount == null)
				musicCount = 0l;
			if (musicPrefsAlreadyAdded.add(music))
				musicCountMap.put(music, musicCount += (3 - i));
		}

		Set<Long> atmospherePrefsAlreadyAdded = new HashSet<Long>();
		for (int i = 0; i < dna.atmospherePrefs.size(); i++) {
			long atm = dna.atmospherePrefs.get(i);
			Long atmCount = atmosphereCountMap.get(atm);
			if (atmCount == null)
				atmCount = 0l;
			if (atmospherePrefsAlreadyAdded.add(atm))
				atmosphereCountMap.put(atm, atmCount += (3 - i));
		}

		Long spendingCount = spendingCountMap.get(dna.spendingLimit);
		if (spendingCount == null)
			spendingCount = 0l;
		spendingCountMap.put(dna.spendingLimit, ++spendingCount);
	}

	public static void addToCrowd(Long age, ConcurrentHashMap<Long, Long> ageBucketCountMap) {
		Long ageCount = ageBucketCountMap.get(age);
		if (ageCount == null)
			ageCount = 0l;
		ageBucketCountMap.put(age, ++ageCount);
	}

	public static void subtractFromCrowd(Long age, DNA dna, ConcurrentHashMap<Long, Long> ageBucketCountMap,
			ConcurrentHashMap<Long, Long> musicCountMap, ConcurrentHashMap<Long, Long> atmosphereCountMap,
			ConcurrentHashMap<Long, Long> spendingCountMap) {
		Long ageCount = ageBucketCountMap.get(age);
		if (ageCount != null) {
			if (ageCount < 2) {
				ageBucketCountMap.remove(age);
			} else {
				ageBucketCountMap.put(age, --ageCount);
			}
		}

		Set<Long> musicPrefsAlreadyAdded = new HashSet<Long>();
		for (int i = 0; i < dna.musicPrefs.size(); i++) {
			long music = dna.musicPrefs.get(i);
			Long musicCount = musicCountMap.get(music);
			if (musicCount != null) {
				if (musicCount <= (3 - i)) {
					musicCountMap.remove(music);
				} else {
					if (musicPrefsAlreadyAdded.add(music))
						musicCountMap.put(music, musicCount -= (3 - i));
				}
			}
		}

		Set<Long> atmospherePrefsAlreadyAdded = new HashSet<Long>();
		for (int i = 0; i < dna.atmospherePrefs.size(); i++) {
			long atm = dna.atmospherePrefs.get(i);
			Long atmCount = atmosphereCountMap.get(atm);
			if (atmCount != null) {
				if (atmCount <= (3 - i)) {
					atmosphereCountMap.remove(atm);
				} else {
					if (atmospherePrefsAlreadyAdded.add(atm))
						atmosphereCountMap.put(atm, atmCount -= (3 - i));
				}
			}
		}

		Long spendingCount = spendingCountMap.get(dna.spendingLimit);
		if (spendingCount != null) {
			if (spendingCount < 2) {
				spendingCountMap.remove(dna.spendingLimit);
			} else {
				spendingCountMap.put(dna.spendingLimit, --spendingCount);
			}
		}
	}

	public static void subtractFromCrowd(Long age, ConcurrentHashMap<Long, Long> ageBucketCountMap) {
		Long ageCount = ageBucketCountMap.get(age);
		if (ageCount != null) {
			if (ageCount < 2) {
				ageBucketCountMap.remove(age);
			} else {
				ageBucketCountMap.put(age, --ageCount);
			}
		}
	}

	public static long getBucket(ConcurrentHashMap<Long, Long> venueCrowd) {
		long max = -1;
		long bucket = -1;

		for (Map.Entry<Long, Long> entry : venueCrowd.entrySet()) {
			long val = entry.getValue();
			if (val > max) {
				max = val;
				bucket = Long.valueOf(entry.getKey().toString());
			} else if (val == max) {
				bucket = Math.min(bucket, Long.valueOf(entry.getKey().toString()));

			}
		}
		return bucket;
	}
}
