package databases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.util.IgnightProperties;

public class MySQLConnection {
	private static final Logger log = LoggerFactory.getLogger(MySQLConnection.class);

	private static Connection connection = null;
	private String dbUserName = IgnightProperties.getInstance().getMySqlUser();
	private String dbPassword = IgnightProperties.getInstance().getMySqlPassword();
	private String dbUrl = "jdbc:mysql://" + IgnightProperties.getInstance().getMySqlHost() + "/"
			+ IgnightProperties.getInstance().getMySqlDbName();

	private static MySQLConnection instance;

	public MySQLConnection() {
		try {
			connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
		} catch (SQLException e) {
			log.error("MySql Connection Failed. Error: {}", e.getMessage(), e);
		}
	}

	public static synchronized MySQLConnection getInstance() {
		if (instance == null) {
			instance = new MySQLConnection();
		}
		return instance;
	}

	public int executeUpdateQuery(String query) throws SQLException {
		log.info("Preparing to perform executeUpdateQuery with query: {}", query);
		Statement stmt = null;
		int result = 0;
		try {
			stmt = connection.createStatement();
			result = stmt.executeUpdate(query);
			log.info("Number of rows impacted by update is: {}", result);
		} catch (SQLException e) {
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
		return result;
	}

	public ResultSet executeQuery(String query) throws SQLException {
		log.info("Preparing to perform executeQuery with query: {}", query);
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = connection.createStatement();
			rs = stmt.executeQuery(query);
		} catch (SQLException e) {
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
		return rs;
	}

}
