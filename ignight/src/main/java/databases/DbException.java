package databases;

import java.sql.SQLException;

public class DbException extends RuntimeException {
	public DbException() {
	}

	public DbException(String s) {
		super(s);
	}

	public DbException(String s, Throwable throwable) {
		super(s, throwable);
	}

	public DbException(Throwable throwable) {
		super(throwable);
	}

	public int getErrorCode() {
		if (getCause() instanceof SQLException) {
			return ((SQLException) getCause()).getErrorCode();
		}
		return -1;
	}
}
