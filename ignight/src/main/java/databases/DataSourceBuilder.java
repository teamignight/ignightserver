package databases;

import javax.sql.DataSource;

import com.util.IgnightProperties;
import com.zaxxer.hikari.HikariDataSource;

public class DataSourceBuilder {
	public static DataSource createDataSource() {
		HikariDataSource source = new HikariDataSource();
		source.setMaximumPoolSize(IgnightProperties.getInstance().getMySqlMaxPoolSize().intValue());
		source.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
		source.addDataSourceProperty("serverName", IgnightProperties.getInstance().getMySqlHost());
		source.addDataSourceProperty("port", "3306");
		source.addDataSourceProperty("databaseName", IgnightProperties.getInstance().getMySqlDbName());
		source.addDataSourceProperty("user", IgnightProperties.getInstance().getMySqlUser());
		source.addDataSourceProperty("password", IgnightProperties.getInstance().getMySqlPassword());
		source.setConnectionTestQuery("SELECT 1");
		source.setReadOnly(false);
		return source;
	}

	public static DataSource createDataSourceArchive() {
		HikariDataSource source = new HikariDataSource();
		source.setMaximumPoolSize(IgnightProperties.getInstance().getMySqlMaxPoolSize().intValue());
		source.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
		source.addDataSourceProperty("serverName", IgnightProperties.getInstance().getMySqlHostArchive());
		source.addDataSourceProperty("port", "3306");
		source.addDataSourceProperty("databaseName", IgnightProperties.getInstance().getMySqlDbNameArchive());
		source.addDataSourceProperty("user", IgnightProperties.getInstance().getMySqlUserArchive());
		source.addDataSourceProperty("password", IgnightProperties.getInstance().getMySqlPasswordArchive());
		source.setConnectionTestQuery("SELECT 1");
		source.setReadOnly(false);
		return source;
	}

}
