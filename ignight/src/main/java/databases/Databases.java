package databases;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Databases {
	private static Logger log = LoggerFactory.getLogger(Databases.class);
	public static DataSource dataSource;
	public static DataSource dataSourceArchiving;

	public static boolean update(String sql, Object... objects) throws SQLException {
		Connection conn = null;
		boolean updated = false;
		try {
			conn = dataSource.getConnection();
			updated = update(conn, sql, objects);
		} finally {
			Resources.close(conn);
		}
		return updated;
	}

	public static boolean update(Connection conn, String sql, Object... objects) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(sql);
			int i = 1;
			for (Object o : objects) {
				ps.setObject(i++, o);
			}
			return ps.executeUpdate() != 0;
		} finally {
			Resources.close(ps, rs);
		}
	}

	public static Long insert(String sql, Object... objects) throws SQLException {
		Connection conn = null;
		try {
			conn = dataSource.getConnection();
			return insert(conn, sql, objects);
		} finally {
			Resources.close(conn);
		}
	}

	public static Long insert(Connection conn, String sql, Object... objects) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			int i = 1;
			for (Object o : objects) {
				ps.setObject(i++, o);
			}

			int updated = ps.executeUpdate();
			if (updated < 1) {
				throw new SQLException("Insert Failed");
			}
			rs = ps.getGeneratedKeys();

			if (rs.next()) {
				return (long) rs.getInt(1);
			}
			return null;
		} finally {
			Resources.close(ps, rs);
		}
	}

	public static boolean deleted(String sql, Object... objects) throws SQLException {
		Connection conn = null;
		try {
			conn = dataSource.getConnection();
			return delete(conn, sql, objects);
		} finally {
			Resources.close(conn);
		}
	}

	public static boolean delete(Connection conn, String sql, Object... objects) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(sql);
			int i = 1;
			for (Object o : objects) {
				ps.setObject(i++, o);
			}

			int deleted = ps.executeUpdate();
			if (deleted == 0) {
				return false;
			}
			return true;
		} finally {
			Resources.close(ps, rs);
		}
	}

	public static void upsert(String sql, Object... objects) throws SQLException {
		Connection conn = null;
		try {
			conn = dataSource.getConnection();
			upsert(conn, sql, objects);
		} finally {
			Resources.close(conn);
		}
	}

	public static void upsert(Connection conn, String sql, Object... objects) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(sql);
			int i = 1;
			for (Object o : objects) {
				ps.setObject(i++, o);
			}
			ps.executeUpdate();
		} finally {
			Resources.close(ps, rs);
		}
	}

	public static long selectCount(String sql, Object... objects) throws SQLException {
		Connection conn = null;
		try {
			conn = dataSource.getConnection();
			return selectCount(conn, sql, objects);
		} finally {
			Resources.close(conn);
		}
	}

	public static long selectCount(Connection conn, String sql, Object... objects) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(sql);
			int i = 1;
			for (Object o : objects) {
				ps.setObject(i++, o);
			}
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getLong(1);
			}
			return 0;
		} finally {
			Resources.close(ps, rs);
		}
	}

	public static Long selectLong(String sql, Object... objects) throws SQLException {
		Connection conn = null;
		try {
			conn = dataSource.getConnection();
			return selectLong(conn, sql, objects);
		} finally {
			Resources.close(conn);
		}
	}

	public static Long selectLong(Connection conn, String sql, Object... objects) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(sql);
			int i = 1;
			for (Object o : objects) {
				ps.setObject(i++, o);
			}
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getLong(1);
			}
			return null;
		} finally {
			Resources.close(ps, rs);
		}
	}

	public static <T> T transact(Transactable<T> transactable) {
		Connection conn = null;
		try {
			conn = openConnection();
		} catch (Exception e) {
			throw new DbException(e);
		}

		try {
			final T t = transactable.transact(conn);
			conn.commit();
			return t;
		} catch (Exception e) {
			Resources.rollback(conn);
			throw new DbException(e);
		} finally {
			Resources.close(conn);
		}
	}

	public static Connection openConnection() {
		Connection c = null;
		try {
			c = dataSource.getConnection();
			if (c != null) {
				c.setAutoCommit(false);
				return c;
			} else {
				throw new DbException("Failed to create connection");
			}
		} catch (SQLException e) {
			Resources.close(c);
			throw new DbException(e);
		}
	}
}
