package databases;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Resources {
	private static final Logger logger = LoggerFactory.getLogger(Resources.class);

	public static void close(Connection conn, Statement stmt, ResultSet rs) {
		close(conn);
		close(stmt);
		close(rs);
	}

	public static void close(Connection conn, Statement stmt) {
		close(conn);
		close(stmt);
	}

	public static void close(Statement stmt, ResultSet rs) {
		close(stmt);
		close(rs);
	}

	public static void close(Connection resource) {
		try {
			if (resource != null) {
				resource.close();
			}
		} catch (SQLException e) {

		}
	}

	public static void close(Statement resource) {
		try {
			if (resource != null) {
				resource.close();
			}
		} catch (SQLException e) {

		}
	}

	public static void close(ResultSet resource) {
		try {
			if (resource != null) {
				resource.close();
			}
		} catch (SQLException e) {

		}
	}

	public static void rollback(Connection conn) {
		if (conn != null) {
			try {
				conn.rollback();
			} catch (SQLException e) {
				logger.error("Failed to rollback transaction", e);
			}
		}
	}
}
