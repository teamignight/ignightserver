package databases;

public interface Transactable<V> {
	V transact(java.sql.Connection connection) throws Exception;
}
