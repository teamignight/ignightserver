package databases;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Transaction;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.util.IgnightProperties;

import exceptions.PropertiesException;

public class JedisConnection {
	private static Logger log = LoggerFactory.getLogger(JedisConnection.class);
	private static JedisConnection cityInstance = null;

	public static final Integer SECS_IN_DAY = 86400;

	private static JedisPool pool = null;
	Gson gson = null;

	private JedisConnection() throws PropertiesException {
		gson = new GsonBuilder().disableHtmlEscaping().create();

		String host = IgnightProperties.getInstance().getRedisHost();
		if (host == null) {
			log.error("Redis host not set");
			throw new PropertiesException("Redis host not set");
		}

		pool = new JedisPool(new JedisPoolConfig(), host);

		//		Properties config = new Properties();
		//		File configFile = new File("./lib/dev.config");
		//		if (configFile.exists()) {
		//			try {
		//				InputStream in = new FileInputStream(configFile);
		//				config.load(in);
		//				pool = new JedisPool(new JedisPoolConfig(), config.getProperty("redis"));
		//			} catch (FileNotFoundException e) {
		//				e.printStackTrace();
		//			} catch (IOException e) {
		//				e.printStackTrace();
		//			}
		//		} else {
	}

	public static void shutdown() {
		pool.destroy();
	}

	public static synchronized JedisConnection getInstance() throws PropertiesException {
		if (cityInstance == null) {
			cityInstance = new JedisConnection();
		}
		return cityInstance;
	}

	public boolean exists(String key) {
		Jedis jedis = pool.getResource();
		try {
			return jedis.exists(key);
		} finally {
			pool.returnResource(jedis);
		}
	}

	public void set(String key, Object value) {
		Jedis jedis = pool.getResource();
		try {
			if (value instanceof String) {
				jedis.set(key, (String) value);
			} else {
				jedis.set(key, gson.toJson(value));
			}
		} finally {
			pool.returnResource(jedis);
		}
	}

	//
	//	public void lpush(String key, String... values) {
	//		Jedis jedis = pool.getResource();
	//		try {
	//			jedis.lpush(key, values);
	//		} finally {
	//			pool.returnResource(jedis);
	//		}
	//	}

	public double zincrby(String key, double incr, String member) {
		Jedis jedis = pool.getResource();
		try {
			return jedis.zincrby(key, incr, member);
		} finally {
			pool.returnResource(jedis);
		}
	}

	public Long zadd(String key, double score, String member) {
		Jedis jedis = pool.getResource();
		try {
			return jedis.zadd(key, score, member);
		} finally {
			pool.returnResource(jedis);
		}
	}

	public long zrem(String key, String member) {
		Jedis jedis = pool.getResource();
		try {
			return jedis.zrem(key, member);
		} finally {
			pool.returnResource(jedis);
		}
	}

	public Set<String> zrevrange(String key, long start, long stop) {
		Jedis jedis = pool.getResource();
		try {
			return jedis.zrevrange(key, start, stop);
		} finally {
			pool.returnResource(jedis);
		}
	}

	public Set<String> hkeys(String key) {
		Jedis jedis = pool.getResource();
		try {
			return jedis.hkeys(key);
		} finally {
			pool.returnResource(jedis);
		}
	}

	public boolean hset(String key, String field, Object value) {
		Jedis jedis = pool.getResource();
		try {
			if (value instanceof String) {
				jedis.hset(key, field, (String) value);
			} else {
				jedis.hset(key, field, gson.toJson(value));
			}
			return true;
		} finally {
			pool.returnResource(jedis);
		}
	}

	public void hmset(String key, Map<String, String> values) {
		Jedis jedis = pool.getResource();
		try {
			jedis.hmset(key, values);
		} finally {
			pool.returnResource(jedis);
		}
	}

	public List<String> hmget(String key, Object... values) {
		if (values.length == 0) {
			return new ArrayList<String>();
		}
		String[] vals = new String[values.length];
		for (int i = 0; i < values.length; i++) {
			Object value = values[i];
			if (value instanceof String) {
				vals[i] = (String) value;
			} else {
				vals[i] = gson.toJson(value);
			}
		}
		return hmget(key, vals);
	}

	public List<String> hmget(String key, String... fields) {
		if (fields.length == 0) {
			return new ArrayList<String>();
		}
		Jedis jedis = pool.getResource();
		try {
			return jedis.hmget(key, fields);
		} finally {
			pool.returnResource(jedis);
		}
	}

	public boolean hdel(String key, String... fields) {
		if (fields.length <= 0) {
			return true;
		}
		Jedis jedis = pool.getResource();
		try {
			return (jedis.hdel(key, fields) > 0) ? true : false;
		} finally {
			pool.returnResource(jedis);
		}
	}

	public boolean hdel(String key, String field) {
		Jedis jedis = pool.getResource();
		try {
			return (jedis.hdel(key, field) > 0) ? true : false;
		} finally {
			pool.returnResource(jedis);
		}
	}

	public boolean del(String key) {
		Jedis jedis = pool.getResource();
		try {
			return (jedis.del(key) > 0) ? true : false;
		} finally {
			pool.returnResource(jedis);
		}
	}

	public String get(String key) {
		Jedis jedis = pool.getResource();
		try {
			return (jedis.get(key));
		} finally {
			pool.returnResource(jedis);
		}
	}

	public boolean sadd(String key, String... values) {
		Jedis jedis = pool.getResource();
		try {
			return (jedis.sadd(key, values) > 0) ? true : false;
		} finally {
			pool.returnResource(jedis);
		}
	}

	public boolean sadd(String key, String value) {
		Jedis jedis = pool.getResource();
		try {
			return (jedis.sadd(key, value.toLowerCase()) > 0) ? true : false;
		} finally {
			pool.returnResource(jedis);
		}
	}

	public void srem(String key, String... values) {
		Jedis jedis = pool.getResource();
		try {
			jedis.srem(key, values);
		} finally {
			pool.returnResource(jedis);
		}
	}

	public Set<String> smembers(String key) {
		Jedis jedis = pool.getResource();
		try {
			return jedis.smembers(key);
		} finally {
			pool.returnResource(jedis);
		}
	}

	public boolean sismember(String key, String member) {
		Jedis jedis = pool.getResource();
		try {
			return jedis.sismember(key, member.toLowerCase());
		} finally {
			pool.returnResource(jedis);
		}
	}

	public long scard(String key) {
		Jedis jedis = pool.getResource();
		try {
			return jedis.scard(key);
		} finally {
			pool.returnResource(jedis);
		}
	}

	public String srand(String key) {
		Jedis jedis = pool.getResource();
		try {
			return jedis.srandmember(key);
		} finally {
			pool.returnResource(jedis);
		}
	}

	public long getAndIncr(String key) {
		Jedis jedis = pool.getResource();
		try {
			String val = jedis.get(key);
			if (val != null) {
				jedis.incr(key);
				return Long.parseLong(val);
			}

			jedis.set(key, "0");
			return 0;
		} finally {
			pool.returnResource(jedis);
		}
	}

	public Long hlen(String key) {
		Jedis jedis = pool.getResource();
		try {
			return jedis.hlen(key);
		} finally {
			pool.returnResource(jedis);
		}
	}

	public Map<String, String> hgetall(String key) {
		Jedis jedis = pool.getResource();
		try {
			return jedis.hgetAll(key);
		} finally {
			pool.returnResource(jedis);
		}
	}

	public String hget(String key, String field) {
		Jedis jedis = pool.getResource();
		try {
			return jedis.hget(key, field);
		} finally {
			pool.returnResource(jedis);
		}
	}

	public long incr(String key) {
		Jedis jedis = pool.getResource();
		try {
			return jedis.incr(key);
		} finally {
			pool.returnResource(jedis);
		}
	}

	public Set<String> keys(String regex) {
		Jedis jedis = pool.getResource();
		try {
			return jedis.keys(regex);
		} finally {
			pool.returnResource(jedis);
		}
	}

	public void setex(String key, String val, int expireSecs) {
		Jedis jedis = pool.getResource();
		try {
			jedis.setex(key, expireSecs, val);
		} finally {
			pool.returnResource(jedis);
		}
	}

	public void set(String key, String val) {
		Jedis jedis = pool.getResource();
		try {
			jedis.set(key, val);
		} finally {
			pool.returnResource(jedis);
		}
	}

	public Transaction transaction() {
		Jedis jedis = pool.getResource();
		try {
			return jedis.multi();
		} finally {
			pool.returnResource(jedis);
		}
	}
}
