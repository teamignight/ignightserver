package databases;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SqlQueryExecutor {
	private static final Logger log = LoggerFactory.getLogger(MySQLConnection.class);

	public Connection connection = null;

	private static SqlQueryExecutor instance;

	private SqlQueryExecutor() {
	}

	public static synchronized SqlQueryExecutor getInstance() {
		if (instance == null) {
			instance = new SqlQueryExecutor();
		}
		return instance;
	}

	public static void closeStatementAndConnection(Statement statement, Connection connection) {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				log.error("Error closing prepared statement!");
				log.error("Error: {}", e.getMessage(), e);
			}
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				log.error("Error closing connection!");
				log.error("Error: {}", e.getMessage(), e);
			}
		}
	}
}
