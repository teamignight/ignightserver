package com.util;

public class IgnightMessage {

	public Long timeStamp;
	public String subject;
	public String message;

	public IgnightMessage(Long timeStamp, String subject, String message) {
		super();
		this.timeStamp = timeStamp;
		this.subject = subject;
		this.message = message;
	}
}
