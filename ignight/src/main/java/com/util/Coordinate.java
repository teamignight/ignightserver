package com.util;

public class Coordinate {
	public Double latitude;
	public Double longitude;

	public Coordinate(double lat, double lon) {
		this.latitude = lat;
		this.longitude = lon;
	}

	@Override
	public String toString() {
		return "latitude: " + latitude + ", longitude: " + longitude;
	}
}