package com.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;

public class HTTPRequestSender {

	public static String addExistingUserPostMessageBuilder(String existingUserJSON) throws IOException {
		Map<String, String> postMap = new HashMap<String, String>();
		Map<String, Map<String, String>> msgMap = new HashMap<String, Map<String, String>>();

		postMap.put("type", "addExistingUser");
		postMap.put("existingUserInfo", existingUserJSON);
		msgMap.put("msg", postMap);

		Gson gson = new Gson();
		String postData = gson.toJson(msgMap);
		return postData;
	}

	public static String remoteLoginPostMessageBuilder(String userName, String password) throws IOException {
		Map<String, String> postMap = new HashMap<String, String>();
		Map<String, Map<String, String>> msgMap = new HashMap<String, Map<String, String>>();

		postMap.put("type", "login");
		postMap.put("userName", userName);
		postMap.put("password", password);
		msgMap.put("msg", postMap);
		Gson gson = new Gson();
		String postData = gson.toJson(msgMap);
		return postData;
	}

	public static String remoteLoginCheckIfEmailIsAvailableMessageBuilder(String email) throws IOException {
		Map<String, String> postMap = new HashMap<String, String>();
		Map<String, Map<String, String>> msgMap = new HashMap<String, Map<String, String>>();
		postMap.put("type", "checkIfEmailExists");
		postMap.put("email", email);
		msgMap.put("msg", postMap);
		Gson gson = new Gson();
		String postData = gson.toJson(msgMap);
		return postData;
	}

	public static String remoteLoginGetUserMessageBuilder(Long userId) throws IOException {
		Map<String, String> postMap = new HashMap<String, String>();
		Map<String, Map<String, String>> msgMap = new HashMap<String, Map<String, String>>();
		postMap.put("type", "getUser");
		postMap.put("userId", userId.toString());
		msgMap.put("msg", postMap);
		Gson gson = new Gson();
		String postData = gson.toJson(msgMap);
		return postData;
	}

	public static String remoteLoginAllowMembershipMessageBuilder(Long userId) throws IOException {
		Map<String, String> postMap = new HashMap<String, String>();
		Map<String, Map<String, String>> msgMap = new HashMap<String, Map<String, String>>();
		postMap.put("type", "allowMembership");
		postMap.put("userId", userId.toString());
		msgMap.put("msg", postMap);
		Gson gson = new Gson();
		String postData = gson.toJson(msgMap);
		return postData;
	}

	public static String remoteLoginRemoveIOSDeviceTokenMessageBuilder(String deviceToken, Long userId)
			throws IOException {
		Map<String, String> postMap = new HashMap<String, String>();
		Map<String, Map<String, String>> msgMap = new HashMap<String, Map<String, String>>();
		postMap.put("type", "removeIOSDeviceToken");
		postMap.put("userId", userId.toString());
		postMap.put("iOSDeviceToken", deviceToken);
		msgMap.put("msg", postMap);
		Gson gson = new Gson();
		String postData = gson.toJson(msgMap);
		return postData;
	}

	public static String remoteLoginGetGroupMessageBuilder(Long cityId, Long groupId) throws IOException {
		Map<String, String> postMap = new HashMap<String, String>();
		Map<String, Map<String, String>> msgMap = new HashMap<String, Map<String, String>>();
		postMap.put("type", "getGroup");
		postMap.put("cityId", cityId.toString());
		postMap.put("groupId", groupId.toString());
		msgMap.put("msg", postMap);
		Gson gson = new Gson();
		String postData = gson.toJson(msgMap);
		return postData;
	}

	public static String remoteLoginGetGroupMessageBuilder2(Long cityId, Long groupId) throws IOException {
		Map<String, String> postMap = new HashMap<String, String>();
		Map<String, Map<String, String>> msgMap = new HashMap<String, Map<String, String>>();
		postMap.put("type", "getGroup");
		postMap.put("cityId", cityId.toString());
		postMap.put("groupId", groupId.toString());
		msgMap.put("msg", postMap);
		Gson gson = new Gson();
		String postData = gson.toJson(msgMap);
		return postData;
	}

	public static String remoteLoginUpdateEmailToIdMapMessageBuilder(Long userId, String oldEmail, String newEmail)
			throws IOException {
		Map<String, String> postMap = new HashMap<String, String>();
		Map<String, Map<String, String>> msgMap = new HashMap<String, Map<String, String>>();
		postMap.put("type", "updateEmailToIdMap");
		postMap.put("userId", userId.toString());
		postMap.put("email", newEmail);
		postMap.put("oldEmail", oldEmail);
		msgMap.put("msg", postMap);
		Gson gson = new Gson();
		String postData = gson.toJson(msgMap);
		return postData;
	}

	public static String sendPost(String urlString, String postData) throws IOException {

		URL url = new URL(urlString);

		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setDoOutput(true);
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
		connection.setRequestProperty("Content-Length", String.valueOf(postData.length()));

		// Write data
		OutputStream os = connection.getOutputStream();
		os.write(postData.getBytes());

		// Read response
		StringBuilder responseSB = new StringBuilder();
		BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

		String line;
		while ((line = br.readLine()) != null)
			responseSB.append(line);

		// Close streams
		br.close();
		os.close();

		return responseSB.toString();
	}

	public static String sendGet(String urlString, HashMap<String, String> getData) throws IOException {

		String fullUrl = urlString + "?";
		for (Map.Entry<String, String> entry : getData.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			fullUrl = fullUrl + key + "=" + value + "&";
		}
		fullUrl = StringUtils.left(fullUrl, fullUrl.length() - 1);
		URL url = new URL(fullUrl);

		HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

		connection.setRequestMethod("GET");
		connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");

		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		return response.toString();
	}
	
	public static String sendSmsGet(String urlString, HashMap<String, String> getData) throws IOException {

		String fullUrl = urlString + "?";
		for (Map.Entry<String, String> entry : getData.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			fullUrl = fullUrl + key + "=" + value + "&";
		}
		fullUrl = StringUtils.left(fullUrl, fullUrl.length() - 1);
		URL url = new URL(fullUrl);

		HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

		connection.setRequestMethod("GET");
		
		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		return response.toString();
	}

}