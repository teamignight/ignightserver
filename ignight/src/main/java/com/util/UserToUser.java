package com.util;

public class UserToUser {
	public static String getUserToUserKey(long userOneID, long userTwoID) {
		if (userOneID == userTwoID)
			return null;
		if (userOneID < userTwoID) {
			return userOneID + "::" + userTwoID;
		} else
			return userTwoID + "::" + userOneID;
	}
}
