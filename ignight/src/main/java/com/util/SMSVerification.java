package com.util;

import java.io.IOException;
import java.util.HashMap;

import com.amazonaws.services.sns.AmazonSNSClient;
import com.nexmo.messaging.sdk.NexmoSmsClient;
import com.nexmo.messaging.sdk.SmsSubmissionResult;
import com.nexmo.messaging.sdk.messages.TextMessage;

public class SMSVerification {

	static final String NEXMO_API_KEY = IgnightProperties.getInstance().getNexmoKey();
	static final String NEXMO_API_SECRET = IgnightProperties.getInstance().getNexmoSecret();
	static final String NEXMO_URL_PREFIX = "https://rest.nexmo.com/sms/json";
	static final String NEXMO_FROM_ADDRESS = IgnightProperties.getInstance().getNexmoFromAddress();
	static HashMap<String, String> getData = new HashMap<String, String>(); 
	static NexmoSmsClient client;
	AmazonSNSClient snsClient;
	
	
	public SMSVerification() throws Exception {
		client = new NexmoSmsClient(NEXMO_API_KEY, NEXMO_API_SECRET);
	}
	
	
	public static String sendSMSViaNexmo(String to, String verificationCode) throws IOException{
		HashMap<String, String> getData = new HashMap<String, String>();
		getData.put("api_key", NEXMO_API_KEY);
		getData.put("api_secret", NEXMO_API_SECRET);
		getData.put("from", NEXMO_FROM_ADDRESS);
		getData.put("to", to);
		getData.put("text", "IgNight+Code:+" + verificationCode);
		return HTTPRequestSender.sendSmsGet(NEXMO_URL_PREFIX, getData);
	}
	

}
