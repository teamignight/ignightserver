package com.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.cloudfront.model.InvalidArgumentException;

public class IgnightProperties {
	protected Logger log = LoggerFactory.getLogger(IgnightProperties.class);
	private static final IgnightProperties INSTANCE = new IgnightProperties();

	public static IgnightProperties getInstance() {
		return INSTANCE;
	}

	private final String env;

	private final String mySqlHost;
	private final Long mySqlPort;
	private final String mySqlDbName;
	private final String mySqlUser;
	private final String mySqlPassword;
	private final Long mySqlMaxPoolSize;

	private final String mySqlHostArchive;
	private final Long mySqlPortArchive;
	private final String mySqlDbNameArchive;
	private final String mySqlUserArchive;
	private final String mySqlPasswordArchive;

	private final String redisHost;

	private final String localTmpDirectory;

	private final String sqsQueue;
	private final String s3AppBucket;
	private final String profilePictureDirectory;
	private final String venueBuzzPictureDirectory;
	private final String groupBuzzPictureDirectory;
	private final String googlePlacesPictureDirectory;
	
	private final String cloudFrontDistributionDomain;

	private final String iosKeystorePassword;
	private final String iosKeystoreFile;
	private final boolean iosProductionPushStatus;

	private final String androidApiKey;
	private final int androidRetryCount;

	private final String smtpUsername;
	private final String smtpPassword;
	private final String smtpHost;
	private final Integer smtpPort;

	private final Long waitForTrendingDataMillis;

	private final Long cityId;
	private final long noOfGroupBuzzToKeep;

	private final Properties prop;
	
	private final String nexmoKey;
	private final String nexmoSecret;
	private final String nexmoFromAddress;

	/*
	 * These values should not be final.
	 * TODO: We should probably move these into redis at some point.
	 */
	private int colorLimits;
	private double greenFloor;
	private double redCeiling;
	private Long barColorRefreshMillis;
	private double unweightedDnaRedCeiling;

	private IgnightProperties() {
		InputStream input = null;

		try {
			prop = new Properties();
			input = new FileInputStream("/properties/ignight.properties");
			prop.load(input);

			env = prop.containsKey("env") ? prop.getProperty("env").toLowerCase() : "dev";

			mySqlHost = prop.getProperty("mySqlHost");
			mySqlPort = Long.valueOf(prop.getProperty("mySqlPort"));
			mySqlDbName = prop.getProperty("mySqlDbName");
			mySqlUser = prop.getProperty("mySqlUser");
			mySqlPassword = prop.getProperty("mySqlPassword");
			mySqlMaxPoolSize = Long.valueOf(prop.getProperty("mySqlMaxPoolSize"));

			mySqlHostArchive = prop.getProperty("mySqlHostArchive");
			mySqlPortArchive = Long.valueOf(prop.getProperty("mySqlPortArchive"));
			mySqlDbNameArchive = prop.getProperty("mySqlDbNameArchive");
			mySqlUserArchive = prop.getProperty("mySqlUserArchive");
			mySqlPasswordArchive = prop.getProperty("mySqlPasswordArchive");

			redisHost = prop.getProperty("redisHost");
			sqsQueue = prop.getProperty("sqsQueue");

			localTmpDirectory = prop.getProperty("localTmpDirectory");

			s3AppBucket = prop.getProperty("s3AppBucket");
			venueBuzzPictureDirectory = prop.getProperty("venueBuzzPictureDirectory");
			profilePictureDirectory = prop.getProperty("profilePictureDirectory");
			groupBuzzPictureDirectory = prop.getProperty("groupBuzzPictureDirectory");
			googlePlacesPictureDirectory = prop.getProperty("googlePlacesPictureDirectory");
			cloudFrontDistributionDomain = prop.getProperty("cloudFrontDistributionDomain");

			iosKeystorePassword = prop.getProperty("iosKeystorePassword");
			iosKeystoreFile = prop.getProperty("iosKeystoreFile");
			iosProductionPushStatus = Boolean.valueOf(prop.getProperty("iosProductionPushStatus"));

			androidApiKey = prop.getProperty("androidApiKey");
			androidRetryCount = Integer.valueOf(prop.getProperty("androidRetryCount"));

			colorLimits = Integer.valueOf(prop.getProperty("colorLimits"));
			greenFloor = Double.valueOf(prop.getProperty("greenFloor"));
			redCeiling = Double.valueOf(prop.getProperty("redCeiling"));
			barColorRefreshMillis = Long.valueOf(prop.getProperty("barColorRefreshMillis"));
			unweightedDnaRedCeiling = Double.valueOf(prop.getProperty("unweightedDnaRedCeiling"));

			smtpUsername = prop.getProperty("smtpUsername");
			smtpPassword = prop.getProperty("smtpPassword");
			smtpHost = prop.getProperty("smtpHost");
			smtpPort = Integer.valueOf(prop.getProperty("smtpPort"));

			waitForTrendingDataMillis = Long.valueOf(prop.getProperty("waitForTrendingDataMillis"));

			cityId = Long.valueOf(prop.getProperty("cityId"));

			noOfGroupBuzzToKeep = getLongProperty("noOfGroupBuzzToKeep", 200);

			nexmoKey = prop.getProperty("nexmoKey");
			nexmoSecret = prop.getProperty("nexmoSecret");
			nexmoFromAddress = prop.getProperty("nexmoFromAddress");
			
		} catch (Exception e) {
			log.error("Failed to load properties file", e);
			throw new InvalidArgumentException(e.getMessage());
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					log.error("Failed to close properties file", e);
				}
			}
		}
	}

	private long getLongProperty(String key, long defaultValue) {
		if (prop.contains(key)) {
			return Long.valueOf(prop.getProperty(key));
		}
		return defaultValue;
	}

	public long getnNoOfGroupBuzzToKeep() {
		return noOfGroupBuzzToKeep;
	}

	public double getUnweightedDnaRedCeiling() {
		return unweightedDnaRedCeiling;
	}

	public String getEnv() {
		return env;
	}

	public String getIosKeystorePassword() {
		return iosKeystorePassword;
	}

	public String getIosKeystoreFile() {
		return iosKeystoreFile;
	}

	public boolean getIosProductionPushStatus() {
		return iosProductionPushStatus;
	}

	public String getAndroidApiKey() {
		return androidApiKey;
	}

	public int getAndroidRetryCount() {
		return androidRetryCount;
	}

	public String getMySqlHost() {
		return mySqlHost;
	}

	public Long getMySqlPort() {
		return mySqlPort;
	}

	public String getMySqlDbName() {
		return mySqlDbName;
	}

	public String getMySqlUser() {
		return mySqlUser;
	}

	public String getMySqlPassword() {
		return mySqlPassword;
	}

	public Long getMySqlMaxPoolSize() {
		return mySqlMaxPoolSize;
	}

	public String getMySqlHostArchive() {
		return mySqlHostArchive;
	}

	public Long getMySqlPortArchive() {
		return mySqlPortArchive;
	}

	public String getMySqlDbNameArchive() {
		return mySqlDbNameArchive;
	}

	public String getMySqlUserArchive() {
		return mySqlUserArchive;
	}

	public String getMySqlPasswordArchive() {
		return mySqlPasswordArchive;
	}

	public String getRedisHost() {
		return redisHost;
	}

	public String getSqsQueue() {
		return sqsQueue;
	}

	public String getProfilePictureDirectory() {
		return profilePictureDirectory;
	}

	public String getVenueBuzzPictureDirectory() {
		return venueBuzzPictureDirectory;
	}

	public String getGroupBuzzPictureDirectory() {
		return groupBuzzPictureDirectory;
	}
	
	public String getGooglePlacesPictureDirectory() {
		return googlePlacesPictureDirectory;
	}

	public String getCloudFrontDistributionDomain() {
		return cloudFrontDistributionDomain;
	}

	public String getLocalTmpDirectory() {
		return localTmpDirectory;
	}

	public int getColorLimits() {
		return colorLimits;
	}

	public double getGreenFloor() {
		return greenFloor;
	}

	public double getRedCeiling() {
		return redCeiling;
	}

	public String getSmtpUsername() {
		return smtpUsername;
	}

	public String getSmtpPassword() {
		return smtpPassword;
	}

	public String getSmtpHost() {
		return smtpHost;
	}

	public Integer getSmtpPort() {
		return smtpPort;
	}

	public Long getWaitForTrendingDataMillis() {
		return waitForTrendingDataMillis;
	}

	public Long getBarColorRefreshMillis() {
		return barColorRefreshMillis;
	}

	public Long getCityId() {
		return cityId;
	}
	
	public String getNexmoKey(){
		return nexmoKey;
	}
	
	public String getNexmoSecret(){
		return nexmoSecret;
	}
	
	public String getNexmoFromAddress(){
		return nexmoFromAddress;
	}

	/*
	 * Setters
	 */
	public void setColorLimits(int colorLimits) {
		this.colorLimits = colorLimits;
	}

	public void setGreenFloor(double greenFloor) {
		this.greenFloor = greenFloor;
	}

	public void setRedCeiling(double redCeiling) {
		this.redCeiling = redCeiling;
	}

	public void setBarColorRefreshMillis(Long barColorRefreshMillis) {
		this.barColorRefreshMillis = barColorRefreshMillis;
	}

	public void setUnweightedDnaRedCeiling(double unweightedDnaRedCeiling) {
		this.unweightedDnaRedCeiling = unweightedDnaRedCeiling;
	}

	public String toString() {
		return "IgnightProperties [mySqlHost=" + mySqlHost + ", mySqlPort=" + mySqlPort + ", mySqlDbName="
				+ mySqlDbName + ", mySqlUser=" + mySqlUser + ", mySqlPassword=" + mySqlPassword + ", redisHost="
				+ redisHost + ", s3AppBucket=" + s3AppBucket + "]";
	}

	public String getS3AppBucket() {
		return s3AppBucket;
	}
}
