package com.util;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ignight.dna.CalcVenueDna;
import com.ignight.dna.DnaElementCount;
import com.ignight.dna.VenueDna;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.user.UserDna;

public class UserToVenueDna {
	private static final Logger logger = LoggerFactory.getLogger(UserToVenueDna.class);
	private static final Integer MULTIPLIER = 1000;
	private static final long musicWeight = 500;
	private static final long atmosphereWeight = 200;
	private static final long ageWeight = 150;
	private static final long spendingWeight = 150;

	private static final long[] defaultWeights = new long[] { 500, 350, 150 };

	public static Double getUserDnaValue(Long userId, CalcVenueDna venueDna) {
		if (userId == null) {
			logger.error("UserId is null");
			return null;
		}
		if (venueDna == null) {
			logger.error("VenueDna is null");
			return null;
		}
		UserDna userDna = UserDataSource.getCachedUserDna(userId);
		if (userDna == null) {
			logger.error("UserDna is null for userId: {}", userId);
			return null;
		}

		if (venueDna.upvotedDna.counter.equals(0) && venueDna.downvotedDna.counter.equals(0)) {
			return null;
		}

		return getUserDnaValue(userDna, venueDna);
	}

	public static double getUserDnaValue(UserDna userDna, CalcVenueDna venueDna) {
		double upvoteUserVenueDnaValue = getUserDnaValue(userDna, venueDna.upvotedDna);
		double downvoteUserVenueDnaValue = getUserDnaValue(userDna, venueDna.downvotedDna);
		venueDna.setBaseUserVenueScore(upvoteUserVenueDnaValue / 1000.0);
		return ((upvoteUserVenueDnaValue * venueDna.upvotedDna.counter) - (downvoteUserVenueDnaValue * venueDna.downvotedDna.counter)) / 1000000.0;
	}

	public static double getUserDnaValue(UserDna userDna, VenueDna venueDna) {
		if (venueDna.counter > 0) {
			long userValue = 0;
			userValue += spendingWeight * calculateCompatibility(userDna.spendingLimit, venueDna.spendingLimit);
			userValue += ageWeight * calculateCompatibility(userDna.ageId, venueDna.ageId);
			userValue += musicWeight * calculatePreferencesCompatibility(userDna.musicIds, venueDna.musicPreferences);
			userValue += atmosphereWeight * calculatePreferencesCompatibility(userDna.atmosphereIds, venueDna.atmospherePreferences);
			return userValue / 1000.0;
		}
		return 0;
	}

	public static long calculateCompatibility(long elementOne, long elementTwo) {
		long diff = Math.abs(elementOne - elementTwo);
		long spendingCompatibility = 0;
		if (diff == 1) {
			spendingCompatibility = 500;
		} else if (diff == 0) {
			spendingCompatibility = 1000;
		}
		return spendingCompatibility;
	}

	public static long calculatePercentageAtPos(List<DnaElementCount> preferences, long total, int pos) {
		if (preferences.size() < pos || total == 0) {
			return 0;
		}
		return (1000 * preferences.get(pos).count) / total;
	}

	private static long getTotalCount(List<DnaElementCount> elems) {
		long count = 0;
		for (DnaElementCount elem : elems) {
			count += elem.count;
		}
		return count;
	}

	public static long calculatePreferencesCompatibility(List<Long> one, List<DnaElementCount> two) {
		long value = 0;

		long prefWeights[] = fillOutArray(one);

		long total = getTotalCount(two);
		for (int i = 0; i < one.size(); i++) {
			Long currentPreference = one.get(i);
			for (int j = 0; j < two.size(); j++) {
				if (currentPreference.equals(two.get(j).dnaId)) {
					long userPercentage = prefWeights[i];
					long venuePercentage = calculatePercentageAtPos(two, total, j);
					value += Math.min(userPercentage, venuePercentage);
				}
			}
		}
		return value;
	}

	private static long[] fillOutArray(List<Long> preferences) {
		Iterator<Long> iter = preferences.iterator();
		while (iter.hasNext()) {
			Long val = iter.next();
			if (val < 0) {
				iter.remove();
			}
		}

		int size = preferences.size();
		long prefWeights[] = new long[size];

		long totalWeight = 1000;
		for (int i = 0; i < size; i++) {
			prefWeights[i] = defaultWeights[i];
			totalWeight -= defaultWeights[i];
		}
		prefWeights[0] += totalWeight;

		return prefWeights;
	}
}
