package com.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.factual.driver.Circle;
import com.factual.driver.Factual;
import com.factual.driver.Query;
import com.factual.driver.ReadResponse;
import com.ignight.group.GroupDataSource;
import com.ignight.imagehandling.ImageHandler;
import com.ignight.venue.Venue;
import com.ignight.venue.VenueDataSource;

import databases.Databases;
import databases.Resources;
import databases.Transactable;

public class GooglePlaceImagesUtils {

	private final static String MY_KEY = "lIwVn3JhNSQOTkqcPveaD0QQ1KEiRogUuis5wFh9";
	private final static String MY_SECRET = "uVX1Uw3GtmZar5tyVdNhYuR00YghQCJFThF0TVoU";
	private static Factual factual = new Factual(MY_KEY, MY_SECRET);
	private static Query q = new Query();


	private final static String GOOGLE_API_KEY = "AIzaSyDC6TzuXUYUrip99ewdnuNHTedDSK0Ob6c";
	private final static String GOOGLE_TYPES = "bar|cafe|food|night_club|restaurant|food|establishment";
	private final static String GOOGLE_SEARCH_RADIUS = "5000";
	private final static String GOOGLE_SEARCH_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json";
	private final static String GOOGLE_PLACE_DETAIL_URL = "https://maps.googleapis.com/maps/api/place/details/json";
	private final static Integer GOOGLE_PLACE_PHOTO_MAX_WIDTH = 600;
	private final static String GOOGLE_PLACE_PHOTOL_URL = "https://maps.googleapis.com/maps/api/place/photo";





	public GooglePlaceImagesUtils() {
		// TODO Auto-generated constructor stub
	}


	public static HashMap<String, String> createGooglePlaceSearchGetHashMap(String name, Double latitude, Double longitude) {
		HashMap<String, String> output = new HashMap<String, String>();
		String formattedName = name.trim();
		formattedName = formattedName.replace(" ", "+");
		formattedName = formattedName.replace("&", "%26");
		formattedName = formattedName.replace("'", "%27");
		formattedName = formattedName.replace(",", "%2C");
		output.put("name", formattedName);
		String formattedLocation = "" + latitude + "," + longitude;
		output.put("location", formattedLocation);
		output.put("radius", GOOGLE_SEARCH_RADIUS);
		output.put("types", GOOGLE_TYPES);
		output.put("key", GOOGLE_API_KEY);
		return output;
	}

	public static HashMap<String, String> createGoogleDetailedPlaceInfoGetHashMap(String placeId) {
		HashMap<String, String> output = new HashMap<String, String>();
		output.put("placeid", placeId);
		output.put("key", GOOGLE_API_KEY);
		return output;
	}

	public static String fetchDetailedPlaceInfoForVenue(String placeId) throws IOException{
		String resp = null;
		HashMap<String, String> getData = createGoogleDetailedPlaceInfoGetHashMap(placeId);
		resp = HTTPRequestSender.sendGet(GOOGLE_PLACE_DETAIL_URL, getData);
		return resp;
	}

	public static String fetchGooglePlacesData() throws IOException, SQLException, JSONException{

		List<Venue> venues = VenueDataSource.loadVenueList();
		for (Venue v : venues) {
			Long id = v.id;
			String name = v.name;
			Double lat = v.latitude;
			Double lon = v.longitude;	
			
			String gPlacesId = v.googlePlacesId;
			
			if(gPlacesId == "--")
				continue;

			HashMap<String, String> getData = createGooglePlaceSearchGetHashMap(name, lat, lon);
			String resp = HTTPRequestSender.sendGet(GOOGLE_SEARCH_URL, getData);

			JSONArray resultsArray = (new JSONObject(resp.toString())).getJSONArray("results");

			//for(int i = 0; i < resultsArray.length(); i++){
				String googlePlacesId = null;
				String googlePlacesName = null;
				
				//JSONObject currentResult = resultsArray.getJSONObject(i);
				if(resultsArray.length() > 0){
				JSONObject currentResult = resultsArray.getJSONObject(0);
				if(!currentResult.isNull("place_id"))
					googlePlacesId = currentResult.getString("place_id");
				if(!currentResult.isNull("name"))
					googlePlacesName = currentResult.getString("name");

				Databases.update( 
						"UPDATE venues set google_places_id = ?, google_places_name = ? where id = ?", 
						googlePlacesId, googlePlacesName, id);
				}
			//}


		}
		return "Done";
	}

	public static String fetchDetailedGooglePlacesData() throws IOException, SQLException, JSONException{
		List<List<Object>> list = VenueDataSource.loadGooglePlacesVenueDataResultsSet();

		for(int i = 0; i < list.size(); i++){

			List<Object> currentVenue = list.get(i);
			Long id = (Long) currentVenue.get(0);
			String googlePlacesId = (String) currentVenue.get(1);
			
			String placeInfo = fetchDetailedPlaceInfoForVenue(googlePlacesId);
			if(placeInfo == null)
				continue;

			if((new JSONObject(placeInfo.toString())).isNull("result"))
				continue;

			JSONObject result = (new JSONObject(placeInfo.toString())).getJSONObject("result");


			if(result != null){

				Integer googlePlacesPriceLevel = null;
				Double googlePlacesRating = null;
				String hours = "";

				if(!result.isNull("price_level"))
					googlePlacesPriceLevel = result.getInt("price_level");
				if(!result.isNull("rating"))
					googlePlacesRating = result.getDouble("rating");

				if(!result.isNull("opening_hours")){
					JSONArray hoursArray = result.getJSONObject("opening_hours").getJSONArray("weekday_text");

					for(int j = 0; j < hoursArray.length(); j++){
						hours = hours + "|" + hoursArray.getString(j).replace("–", "-");
					}
				}
				
				Databases.update(
						"UPDATE venues set google_places_id = ?, price_level = ?, rating = ?, hours = ? WHERE id = ?",
						googlePlacesId, googlePlacesPriceLevel, googlePlacesRating, hours, id);
				
				if(!result.isNull("photos")){
					JSONArray photosArray = result.getJSONArray("photos");
					
					Databases.update("UPDATE venues SET no_of_pictures = ? WHERE id = ?", photosArray.length(), id);
					
					for(int k = 0; k < photosArray.length(); k++){
						String currentPhotoReference = photosArray.getJSONObject(k).getString("photo_reference");
						
						String currentPlacePictureUrl = getPlacePictureUrl(currentPhotoReference);
						String currentPlacePictureCachedUrl = uploadGooglePlaceImage(id, k, currentPlacePictureUrl);
						
						Databases.insert( 
								"INSERT INTO google_places_pictures (id, google_places_id, photo_reference, photo_number, url) VALUES (?, ?, ?, ?, ?)"
								+ "ON DUPLICATE KEY UPDATE id = ?, photo_number = ?, url = ?", 
								id, googlePlacesId, currentPhotoReference, k, currentPlacePictureCachedUrl, id, k, currentPlacePictureCachedUrl);
					}
				}
			}
		}

		return "Done";
	}

	public static String getPlacePictureUrl(String photoReference){
		String url = GOOGLE_PLACE_PHOTOL_URL + "?maxwidth="+ GOOGLE_PLACE_PHOTO_MAX_WIDTH + "&photoreference=" + photoReference + "&key=" + GOOGLE_API_KEY;
		return url;
	}
	
	public static String uploadGooglePlaceImage(Long venueId, Integer pictureNumber, String urlString){
		String output = null;   
		BufferedImage image = null;
		try {
		    URL url = new URL(urlString);
		    image = ImageIO.read(url);
		    ByteArrayOutputStream baos = new ByteArrayOutputStream();
		    ImageIO.write(image, "jpg", baos);
		    
		    InputStream uploadStream = new ByteArrayInputStream(baos.toByteArray());
		    output = ImageHandler.uploadGooglePlaceImage(venueId, pictureNumber, uploadStream);
		} catch (Exception e) {
			
		}
		return output;
	}
	
	public static String updateNumberOfImages() throws IOException, SQLException{

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ResultSet rs2 = null;

		try {
			conn = Databases.dataSource.getConnection();
			ps = conn.prepareStatement("SELECT DISTINCT(id) FROM google_places_pictures");
			rs = ps.executeQuery();
			
			while(rs.next()){
				PreparedStatement ps2 = null;
				Long id = rs.getLong(1);
				ps2 = conn.prepareStatement("SELECT count(id) FROM google_places_pictures where id=" + id);
				rs2 = ps2.executeQuery();
				rs2.next();
				Integer noOfPics = rs2.getInt(1);
				Databases.update("Update google_places_detailed_venue_data set no_Of_Pictures = ? where id = ?", noOfPics, id);
			}
			
		} finally {
			Resources.close(conn, ps, rs);
		}
		
		return "Done";
	}

	/*
	public static void main(String[] args) {

		BufferedImage image = null;
		try {
		    URL url = new URL("https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=CnRnAAAADpUPwDuokAqIUVBmOuuq2TsOLggP3QGM5mTxha7ZBsDgvIaxBoMki2ZYvuf2RhRJaieLkdKOg0G1r914eygip9kpRisTBDEKeLg8Gyz3_Z7RZDdA8TqSEU737f4lj1f1TP2jHNue19HpARUSYTi0jxIQfOMZwSM4ZMJstgE9yAk3zBoUJRtUdFA4nR9r0OVz_aor1vnAlVE&key=AIzaSyDC6TzuXUYUrip99ewdnuNHTedDSK0Ob6c");
		    image = ImageIO.read(url);
		    File outputfile = new File("test.jpg");
		    ImageIO.write(image, "jpg", outputfile);
		    
		    ImageHandler.uploadSingleFileToS3("/googleImages/test.jpg", outputfile);
		    
		} catch (Exception e) {
			
		}

	}
	
	*/

}
