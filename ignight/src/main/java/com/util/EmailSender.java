package com.util;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EmailSender implements Runnable {
	private static final Logger log = LoggerFactory.getLogger(EmailSender.class);

	static final String FROM = "forgotpassword@ignight.com";
	static final String SMTP_USERNAME = IgnightProperties.getInstance().getSmtpUsername();
	static final String SMTP_PASSWORD = IgnightProperties.getInstance().getSmtpPassword();
	static final String HOST = IgnightProperties.getInstance().getSmtpHost();
	static final int PORT = IgnightProperties.getInstance().getSmtpPort();

	public String from;
	public String emailTo;
	public String subject;
	public String body;

	public EmailSender(String to, String subject, String body) {
		this.from = FROM;
		this.emailTo = to;
		this.subject = subject;
		this.body = body;
	}

	public EmailSender(String from, String to, String subject, String body) {
		this.from = from;
		this.emailTo = to;
		this.subject = subject;
		this.body = body;
	}

	private void sendEmail(String from, String to, String subject, String body) throws AddressException, MessagingException {
		// Create a Properties object to contain connection configuration
		// information.
		Properties props = System.getProperties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.port", PORT);

		// Set properties indicating that we want to use STARTTLS to encrypt the
		// connection.
		// The SMTP session will begin on an unencrypted connection, and then
		// the client
		// will issue a STARTTLS command to upgrade to an encrypted connection.
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.starttls.required", "true");

		// Create a Session object to represent a mail session with the
		// specified properties.
		Session session = Session.getDefaultInstance(props);

		// Create a message with the specified information.
		MimeMessage msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(from));
		msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
		msg.setSubject(subject);
		msg.setContent(body, "text/plain");

		// Create a transport.
		Transport transport = session.getTransport();

		// Send the message.
		try {
			log.info("Attempting to send an email through the Amazon SES SMTP interface...");

			// Connect to Amazon SES using the SMTP username and password you
			// specified above.
			transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);

			// Send the email.
			transport.sendMessage(msg, msg.getAllRecipients());
		} finally {
			// Close and terminate the connection.
			transport.close();
		}
	}

	@Override
	public void run() {
		try {
			sendEmail(from, emailTo, subject, body);
		} catch (Exception e) {
			log.error("Failed to send email to : {}", emailTo, e);
		}
	}
}