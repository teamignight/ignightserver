package com.stats;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.primitives.Longs;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Stats {
	private final Set<Long> newUsers = new HashSet<Long>();
	private final Set<Long> newGroups = new HashSet<Long>();

	private final Set<Long> activeUsers = new HashSet<Long>();
	private final Set<Long> activeVenues = new HashSet<Long>();
	private final Set<Long> activeGroups = new HashSet<Long>();

	private final Map<Long, Set<Long>> activeUsersByHour = new HashMap<Long, Set<Long>>();
	private final Map<Long, Set<Long>> activeVenuesByHour = new HashMap<Long, Set<Long>>();
	private final Map<Long, Set<Long>> activeGroupsByHour = new HashMap<Long, Set<Long>>();

	private ConcurrentHashMap<String, AtomicLong> uriCounts = new ConcurrentHashMap<String, AtomicLong>();

	private static Gson gson = new GsonBuilder().disableHtmlEscaping().create();
	private static final Logger logger = LoggerFactory.getLogger(Stats.class);

	private static Stats INSTANCE;
	private Long beginTime;
	private long milliSecondsInHour = 1000 * 60 * 60;

	private Stats() {
		this.beginTime = System.currentTimeMillis();
	}

	public synchronized static Stats getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Stats();
		}
		return INSTANCE;
	}

	public String getStats() {
		Map<String, Object> stats = new HashMap<String, Object>();
		stats.put("newUsers", newUsers.size());
		stats.put("newGroups", newGroups.size());
		stats.put("activeUsers", activeUsers.size());
		stats.put("activeVenues", activeVenues.size());
		stats.put("activeGroups", activeGroups.size());

		Map<Object, Object> activeUsers = new HashMap<Object, Object>();
		for (Map.Entry<Long, Set<Long>> entry : activeUsersByHour.entrySet()) {
			activeUsers.put(entry.getKey(), entry.getValue().size());
		}
		stats.put("activeUsersByHours", activeUsers);

		Map<Object, Object> activeVenues = new HashMap<Object, Object>();
		for (Map.Entry<Long, Set<Long>> entry : activeVenuesByHour.entrySet()) {
			activeVenues.put(entry.getKey(), entry.getValue().size());
		}
		stats.put("activeVenuesByHour", activeVenues);

		Map<Object, Object> activeGroups = new HashMap<Object, Object>();
		for (Map.Entry<Long, Set<Long>> entry : activeGroupsByHour.entrySet()) {
			activeGroups.put(entry.getKey(), entry.getValue().size());
		}
		stats.put("activeGroupsByHour", activeGroups);

		Map<String, Long> uriCounter = new HashMap<String, Long>();
		for (Map.Entry<String, AtomicLong> entry : uriCounts.entrySet()) {
			uriCounter.put(entry.getKey(), entry.getValue().get());
		}
		stats.put("uriCounter", uriCounter);

		return gson.toJson(stats);
	}

	public void clear() {
		newUsers.clear();
		newGroups.clear();

		activeUsers.clear();
		activeVenues.clear();
		activeGroups.clear();

		activeUsersByHour.clear();
		activeVenuesByHour.clear();
		activeGroupsByHour.clear();

		uriCounts.clear();

		beginTime = System.currentTimeMillis();
	}

	public void addNewUser(long userId) {
		newUsers.add(userId);
	}

	public void addNewGroup(long groupId) {
		newGroups.add(groupId);
	}

	public void updateActiveUser(long userId) {
		activeUsers.add(userId);
		updateActiveByHours(userId, activeUsersByHour);
	}

	public void updateActiveVenue(long userId, long venueId) {
		updateActiveUser(userId);
		updateActiveVenue(venueId);
	}

	public void updateActiveGroup(long userId, long groupId) {
		updateActiveUser(userId);
		updateActiveGroup(groupId);
	}

	private void updateActiveVenue(long venueId) {
		activeVenues.add(venueId);
		updateActiveByHours(venueId, activeVenuesByHour);
	}

	private void updateActiveGroup(long groupId) {
		activeGroups.add(groupId);
		updateActiveByHours(groupId, activeGroupsByHour);
	}

	public void updateUri(final String uri) {
		try {
			if (uri == null) {
				return;
			}

			String finalUrl = stripUrl(uri);
			AtomicLong counter = uriCounts.get(finalUrl);
			if (counter == null) {
				counter = new AtomicLong(0);
				AtomicLong ret = uriCounts.putIfAbsent(finalUrl, counter);
				if (ret != null) {
					counter = ret;
				}
			}
			counter.incrementAndGet();
		} catch (Exception e) {
			logger.error("Failed to update count for uri={}", uri, e);
		}
	}

	private String stripUrl(String url) {
		if (url == null) {
			return null;
		}

		String[] urlSplit = url.split("//");
		int count = urlSplit.length;

		String last = urlSplit[count - 1];
		Long val = Longs.tryParse(last);
		if (val == null) {
			return url;
		}

		return url.substring(0, url.length() - last.length() - 1);
	}

	private void updateActiveByHours(long value, Map<Long, Set<Long>> activeMap) {
		long key = getCurrentHour();
		Set<Long> actives = activeMap.get(key);
		if (actives == null) {
			actives = new HashSet<Long>();
			activeMap.put(key, actives);
		}
		actives.add(value);
	}

	private long getCurrentHour() {
		long now = System.currentTimeMillis();
		long diff = now - beginTime;
		long hour = diff / milliSecondsInHour; //TODO: Adjust this based on beginTime hour
		return hour;
	}
}
