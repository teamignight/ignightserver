package com.admin;

import net.karneim.pojobuilder.GeneratePojoBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.repackaged.com.google.common.base.Preconditions;
import com.google.gson.JsonObject;
import com.ignight.servlets.v2.RequestUtil;
import com.util.IgnightProperties;

public class AdminHandler {
	private final static Logger logger = LoggerFactory.getLogger(AdminHandler.class);
	private final static String USERNAME = "IGNIGHT_ADMIN_USER";
	private final static String PASSWORD = "IMGOINGTOCREATEAREALLYLONGPASSWORDFORSECURITYREASONS";

	@GeneratePojoBuilder
	public static class TrendingConfig {
		Integer colorLimits;
		Double greenFloor;
		Double redCeiling;
		Long barColorRefreshMillis;
		Double unweightedDnaRedCeiling;
	}

	public Object getTrendingCalcConfig() {
		IgnightProperties properties = IgnightProperties.getInstance();
		return new TrendingConfigBuilder()
				.withColorLimits(properties.getColorLimits())
				.withGreenFloor(properties.getGreenFloor())
				.withRedCeiling(properties.getRedCeiling())
				.withBarColorRefreshMillis(properties.getBarColorRefreshMillis())
				.withUnweightedDnaRedCeiling(properties.getUnweightedDnaRedCeiling()).build();
	}

	public Object updateTrendingCalcConfig(JsonObject body) {
		String username = RequestUtil.getString(body, "username");
		Preconditions.checkNotNull(username, "Username is null");
		String password = RequestUtil.getString(body, "password");
		Preconditions.checkNotNull(password, "Password is null");

		IgnightProperties properties = IgnightProperties.getInstance();

		Integer colorLimits = RequestUtil.getInteger(body, "colorLimits");
		if (colorLimits != null) {
			//Log as error to get alert email
			logger.error("Updateing Trending Calc Config Property : {}, before={}, after={}", "colorLimits", properties.getColorLimits(), colorLimits);
			properties.setColorLimits(colorLimits);
		}
		Double greenFloor = RequestUtil.getDouble(body, "greenFloor");
		if (greenFloor != null) {
			logger.error("Updateing Trending Calc Config Property : {}, before={}, after={}", "greenFloor", properties.getGreenFloor(), greenFloor);
			properties.setGreenFloor(greenFloor);
		}
		Double redCeiling = RequestUtil.getDouble(body, "redCeiling");
		if (redCeiling != null) {
			logger.error("Updateing Trending Calc Config Property : {}, before={}, after={}", "redCeiling", properties.getRedCeiling(), redCeiling);
			properties.setRedCeiling(redCeiling);
		}
		Long barColorRefreshMillis = RequestUtil.getLong(body, "barColorRefreshMillis");
		if (barColorRefreshMillis != null) {
			logger.error("Updateing Trending Calc Config Property : {}, before={}, after={}", "barColorRefreshMillis", properties.getBarColorRefreshMillis(), barColorRefreshMillis);
			properties.setBarColorRefreshMillis(barColorRefreshMillis);
		}
		Double unweightedDnaRedCeiling = RequestUtil.getDouble(body, "unweightedDnaRedCeiling");
		if (unweightedDnaRedCeiling != null) {
			logger.error("Updateing Trending Calc Config Property : {}, before={}, after={}", "unweightedDnaRedCeiling", properties.getUnweightedDnaRedCeiling(), unweightedDnaRedCeiling);
			properties.setUnweightedDnaRedCeiling(unweightedDnaRedCeiling);
		}

		return getTrendingCalcConfig();
	}
}
