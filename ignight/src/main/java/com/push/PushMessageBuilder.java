package com.push;

import javapns.notification.PushNotificationPayload;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.android.gcm.server.Message;
import com.ignight.group.Group;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.user.User;
import com.ignight.user.UserImages;

public class PushMessageBuilder {
	private static final Logger logger = LoggerFactory.getLogger(PushMessageBuilder.class);

	public static final String INVITE_USER_TO_GROUP_REQUEST = "INVITE_USER_TO_GROUP_REQUEST";
	public static final String NEW_VENUE_BUZZ_AVAILABLE_NOTIFICATION = "NEW_VENUE_BUZZ_AVAILABLE_NOTIFICATION";
	public static final String NEW_GROUP_BUZZ_AVAILABLE_NOTIFICATION = "NEW_GROUP_BUZZ_AVAILABLE_NOTIFICATION";

	/*
	 * New Venue Buzz
	 */

	public static PushNotificationPayload newVenueBuzz(Long venueId) {
		PushNotificationPayload payload = new PushNotificationPayload();
		try {
			payload.addCustomDictionary("notificationType", NEW_VENUE_BUZZ_AVAILABLE_NOTIFICATION);
			payload.addCustomDictionary("venueId", venueId.toString());
		} catch (JSONException e) {
			logger.error("Failed to serialize push message for venue: ", venueId, e);
		}
		return payload;
	}

	public static Message newVenueBuzz(Long venueId, Long latestBuzzId) {
		Message message = new Message.Builder().collapseKey("VenueID=" + venueId + "_Buzz")
				.addData("notificationType", NEW_VENUE_BUZZ_AVAILABLE_NOTIFICATION)
				.addData("venueId", venueId.toString())
				.addData("latestBuzzId", latestBuzzId.toString()).build();
		return message;
	}

	/*
	 * New Group Buzz
	 */

	public static PushNotificationPayload newGroupBuzzAlert(Group group, User user, Long latestBuzzId) {
		PushNotificationPayload payload = newGroupBuzz(group, latestBuzzId);
		try {
			payload.addCustomDictionary("groupId", group.id.intValue());
			payload.addAlert(user.username + " sent a message in " + group.name);
			payload.addBadge(1);
		} catch (JSONException e) {
			logger.error("Failed to serialize push message to group: {}, latestBuzzId: {}", group, latestBuzzId, e);
		}
		return payload;
	}

	public static PushNotificationPayload newGroupBuzz(Group group, Long latestBuzzId) {
		PushNotificationPayload payload = new PushNotificationPayload();
		try {
			payload.addCustomDictionary("notificationType", NEW_GROUP_BUZZ_AVAILABLE_NOTIFICATION);
			payload.addCustomDictionary("groupId", group.id.toString());
			payload.addCustomDictionary("groupName", group.name);
			payload.addCustomDictionary("privateGroup", group.isPrivate ? "1" : "0");
			payload.addCustomDictionary("latestBuzzId", latestBuzzId.toString());
		} catch (JSONException e) {
			logger.error("Failed to serialize push message to group: {}, latestBuzzId: {}", group, latestBuzzId, e);
		}
		return payload;
	}

	public static Message androidBuildNewGroupBuzzAvailableMessage(Group group, Long latestBuzzId) {
		Message message = new Message.Builder().collapseKey("GroupID=" + group.id + "_Buzz")
				.addData("notificationType", NEW_GROUP_BUZZ_AVAILABLE_NOTIFICATION)
				.addData("groupId", group.id.toString()).addData("groupName", group.name)
				.addData("privateGroup", group.isPrivate ? "1" : "0")
				.addData("latestBuzzId", latestBuzzId.toString()).build();
		return message;
	}

	/*
	 * Group Invite
	 */

	public static PushNotificationPayload inviteUserToGroup(String username, String groupName) {
		PushNotificationPayload payload = new PushNotificationPayload();
		try {
			payload.addCustomDictionary("notificationType", INVITE_USER_TO_GROUP_REQUEST);
			payload.addAlert(username + " has invited you to join the group: " + groupName + ".");
			payload.addBadge(1);
		} catch (JSONException e) {
			logger.error("Failed to serialize push message to user: {}, for Group: {}", username, groupName, e);
		}
		return payload;
	}

	public static Message inviteUserToGroup(User user, Group group) {
		UserImages userImages = UserDataSource.getCachedUserImages(user.id);
		Message message = new Message.Builder()
				.addData("notificationType", INVITE_USER_TO_GROUP_REQUEST)
				.addData("userId", user.id.toString())
				.addData("userName", user.username)
				.addData("userPicture", (userImages != null) ? userImages.chatPictureUrl : null)
				.addData("groupId", group.id.toString())
				.addData("groupName", group.name).build();
		return message;
	}

	/*
	 * IOS PUSH NOTIFICATIONS
	 */

	public static PushNotificationPayload iosBuildTestMessage() {
		PushNotificationPayload payload = new PushNotificationPayload();
		try {
			payload.addCustomDictionary("notificationType", NEW_GROUP_BUZZ_AVAILABLE_NOTIFICATION);
			payload.addCustomDictionary("notificationName", "Test");
		} catch (JSONException e) {
			logger.error("Failed to serialize test push message", e);
		}
		return payload;
	}

	/*
	 * ANDROID PUSH NOTIFICATIONS
	 */

	public static Message androidBuildTestMessage() {
		Message message = new Message.Builder().addData("notificationType", NEW_GROUP_BUZZ_AVAILABLE_NOTIFICATION)
				.addData("notificationMessage", "Test").build();
		return message;
	}
}
