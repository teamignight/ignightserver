package com.push;

import java.util.HashSet;
import java.util.Set;

import javapns.notification.PushNotificationPayload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.android.gcm.server.Message;
import com.ignight.servlets.user.UserDataSource;

public class SendPushNotifications implements Runnable {
	private static final Logger log = LoggerFactory.getLogger(SendPushNotifications.class);
	private Set<Long> users;

	private Message message = null;
	private PushNotificationPayload payload = null;

	public SendPushNotifications(Set<Long> users) {
		this.users = users;
	}

	public SendPushNotifications(Long user) {
		this.users = new HashSet<Long>();
		this.users.add(user);
	}

	public void addMessage(Message message) {
		this.message = message;
	}

	public void addPayload(PushNotificationPayload payload) {
		this.payload = payload;
	}

	@Override
	public void run() {
		try {
			if (users == null || users.isEmpty()) {
				log.error("Trying to send to empty users");
				return;
			}

			log.info("Sending Push Notification to : {}", users);
			for (Long userId : users) {
				String token = UserDataSource.getIosTokenForUser(userId);
				if (token != null) {
					log.info("Sending Push Notification To User: {} with Token: {} with Payload: {}", userId, token, payload);
					PushNotificationSender.sendIOSPushNotification(token, payload);
					continue;
				}

				token = UserDataSource.getAndroidRegistrationForUser(userId);
				if (token != null) {
					log.info("Sending Android Notification To User: {} with Token: {} with Message: {}", userId, token, message);
					PushNotificationSender.sendAndroidPushNotification(token, message);
					continue;
				}
			}
		} catch (Exception e) {
			log.error("Failed to send push notifications to : {}", users, e);
		}
	}
}
