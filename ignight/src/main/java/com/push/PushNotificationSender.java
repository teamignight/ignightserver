package com.push;

import java.io.File;

import javapns.Push;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.notification.PushNotificationPayload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Sender;
import com.util.IgnightProperties;

public class PushNotificationSender {
	private static final Logger log = LoggerFactory.getLogger(PushNotificationSender.class);

	//TODO: Move these to properties file
	private static String iOSKeyStorePassword = IgnightProperties.getInstance().getIosKeystorePassword();
	private static File iOSKeyStore = new File(IgnightProperties.getInstance().getIosKeystoreFile());

	private static String androidAPIKey = IgnightProperties.getInstance().getAndroidApiKey();
	private static int androidNoOfRetries = IgnightProperties.getInstance().getAndroidRetryCount();

	public static void sendIOSPushNotification(String token, PushNotificationPayload payload) {
		if (token == null) {
			log.error("Token is null: {}", payload);
			return;
		}

		if (iOSKeyStore == null || !iOSKeyStore.exists()) {
			log.error("Could Not Load iOS KeyStore File");
			return;
		}

		try {
			Push.payload(payload, iOSKeyStore, iOSKeyStorePassword, IgnightProperties.getInstance().getIosProductionPushStatus(), token);
		} catch (CommunicationException e) {
			log.error(e.getMessage(), e);
		} catch (KeystoreException e) {
			log.error(e.getMessage(), e);
		} catch (Exception e) {
			log.error("Failed to send ios push notification", e);
		}
	}

	public static void sendAndroidPushNotification(String token, Message message) {
		if (token == null) {
			log.error("Token is null: {}", message);
			return;
		}

		Sender sender = new Sender(androidAPIKey);
		try {
			sender.send(message, token, androidNoOfRetries);
		} catch (Exception e) {
			log.error("Failed to send android push notification", e);
		}
	}
}
