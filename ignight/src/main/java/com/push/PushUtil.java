package com.push;

import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javapns.notification.PushNotificationPayload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.android.gcm.server.Message;
import com.ignight.group.Group;
import com.ignight.user.User;

public class PushUtil {
	private static Logger log = LoggerFactory.getLogger(PushUtil.class);
	private static ExecutorService executors = Executors.newCachedThreadPool();

	/*
	 * Push Notification Helpers
	 */

	public static void sendGroupInvitation(User user, User invitee, Group group) {
		if (!user.notifyOfGroupInvites) {
			log.info("User {} had notifications off for invites", user.username);
			return;
		}

		SendPushNotifications push = new SendPushNotifications(user.id);

		PushNotificationPayload payload = PushMessageBuilder.inviteUserToGroup(invitee.username, group.name);
		push.addPayload(payload);

		Message message = PushMessageBuilder.inviteUserToGroup(invitee, group);
		push.addMessage(message);

		log.info("Sending group invite: userId: {}, invitee: {}, groupId: {}", user.id, invitee.id, group.id);
		executors.execute(push);
	}

	public static void sendNewGroupBuzzAlerts(Set<Long> users, Group group, User user, Long lastBuzzId) {
		SendPushNotifications push = new SendPushNotifications(users);

		PushNotificationPayload payload = PushMessageBuilder.newGroupBuzzAlert(group, user, lastBuzzId);
		push.addPayload(payload);

		Message message = PushMessageBuilder.androidBuildNewGroupBuzzAvailableMessage(group, lastBuzzId);
		push.addMessage(message);

		executors.execute(push);
	}

	public static void sendNewGroupBuzz(Set<Long> users, Group group, User user, Long lastBuzzId) {
		SendPushNotifications push = new SendPushNotifications(users);

		PushNotificationPayload payload = PushMessageBuilder.newGroupBuzz(group, lastBuzzId);
		push.addPayload(payload);

		Message message = PushMessageBuilder.androidBuildNewGroupBuzzAvailableMessage(group, lastBuzzId);
		push.addMessage(message);

		executors.execute(push);
	}

	public static void sendNewVenueBuzz(Set<Long> users, Long venueId, Long latestBuzzId) {
		SendPushNotifications push = new SendPushNotifications(users);

		PushNotificationPayload payload = PushMessageBuilder.newVenueBuzz(venueId);
		push.addPayload(payload);

		Message message = PushMessageBuilder.newVenueBuzz(venueId, latestBuzzId);
		push.addMessage(message);

		executors.execute(push);
	}
}
