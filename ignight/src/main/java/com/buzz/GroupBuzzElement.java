package com.buzz;

public class GroupBuzzElement extends BuzzElement {
	public Long groupId;

	public GroupBuzzElement(long userId, String userName, String chatPictureURL, long groupId, int buzzType,
			String buzzText) {
		super(userId, userName, chatPictureURL, buzzType, buzzText);
		this.groupId = groupId;
	}

	public GroupBuzzElement(long userId, String userName, String chatPictureURL, long groupId, int buzzType) {

		super(userId, userName, chatPictureURL, buzzType);
		this.groupId = groupId;
	}

	@Override
	public int compareTo(BuzzElement o) {
		if (this.getBuzzId() < o.getBuzzId()) {
			return -1;
		} else if (this.getBuzzId() > o.getBuzzId()) {
			return 1;
		} else {
			return 0;
		}
	}
}
