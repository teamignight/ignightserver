package com.buzz;

public class VenueBuzzElement extends BuzzElement {
	long venueId;

	public VenueBuzzElement(long userId, String userName, String chatPictureURL, long venueId, int buzzType,
			String buzzText) {
		super(userId, userName, chatPictureURL, buzzType, buzzText);
		this.venueId = venueId;
	}

	public VenueBuzzElement(long userId, String userName, String chatPictureURL, long venueId, int buzzType) {
		super(userId, userName, chatPictureURL, buzzType);
		this.venueId = venueId;
	}

}
