package com.buzz;

public class ClientBuzzData extends Buzz {
	public String userName;
	public String chatPictureURL = "";

	public ClientBuzzData(Buzz b) {
		this.buzzDateStamp = b.buzzDateStamp;
		this.buzzId = b.buzzId;
		this.buzzText = b.buzzText;
		this.buzzType = b.buzzType;
		this.userId = b.userId;
	}
}
