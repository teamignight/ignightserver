package com.buzz;

public class Buzz implements Comparable<Buzz> {
	public Long buzzId;
	public Long userId;
	public BuzzType buzzType;
	public String buzzText;
	public Long buzzDateStamp;

	public static enum BuzzType {
		TEXT, IMAGE;
	}

	@Override
	public int compareTo(Buzz o) {
		return this.buzzDateStamp.compareTo(o.buzzDateStamp);
	}
}
