package com.buzz;

public class BuzzElement implements Comparable<BuzzElement> {
	Long userId;
	String userName;
	String chatPictureURL;
	Long buzzId;
	long buzzDateStamp;
	int buzzType;
	String buzzText;
	int dnaMatchBarColor;

	long buzzImageIndex = -1;

	public static final int BUZZTYPE_TXT = 0;
	public static final int BUZZTYPE_IMAGE = 1;

	public BuzzElement() {
		this.buzzImageIndex = -1;
	}

	public BuzzElement(long userId, String userName, String chatPictureURL, int buzzType, String buzzText) {
		this.buzzDateStamp = System.currentTimeMillis();
		this.userId = userId;
		this.userName = userName;
		this.chatPictureURL = chatPictureURL;
		this.buzzType = buzzType;
		this.buzzText = buzzText;
	}

	public BuzzElement(long userId, String userName, String chatPictureURL, int buzzType) {
		this.buzzDateStamp = System.currentTimeMillis();
		this.userId = userId;
		this.userName = userName;
		this.chatPictureURL = chatPictureURL;
		this.buzzType = buzzType;
		this.buzzText = "IMAGE_URL_TBD";
	}

	public long getUserId() {
		return this.userId;
	}

	public String getChatPictureURL() {
		return this.chatPictureURL;
	}

	public void setChatPictureURL(String url) {
		this.chatPictureURL = url;
	}

	public long getVenueId() {
		return this.userId;
	}

	public long getBuzzDateStamp() {
		return this.buzzDateStamp;
	}

	public String getBuzzText() {
		return this.buzzText;
	}

	public int getBuzzType() {
		return this.buzzType;
	}

	public void setBuzzText(String buzzText) {
		this.buzzText = buzzText;
	}

	public long getBuzzId() {
		return this.buzzId;
	}

	public void setBuzzId(long buzzId) {
		this.buzzId = buzzId;
	}

	public void setBuzzImageIndex(Long index) {
		this.buzzImageIndex = index;
	}

	public void setDnaMatchBarColor(Double compatibility) {
		if (compatibility <= 0.25) {
			this.dnaMatchBarColor = 3;
			return;
		} else if (compatibility <= 0.5) {
			this.dnaMatchBarColor = 2;
			return;
		} else if (compatibility <= 0.75) {
			this.dnaMatchBarColor = 1;
			return;
		} else {
			this.dnaMatchBarColor = 0;
			return;
		}
	}

	@Override
	public int compareTo(BuzzElement o) {
		if (this.buzzId < o.buzzId) {
			return -1;
		} else if (this.buzzId > o.buzzId) {
			return 1;
		} else {
			return 0;
		}
	}

	@Override
	public String toString() {
		return "BuzzElement [userId=" + userId + ", buzzDateStamp=" + buzzDateStamp + ", buzzType=" + buzzType + "]";
	}

}
