package com.ignight.user;

public class RedisKeys {
	private static final Long REDIS_BUCKET_DIVISOR = 1000l;

	private static final String USERS_HASH_KEY_PREFIX = "users::";
	private static final String USER_DNA_HASH_KEY_PREFIX = "userdna::";
	private static final String USER_IMAGES_HASH_KYE_PREFIX = "userImages::";
	public static final String USERS_GREEN_VENUES_HASH_KEY_PREFIX = "userGreenVenues::";

	private static final String IOS_DEVICE_TOKEN_HASH_KEY_PREFIX = "iosDeviceTokens::";
	private static final String ANDROID_REGISTRATION_HASH_KEY_PREFIX = "androidRegistration::";

	private static final String USERNAME_KEY = "usernameKey";
	private static final String USER_EMAIL_SET_KEY = "userEmailSet";
	private static final String USER_TEMP_PSWD_KEY = "userTempPswd::";

	//private static final String USER_EMAIL_SET_KEY = "userEmailSet";
	private static final String USER_SMS_VERIFICATION_KEY = "userSmsVerification::";
	private static final String PHONE_NUMBER_KEY = "phonenumberKey";
	private static final String ADDRESSBOOK_HASH_KEY_PREFIX = "addressBook::";

	private static final String USER_GROUPS_KEY_PREFIX = "userGroups::";
	private static final String USER_INBOX_PREFIX = "userInbox::";

	public static final String USER_ACTIVITY_SET_KEY_PREFIX = "userActivity::";
	public static final String USER_TRENDING_VENUES_KEY_PREFIX = "userTrending::";
	public static final String USER_TRENDING_BAR_COLOR_PREFIX = "userBarColors::";
	public static final String USER_TRENDING_BAR_REFRESH = "userBarRefresh::";
	public static final String USER_CURRENT_BUZZ_LISTENER_PREFIX = "userBuzzListening::";

	public static final String USER_GROUP_BUZZ_LAST_ACCESS_TIMESTAMP = "userGroupBuzzLastAccessTimeStamps::";

	/*
	 * Users Notifications Keys
	 */
	public static final String USERS_GROUP_BUZZ_NOTIFICATIONS_SET = "userGroupBuzzNotifications";
	public static final String USERS_GROUP_INVITES_NOTIFICATIONS_SET = "userGroupInvitesNotifications";

	public static String getUserGroupBuzzNotificationSetKey() {
		return USERS_GROUP_BUZZ_NOTIFICATIONS_SET;
	}

	public static String getUserGroupInvitesNotificationsSetKey() {
		return USERS_GROUP_INVITES_NOTIFICATIONS_SET;
	}

	/****************************/

	public static String getUserGroupBuzzLastAccessTimeStampKey(Long userId) {
		return USER_GROUP_BUZZ_LAST_ACCESS_TIMESTAMP + userId;
	}

	public static String getUserCurrentBuzzListenerKey(Long userId) {
		return USER_CURRENT_BUZZ_LISTENER_PREFIX + userId;
	}

	public static String getUserInboxKey(Long userId) {
		return USER_INBOX_PREFIX + userId;
	}

	//Map of userId - > User
	public static String getUserBucketKey(Long userId) {
		return USERS_HASH_KEY_PREFIX + userId / REDIS_BUCKET_DIVISOR;
	}

	//Map of userId -> UserDna
	public static String getUserDnaBucketKey(Long userId) {
		return USER_DNA_HASH_KEY_PREFIX + userId / REDIS_BUCKET_DIVISOR;
	}

	//Map of token -> userId
	public static String getIosDeviceTokensKey(Long userId) {
		return IOS_DEVICE_TOKEN_HASH_KEY_PREFIX + userId / REDIS_BUCKET_DIVISOR;
	}

	//Map of token -> userId
	public static String getAndroidRegistartionKey(Long userId) {
		return ANDROID_REGISTRATION_HASH_KEY_PREFIX + userId / REDIS_BUCKET_DIVISOR;
	}

	//Map of userId -> UserImages
	public static String getUserImagesKey(Long userId) {
		return USER_IMAGES_HASH_KYE_PREFIX + userId / REDIS_BUCKET_DIVISOR;
	}

	//Map of userId - > Green Venues
	public static String getUserGreenVenuesKey(Long userId) {
		return USERS_GREEN_VENUES_HASH_KEY_PREFIX + userId;
	}

	//user key -> temporrary password
	public static String getUserTempPasswordKey(String username) {
		return USER_TEMP_PSWD_KEY + username;
	}

	public static String getSmsVerificationCodeKey(String username) {
		return USER_SMS_VERIFICATION_KEY + username;
	}

	//user email set
	public static String getUserEmailKey() {
		return USER_EMAIL_SET_KEY;
	}

	//username set
	public static String getUsernameKeyPrefix() {
		return USERNAME_KEY;
	}

	public static String getUserDnaBucketPrefix() {
		return USER_DNA_HASH_KEY_PREFIX;
	}

	public static String getIosDeviceKeyPrefix() {
		return IOS_DEVICE_TOKEN_HASH_KEY_PREFIX;
	}

	public static String getAndroidRegistrationKeyPrefix() {
		return ANDROID_REGISTRATION_HASH_KEY_PREFIX;
	}

	//List of groups
	public static String getUserGroupsKey(Long userId) {
		return USER_GROUPS_KEY_PREFIX + userId;
	}

	public static String getUserTrendingVenuesKey(Long userId) {
		return USER_TRENDING_VENUES_KEY_PREFIX + userId;
	}

	public static String getUserTrendingBarColorsKey(Long userId) {
		return USER_TRENDING_BAR_COLOR_PREFIX + userId;
	}

	public static String getUserTrendingBarRefreshKey(Long userId) {
		return USER_TRENDING_BAR_REFRESH + userId;
	}

	public static String getPhoneNumberKey() {
		return PHONE_NUMBER_KEY;
	}

	public static String getUserAddressBookKey(Long userId) {
		return ADDRESSBOOK_HASH_KEY_PREFIX + userId / REDIS_BUCKET_DIVISOR;
	}

	//Set of user -> upvoted venues
	public static String getUserActivityKey(Long userId) {
		return USER_ACTIVITY_SET_KEY_PREFIX + userId;
	}

	/****************************** VENUES *********************************/

	private static final String VENUE_HASH_KEY_PREFIX = "venues::";
	public static final String VENUE_TRENDING_KEY_PREFIX = "venueTrending::";
	public static final String VENUE_DNA_KEY = "venueDna";
	public static final String VENUE_ACTIVITY_KEY_PREFIX = "venueActivity::";
	public static final String VENUE_NO_OF_GOOGLE_IMAGES_KEY = "venueNoOfGoogleImages";

	public static final String VENUE_EVENT_INFO_PREFIX = "venueEventInfo::";

	public static final String VENUE_BUZZ_PREFIX = "venueBuzz::";
	public static final String VENUE_BUZZ_ACTIVE_SET = "venueBuzzActiveSet";
	public static final String VENUE_BUZZ_ID_PREFIX = "venueBuzzId::";
	private static final String VENUE_BUZZ_IMAGES_PREFIX = "venueBuzzImages::";
	private static final String VENUE_BUZZ_IMAGES_ID_PREFIX = "venueBuzzImagesId::";
	public static final String VENUE_CURRENT_BUZZ_LISTENERS_PREFIX = "venueBuzzListeners::";


	public static String getVenueBuzzActiveSetKey() {
		return VENUE_BUZZ_ACTIVE_SET;
	}

	public static String getVenueActivityPrefix() {
		return VENUE_ACTIVITY_KEY_PREFIX;
	}

	public static String getVenueBucketKey(Long venueId) {
		return VENUE_HASH_KEY_PREFIX + venueId / REDIS_BUCKET_DIVISOR;
	}

	public static String getVenueKey(Long venueId) {
		return VENUE_TRENDING_KEY_PREFIX + venueId / REDIS_BUCKET_DIVISOR;
	}

	public static String getVenueEventInfoKey(Long venueId) {
		return VENUE_EVENT_INFO_PREFIX + venueId / REDIS_BUCKET_DIVISOR;
	}

	public static String getNoOfGoogleImagesKey() {
		return VENUE_NO_OF_GOOGLE_IMAGES_KEY;
	}

	public static String getVenueDnaKey() {
		return VENUE_DNA_KEY;
	}

	//Storing in set activity
	public static String getVenueActivityKey(Long venueId) {
		return VENUE_ACTIVITY_KEY_PREFIX + venueId;
	}

	public static String getVenueBuzzImagesKey(Long venueId) {
		return VENUE_BUZZ_IMAGES_PREFIX + venueId;
	}

	public static String getVenueBuzzImagesIdKey(Long venueId) {
		return VENUE_BUZZ_IMAGES_ID_PREFIX + venueId;
	}

	//Storing as map per venue : buzzId -> Buzz
	public static String getVenueBuzzKey(Long venueId) {
		return VENUE_BUZZ_PREFIX + venueId;
	}

	//Atmoicly incremented keys per venue
	public static String getVenueBuzzIdPrefix(Long venueId) {
		return VENUE_BUZZ_ID_PREFIX + venueId;
	}

	//Per Venue : Set?
	public static String getVenueBuzzListeners(Long venueId) {
		return VENUE_CURRENT_BUZZ_LISTENERS_PREFIX + venueId;
	}

	/****************************** GROUPS *********************************/

	public static final String GROUP_HASH_KEY_PREFIX = "groups::";
	private static final String GROUP_DNA_KEY_PREFIX = "groupDna::";
	public static final String GROUP_TRENDING_KEY_PREFIX = "groupTrending::";

	private static final String GROUP_MEMBERS_PREFIX = "groupMembers::";
	private static final String GROUP_INVITEE_PREFIX = "groupInvitee::";

	public static final String GROUP_BUZZ_PREFIX = "groupBuzz::";
	public static final String GROUP_BUZZ_ID_PREFIX = "groupBuzzId::";
	private static final String GROUP_BUZZ_IMAGES_PREFIX = "groupBuzzImages::";
	private static final String GROUP_BUZZ_IMAGES_ID_PREFIX = "groupBuzzImagesId::";

	public static final String GROUP_BUZZ_SUBSCRIBERS = "groupBuzzSubscribers::";
	public static final String GROUP_CURRENT_BUZZ_LISTENERS_PREFIX = "groupBuzzListeners::";

	public static final String GROUP_BUZZ_LAST_LISTENERS_PREFIX = "groupBuzzListeners::";

	public static final String POPULAR_GORUPS = "popularGroups";

	public static String getPopularGroupsKey() {
		return POPULAR_GORUPS;
	}

	public static String getGroupMembersAllKey() {
		return GROUP_MEMBERS_PREFIX + "*";
	}

	public static String getGroupTrendingKey(Long groupId) {
		return GROUP_TRENDING_KEY_PREFIX + groupId;
	}

	public static String getGroupsBucketKey(Long groupId) {
		return GROUP_HASH_KEY_PREFIX + groupId / REDIS_BUCKET_DIVISOR;
	}

	public static String getGroupDnaKey(Long groupId) {
		return GROUP_DNA_KEY_PREFIX + groupId / REDIS_BUCKET_DIVISOR;
	}

	public static String getGroupMembersKey(Long groupId) {
		return GROUP_MEMBERS_PREFIX + groupId;
	}

	public static String getGroupInviteeKey(Long groupId) {
		return GROUP_INVITEE_PREFIX + groupId;
	}

	//Storing as map per group : buzzId -> Buzz
	public static String getGroupBuzzKey(Long groupId) {
		return GROUP_BUZZ_PREFIX + groupId;
	}

	//Atmoicly incremented keys per group
	public static String getGroupBuzzIdPrefix(Long groupId) {
		return GROUP_BUZZ_ID_PREFIX + groupId;
	}

	public static String getGroupBuzzImagesKey(Long groupId) {
		return GROUP_BUZZ_IMAGES_PREFIX + groupId;
	}

	public static String getGroupBuzzImagesIdPrefix(Long groupId) {
		return GROUP_BUZZ_IMAGES_ID_PREFIX + groupId;
	}

	public static String getGroupBuzzSubscribers(Long groupId) {
		return GROUP_BUZZ_SUBSCRIBERS + groupId;
	}

	//Per Group : Set?
	public static String getGroupBuzzListeners(Long groupId) {
		return GROUP_CURRENT_BUZZ_LISTENERS_PREFIX + groupId;
	}

	/****************************** DATA *********************************/
	public static final String DATA_ARCHIVE_IN_PROGRESS_PREFIX = "archiveInProgress::";

	public static String getArchiveInProgressKey(Long cityId) {
		return DATA_ARCHIVE_IN_PROGRESS_PREFIX + cityId;
	}

	/************************************ SQS *************************************/

	public static final String SQS_MSG_QUEUE_PREFIX = "sqsIgMsgs::";

	public static final String APP_SERVERS_LIST = "appServers::";
}
