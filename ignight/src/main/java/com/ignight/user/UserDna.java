package com.ignight.user;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class UserDna {
	@SerializedName("id")
	public Long userId = null;
	@SerializedName("a")
	public Long ageId = null;
	@SerializedName("c")
	public Long cityId = null;
	@SerializedName("m")
	public List<Long> musicIds = new ArrayList<Long>();
	@SerializedName("atm")
	public List<Long> atmosphereIds = new ArrayList<Long>();
	@SerializedName("s")
	public Long spendingLimit = null;

	public UserDna() {
	}

	public UserDna(Long userId) {
		this.userId = userId;
	}

	public UserDna userId(Long userId) {
		this.userId = userId;
		return this;
	}

	public UserDna ageId(Long ageId) {
		this.ageId = ageId;
		return this;
	}

	public UserDna cityId(Long cityId) {
		this.cityId = cityId;
		return this;
	}

	public UserDna musicIds(List<Long> musicIds) {
		this.musicIds = musicIds;
		return this;
	}

	public UserDna atmosphereIds(List<Long> atmosphereIds) {
		this.atmosphereIds = atmosphereIds;
		return this;
	}

	public UserDna spendingLimit(Long spendingLimit) {
		this.spendingLimit = spendingLimit;
		return this;
	}

	public UserDna clone() {
		UserDna dna = new UserDna();
		dna.userId = this.userId;
		dna.ageId = this.ageId;
		dna.cityId = this.cityId;
		dna.musicIds = new ArrayList<Long>(this.musicIds.size());
		for (Long mIds : this.musicIds) {
			dna.musicIds.add(mIds);
		}
		dna.atmosphereIds = new ArrayList<Long>(this.atmosphereIds.size());
		for (Long aIds : this.atmosphereIds) {
			dna.atmosphereIds.add(aIds);
		}
		dna.spendingLimit = this.spendingLimit;
		return dna;
	}

	public boolean validate() {
		return (userId != null &&
				ageId != null &&
				cityId != null &&
				musicIds != null && musicIds.size() > 0 &&
				atmosphereIds != null && atmosphereIds.size() > 0 && spendingLimit != null);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserDna [userId=");
		builder.append(userId);
		builder.append(", ageId=");
		builder.append(ageId);
		builder.append(", cityId=");
		builder.append(cityId);
		builder.append(", musicIds=");
		builder.append(musicIds);
		builder.append(", atmosphereIds=");
		builder.append(atmosphereIds);
		builder.append(", spendingLimit=");
		builder.append(spendingLimit);
		builder.append("]");
		return builder.toString();
	}
}
