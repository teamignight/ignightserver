package com.ignight.user;

import java.util.ArrayList;
import java.util.Arrays;

public class PhoneContact {

	public String fName;
	public String lName;
	public ArrayList<String> numbers;
	
	public PhoneContact(String fName, String lName, String[] numbers) {
		this.fName = fName;
		this.lName = lName;
		this.numbers = new ArrayList<String>(Arrays.asList(numbers));
	}
}
