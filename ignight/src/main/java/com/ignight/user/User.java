package com.ignight.user;

import java.util.HashMap;
import java.util.Map;

import net.karneim.pojobuilder.GeneratePojoBuilder;

import com.google.gson.annotations.SerializedName;

@GeneratePojoBuilder
public class User {
	@SerializedName("id")
	public Long id;
	@SerializedName("e")
	public String email;
	@SerializedName("u")
	public String username; // TODO: Update clients
	@SerializedName("g")
	public Long genderId;
	@SerializedName("c")
	public Long cityId;
	@SerializedName("nG")
	public boolean notifyOfGroupInvites = true;
	@SerializedName("p")
	public String profilePictureUrl;
	@SerializedName("no")
	public String number;
	
	
	private String encryptedPassword;

	public String getEncryptedPassword() {
		return encryptedPassword;
	}

	public void setEncryptedPassword(String p) {
		this.encryptedPassword = p;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("User [id=");
		builder.append(id);
		builder.append(", email=");
		builder.append(email);
		builder.append(", username=");
		builder.append(username);
		builder.append(", genderId=");
		builder.append(genderId);
		builder.append(", cityId=");
		builder.append(cityId);
		builder.append(", notifyOfGroupInvites=");
		builder.append(notifyOfGroupInvites);
		builder.append(", profilePictureUrl=");
		builder.append(profilePictureUrl);
		builder.append(", number=");
		builder.append(number);
		builder.append("]");
		return builder.toString();
	}

}
