package com.ignight.user;

public class GroupInvite implements Comparable<GroupInvite> {
	public Long groupId;
	public Long inviteeId;
	public Long ts;

	public GroupInvite(Long groupId, Long inviteeId) {
		this.groupId = groupId;
		this.inviteeId = inviteeId;
		this.ts = System.currentTimeMillis();
	}

	@Override
	public int compareTo(GroupInvite o) {
		if (o == null) {
			return -1;
		}
		if (this.ts > o.ts) {
			return -1;
		} else if (this.ts < o.ts) {
			return 1;
		}
		return 0;
	}

}
