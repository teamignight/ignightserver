package com.ignight.user;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DailyUserData {
	public long userId;
	public Set<Long> upVoted = new HashSet<Long>();
	public Set<Long> downVoted = new HashSet<Long>();
	public Long dateCreatedTimeStamp;
	private static final Logger log = LoggerFactory.getLogger(DailyUserData.class);

	public DailyUserData(long userID) {
		this.userId = userID;
		this.dateCreatedTimeStamp = System.currentTimeMillis();
	}

	public boolean upVote(long venueId) {
		removeDownVote(venueId);
		return upVoted.add(venueId);
	}

	public boolean downVote(long venueId) {
		removeUpVote(venueId);
		return downVoted.add(venueId);
	}

	public boolean removeUpVote(long venueId) {
		return upVoted.remove(venueId);
	}

	public boolean removeDownVote(long venueId) {
		return downVoted.remove(venueId);
	}

	/*
	 * public int getNoOfVotes(){ return this.upVoted.size() +
	 * this.downVoted.size(); }
	 */

	public int getNoOfUpvotes() {
		return this.upVoted.size();
	}

	public int getNoOfDownvotes() {
		return this.downVoted.size();
	}

	public void setUserActivity(long venueId, long val) {
		if (val == 0) {
			removeDownVote(venueId);
			removeUpVote(venueId);
		} else if (val == 1) {
			upVote(venueId);
		} else if (val == -1) {
			downVote(venueId);
		} else {
			log.error("Got Unknown Value - VenueId: {}, userActivity: {}", venueId, val);
		}
	}

}
