package com.ignight.user;

import com.ignight.servlets.user.UserDataSource.UserSearch;

public class SearchedUser implements Comparable<SearchedUser> {
	public Long userId;
	public String userName;
	public String chatPictureURL;
	public int groupStatus;
	public String fName;
	public String lName;

	public SearchedUser(UserSearch user) {
		this.userId = user.id;
		this.userName = user.username;
		this.groupStatus = GroupStatus.NONE.ordinal();
	}

	@Override
	public int compareTo(SearchedUser o) {
		if (o == null || o.userName == null) {
			return -1;
		}

		if (userName == null) {
			return 1;
		}

		return userName.compareTo(o.userName);
	}

	public static enum GroupStatus {
		NONE, INVITED, MEMBER;
	}

}
