package com.ignight.venue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VenueActivity {
	public long id;
	public List<Long> upvotes = new ArrayList<Long>();
	public List<Long> downvotes = new ArrayList<Long>();
	public Map<Long, Long> groupActivityCounter = new HashMap<Long, Long>();

	public VenueActivity(long venueId) {
		this.id = venueId;
	}
}
