package com.ignight.venue;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.buzz.Buzz;
import com.buzz.Buzz.BuzzType;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ignight.core.CoreData;
import com.ignight.imagehandling.ImageHandler;
import com.ignight.user.RedisKeys;

import databases.Databases;
import databases.JedisConnection;
import databases.Resources;

public class VenueDataSource {
	private static Logger log = LoggerFactory.getLogger(VenueDataSource.class);
	private static Gson gson = new GsonBuilder().disableHtmlEscaping().create();

	public static final JedisConnection jedis = JedisConnection.getInstance();

	public static Set<Long> getListOfVenueIdsForCity(Long cityId) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		Set<Long> venueIds = new HashSet<Long>();

		try {
			conn = Databases.dataSource.getConnection();
			ps = conn.prepareStatement(
					"SELECT id FROM venues WHERE city_id = " + cityId);
			rs = ps.executeQuery();

			while (rs.next()) {
				Long nextVenueId = rs.getLong("id");
				venueIds.add(nextVenueId);
			}

			return venueIds;

		} finally {
			Resources.close(conn, ps, rs);
		}
	}

	public static Long getVenueId(String name) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			conn = Databases.dataSource.getConnection();
			ps = conn.prepareStatement(
					"SELECT id FROM venues WHERE city_id = 0 AND name = '" + name + "'");
			rs = ps.executeQuery();

			if (rs.next()) {
				return rs.getLong("id");
			}

			return null;

		} finally {
			Resources.close(conn, ps, rs);
		}
	}

	public static void loadAndSavedNoOfGooglePlaceImagesToRedis() throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			conn = Databases.dataSource.getConnection();
			ps = conn.prepareStatement("SELECT id, no_of_pictures FROM venues");
			rs = ps.executeQuery();
			while (rs.next()) {
				Long id = rs.getLong(1);
				Integer noOfPictures = rs.getInt(2);
				String key = RedisKeys.getNoOfGoogleImagesKey();
				
				log.info("Adding No. of Pictures To Cache for Venue Id: {}", id);
				jedis.hset(key, id.toString(), noOfPictures.toString());
			}		
		} finally {
			Resources.close(conn, ps, rs);
		}
	}
	
	public static void loadAndSaveVenuesToRedis() throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			conn = Databases.dataSource.getConnection();
			ps = conn.prepareStatement("SELECT * FROM venues");
			rs = ps.executeQuery();
			List<Venue> venues = new ArrayList<Venue>();
			while (rs.next()) {
				Venue v = new VenueBuilder().withId(rs.getLong(1))
						.withName(rs.getString(2))
						.withCityId(rs.getLong(3))
						.withAddress(rs.getString(4))
						.withNumber(rs.getString(5))
						.withLat(rs.getDouble(6))
						.withLon(rs.getDouble(7))
						.withUrl(rs.getString(8)).build();
				v.priceLevel = rs.getInt(10);
				v.rating = rs.getDouble(11);
				v.hours = rs.getString(12);
				venues.add(v);
			}

			log.info("Loaded {} venues from SQL", venues.size());

			for (Venue v : venues) {
				String key = RedisKeys.getVenueBucketKey(v.id);
				String obj = jedis.hget(key, v.id.toString());
				if (obj == null) {
					log.warn("Adding Venue To Cache: {}, {}", v.id, v.name);
					jedis.hset(key, v.id.toString(), v);
				}
				CoreData.addVenue(v.id, v);
			}
		} finally {
			Resources.close(conn, ps, rs);
		}
	}
	
	public static List<Venue> loadVenueList() throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			conn = Databases.dataSource.getConnection();
			ps = conn.prepareStatement("SELECT * FROM venues");
			rs = ps.executeQuery();
			List<Venue> venues = new ArrayList<Venue>();
			while (rs.next()) {
				Venue v = new VenueBuilder().withId(rs.getLong(1))
						.withName(rs.getString(2))
						.withCityId(rs.getLong(3))
						.withAddress(rs.getString(4))
						.withNumber(rs.getString(5))
						.withLat(rs.getDouble(6))
						.withLon(rs.getDouble(7))
						.withUrl(rs.getString(8)).build();
				v.googlePlacesId = rs.getString(9);
				v.priceLevel = rs.getInt(10);
				v.rating = rs.getDouble(11);
				v.hours = rs.getString(12);
				venues.add(v);
			}

			log.info("Loaded {} venues from SQL", venues.size());

			return venues;
		} finally {
			Resources.close(conn, ps, rs);
		}
	}
	
	

	public static Venue loadVenue(final long venueId) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			conn = Databases.dataSource.getConnection();
			ps = conn.prepareStatement("SELECT * FROM venues where id = ?");
			ps.setObject(1, venueId);
			rs = ps.executeQuery();

			if (rs.next()) {
				Venue v = new VenueBuilder().withId(rs.getLong(1))
						.withName(rs.getString(2))
						.withCityId(rs.getLong(3))
						.withAddress(rs.getString(4))
						.withNumber(rs.getString(5))
						.withLat(rs.getDouble(6))
						.withLon(rs.getDouble(7))
						.withUrl(rs.getString(8)).build();
				return v;
			}
		} catch (SQLException e) {
			log.error("Failed to load venue with id: {}", venueId, e);
		} finally {
			Resources.close(conn, ps, rs);
		}
		return null;
	}

	public static List<List<Object>> loadGooglePlacesVenueDataResultsSet() throws SQLException {
		List<List<Object>> output = new ArrayList<List<Object>>();
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			conn = Databases.dataSource.getConnection();
			ps = conn.prepareStatement("SELECT id, google_places_id FROM venues");
			rs = ps.executeQuery();
			
			while(rs.next()){
				ArrayList<Object> currentPlaceInfo = new ArrayList<Object>();
				currentPlaceInfo.add(0, rs.getLong(1));
				currentPlaceInfo.add(1, rs.getString(2));
				output.add(currentPlaceInfo);
			}
			
			return output;
		} finally {
			Resources.close(conn, ps, rs);
		}
	}
	

	
	
	/*
	 * Venue Activity
	 */

	public static void saveVenueError(VenueReport error, Long userId) throws SQLException {
		Databases.insert("INSERT INTO venue_errors " +
				"(user_id, venue_id, name, city_id, address, number, url, comments) " +
				"VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
				userId, error.id, error.name, error.cityId,
				error.address, error.number, error.url, error.comments);
	}

	public static void saveVenueAddition(VenueReport add, Long userId) throws SQLException {
		Databases.insert("INSERT INTO venue_additions " +
				"(user_id, name, city_id, address, number, url, comments) " +
				"VALUES (?, ?, ?, ?, ?, ?, ?)",
				userId, add.name, add.cityId, add.address, add.number, add.url, add.comments);
	}

	public static void flagVenueBuzz(Long userId, Long venueId, Long buzzId) throws SQLException {
		Databases.insert("INSERT INTO flagged_buzz " +
				"(user_id, venue_id, buzz_id) " +
				"VALUES (?, ?, ?)",
				userId, venueId, buzzId);
	}

	public static boolean isVenueActive(Long venueId) {
		String key = RedisKeys.getVenueActivityKey(venueId);
		return jedis.scard(key) > 0;
	}

	public static void setUserActivity(Long venueId, Long userId, Long activity) {
		if (Math.abs(activity) > 1) {
			return;
		}
		String key = RedisKeys.getVenueActivityKey(venueId);
		Long upvote = userId;
		Long downvote = userId * -1;

		jedis.srem(key, upvote.toString());
		jedis.srem(key, downvote.toString());
		if (activity != 0) {
			Long userActivity = userId * activity;
			jedis.sadd(key, userActivity.toString());
		} else {

		}
	}
	
	/*
	 * Check if this should just be upvotes
	 */
	
	public static Set<Long> getUserActivity(Long venueId){
		String key = RedisKeys.getVenueActivityKey(venueId);
		Set<String> userVotesString = jedis.smembers(key);
		
		Set<Long> userVotesLong = new HashSet<Long>();
		for(String id: userVotesString){
			Long currentUserId = Long.valueOf(id);
			userVotesLong.add(currentUserId);
		}
		return userVotesLong;
	}
	
	
	

	public static long getUserActivity(Long venueId, Long userId) {
		String key = RedisKeys.getVenueActivityKey(venueId);
		Long upvote = userId;
		if (jedis.sismember(key, upvote.toString())) {
			return 1;
		}
		Long downvote = userId * -1;
		if (jedis.sismember(key, downvote.toString())) {
			return -1;
		}
		return 0;
	}

	public static VenueTrending getVenueTrending(Long venueId) {
		String key = RedisKeys.getVenueKey(venueId);
		String data = jedis.hget(key, venueId.toString());
		if (data != null) {
			return gson.fromJson(data, VenueTrending.class);
		}
		return new VenueTrending();
	}

	public static void addUserAsListener(Long venueId, Long userId) {
		String userBuzzKey = RedisKeys.getUserCurrentBuzzListenerKey(userId);
		String val = jedis.get(userBuzzKey);
		if (val != null) {
			String gOrV = val.split("-")[0];
			Long id = Long.valueOf(val.split("-")[1]);
			if (gOrV.equalsIgnoreCase("G")) {

			} else {
				VenueDataSource.removeUserAsListener(id, userId);
			}
		}
		jedis.set(userBuzzKey, "V-" + venueId);

		String key = RedisKeys.getVenueBuzzListeners(venueId);
		jedis.sadd(key, userId.toString());
	}

	public static void removeUserAsListener(Long venueId, Long userId) {
		String userBuzzKey = RedisKeys.getUserCurrentBuzzListenerKey(userId);
		jedis.del(userBuzzKey);

		String key = RedisKeys.getVenueBuzzListeners(venueId);
		jedis.srem(key, userId.toString());
	}

	public static Set<Long> getVenueBuzzListeners(Long venueId, Long userId) {
		String userBuzzKey = RedisKeys.getVenueBuzzListeners(venueId);
		Set<String> members = jedis.smembers(userBuzzKey);

		Set<Long> listeners = new HashSet<Long>();
		for (String m : members) {
			listeners.add(Long.valueOf(m));
		}
		listeners.remove(userId);
		return listeners;
	}

	//	public static boolean isUserLiseningToBuzz(Long venueId, Long userId) {
	//		String key = RedisKeys.getVenueBuzzListeners(venueId);
	//		return jedis.sismember(key, userId.toString());
	//	}

	
	public static void addVenueEventInfo(Long venueId, String venueEventInfo){
		String venueEventInfoKey = RedisKeys.getVenueEventInfoKey(venueId);
		jedis.hset(venueEventInfoKey, venueId.toString(), venueEventInfo);
	}
	
	public static String getVenueEventInfo(Long venueId){
		String venueEventInfoKey = RedisKeys.getVenueEventInfoKey(venueId);
		String venueEventInfo = jedis.hget(venueEventInfoKey, venueId.toString());
		return venueEventInfo;
	}
	
	
	public static long addBuzz(Long venueId, Buzz b) {
		String buzzIdKey = RedisKeys.getVenueBuzzIdPrefix(venueId);
		Long buzzId = jedis.incr(buzzIdKey) - 1;
		b.buzzId = buzzId;
		String buzzKey = RedisKeys.getVenueBuzzKey(venueId);
		jedis.hset(buzzKey, buzzId.toString(), b);
		if (b.buzzType == BuzzType.IMAGE) {
			String imageKey = RedisKeys.getVenueBuzzImagesKey(venueId);
			String imagesIdKey = RedisKeys.getVenueBuzzImagesIdKey(venueId);
			Long id = jedis.incr(imagesIdKey) - 1;
			jedis.hset(imageKey, id.toString(), b.buzzText);
		}
		setVenueBuzzAsActive(venueId);
		return buzzId;
	}

	public static Long getLastBuzzId(Long venueId) {
		String buzzIdKey = RedisKeys.getVenueBuzzIdPrefix(venueId);
		String val = jedis.get(buzzIdKey);
		if (val == null) {
			return null;
		}
		return Long.valueOf(val);
	}

	public static List<Buzz> getBuzz(Long venueId, Long lastBuzzSeen, int count, boolean latest) {
		ArrayList<Buzz> buzzes = new ArrayList<Buzz>();
		Long last = getLastBuzzId(venueId);

		//If no buzz, return empty
		if (last == null) {
			return buzzes;
		}

		//iF at last buzz, return empty
		if (lastBuzzSeen.equals(last)) {
			return buzzes;
		}

		Long start, end;

		if (lastBuzzSeen < 0) {
			start = Math.max(0, last - count + 1);
			end = last;
		} else {
			start = latest ? lastBuzzSeen + 1 : Math.max(0, lastBuzzSeen - count + 1);
			end = latest ? Math.max(last, lastBuzzSeen + count) : Math.max(0, lastBuzzSeen - 1);
		}

		int c = 0;
		String[] fields = new String[(int) (end - start + 1)];
		for (Long i = start; i <= end; i++) {
			fields[c++] = i.toString();
		}

		String key = RedisKeys.getVenueBuzzKey(venueId);
		List<String> vals = jedis.hmget(key, fields);
		for (String s : vals) {
			if (s == null) {
				continue;
			}
			Buzz b = gson.fromJson(s, Buzz.class);
			buzzes.add(b);
		}

		return buzzes;
	}

	public static List<Buzz> getAllVenueBuzz(Long venueId) {
		ArrayList<Buzz> buzzes = new ArrayList<Buzz>();
		Long last = getLastBuzzId(venueId);

		//If no buzz, return empty
		if (last == null) {
			return buzzes;
		}
		Long start, end;

		start = 0L;
		end = last;

		int c = 0;
		String[] fields = new String[(int) (end - start + 1)];
		for (Long i = start; i <= end; i++) {
			fields[c++] = i.toString();
		}

		String key = RedisKeys.getVenueBuzzKey(venueId);
		List<String> vals = jedis.hmget(key, fields);
		for (String s : vals) {
			if (s == null) {
				continue;
			}
			Buzz b = gson.fromJson(s, Buzz.class);
			buzzes.add(b);
		}

		return buzzes;
	}

	public static class ClientBuzzImageData implements Comparable<ClientBuzzImageData> {
		public Long buzzId;
		public String buzzText;

		@Override
		public int compareTo(ClientBuzzImageData o) {
			return o.buzzId.compareTo(this.buzzId);
		}
	}

	public static List<ClientBuzzImageData> getBuzzImages(Long venueId, Long start, long count) {
		List<ClientBuzzImageData> images = new ArrayList<VenueDataSource.ClientBuzzImageData>();

		String[] fields = new String[(int) count];
		int c = 0;
		for (Long i = start; i < start + count; i++) {
			fields[c++] = i.toString();
		}

		String key = RedisKeys.getVenueBuzzImagesKey(venueId);
		List<String> results = jedis.hmget(key, fields);

		for (Long i = start; i < results.size(); i++) {
			String r = results.get(i.intValue());
			if (r != null) {
				ClientBuzzImageData buzz = new ClientBuzzImageData();
				buzz.buzzId = i;
				buzz.buzzText = r;
				images.add(buzz);
			}
		}
		return images;
	}
	
	
	public static ArrayList<String> getGooglePlaceImagesArray(Long venueId){
		ArrayList<String> placeImages = new ArrayList<String>();
		
		String noOfGoogleImagesKey = RedisKeys.getNoOfGoogleImagesKey();
		String noOfImagesString = jedis.hget(noOfGoogleImagesKey, venueId.toString());
		if(noOfImagesString == null)
			return placeImages;
		
		Integer noOfImages = Integer.valueOf(noOfImagesString);		
		for(int i = 0; i < noOfImages; i++){
			placeImages.add(ImageHandler.generateGooglePlacesImageUrl(venueId, Long.valueOf(i)));
		}
		return placeImages;
	}
	
	public static String getGooglePlaceImageForTrendingList(Long venueId){
		String placeImage = null;
		
		String noOfGoogleImagesKey = RedisKeys.getNoOfGoogleImagesKey();
		String noOfImagesString = jedis.hget(noOfGoogleImagesKey, venueId.toString());
		if(noOfImagesString == null)
			return placeImage;
		Integer noOfImages = Integer.valueOf(noOfImagesString);		
		if(noOfImages > 0)
			placeImage = ImageHandler.generateGooglePlacesImageUrl(venueId, Long.valueOf(0));
		return placeImage;
	}

	/*
	 * Venue Buzz Active
	 */
	public static void setVenueBuzzAsActive(Long venueId) {
		String key = RedisKeys.getVenueBuzzActiveSetKey();
		jedis.sadd(key, venueId.toString());
	}

	public static boolean isVenueBuzzActive(Long venueId) {
		if (venueId == null) {
			return false;
		}
		String key = RedisKeys.getVenueBuzzActiveSetKey();
		return jedis.sismember(key, venueId.toString());
	}
}
