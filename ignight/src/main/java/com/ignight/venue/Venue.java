package com.ignight.venue;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import net.karneim.pojobuilder.GeneratePojoBuilder;

import com.google.gson.annotations.SerializedName;
import com.util.Coordinate;

public class Venue implements Comparable<Venue> {
	@SerializedName("id")
	public Long id;
	@SerializedName("n")
	public String name;
	@SerializedName("c")
	public Long cityId;
	@SerializedName("a")
	public String address;
	@SerializedName("no")
	public String number;
	@SerializedName("la")
	public Double latitude;
	@SerializedName("lo")
	public Double longitude;
	@SerializedName("u")
	public String url;
	@SerializedName("gpid")
	public String googlePlacesId;
	@SerializedName("pl")
	public Integer priceLevel;
	@SerializedName("r")
	public Double rating;
	@SerializedName("h")
	public String hours;
	private Coordinate coordinate;

	@GeneratePojoBuilder
	public Venue(Long id, String name, Long cityId, String address, String number, Double lat, Double lon, String url) {
		this.id = id;
		this.name = name;
		this.cityId = cityId;
		this.address = address;
		this.number = number;
		this.latitude = lat;
		this.longitude = lon;
		this.url = url;
		if (latitude != null && longitude != null) {
			this.coordinate = new Coordinate(latitude, longitude);
		}
	}

	public Coordinate getCoordinate() {
		return coordinate;
	}

	@Override
	public String toString() {
		return "Venue [id=" + id + ", name=" + name + ", cityId=" + cityId + ", address=" +
				address + ", number=" + number + ", latitude=" + latitude + ", longitude=" +
				longitude + ", url=" + url + "]";
	}

	@Override
	public boolean equals(Object o) {
		return id == ((Venue) o).id;
	}

	@Override
	public int compareTo(Venue o) {
		return this.name.toLowerCase().compareTo(o.name.toLowerCase());
	}

	public static Comparator<Venue> VenueNameComparator = new Comparator<Venue>() {
		public int compare(Venue v1, Venue v2) {
			return v1.compareTo(v2);
		}
	};

	public Map<String, Object> toDataInMap() {
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("venueId", id);
		data.put("name", name);
		data.put("address", address);
		data.put("phoneNumber", number);
		data.put("url", url);
		data.put("placesId", googlePlacesId);
		data.put("latitude", latitude);
		data.put("longitude", longitude);
		data.put("priceLevel", priceLevel);
		data.put("hours", hours);
		data.put("rating", rating);
		return data;
	}
}
