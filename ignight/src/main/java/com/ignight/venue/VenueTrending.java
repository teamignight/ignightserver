package com.ignight.venue;

import java.util.ArrayList;
import java.util.List;

import com.ignight.dna.TrendingValues;

public class VenueTrending {
	public TrendingValues trendingValues;
	public List<Long> trendingGroups = new ArrayList<Long>();

	public VenueTrending() {
		this.trendingValues = new TrendingValues(-1, -1, -1, -1);
	}

	public VenueTrending(TrendingValues dna, List<TrendingGroup> groups) {
		this.trendingValues = dna;
		for (TrendingGroup group : groups) {
			trendingGroups.add(group.groupId);
		}
	}
}
