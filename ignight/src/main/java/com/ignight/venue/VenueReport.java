package com.ignight.venue;

public class VenueReport extends Venue {
	public String comments;

	public VenueReport(Long id,
			String name,
			Long cityId,
			String address,
			String number,
			String url,
			String comments) {
		super(id, name, cityId, address, number, null, null, url);
		this.comments = comments;
	}
}
