package com.ignight.venue;

import java.util.ArrayList;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TrendingDataBarCalculator {
	private static final Logger log = LoggerFactory.getLogger(TrendingDataBarCalculator.class);

	public static final int GRAYBAR = 4;
	public static final int REDBAR = 3;
	public static final int YELLOWBAR = 2;
	public static final int ORANGEBAR = 1;
	public static final int GREENBAR = 0;

	private double absoluteRedCutoff = 1.0;
	private double absoluteYellowCutoff = 3.0;
	private double absoluteOrangeCutoff = 5.0;

	private double relativeRedCutoff;
	private double relativeYellowCutoff;
	private double relativeOrangeCutoff;

	private double redStDevMultiple = 1.0;
	private double orangeStDevMultiple = 1.0;

	private ArrayList<Integer> barColorCounts = new ArrayList<Integer>();
	public static ArrayList<Integer> barColorMaximumCounts = new ArrayList<Integer>();
	private static Random random = new Random();

	static {
		barColorMaximumCounts.add(GREENBAR, generateRandomIntInRange(5, 10));
		barColorMaximumCounts.add(ORANGEBAR, generateRandomIntInRange(5, 10));
		barColorMaximumCounts.add(YELLOWBAR, generateRandomIntInRange(5, 10));
	}

	private double mean;
	private double stDev;

	public double avgVotesPerUser;
	public boolean areTrendingBarCutoffSet = false;
	private TrendingListStatisticsHelper trendingListHelper;

	public TrendingDataBarCalculator() {
		this.trendingListHelper = new TrendingListStatisticsHelper();
		for (int i = 0; i < 4; i++) {
			barColorCounts.add(0);
		}

		setAbsoluteCutoffs(avgVotesPerUser);

		log.debug("Maximum Venue counts are Green: {}, Orange: {}, Yellow: {}", barColorMaximumCounts.get(GREENBAR),
				barColorMaximumCounts.get(ORANGEBAR), barColorMaximumCounts.get(YELLOWBAR));
	}

	// Create Function
	private void setAbsoluteCutoffs(Double avgVotesPerUser) {
		this.absoluteRedCutoff = 0.0;
		this.absoluteYellowCutoff = 10.0;
		this.absoluteOrangeCutoff = 15.0;
	}

	public int getTrendingBarColor(Double userVenueValue) {
		if (userVenueValue < -99999) {
			return GRAYBAR;
		}

		int output = 3;
		int absoluteBarColor = getBarColorOnAbsoluteScale(userVenueValue);
		int relativeBarColor = getBarColorOnRelativeScale(userVenueValue);

		if (userVenueValue < absoluteRedCutoff) {
			output = REDBAR;
		} else if (absoluteBarColor <= relativeBarColor) {
			incrementBarCount(absoluteBarColor);
			output = absoluteBarColor;
		} else {
			switch (relativeBarColor) {

			case GREENBAR:
				if (this.barColorCounts.get(GREENBAR) < TrendingDataBarCalculator.barColorMaximumCounts.get(GREENBAR)) {
					incrementBarCount(GREENBAR);
					output = GREENBAR;
				} else {
					for (int i = GREENBAR; i < REDBAR; i++) {
						if (this.barColorCounts.get(i) < TrendingDataBarCalculator.barColorMaximumCounts.get(i)) {
							incrementBarCount(i);
							break;
						}
					}
				}
				break;

			case ORANGEBAR:
				if (this.barColorCounts.get(ORANGEBAR) < TrendingDataBarCalculator.barColorMaximumCounts.get(ORANGEBAR)) {
					incrementBarCount(ORANGEBAR);
					output = ORANGEBAR;
				} else {
					for (int i = ORANGEBAR; i < REDBAR; i++) {
						if (this.barColorCounts.get(i) < TrendingDataBarCalculator.barColorMaximumCounts.get(i)) {
							incrementBarCount(i);
							output = i;
							break;
						}
					}
				}
				break;

			case YELLOWBAR:
				if (this.barColorCounts.get(YELLOWBAR) < TrendingDataBarCalculator.barColorMaximumCounts.get(YELLOWBAR)) {
					incrementBarCount(YELLOWBAR);
					output = YELLOWBAR;
				} else {
					for (int i = YELLOWBAR; i < REDBAR; i++) {
						if (this.barColorCounts.get(i) < TrendingDataBarCalculator.barColorMaximumCounts.get(i)) {
							incrementBarCount(i);
							output = i;
							break;
						}
					}
				}
				break;

			default:
				output = REDBAR;
			}
		}
		return output;
	}

	private void incrementBarCount(int barColor) {
		switch (barColor) {
		case REDBAR:
			this.barColorCounts.set(REDBAR, this.barColorCounts.get(REDBAR) + 1);
			break;
		case YELLOWBAR:
			this.barColorCounts.set(YELLOWBAR, this.barColorCounts.get(YELLOWBAR) + 1);
			break;
		case ORANGEBAR:
			this.barColorCounts.set(ORANGEBAR, this.barColorCounts.get(ORANGEBAR) + 1);
			break;
		case GREENBAR:
			this.barColorCounts.set(GREENBAR, this.barColorCounts.get(GREENBAR) + 1);
			break;
		}
	}

	public static int generateRandomIntInRange(int low, int high) {
		int randomInt = random.nextInt(high - low) + low;
		return randomInt;
	}

	private boolean isRedOnRelativeScale(Double userVenueValue) {
		return (userVenueValue < this.relativeRedCutoff);
	}

	private boolean isYellowOnRelativeScale(Double userVenueValue) {
		return (!isRedOnRelativeScale(userVenueValue) && (userVenueValue < relativeYellowCutoff));
	}

	private boolean isOrangeOnRelativeScale(Double userVenueValue) {
		return (!isYellowOnRelativeScale(userVenueValue) && (userVenueValue < relativeOrangeCutoff));
	}

	/*
	 * private boolean isGreenOnRelativeScale(Double userVenueValue){ return
	 * (userVenueValue > this.relativeOrangeCutoff); }
	 */

	private int getBarColorOnRelativeScale(Double userVenueValue) {
		if (isRedOnRelativeScale(userVenueValue))
			return REDBAR;
		if (isYellowOnRelativeScale(userVenueValue))
			return YELLOWBAR;
		if (isOrangeOnRelativeScale(userVenueValue))
			return ORANGEBAR;
		return GREENBAR;
	}

	private boolean isRedOnAbsoluteScale(Double userVenueValue) {
		return (userVenueValue < this.absoluteRedCutoff);
	}

	private boolean isYellowOnAbsoluteScale(Double userVenueValue) {
		return (!isRedOnAbsoluteScale(userVenueValue) && (userVenueValue < absoluteYellowCutoff));
	}

	private boolean isOrangeOnAbsoluteScale(Double userVenueValue) {
		return (!isYellowOnAbsoluteScale(userVenueValue) && (userVenueValue < absoluteOrangeCutoff));
	}

	/*
	 * private boolean isGreenOnAbsoluteScale(Double userVenueValue){ return
	 * (userVenueValue > this.absoluteOrangeCutoff); }
	 */

	private int getBarColorOnAbsoluteScale(Double userVenueValue) {
		if (isRedOnAbsoluteScale(userVenueValue))
			return REDBAR;
		if (isYellowOnAbsoluteScale(userVenueValue))
			return YELLOWBAR;
		if (isOrangeOnAbsoluteScale(userVenueValue))
			return ORANGEBAR;
		return GREENBAR;
	}

	public void updateStatistics(Double currentDataValue) {
		this.trendingListHelper.updateStatistics(currentDataValue);
	}

	public void updateTrendingBarCutoffs() {
		Double mean = getMean();
		Double stDev = getStDev();
		this.mean = mean;
		this.stDev = stDev;
		relativeRedCutoff = mean - redStDevMultiple * stDev;
		relativeYellowCutoff = mean;
		relativeOrangeCutoff = mean + orangeStDevMultiple * stDev;
		areTrendingBarCutoffSet = true;
		log.info("Trending data bar calculator statistics are as follows Mean: {}, StDev: {}, RelativeRedCutoff: {},"
				+ " RelativeYellowCutoff: {}, RelativeOrangeCutoff: {}, AbsoluteRedCutoff: {},"
				+ " AbsoluteYellowCutoff: {}, AbsoluteOrangeCutoff: {}", this.mean, this.stDev, relativeRedCutoff,
				relativeYellowCutoff, relativeOrangeCutoff, absoluteRedCutoff,
				absoluteYellowCutoff, absoluteOrangeCutoff);
	}

	public Double getMean() {
		return trendingListHelper.getMean();
	}

	public Double getStDev() {
		return trendingListHelper.getStDev();
	}

	public void clearData() {
		trendingListHelper.clearData();
	}

	public class TrendingListStatisticsHelper {
		private Double mK = 0.0;
		private Double qK = 0.0;
		private Double sumOfData = 0.0;
		private int count = 1;

		public TrendingListStatisticsHelper() {
			super();
		}

		public void updateStatistics(Double currentDataValue) {
			sumOfData += currentDataValue;
			if (count == 1) {
				mK = currentDataValue;
			} else {
				Double oldMk = mK;
				mK += (currentDataValue - mK) / count;
				qK += ((count - 1) * Math.pow(currentDataValue - oldMk, 2)) / count;
			}
			count++;
		}

		public Double getMean() {
			return sumOfData / (count - 1);
		}

		public Double getStDev() {
			return Math.sqrt(qK / (count - 2));
		}

		public void clearData() {
			mK = 0.0;
			qK = 0.0;
			sumOfData = 0.0;
			count = 1;
		}

	}

}
