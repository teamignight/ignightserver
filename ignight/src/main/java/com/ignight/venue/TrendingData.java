package com.ignight.venue;

public class TrendingData implements Comparable<TrendingData> {
	public long venueId;
	public String venueName;
	public double userVenueValue;
	public double overallVenueScore;
	public int userBarColor;
	public int userInput;
	public double longitude;
	public double latitude;

	public TrendingData(long venueId, double userVenueValue, int userInput, String venueName, double lon, double lat) {
		this.venueId = venueId;
		this.userVenueValue = userVenueValue;
		this.userInput = userInput;
		this.venueName = venueName;
		this.longitude = lon;
		this.latitude = lat;
	}

	@Override
	public int hashCode() {
		return (int) (venueId);
	}

	@Override
	public boolean equals(Object o) {
		TrendingData v = (TrendingData) o;

		if (venueId != v.venueId)
			return false;

		return true;
	}

	@Override
	public int compareTo(TrendingData o) {
		if (this.userVenueValue < o.userVenueValue) {
			return -1;
		} else if (this.userVenueValue > o.userVenueValue) {
			return 1;
		} else {
			if (this.userInput < o.userInput) {
				return -1;
			} else if (this.userInput > o.userInput) {
				return 1;
			} else {
				if (this.overallVenueScore < o.overallVenueScore) {
					return -1;
				} else if (this.overallVenueScore > o.overallVenueScore) {
					return 1;
				}
				return 0;
			}
		}
	}

	public TrendingData incrementUserVenueValue() {
		this.userVenueValue = this.userVenueValue + 1.0;
		return this;
	}

	public TrendingData decrementUserVenueValue() {
		this.userVenueValue = this.userVenueValue - 1.0;
		return this;
	}
}
