package com.ignight.venue;

import java.util.Map;

public class VenueCrowdStats {
	public Long venueId;
	public long ageBucketMode = -1;
	public long musicTypeMode = -1;
	public long atmosphereTypeMode = -1;
	public long avgSpendingLimit = -1;

	public VenueCrowdStats(Long venueId, long ageBucketMode, long musicTypeMode, long atmosphereTypeMode,
			Integer avgSpendingLimit, Map<Long, String> trendingGroups) {
		this.venueId = venueId;
		this.ageBucketMode = ageBucketMode;
		this.musicTypeMode = musicTypeMode;
		this.atmosphereTypeMode = atmosphereTypeMode;
		this.avgSpendingLimit = avgSpendingLimit;
	}

	public VenueCrowdStats(Long venueId) {
		this.venueId = venueId;
	}

	@Override
	public boolean equals(Object o) {
		VenueCrowdStats v = (VenueCrowdStats) o;
		if (!venueId.equals(v.venueId))
			return false;
		if (ageBucketMode != (v.ageBucketMode))
			return false;
		if (musicTypeMode != v.musicTypeMode)
			return false;
		if (atmosphereTypeMode != v.atmosphereTypeMode)
			return false;
		if (avgSpendingLimit != v.avgSpendingLimit)
			return false;
		return true;
	}
}
