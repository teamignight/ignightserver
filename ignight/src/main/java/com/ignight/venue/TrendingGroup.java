package com.ignight.venue;

public class TrendingGroup implements Comparable {
	public Long groupId;
	public Long count;

	public TrendingGroup(Long groupId) {
		this.groupId = groupId;
		this.count = 0l;
	}

	@Override
	public int compareTo(Object o) {
		TrendingGroup other = (TrendingGroup) o;
		if (count == null && other.count == null) {
			return 0;
		}

		if (count == null) {
			return -1;
		}

		if (other.count == null) {
			return 1;
		}

		return count.compareTo(other.count);
	}
}
