package com.ignight;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.RollingFileAppender;

public class MyRollingFileAppender extends RollingFileAppender {
	@Override
	public synchronized void setFile(String fileName, boolean append, boolean bufferedIO, int bufferSize) throws IOException {
		//Your logic goes here
		File logFile = new File(fileName);
		if (logFile.exists()) {
			logFile.mkdirs();
			logFile.createNewFile();
		}
		super.setFile(fileName, append, bufferedIO, bufferSize);
	}
}
