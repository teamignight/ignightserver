package com.ignight.imagehandling;

import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ImagingOpException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import mediautil.gen.Log;
import mediautil.image.jpeg.AbstractImageInfo;
import mediautil.image.jpeg.Entry;
import mediautil.image.jpeg.Exif;
import mediautil.image.jpeg.LLJTran;

import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Rotation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.ignight.dna.elements.City;
import com.ignight.group.Group;
import com.ignight.servlets.v2.ApiException;
import com.ignight.user.User;
import com.ignight.venue.Venue;
import com.util.IgnightProperties;

public class ImageHandler {
	private static final Logger log = LoggerFactory.getLogger(ImageHandler.class);

	public static enum ImgType {
		NONE, IMG_PROFILE, IMG_CHAT, IMG_VENUE_BUZZ, IMG_GROUP_BUZZ, IMG_GOOGLE_PLACES
	}

	public static final int PROFILE_IMG_MAX_DIMENSION = 240;
	public static final int CHAT_IMG_MAX_DIMENSION = 240;
	public static final int BUZZ_IMG_MAX_DIMENSION = 650;

	private static final AWSCredentials s3Credentials = new ProfileCredentialsProvider().getCredentials();
	private static final AmazonS3Client s3Client = new AmazonS3Client(s3Credentials);

	private static final String s3AppBucket = IgnightProperties.getInstance().getS3AppBucket();
	private static String distributionDomain = IgnightProperties.getInstance().getCloudFrontDistributionDomain();

	public static String generateCloudFrontUrl(String s3ObjectKey) {
		String url = "https://" + distributionDomain + "/" + s3ObjectKey;
		return url;
	}

	public static String getS3Bucket() {
		return s3AppBucket;
	}

	private static String getTemporaryFileKey(Long userId) {
		return IgnightProperties.getInstance().getLocalTmpDirectory() + userId + "_" + System.currentTimeMillis() + ".jpg";
	}

	private static File storeFile(Long userId, InputStream uploadStream) throws IOException {
		File file = new File(getTemporaryFileKey(userId));
		BufferedImage uploadedImage = ImageIO.read(uploadStream);
		ImageIO.write(uploadedImage, "JPG", file);
		return file;
	}

	private static void rotateImage(File file) throws IllegalArgumentException, ImagingOpException, IOException {
		InputStream is = new FileInputStream(file);
		Rotation rotation = ImageHandler.getOperationToCorrectOrientation(is);
		if (rotation != null) {
			BufferedImage bf = Scalr.rotate(ImageIO.read(file), rotation, (BufferedImageOp[]) null);
			ImageIO.write(bf, "JPG", file);
			bf.flush();
		}
		is.close();
	}

	private static String getS3Key(ImgType imgType, Long mainId, Long userId) {
		String directory = null;
		String imageName = null;
		switch (imgType) {
		case IMG_GROUP_BUZZ:
			directory = IgnightProperties.getInstance().getGroupBuzzPictureDirectory();
			directory += "/";
			directory += City.getCity(userId);
			directory += "/";
			imageName = mainId + "_" + userId + "_" + System.currentTimeMillis();
			break;
		case IMG_VENUE_BUZZ:
			directory = IgnightProperties.getInstance().getVenueBuzzPictureDirectory();
			directory += "/";
			directory += City.getCity(userId);
			directory += "/";
			imageName = mainId + "_" + userId + "_" + System.currentTimeMillis();
			break;
		case IMG_CHAT:
			directory = IgnightProperties.getInstance().getProfilePictureDirectory();
			directory += "/";
			imageName = userId + "_chat_" + System.currentTimeMillis();
			break;
		case IMG_PROFILE:
			directory = IgnightProperties.getInstance().getProfilePictureDirectory();
			directory += "/";
			imageName = userId + "_profile_" + System.currentTimeMillis();
			break;
		case IMG_GOOGLE_PLACES:
			directory = IgnightProperties.getInstance().getGooglePlacesPictureDirectory();
			directory += "/";
			imageName = mainId + "_places_" + userId;
			break;
		}

		return directory + imageName;
	}

	private static String getGooglePlaceImageS3Key(Long mainId, Long imageNo) {
		String directory = null;
		String imageName = null;
		directory = IgnightProperties.getInstance().getGooglePlacesPictureDirectory();
		directory += "/";
		imageName = mainId + "_places_" + imageNo;		
		return directory + imageName;
	}
	
	public static String generateGooglePlacesImageUrl(Long mainId, Long imageNo) {
		String s3ObjectKey = getGooglePlaceImageS3Key(mainId, imageNo);
		return generateCloudFrontUrl(s3ObjectKey);
	}
	
	public static String uploadVenueBuzzImage(Venue venue, User user, InputStream uploadStream) throws Exception {
		File file = storeFile(user.id, uploadStream);
		rotateImage(file);

		//		BufferedImage venueImage = Scalr.resize(ImageIO.read(file), Scalr.Mode.FIT_TO_WIDTH, BUZZ_IMG_MAX_DIMENSION);
		//		ImageIO.write(venueImage, "JPG", file);

		String key = getS3Key(ImgType.IMG_VENUE_BUZZ, venue.id, user.id);
		return uploadSingleFileToS3(key, file);
	}

	public static String uploadGroupBuzzImage(Group group, User user, InputStream uploadStream) throws Exception {
		File file = storeFile(user.id, uploadStream);
		rotateImage(file);

		//		BufferedImage venueImage = Scalr.resize(ImageIO.read(file), Scalr.Mode.FIT_TO_WIDTH, BUZZ_IMG_MAX_DIMENSION);
		//		ImageIO.write(venueImage, "JPG", file);

		String key = getS3Key(ImgType.IMG_GROUP_BUZZ, group.id, user.id);
		return uploadSingleFileToS3(key, file);
	}
	
	public static String uploadGooglePlaceImage(Long venueId, Integer pictureNumber, InputStream uploadStream) throws Exception {
		File file = storeFile(venueId, uploadStream);
		
		String key = getGooglePlaceImageS3Key(venueId, Long.valueOf(pictureNumber));
		return uploadSingleFileToS3(key, file);
	}

	public static Map<ImgType, String> uploadProfileImageI(Long userId, InputStream uploadStream) throws Exception {
		File file = storeFile(userId, uploadStream);
		rotateImage(file);

		BufferedImage profile = Scalr.resize(ImageIO.read(file), Scalr.Mode.FIT_TO_WIDTH, PROFILE_IMG_MAX_DIMENSION);
		ImageIO.write(profile, "JPG", file);
		profile.flush();

		String key = getS3Key(ImgType.IMG_PROFILE, null, userId);
		String profileUrl = uploadSingleFileToS3(key, file);

		BufferedImage chat = Scalr.resize(ImageIO.read(file), Scalr.Mode.FIT_TO_WIDTH, CHAT_IMG_MAX_DIMENSION);
		File chatFile = new File(IgnightProperties.getInstance().getLocalTmpDirectory() + userId + "_chat.jpg");
		ImageIO.write(chat, "JPG", chatFile);
		chat.flush();

		key = getS3Key(ImgType.IMG_CHAT, null, userId);
		String chatUrl = uploadSingleFileToS3(key, chatFile);

		Map<ImgType, String> imageUrls = new HashMap<ImageHandler.ImgType, String>();
		imageUrls.put(ImgType.IMG_PROFILE, profileUrl);
		imageUrls.put(ImgType.IMG_CHAT, chatUrl);
		return imageUrls;
	}

	public static String uploadSingleFileToS3(String key, File file) throws ApiException {
		if (!s3Client.doesBucketExist(s3AppBucket)) {
			s3Client.createBucket(s3AppBucket);
		}

		try {
			PutObjectRequest request = new PutObjectRequest(s3AppBucket, key, file);
			request.setCannedAcl(CannedAccessControlList.PublicRead);
			s3Client.putObject(request);
		} catch (Exception e) {
			throw new ApiException("Failed to upload image.");
		}

		String url = generateCloudFrontUrl(key);
		return url;
	}

	public static Rotation getOperationToCorrectOrientation(InputStream is) {
		try {
			Log.debugLevel = Log.LEVEL_DEBUG;

			LLJTran ll = new LLJTran(is);
			ll.read(LLJTran.READ_INFO, true);
			AbstractImageInfo<?> imageInfo = ll.getImageInfo();

			int orientation = 1;
			int operation = 0;
			if (imageInfo instanceof mediautil.image.jpeg.Exif) {
				Exif exif = (Exif) imageInfo;

				Entry orientationTag = exif.getTagValue(Exif.ORIENTATION, true);
				if (orientationTag != null) {
					orientation = (Integer) orientationTag.getValue(0);
				}
				operation = Exif.opToCorrectOrientation[orientation];
				is.close();
				ll.freeMemory();
			}
			is.close();
			ll.freeMemory();
			if (orientation == 2) {
				return Rotation.FLIP_HORZ;
			} else if (operation == 3) {
				return Rotation.CW_180;
			} else if (operation == 4) {
				return Rotation.FLIP_VERT;
			} else if (operation == 5) {
				return Rotation.FLIP_VERT;
			} else if (operation == 6) {
				return Rotation.CW_90;
			} else if (operation == 7) {
				return Rotation.FLIP_VERT;
			} else if (operation == 8) {
				return Rotation.CW_270;
			} else {
				return null;
			}
		} catch (Exception e) {
			log.error("Failed to get correct orientation", e);
		}
		return null;
	}
}
