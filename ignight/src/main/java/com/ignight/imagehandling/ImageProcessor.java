package com.ignight.imagehandling;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import mediautil.image.jpeg.LLJTran;
import mediautil.image.jpeg.LLJTranException;

import com.mortennobel.imagescaling.AdvancedResizeOp;
import com.mortennobel.imagescaling.ResampleOp;

public class ImageProcessor {

	public static BufferedImage resizeImage(BufferedImage originalImage, int img_width, int img_height) {
		ResampleOp resampleOp = new ResampleOp(img_width, img_height);
		resampleOp.setUnsharpenMask(AdvancedResizeOp.UnsharpenMask.Normal);
		BufferedImage resizedImage = resampleOp.filter(originalImage, null);
		return resizedImage;
	}

	public static BufferedImage rotateAndResizeImage(BufferedImage originalImage, int img_width, int img_height) {
		ResampleOp resampleOp = new ResampleOp(img_width, img_height);
		resampleOp.setUnsharpenMask(AdvancedResizeOp.UnsharpenMask.VerySharp);
		BufferedImage resizedImage = resampleOp.filter(originalImage, null);
		return resizedImage;
	}

	public static File rotate2(File f, Integer rotationOperation) throws LLJTranException, IOException {
		LLJTran llj = new LLJTran(f);
		llj.read(LLJTran.READ_ALL, true);
		llj.transform(rotationOperation, LLJTran.OPT_DEFAULTS | LLJTran.OPT_XFORM_ORIENTATION);

		String originalFilePath = f.getAbsolutePath();
		String tempFilePath = originalFilePath.replace(".jpeg", "") + "temp.jpg";
		f.delete();
		OutputStream output = new BufferedOutputStream(new FileOutputStream(tempFilePath));

		llj.save(output, LLJTran.OPT_WRITE_ALL);
		output.close();
		llj.freeMemory();
		return new File(tempFilePath);
	}

}
