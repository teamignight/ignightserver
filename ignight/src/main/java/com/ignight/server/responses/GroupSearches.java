package com.ignight.server.responses;

public class GroupSearches {
	public String name;
	public Long id;
	public Long members;
	public boolean isMember;
}
