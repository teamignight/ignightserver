package com.ignight.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ignight.user.RedisKeys;

import databases.Databases;
import databases.JedisConnection;
import databases.Resources;

public class DataSource {
	private static Logger log = LoggerFactory.getLogger(DataSource.class);
	public static final JedisConnection jedis = JedisConnection.getInstance();

	public DataSource() {
		// TODO Auto-generated constructor stub
	}

	public static void loadAndSaveArchiveStatusToRedis() throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "Select city_id, archive_in_progress from city_archive_status";

		try {
			conn = Databases.dataSourceArchiving.getConnection();
			ps = conn.prepareStatement(sql);

			rs = ps.executeQuery();
			while (rs.next()) {
				int cityId;
				Integer output = 0;
				cityId = rs.getInt(1);
				output = rs.getInt(2);
				String key = RedisKeys.getArchiveInProgressKey((long) cityId);
				String obj = output.toString();

				log.warn("Adding archive in progress to cache for cityId: {}", cityId);
				jedis.set(key, obj);
			}

		} finally {
			Resources.close(conn, ps, rs);
		}
	}

	public static boolean isArchiveInProgress(Long cityId) {
		String key = RedisKeys.getArchiveInProgressKey(cityId);
		String status = jedis.get(key);
		if (status != null) {
			return Integer.valueOf(status) == 1;
		}
		return false;
	}

	public static void setArchiveInProgressStatus(Long cityId, Integer status) {
		String key = RedisKeys.getArchiveInProgressKey(cityId);
		jedis.set(key, status);
	}

}
