package com.ignight.dna;

import com.ignight.user.UserDna;

public class CalcVenueDna {
	public Long venueId;
	public VenueDna upvotedDna;
	public VenueDna downvotedDna;

	private Double baseUserVenueScore = null;

	public CalcVenueDna() {
	}

	public CalcVenueDna(VenueDna upvotedDna, VenueDna downvotedDna) {
		this.venueId = upvotedDna.venueId;
		this.upvotedDna = upvotedDna;
		this.downvotedDna = downvotedDna;
	}

	@Override
	public String toString() {
		return "CalcVenueDna{" +
				"venueId=" + venueId +
				", upvotedDna=" + upvotedDna +
				", downvotedDna=" + downvotedDna +
				'}';
	}

	public void setBaseUserVenueScore(double val) {
		this.baseUserVenueScore = val;
	}

	public Double getBaseUserVenueScore() {
		return this.baseUserVenueScore;
	}

	public void removeUserDnaFromUpvote(UserDna dna, long activitySize) {
		upvotedDna.counter -= (1000 / activitySize);
		for (DnaElementCount atm : upvotedDna.atmospherePreferences) {
			int pos = 0;
			for (Long userPrefId : dna.atmosphereIds) {
				if (atm.dnaId == userPrefId) {
					atm.count -= (getPosScore(pos) / activitySize);
				}
				pos++;
			}
		}
		for (DnaElementCount mus : upvotedDna.musicPreferences) {
			int pos = 0;
			for (Long userPrefId : dna.atmosphereIds) {
				if (mus.dnaId == userPrefId) {
					mus.count -= (getPosScore(pos) / activitySize);
				}
				pos++;
			}
		}
	}

	public void addUserDnaFromDownvote(UserDna dna, long activitySize) {
		downvotedDna.counter += (1000 / activitySize);
		for (DnaElementCount atm : downvotedDna.atmospherePreferences) {
			int pos = 0;
			for (Long userPrefId : dna.atmosphereIds) {
				if (atm.dnaId == userPrefId) {
					atm.count += (getPosScore(pos) / activitySize);
				}
				pos++;
			}
		}
		for (DnaElementCount mus : downvotedDna.musicPreferences) {
			int pos = 0;
			for (Long userPrefId : dna.atmosphereIds) {
				if (mus.dnaId == userPrefId) {
					mus.count += (getPosScore(pos) / activitySize);
				}
				pos++;
			}
		}
	}

	private int getPosScore(int pos) {
		if (pos == 0) {
			return 3000;
		} else if (pos == 1) {
			return 2000;
		} else if (pos == 2) {
			return 1000;
		}
		return 0;
	}
}
