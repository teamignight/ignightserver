package com.ignight.dna.elements;

import java.util.SortedMap;
import java.util.TreeMap;

public enum Music {
	CLASSIC_ROCK("classic rock", 0L),
	CLASSICAL("classical", 1L),
	COUNTRY("country", 2L),
	ELECTRONIC("electronic", 3L),
	FOLK("folk", 4L),
	INDIE_ROCK("indie rock", 5L),
	JAZZ("jazz", 6L),
	LATIN("latin", 7L),
	OLDIES("oldies", 8L),
	POP("pop", 9L),
	RNB("rnb", 10L),
	RAP("rap", 11L),
	REGGAE("reggae", 12L),
	ROCK("rock", 13L),
	TOP40("top40", 14L),
	NONE("none", -1L);

	private final String name;
	private final Long id;

	Music(String name, Long id) {
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public Long getId() {
		return this.id;
	}

	public static SortedMap<String, Long> getMusicToId() {
		SortedMap<String, Long> map = new TreeMap<String, Long>();
		for (Music m : Music.values())
			map.put(m.name.toLowerCase(), m.id);
		return map;
	}
}
