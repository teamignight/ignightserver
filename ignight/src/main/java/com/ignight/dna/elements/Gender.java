package com.ignight.dna.elements;

import java.util.HashMap;
import java.util.Map;

public enum Gender {
	MALE(0), FEMALE(1);

	private long id;

	private Gender(long id) {
		this.id = id;
	}

	private static Map<Long, Gender> idToGender;

	public static Gender getGender(long id) {
		if (idToGender == null)
			initMapping();
		return idToGender.get(id);
	}

	private static void initMapping() {
		idToGender = new HashMap<Long, Gender>();
		for (Gender g : Gender.values())
			idToGender.put(g.getId(), g);
	}

	public long getId() {
		return id;
	}

	public static Map<String, Long> getGenderToId() {
		Map<String, Long> map = new HashMap<String, Long>();
		for (Gender g : Gender.values())
			map.put(g.name().toLowerCase(), g.id);
		return map;
	}
}
