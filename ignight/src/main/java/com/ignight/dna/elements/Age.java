package com.ignight.dna.elements;

import java.util.ArrayList;

public enum Age {
	_18_20(0), _21_25(1), _26_30(2), _31_35(3), _36_40(4), _41_Plus(5);

	private long id;

	Age(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public static ArrayList<Long> getAgeRange() {
		ArrayList<Long> range = new ArrayList<Long>();
		for (Age s : Age.values())
			range.add(s.id);
		return range;
	}
}