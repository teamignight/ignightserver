package com.ignight.dna.elements;

import java.util.ArrayList;

public enum SpendingLimit {
	BROKE(0), GETTING_BY(1), DOING_WELL(2), LIVING_THE_DREAM(3);

	private long id;

	SpendingLimit(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public static ArrayList<Long> getSpendingLimitRange() {
		ArrayList<Long> range = new ArrayList<Long>();
		for (SpendingLimit s : SpendingLimit.values())
			range.add(s.id);
		return range;
	}
}
