package com.ignight.dna.elements;

import java.util.SortedMap;
import java.util.TreeMap;

public enum Atmosphere {
	COCKTAIL_LOUNGE("cocktail lounge", 0L),
	DIVE_BAR("dive bar", 1L),
	GAY_BAR("gay bar", 2L),
	HIPSTER_LOUNGE("hipster lounge", 3L),
	LIVE_MUSIC("live music", 4L),
	SPORTS_BAR("sports bar", 5L),
	TECHNO_CLUB("techno club", 6L),
	VIP_CLUB("vip club", 7L),
	NONE("none", -1L);

	private final String name;
	private final Long id;

	Atmosphere(String name, Long id) {
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public Long getId() {
		return id;
	}

	public static SortedMap<String, Long> getAtmosphereToId() {
		SortedMap<String, Long> map = new TreeMap<String, Long>();
		for (Atmosphere a : Atmosphere.values())
			map.put(a.name.toLowerCase(), a.id);
		return map;
	}
}
