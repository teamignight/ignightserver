package com.ignight.dna.elements;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public enum City {
	CHICAGO("chicago", 0L);

	private String name;
	private long id;

	private static Map<Long, City> idToCity;

	City(String name, Long id) {
		this.name = name;
		this.id = id;
	}

	public static City getCity(long id) {
		if (idToCity == null)
			initMapping();

		return idToCity.get(id);
	}

	private static void initMapping() {
		idToCity = new HashMap<Long, City>();
		for (City c : values()) {
			idToCity.put(c.id, c);
		}
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public static SortedMap<String, Long> getCityNameToId() {
		SortedMap<String, Long> map = new TreeMap<String, Long>();
		for (City c : City.values())
			map.put(c.name.toLowerCase(), c.id);
		return map;
	}
}
