package com.ignight.dna;

import java.util.ArrayList;
import java.util.List;

public class VenueDna {
	public Long venueId;
	public Long counter = 0l;
	public List<DnaElementCount> musicPreferences = new ArrayList<DnaElementCount>();
	public List<DnaElementCount> atmospherePreferences = new ArrayList<DnaElementCount>();
	public long ageId = -1;
	public long spendingLimit = -1;

	private Long totalMusicCount = null;
	private Long totalAtmosphereCount = null;

	public VenueDna() {
	}

	public VenueDna(long venueId) {
		this.venueId = venueId;
	}

	public Long getTotalMusicCount() {
		if (totalMusicCount == null) {
			totalMusicCount = 0l;
			for (DnaElementCount elem : musicPreferences) {
				totalMusicCount += elem.count;
			}
		}
		return totalMusicCount;
	}

	public Long getTotalAtmosphereCount() {
		if (totalAtmosphereCount == null) {
			totalAtmosphereCount = 0l;
			for (DnaElementCount elem : atmospherePreferences) {
				totalAtmosphereCount += elem.count;
			}
		}
		return totalAtmosphereCount;
	}

	@Override
	public String toString() {
		return "VenueDna{" +
				"venueId=" + venueId +
				", counter=" + counter +
				", musicPreferences=" + musicPreferences +
				", atmospherePreferences=" + atmospherePreferences +
				", ageId=" + ageId +
				", spendingLimit=" + spendingLimit +
				'}';
	}
}
