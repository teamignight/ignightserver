package com.ignight.dna;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ignight.core.CoreData;
import com.ignight.dna.TrendingBarCalculator.BarColor;
import com.ignight.venue.Venue;
import com.ignight.venue.VenueDataSource;

public class TrendingVenue implements Comparable<TrendingVenue> {
	@Override
	public String toString() {
		return "TrendingVenue [id=" + id + ", barColorId=" + barColorId
				+ ", dnaValue=" + dnaValue + ", placeImage=" + placeImage + "]";
	}

	private static Logger log = LoggerFactory.getLogger(TrendingVenue.class);

	public Long id;
	public Long barColorId = 4l;
	public Long userActivity = 0l;
	public VenueInfo venueInfo;
	private Double dnaValue;
	public Double unweightedDnaValue = null;
	public String placeImage = null; 

	public TrendingVenue(Double dnaValue, Double unweightedValue) {
		this.dnaValue = dnaValue;
		this.unweightedDnaValue = unweightedValue;
	}

	public TrendingVenue(Long id, Double dnaValue, Double unweightedDnaValue, Boolean dna) {
		this.id = id;
		if (dna) {
			this.dnaValue = dnaValue;
			this.unweightedDnaValue = unweightedDnaValue;
		} else {
			this.barColorId = dnaValue.longValue();
			this.dnaValue = dnaValue;
		}
		this.venueInfo = new VenueInfo(id);
		this.placeImage = VenueDataSource.getGooglePlaceImageForTrendingList(id);
	}

	public TrendingVenue(Long id, Double dnaValue, Boolean dna) {
		this.id = id;
		if (dna) {
			this.dnaValue = dnaValue;
		} else {
			this.barColorId = dnaValue.longValue();
			this.dnaValue = dnaValue;
		}
		this.venueInfo = new VenueInfo(id);
		this.placeImage = VenueDataSource.getGooglePlaceImageForTrendingList(id); 
	}

	public static class VenueInfo {
		public String name;
		public Double latitude;
		public Double longitude;

		public VenueInfo(Long venueId) {
			Venue v = CoreData.getVenue(venueId);
			if (v == null) {
				log.error("No venue id : {} in CoreData", venueId);
				return;
			}
			this.name = v.name;
			this.latitude = v.latitude;
			this.longitude = v.longitude;
		}
	}

	public Double getDnaValue() {
		return dnaValue;
	}

	public void setUnweightedDnaValue(double val) {
		this.unweightedDnaValue = val;
	}

	public TrendingVenue setDnaValue(double dnaValue) {
		this.dnaValue = dnaValue;
		return this;
	}

	public BarColor getBarColor() {
		return BarColor.values()[barColorId.intValue()];
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrendingVenue other = (TrendingVenue) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public int compareTo(TrendingVenue o) {
		int barComp = o.barColorId.compareTo(this.barColorId);
		if (barComp == 0) {
			int dnaValComp = o.getDnaValue().compareTo(this.getDnaValue());
			if (dnaValComp == 0) {
				if (venueInfo != null) {
					return venueInfo.name.compareTo(o.venueInfo.name);
				}
			}
			return dnaValComp;
		}
		return -1 * barComp;
	}
}
