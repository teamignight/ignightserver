package com.ignight.dna;

import java.util.ArrayList;
import java.util.List;

public class DNA implements Cloneable {
	public final long userId;
	public Long ageId;
	public Long spendingLimit;
	public List<Long> musicPrefs = new ArrayList<Long>();
	public List<Long> atmospherePrefs = new ArrayList<Long>();

	public DNA(DNA dna) {
		this.userId = dna.userId;
		this.spendingLimit = dna.spendingLimit;
		this.musicPrefs = new ArrayList<Long>(dna.musicPrefs);
		this.atmospherePrefs = new ArrayList<Long>(dna.atmospherePrefs);
	}

	public DNA(long userId) {
		this.userId = userId;
	}

	public DNA(long userId, List<Long> musicPrefs, List<Long> atmospherePrefs, Long spendingLimit) {
		this.userId = userId;
		this.musicPrefs = musicPrefs;
		this.atmospherePrefs = atmospherePrefs;
		this.spendingLimit = spendingLimit;
	}

	// Checks to see that arrays are non-empty
	public boolean isDnaSet() {
		return (musicPrefs != null && atmospherePrefs != null && spendingLimit != null && musicPrefs.size() > 0 && atmospherePrefs
				.size() > 0);
	}

	public long getUserId() {
		return userId;
	}

	public void setSpendingLimit(Long spendingLimit) {
		this.spendingLimit = spendingLimit;
	}

	public Long getSpendingLimit() {
		return spendingLimit;
	}

	public void setMusicPrefs(List<Long> prefs) {
		this.musicPrefs = prefs;
	}

	public List<Long> getMusicPrefs() {
		return musicPrefs;
	}

	public List<Long> getTopMusicPrefs() {
		int max = Math.min(musicPrefs.size(), 3);
		return musicPrefs.subList(0, max);
	}

	public Long getMusicPrefAtIndex(int index) {
		if (index > this.musicPrefs.size()) {
			return -1l;
		}
		return this.musicPrefs.get(index);
	}

	public void setAtmospherePrefs(List<Long> prefs) {
		this.atmospherePrefs = prefs;
	}

	public List<Long> getAtmospherePrefs() {
		return atmospherePrefs;
	}

	public List<Long> getTopAtmospheres() {
		int max = Math.min(atmospherePrefs.size(), 3);
		return atmospherePrefs.subList(0, max);
	}

	public Long getAtmospherePrefAtIndex(int index) {
		if (index > this.atmospherePrefs.size()) {
			return -1l;
		}
		return this.atmospherePrefs.get(index);
	}

	@Override
	public boolean equals(Object obj) {
		DNA otherDna = (DNA) obj;
		return this.userId == otherDna.userId;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("userId: ").append(userId);
		sb.append(", musicPrefs: ").append(musicPrefs);
		sb.append(", atmospherePrefs: ").append(atmospherePrefs);
		sb.append(", spendingLimit: ").append(spendingLimit);
		return sb.toString();
	}
}
