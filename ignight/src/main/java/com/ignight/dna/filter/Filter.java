package com.ignight.dna.filter;

import com.util.Coordinate;

public class Filter {
	public FilterType type;
	public String searchString;
	public Coordinate userCoordinate;
	public Double radius;
	public Long musicId;

	public static enum FilterType {
		NONE, UPVOTE, MUSIC, SEARCH, RADIUS
	}

	public Filter(String searchString) {
		this.type = FilterType.SEARCH;
		this.searchString = searchString;
	}

	public Filter(Long musicId) {
		this.type = FilterType.MUSIC;
		this.musicId = musicId;
	}

	public Filter(Coordinate userCoordinate, Double radius) {
		this.type = FilterType.RADIUS;
		this.userCoordinate = userCoordinate;
		this.radius = radius;
	}

	public Filter() {
		this.type = FilterType.NONE;
	}
}
