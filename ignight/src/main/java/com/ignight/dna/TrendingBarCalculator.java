package com.ignight.dna;

import java.util.ArrayList;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.util.IgnightProperties;

public class TrendingBarCalculator {
	private static final Logger log = LoggerFactory.getLogger(TrendingBarCalculator.class);

	private double mean = 0;
	private double stdDev = 0;

	private double stdDevRedCutoff;
	private double stdDevOrangeCutoff;
	private double stdDevYellowCutoff;

	private long colorLimits = IgnightProperties.getInstance().getColorLimits();
	private double absoluteGreenFloor = IgnightProperties.getInstance().getGreenFloor();
	private double absoluteRedCeiling = IgnightProperties.getInstance().getRedCeiling();
	private double unweightedDnaRedCeiling = IgnightProperties.getInstance().getUnweightedDnaRedCeiling();

	private ArrayList<TrendingVenue> trendingVenues;

	public TrendingBarCalculator() {
		trendingVenues = new ArrayList<TrendingVenue>();
	}

	public TrendingBarCalculator(ArrayList<TrendingVenue> trending) {
		this.trendingVenues = trending;
	}

	public void clear() {
		trendingVenues.clear();
	}

	public void setCoefficeints(long colorLimits, double greenFloor, double redCeiling) {
		this.colorLimits = colorLimits;
		this.absoluteGreenFloor = greenFloor;
		this.absoluteRedCeiling = redCeiling;
	}

	public void applyBarColors() {
		log.debug("TrendingVenue Before : {}", trendingVenues);
		calculateMean();
		calculateStdDev();
		setStdDevCutoffs();
		applyRules();
		log.debug("Calculating Bar Colors: Mean = {}, StdDev = {}, Cutoffs = [ {}, {}, {} ]", mean, stdDev, stdDevRedCutoff, stdDevOrangeCutoff, stdDevYellowCutoff);
	}

	private void applyRules() {
		//Apply the stdDev and below floor rules
		for (TrendingVenue tv : trendingVenues) {
			Double dnaValue = tv.getDnaValue();
			if (tv.unweightedDnaValue != null && tv.unweightedDnaValue <= unweightedDnaRedCeiling) {
				tv.barColorId = (long) BarColor.RED.ordinal();
			} else if (dnaValue <= stdDevRedCutoff) {
				tv.barColorId = (long) BarColor.RED.ordinal();
			} else if (dnaValue <= stdDevOrangeCutoff) {
				tv.barColorId = (long) BarColor.ORANGE.ordinal();
			} else if (dnaValue <= stdDevYellowCutoff) {
				tv.barColorId = (long) BarColor.YELLOW.ordinal();
			} else {
				tv.barColorId = (long) BarColor.GREEN.ordinal();
			}
		}
		log.debug("TrendingVenue After StdDev Rule : {}", trendingVenues);

		for (TrendingVenue tv : trendingVenues) {
			double dnaValue = tv.getDnaValue();
			if (tv.unweightedDnaValue != null && tv.unweightedDnaValue <= unweightedDnaRedCeiling) {
				tv.barColorId = (long) BarColor.RED.ordinal();
			} else if (dnaValue <= absoluteRedCeiling) {
				tv.barColorId = (long) BarColor.RED.ordinal();
			} else if (dnaValue >= absoluteGreenFloor) {
				tv.barColorId = (long) BarColor.GREEN.ordinal();
			}
		}
		log.debug("TrendingVenue After Absolute Rule : {}", trendingVenues);

		//Sort base on bar colors now
		Collections.sort(trendingVenues);

		//Apply the color limit rule
		if (trendingVenues.size() <= 1) {
			return;
		}

		boolean done = false;
		int i = 0;
		long greenCount = 0;
		TrendingVenue tv = trendingVenues.get(i);
		if (tv.unweightedDnaValue != null && tv.unweightedDnaValue <= unweightedDnaRedCeiling) {
			return;
		}
		while (!done && (tv.getDnaValue() >= absoluteGreenFloor || tv.barColorId == BarColor.GREEN.ordinal())) {
			greenCount++;
			if (greenCount > colorLimits) {
				if (tv.getDnaValue() < absoluteGreenFloor) {
					tv.barColorId = (long) BarColor.YELLOW.ordinal();
					break;
				}
			}
			tv.barColorId = (long) BarColor.GREEN.ordinal();
			i++;
			if (i >= trendingVenues.size()) {
				done = true;
				break;
			}
			tv = trendingVenues.get(i);
			if (tv.unweightedDnaValue != null && tv.unweightedDnaValue <= unweightedDnaRedCeiling) {
				return;
			}
		}

		long yellowCount = 0;
		while (!done && tv.barColorId <= BarColor.YELLOW.ordinal()) {
			yellowCount++;
			if (yellowCount > colorLimits) {
				tv.barColorId = tv.barColorId + 1;
				break;
			}
			tv.barColorId = (long) BarColor.YELLOW.ordinal();
			i++;
			if (i >= trendingVenues.size()) {
				done = true;
				break;
			}
			tv = trendingVenues.get(i);
			if (tv.unweightedDnaValue != null && tv.unweightedDnaValue <= unweightedDnaRedCeiling) {
				return;
			}
		}

		long orangeCount = 0;
		while (!done && tv.barColorId <= BarColor.ORANGE.ordinal()) {
			orangeCount++;
			if (orangeCount > colorLimits) {
				tv.barColorId = tv.barColorId + 1;
				break;
			}
			tv.barColorId = (long) BarColor.ORANGE.ordinal();
			i++;
			if (i >= trendingVenues.size()) {
				done = true;
				break;
			}
			tv = trendingVenues.get(i);
			if (tv.unweightedDnaValue != null && tv.unweightedDnaValue <= unweightedDnaRedCeiling) {
				return;
			}
		}

		while (!done) {
			if (tv.barColorId <= BarColor.RED.ordinal()) {
				tv.barColorId = (long) BarColor.RED.ordinal();
			}
			i++;
			if (i >= trendingVenues.size()) {
				done = true;
				break;
			}
			tv = trendingVenues.get(i);
			if (tv.unweightedDnaValue != null && tv.unweightedDnaValue <= unweightedDnaRedCeiling) {
				return;
			}
		}

		log.debug("TrendingVenue After Color Limit Rule : {}", trendingVenues);
	}

	private void setStdDevCutoffs() {
		stdDevRedCutoff = mean - stdDev;
		stdDevOrangeCutoff = mean;
		stdDevYellowCutoff = mean + stdDev;
	}

	private void calculateMean() {
		int sum = 0;
		double count = trendingVenues.size();
		for (TrendingVenue tv : trendingVenues) {
			sum += (tv.getDnaValue() * 1000);
		}
		if (count > 0) {
			mean = (sum / 1000.0) / count;
		}
	}

	private void calculateStdDev() {
		int size = trendingVenues.size();

		double totalSum = 0;
		for (int i = 0; i < size; i++) {
			TrendingVenue tv = trendingVenues.get(i);
			double val = tv.getDnaValue() - mean;
			totalSum += (val * val);
		}

		/*
		 * Get result value and calculate stdDev
		 */
		double result = totalSum / (size - 1);
		stdDev = Math.sqrt(result);
	}

	public void add(TrendingVenue tv) {
		trendingVenues.add(tv);
	}

	public void remove(TrendingVenue tv) {
		trendingVenues.remove(tv);
	}

	public ArrayList<TrendingVenue> getTrendingVenues() {
		return trendingVenues;
	}

	public double getStdDevRedCutoff() {
		return stdDevRedCutoff;
	}

	public double getStdDevOrangeCutoff() {
		return stdDevOrangeCutoff;
	}

	public double getStdDevYellowCutoff() {
		return stdDevYellowCutoff;
	}

	public double getStdDev() {
		return stdDev;
	}

	public double getMean() {
		return mean;
	}

	public static enum BarColor {
		GREEN, YELLOW, ORANGE, RED, GRAY
	}
}
