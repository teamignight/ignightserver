package com.ignight.dna;

public class DnaElementCount {
	public long dnaId;
	public long count;

	public DnaElementCount() {
	}

	public DnaElementCount(long id, long count) {
		this.dnaId = id;
		this.count = count;
	}

	@Override
	public String toString() {
		return "DnaElementCount{" +
				"dnaId=" + dnaId +
				", count=" + count +
				'}';
	}
}
