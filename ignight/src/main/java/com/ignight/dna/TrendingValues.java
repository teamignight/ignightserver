package com.ignight.dna;

public class TrendingValues {
	public Long ageId = -1l;
	public Long musicId = -1l;
	public Long atmosphereId = -1l;
	public Long spendingId = -1l;

	public TrendingValues(long age, long music, long atm, long spend) {
		this.ageId = age;
		this.musicId = music;
		this.atmosphereId = atm;
		this.spendingId = spend;
	}
}
