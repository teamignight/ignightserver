package com.ignight.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ignight.venue.Venue;
import com.ignight.venue.VenueDataSource;

public class CoreData {
	private static final Logger log = LoggerFactory.getLogger(CoreData.class);
	private static Map<Long, Venue> venues = new HashMap<Long, Venue>();
	public static ArrayList<Long> alphabeticalVenues = new ArrayList<Long>();

	public static void alphabetizeVenues() {
		Venue[] venueArray = new Venue[venues.values().size()];
		venues.values().toArray(venueArray);
		Arrays.sort(venueArray);
		for (int i = 0; i < venueArray.length; i++) {
			Venue v = venueArray[i];
			alphabeticalVenues.add(v.id);
		}
		log.info("Alphabetical Venue Size: {}", alphabeticalVenues.size());
	}

	public static void addVenue(final long id, final Venue venue) {
		if (id >= 0) {
			venues.put(id, venue);
		}
	}

	public static Venue getVenue(final long id) {
		if (id >= 0) {
			Venue venue = venues.get(id);
			if (venue == null) {
				venue = VenueDataSource.loadVenue(id);
				if (venue != null) {
					venues.put(id, venue);
				}
			}
			return venue;
		}
		return null;
	}
}
