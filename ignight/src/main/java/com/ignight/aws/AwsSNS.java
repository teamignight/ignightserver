package com.ignight.aws;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.sns.AmazonSNSClient;

public class AwsSNS {

	public static AmazonSNSClient snsClient;
	
	public AwsSNS() {
		ProfileCredentialsProvider profileCredentialsProvider = new ProfileCredentialsProvider();
		AWSCredentials awsCredentials = profileCredentialsProvider.getCredentials();
		snsClient = new AmazonSNSClient(awsCredentials);
	}
	
}
