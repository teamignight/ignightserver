package com.ignight.aws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.SendMessageBatchRequest;
import com.amazonaws.services.sqs.model.SendMessageBatchRequestEntry;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ignight.aws.messages.QueueMessage;
import com.ignight.aws.messages.QueueMessageType;
import com.ignight.user.RedisKeys;
import com.util.IgnightProperties;

import databases.JedisConnection;

public class AwsMessageSender {
	private static final Logger log = LoggerFactory.getLogger(AwsMessageSender.class);
	private static AwsMessageSender INSTANCE = null;
	private Gson gson = new GsonBuilder().disableHtmlEscaping().create();
	protected LinkedBlockingQueue<QueueMessage> msgQueue = new LinkedBlockingQueue<QueueMessage>();
	private AwsMessageQueueSender msgQueueSender;
	private ExecutorService executorService = Executors.newCachedThreadPool();

	private AwsMessageSender() {
		msgQueueSender = new AwsMessageQueueSender(msgQueue);
		executorService.execute(msgQueueSender);
	}

	public void shutdown() {
		//Make Sure Jedis and Other Resources are not shutdown first
		executorService.shutdown();
	}

	public class AwsMessageQueueSender implements Runnable {
		private long index = 0;
		private AmazonSQSClient client;
		private LinkedBlockingQueue<QueueMessage> msgQueue = new LinkedBlockingQueue<QueueMessage>();

		private Map<String, String> openQueues = new HashMap<String, String>();
		public List<String> queueUrls = new ArrayList<String>();
		private String defaultQueueUrl;

		private JedisConnection jedis = JedisConnection.getInstance();

		public AwsMessageQueueSender(LinkedBlockingQueue<QueueMessage> queue) {
			try {
				this.msgQueue = queue;
				ProfileCredentialsProvider profileCredentialsProvider = new ProfileCredentialsProvider();
				AWSCredentials awsCredentials = profileCredentialsProvider.getCredentials();
				client = new AmazonSQSClient(awsCredentials);
				client.setRegion(Region.getRegion(Regions.US_EAST_1));

				String defaultQueueName = IgnightProperties.getInstance().getSqsQueue();
				CreateQueueRequest request = new CreateQueueRequest(defaultQueueName);
				this.defaultQueueUrl = client.createQueue(request).getQueueUrl();
				checkQueues();
			} catch (Exception e) {
				log.error("Failed to start AwsMessageQueueSender", e);
			}
		}

		@Override
		public void run() {
			while (true) {
				try {
					QueueMessage msg = null;
					List<SendMessageBatchRequestEntry> entries = new ArrayList<SendMessageBatchRequestEntry>();
					Long id = 1l;
					while ((msg = msgQueue.poll()) != null) {
						if (entries.size() == 10) {
							sendMessage(entries);
							entries.clear();
							id = 1l;
						}
						SendMessageBatchRequestEntry entry = new SendMessageBatchRequestEntry(id.toString(), gson.toJson(msg));
						entries.add(entry);
						id++;
					}
					if (entries.size() > 0) {
						sendMessage(entries);
						entries.clear();
						id = 1l;
					} else {
						Thread.sleep(200);
					}
					checkQueues();
				} catch (Exception e) {
					log.error("Failed to send messages to sqs queues", e);
				}
			}
		}

		private void checkQueues() {
			Map<String, String> newQueues = new HashMap<String, String>();
			String key = RedisKeys.SQS_MSG_QUEUE_PREFIX + "*";
			Set<String> keys = jedis.keys(key);
			for (String k : keys) {
				String name = k.split("::")[1];
				String queueUrl = openQueues.get(name);
				if (queueUrl == null) {
					CreateQueueRequest request = new CreateQueueRequest(name);
					queueUrl = client.createQueue(request).getQueueUrl();
				}
				newQueues.put(name, queueUrl);
			}
			openQueues = newQueues;
			queueUrls = new ArrayList(openQueues.values());
		}

		public void sendMessage(List<SendMessageBatchRequestEntry> entries) {
			if (queueUrls.size() <= 0) {
				SendMessageBatchRequest request = new SendMessageBatchRequest(defaultQueueUrl, entries);
				client.sendMessageBatch(request);
			} else {
				int currentIndex = (int) (index++ % queueUrls.size());
				String queueUrl = queueUrls.get(currentIndex);
				SendMessageBatchRequest request = new SendMessageBatchRequest(queueUrl, entries);
				client.sendMessageBatch(request);
			}
		}

		public void sendToAll(QueueMessage m) {
			while (queueUrls.size() <= 0) {
				checkQueues();
			}
			for (String queueUrl : queueUrls) {
				client.sendMessage(new SendMessageRequest(queueUrl, gson.toJson(m)));
			}
		}
	}

	public static synchronized AwsMessageSender getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new AwsMessageSender();
		}
		return INSTANCE;
	}

	public void sendVenueActivity(Long venueId, Long userId, Long activity) {
		QueueMessage message = new QueueMessage();
		if (activity == 1) {
			message.type = QueueMessageType.ADD_ACTIVITY;
		} else if (activity == -1) {
			message.type = QueueMessageType.REMOVE_ACTIVITY;
		} else if (activity == -2) {
			message.type = QueueMessageType.SWITCH_TO_REMOVE;
		} else if (activity == 2) {
			message.type = QueueMessageType.SWITCH_TO_ADD;
		}
		message.userId = userId;
		message.typeId = venueId;
		msgQueue.offer(message);
		//		sendMessage(message);
	}

	public void sendClearMessage() {
		QueueMessage m = new QueueMessage();
		m.type = QueueMessageType.CLEAR;
		msgQueueSender.sendToAll(m);
	}

	public void sendResetMessage() {
		QueueMessage m = new QueueMessage();
		m.type = QueueMessageType.RESET;
		msgQueueSender.sendToAll(m);
	}

	public void sendNewUser(Long userId) {
		QueueMessage message = new QueueMessage();
		message.type = QueueMessageType.NEW_USER;
		message.userId = userId;
		msgQueue.offer(message);
		//		sendMessage(message);
	}

	public void sendDnaChange(Long userId) {
		QueueMessage message = new QueueMessage();
		message.type = QueueMessageType.DNA_CHANGE;
		message.userId = userId;
		msgQueue.offer(message);
		//		sendMessage(message);
	}

	public void sendNewPublicGroup(Long groupId, Long userId) {
		sendGroupMessage(QueueMessageType.NEW_PUBLIC_GROUP, groupId, userId);
	}

	public void sendNewPrivateGroup(Long groupId, Long userId) {
		sendGroupMessage(QueueMessageType.NEW_PRIVATE_GROUP, groupId, userId);
	}

	public void sendJoinGroup(Long groupId, Long userId) {
		sendGroupMessage(QueueMessageType.JOIN_GROUP, groupId, userId);
	}

	public void sendLeaveGroup(Long groupId, Long userId) {
		sendGroupMessage(QueueMessageType.LEAVE_GROUP, groupId, userId);
	}

	public void sendGroupMessage(QueueMessageType type, Long groupId, Long userId) {
		QueueMessage message = new QueueMessage();
		message.type = type;
		message.userId = userId;
		message.typeId = groupId;
		msgQueue.offer(message);
		//		sendMessage(message);
	}
}
