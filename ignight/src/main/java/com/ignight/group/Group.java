package com.ignight.group;

import com.google.gson.annotations.SerializedName;

public class Group {
	@SerializedName("id")
	public Long id;

	@SerializedName("n")
	public String name;

	@SerializedName("c")
	public Long cityId;

	@SerializedName("admin")
	public Long adminId;

	@SerializedName("d")
	public String description;

	@SerializedName("t")
	public boolean isPrivate;

	@SerializedName("ts")
	public Long lastBuzzTimeStamp = -1l;

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Group [id=");
		builder.append(id);
		builder.append(", name=");
		builder.append(name);
		builder.append(", cityId=");
		builder.append(cityId);
		builder.append(", adminId=");
		builder.append(adminId);
		builder.append(", description=");
		builder.append(description);
		builder.append(", isPrivate=");
		builder.append(isPrivate);
		builder.append(", lastBuzzTimeStamp=");
		builder.append(lastBuzzTimeStamp);
		builder.append("]");
		return builder.toString();
	}

}
