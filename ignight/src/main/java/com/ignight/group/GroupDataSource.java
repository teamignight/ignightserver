package com.ignight.group;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.karneim.pojobuilder.GeneratePojoBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.buzz.Buzz;
import com.buzz.Buzz.BuzzType;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ignight.dna.TrendingValues;
import com.ignight.server.responses.GroupSearches;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.user.GroupInvite;
import com.ignight.user.RedisKeys;
import com.ignight.user.User;
import com.ignight.venue.VenueDataSource;

import databases.Databases;
import databases.JedisConnection;
import databases.Resources;

public class GroupDataSource {
	private static Logger log = LoggerFactory.getLogger(GroupDataSource.class);
	private static Gson gson = new GsonBuilder().disableHtmlEscaping().create();

	public static final JedisConnection jedis = JedisConnection.getInstance();

	public static void loadAndSaveGroupsToRedis() throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			conn = Databases.dataSource.getConnection();
			ps = conn.prepareStatement("SELECT * FROM groups WHERE deleted_time IS NULL");
			rs = ps.executeQuery();

			List<Group> groups = new ArrayList<Group>();
			while (rs.next()) {
				Group g = new Group();
				g.id = rs.getLong(1);
				g.name = rs.getString(2);
				g.cityId = rs.getLong(3);
				g.adminId = rs.getLong(4);
				g.description = rs.getString(5);
				g.isPrivate = rs.getBoolean(6);
				groups.add(g);
			}

			log.info("Loaded {} Group's From SQL", groups.size());

			for (Group g : groups) {
				String key = RedisKeys.getGroupsBucketKey(g.id);
				String obj = jedis.hget(key, g.id.toString());
				if (obj == null) {
					log.warn("Adding Group To Cache: {}, {}", g.id, g.name);
					jedis.hset(key, g.id.toString(), g);
				}
			}
		} finally {
			Resources.close(conn, ps, rs);
		}
	}

	public static void loadAndSaveGroupMembersToRedis() throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			conn = Databases.dataSource.getConnection();
			ps = conn.prepareStatement("SELECT * FROM group_members");
			rs = ps.executeQuery();

			Map<Long, Set<String>> groupMembers = new HashMap<Long, Set<String>>();
			while (rs.next()) {
				Long userId = rs.getLong(1);
				Long groupId = rs.getLong(2);
				Set<String> members = groupMembers.get(groupId);
				if (members == null) {
					members = new HashSet<String>();
					groupMembers.put(groupId, members);
				}
				members.add(userId.toString());
			}

			log.info("Loaded {} Group's Members From SQL", groupMembers.size());

			String popularGroupsKey = RedisKeys.getPopularGroupsKey();
			jedis.del(popularGroupsKey);

			for (Map.Entry<Long, Set<String>> entry : groupMembers.entrySet()) {
				Long groupId = entry.getKey();
				Set<String> members = entry.getValue();

				String key = RedisKeys.getGroupMembersKey(groupId);
				if (!jedis.exists(key)) {
					log.warn("Adding Members To Cache: {} for Group: {}", members, groupId);
					for (String s : members) {
						jedis.sadd(key, s);
					}
				}
				Group group = GroupDataSource.getGroupInCache(groupId);
				if (group == null || group.isPrivate) {
					continue;
				}

				jedis.zadd(popularGroupsKey, members.size(), groupId.toString());
			}
		} finally {
			Resources.close(conn, ps, rs);
		}
	}

	public static void loadAndSaveGroupInvitesToRedis() throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			conn = Databases.dataSource.getConnection();
			ps = conn.prepareStatement("SELECT * FROM group_invites");
			rs = ps.executeQuery();

			Map<Long, Set<String>> groupInvites = new HashMap<Long, Set<String>>();
			Map<Long, List<GroupInvite>> userInvites = new HashMap<Long, List<GroupInvite>>();
			while (rs.next()) {
				Long userId = rs.getLong(1);
				Long inviteeId = rs.getLong(2);
				Long groupId = rs.getLong(3);

				GroupInvite invite = new GroupInvite(groupId, inviteeId);
				List<GroupInvite> invites = userInvites.get(userId);
				if (invites == null) {
					invites = new ArrayList<GroupInvite>();
					userInvites.put(userId, invites);
				}
				invites.add(invite);

				Set<String> groupInvitations = groupInvites.get(groupId);
				if (groupInvitations == null) {
					groupInvitations = new HashSet<String>();
					groupInvites.put(groupId, groupInvitations);
				}
				groupInvitations.add(userId.toString());
			}

			log.info("Loaded {} Group's Invitations From SQL", groupInvites.size());
			for (Map.Entry<Long, Set<String>> entry : groupInvites.entrySet()) {
				Long groupId = entry.getKey();
				Set<String> memberInvites = entry.getValue();

				String key = RedisKeys.getGroupInviteeKey(groupId);
				if (!jedis.exists(key)) {
					log.warn("Adding Member Invitations To Cache: {} for Group: {}", memberInvites, groupId);
					for (String s : memberInvites) {
						jedis.sadd(key, s);
					}
				}
			}

			for (Map.Entry<Long, List<GroupInvite>> entry : userInvites.entrySet()) {
				Long userId = entry.getKey();
				List<GroupInvite> invitations = entry.getValue();

				String key = RedisKeys.getUserInboxKey(userId);
				if (!jedis.equals(key)) {
					for (GroupInvite invitation : invitations) {
						UserDataSource.addInviteForUser(userId, invitation);
					}
				}
			}

		} finally {
			Resources.close(conn, ps, rs);
		}
	}

	public static Set<Long> getListOfGroupIdsForCity(Long cityId) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		Set<Long> groupIds = new HashSet<Long>();

		try {
			conn = Databases.dataSource.getConnection();
			ps = conn.prepareStatement(
					"SELECT id FROM groups WHERE city_id = " + cityId);
			rs = ps.executeQuery();

			while (rs.next()) {
				Long nextVenueId = rs.getLong("id");
				groupIds.add(nextVenueId);
			}

			return groupIds;

		} finally {
			Resources.close(conn, ps, rs);
		}
	}

	public static Long getLastArchivedBuzzIdForGroup(Long cityId, Long groupId) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			conn = Databases.dataSourceArchiving.getConnection();
			ps = conn.prepareStatement(
					"SELECT buzz_id FROM group_buzz WHERE city_id = " + cityId + " AND group_id = " + groupId +
							" ORDER BY buzz_id LIMIT 1");
			rs = ps.executeQuery();

			Long lastArchivedBuzzId = -1l;
			while (rs.next()) {
				lastArchivedBuzzId = rs.getLong("buzz_id");
			}

			return lastArchivedBuzzId;

		} finally {
			Resources.close(conn, ps, rs);
		}
	}

	public static List<GroupSearches> searchForGroups(String search, long offset, long limit) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		List<GroupSearches> groups = new ArrayList<GroupSearches>();
		try {
			conn = Databases.dataSource.getConnection();
			ps = conn.prepareStatement(
					"SELECT id, name FROM groups " +
							"WHERE deleted_time IS NULL " +
							"AND private = False " +
							"AND name LIKE '%" + search + "%' LIMIT ?, ?");
			ps.setObject(1, offset);
			ps.setObject(2, limit);
			rs = ps.executeQuery();

			while (rs.next()) {
				GroupSearches gs = new GroupSearches();
				gs.id = rs.getLong("id");
				gs.name = rs.getString("name");
				groups.add(gs);
			}

			return groups;
		} finally {
			Resources.close(conn, ps, rs);
		}
	}

	public static void addGroup(final Group group, final Long userId) {
		updateGroupInCache(group);
		addMemberToGroup(group.id, userId);

		UserDataSource.addGroupForUser(userId, group.id);

		Set<Long> activity = UserDataSource.getUserActivity(userId);
		for (Long a : activity) {
			GroupDataSource.updateGroupTrending(group.id, Math.abs(a), (a < 0) ? -1l : 1l);
		}

		if (group.isPrivate) {
			registerUserForPush(group, userId);
		}
	}

	public static void registerUserForPush(final Group group, final Long userId) {
		addUserAsSubscriber(group.id, userId);
		updateUserGroupLastBuzzAccessTimestamp(userId, group.id, System.currentTimeMillis());
	}

	public static void deleteGroup(Group group) {
		log.info("DELETING GROUP: {}", group);
		Long groupId = group.id;
		jedis.hdel(RedisKeys.getGroupsBucketKey(groupId), group.id.toString());
		jedis.hdel(RedisKeys.getGroupDnaKey(groupId), group.id.toString());
		jedis.del(RedisKeys.getGroupTrendingKey(groupId));
		jedis.del(RedisKeys.getGroupMembersKey(groupId));
		jedis.del(RedisKeys.getGroupInviteeKey(groupId));
		jedis.del(RedisKeys.getGroupBuzzKey(groupId));
		jedis.del(RedisKeys.getGroupBuzzIdPrefix(groupId));
		jedis.del(RedisKeys.getGroupBuzzImagesKey(groupId));
		jedis.del(RedisKeys.getGroupBuzzImagesIdPrefix(groupId));
		jedis.del(RedisKeys.getGroupBuzzSubscribers(groupId));
		jedis.del(RedisKeys.getGroupBuzzListeners(groupId));
		String key = RedisKeys.getPopularGroupsKey();
		jedis.zrem(key, groupId.toString());
	}

	public static boolean doesGroupNameExist(String name) throws SQLException {
		Long count = Databases.selectLong("SELECT COUNT(*) FROM groups where name = ? AND deleted_time IS NULL", name);
		return count > 0;
	}

	public static void updateGroupInCache(Group group) {
		String key = RedisKeys.getGroupsBucketKey(group.id);
		jedis.hset(key, group.id.toString(), group);
	}

	public static Group getGroupInCache(Long groupId) {
		String key = RedisKeys.getGroupsBucketKey(groupId);
		String data = jedis.hget(key, groupId.toString());
		if (data != null) {
			return gson.fromJson(data, Group.class);
		}
		return null;
	}

	public static boolean isMemberOfGroup(Long groupId, Long userId) {
		String key = RedisKeys.getGroupMembersKey(groupId);
		return jedis.sismember(key, userId.toString());
	}

	public static Long getGroupSize(Long groupId) {
		String key = RedisKeys.getGroupMembersKey(groupId);
		return jedis.scard(key);
	}

	public static Set<Long> getGroupMembers(Long groupId) {
		String key = RedisKeys.getGroupMembersKey(groupId);
		Set<String> members = jedis.smembers(key);
		return convertToLong(members);
	}

	public static Set<Long> getInvitedUsers(Long groupId) {
		String key = RedisKeys.getGroupInviteeKey(groupId);
		Set<String> invited = jedis.smembers(key);
		return convertToLong(invited);
	}

	private static Set<Long> convertToLong(Set<String> objects) {
		Set<Long> result = new HashSet<Long>();
		for (String m : objects) {
			result.add(Long.valueOf(m));
		}
		return result;
	}

	public static class PopularGroups implements Comparable<PopularGroups> {
		public Long groupId;
		public String groupName;
		public Long noOfMembers;
		public boolean isMember;

		@Override
		public int compareTo(PopularGroups o) {
			if (o == null || o.noOfMembers == null) {
				return -1;
			}

			if (noOfMembers == null) {
				return 1;
			}

			long val = noOfMembers.compareTo(o.noOfMembers);
			if (val == 1) {
				return -1;
			} else if (val == -1) {
				return 1;
			}
			return groupName.compareTo(o.groupName);
		}

	}

	public static List<PopularGroups> getPopularGroups(Long userId, long offset, long limit) {
		List<PopularGroups> groups = new ArrayList<PopularGroups>();
		Set<String> idKeys = jedis.zrevrange(RedisKeys.getPopularGroupsKey(), offset, offset + limit);
		for (String key : idKeys) {
			Long groupId = Long.valueOf(key);
			PopularGroups pg = new PopularGroups();
			pg.groupId = groupId;
			Group g = getGroupInCache(pg.groupId);
			if (g == null) {
				log.error("Group not in cache: {}", pg.groupId);
				continue;
			}
			if (g.isPrivate) {
				log.error("Group is private: {}, should not be in popular groups!", groupId);
				continue;
			}

			pg.groupName = g.name;
			pg.noOfMembers = getGroupSize(pg.groupId);
			pg.isMember = false;
			groups.add(pg);
		}
		log.info("Returngin {} Popular Groups", groups.size());
		return groups;
	}

	public static void addMemberToGroup(Long groupId, Long userId) {
		String key = RedisKeys.getGroupMembersKey(groupId);
		jedis.sadd(key, userId.toString());

		Group group = getGroupInCache(groupId);
		if (group.isPrivate) {
			return;
		}
		key = RedisKeys.getPopularGroupsKey();
		jedis.zincrby(key, 1.0, groupId.toString());
	}

	public static void joinedGroup(Long userId, Long groupId) {
		//Add User As Member to Group
		addMemberToGroup(groupId, userId);

		//Remove from invited list
		removeInviteFromGroup(groupId, userId);

		//Add group to users list
		String key = RedisKeys.getUserGroupsKey(userId);
		jedis.sadd(key, groupId.toString());

		//Update Group Trending For User
		Set<Long> activity = UserDataSource.getUserActivity(userId);
		for (Long a : activity) {
			GroupDataSource.updateGroupTrending(groupId, Math.abs(a), (a < 0) ? -1l : 1l);
		}
	}

	public static void removeMemberFromGroup(Long groupId, Long userId) throws SQLException {
		String key = RedisKeys.getGroupMembersKey(groupId);
		jedis.srem(key, userId.toString());
		removeUserAsSubscriber(groupId, userId);
		removeUserAsListener(groupId, userId);
		Group group = getGroupInCache(groupId);
		if (group == null || group.isPrivate) {
			return;
		}
		key = RedisKeys.getPopularGroupsKey();
		jedis.zincrby(key, -1.0, groupId.toString());
	}

	public static void inviteUserToGroup(Long groupId, Long userId, Long targetUserId) throws SQLException {
		String key = RedisKeys.getGroupInviteeKey(groupId);
		jedis.sadd(key, targetUserId.toString());

		GroupInvite invite = new GroupInvite(groupId, userId);
		UserDataSource.addInviteForUser(targetUserId, invite);
	}

	public static void removeInviteFromGroup(Long groupId, Long userId) {
		String key = RedisKeys.getGroupInviteeKey(groupId);
		jedis.srem(key, userId.toString());
	}

	public static void respondToInvite(Long groupId, Long userId, boolean accept) {
		String key = RedisKeys.getGroupInviteeKey(groupId);
		jedis.srem(key, userId.toString());

		UserDataSource.removeInviteForUser(userId, groupId);

		if (accept) {
			addMemberToGroup(groupId, userId);

			UserDataSource.addGroupForUser(userId, groupId);

			Set<Long> activity = UserDataSource.getUserActivity(userId);
			for (Long a : activity) {
				GroupDataSource.updateGroupTrending(groupId, Math.abs(a), (a < 0) ? -1l : 1l);
			}
		}
	}

	public static void addUserAsListener(Long groupId, Long userId) {
		String userBuzzKey = RedisKeys.getUserCurrentBuzzListenerKey(userId);
		String val = jedis.get(userBuzzKey);
		if (val != null) {
			String gOrV = val.split("-")[0];
			Long id = Long.valueOf(val.split("-")[1]);
			if (gOrV.equalsIgnoreCase("G")) {
				GroupDataSource.removeUserAsListener(id, userId);
			} else {
				VenueDataSource.removeUserAsListener(id, userId);
			}
		}
		jedis.set(userBuzzKey, "G-" + groupId);

		String key = RedisKeys.getGroupBuzzListeners(groupId);
		jedis.sadd(key, userId.toString());
	}

	public static void removeUserAsListener(Long groupId, Long userId) {
		String userBuzzKey = RedisKeys.getUserCurrentBuzzListenerKey(userId);
		jedis.del(userBuzzKey);

		String key = RedisKeys.getGroupBuzzListeners(groupId);
		jedis.srem(key, userId.toString());
	}

	public static Set<Long> getGroupBuzzListeners(Long groupId, Long userId) {
		String listenerKey = RedisKeys.getGroupBuzzListeners(groupId);
		Set<String> members = jedis.smembers(listenerKey);

		Set<Long> listeners = new HashSet<Long>();
		for (String m : members) {
			listeners.add(Long.valueOf(m));
		}
		listeners.remove(userId);
		return listeners;
	}

	public static void addUserAsSubscriber(Long groupId, Long userId) {
		String subKey = RedisKeys.getGroupBuzzSubscribers(groupId);
		jedis.sadd(subKey, userId.toString());
	}

	public static void removeUserAsSubscriber(Long groupId, Long userId) {
		String subKey = RedisKeys.getGroupBuzzSubscribers(groupId);
		jedis.srem(subKey, userId.toString());
	}

	public static Set<Long> getGroupSubscribers(Long groupId) {
		String subKey = RedisKeys.getGroupBuzzSubscribers(groupId);
		Set<String> members = jedis.smembers(subKey);
		Set<Long> subscribers = new HashSet<Long>();
		for (String m : members) {
			subscribers.add(Long.valueOf(m));
		}
		return subscribers;
	}

	public static boolean isUserSubscribed(Long groupId, Long userId) {
		String subKey = RedisKeys.getGroupBuzzSubscribers(groupId);
		return jedis.sismember(subKey, userId.toString());
	}

	//	public static boolean isUserLiseningToBuzz(Long groupId, Long userId) {
	//		String key = RedisKeys.getGroupBuzzListeners(groupId);
	//		return jedis.sismember(key, userId.toString());
	//	}

	public static TrendingValues getGroupDna(Long groupId) {
		String key = RedisKeys.getGroupDnaKey(groupId);
		String data = jedis.hget(key, groupId.toString());
		return gson.fromJson(data, TrendingValues.class);
	}

	public static class GroupTrending implements Comparable<GroupTrending> {
		public Long venueId;
		public Long userVenueValue;
		public String placeImage;

		public String venueName;
		public Long userInput;
		public Double longitude;
		public Double latitude;

		@Override
		public int compareTo(GroupTrending o) {
			if (o == null) {
				return -1;
			}
			return o.userVenueValue.compareTo(userVenueValue);
		}
	}

	public static void updateGroupTrending(Long groupId, Long venueId, Long delta) {
		String key = RedisKeys.getGroupTrendingKey(groupId);
		String val = jedis.hget(key, venueId.toString());
		Long newVal = delta;
		if (val != null) {
			newVal += Long.valueOf(val);
		}
		jedis.hset(key, venueId.toString(), newVal);
	}

	public static Long getGroupVenueValue(Long groupId, Long venueId) {
		String key = RedisKeys.getGroupTrendingKey(groupId);
		String val = jedis.hget(key, venueId.toString());
		if (val == null) {
			return null;
		}
		return Long.valueOf(val);
	}

	public static List<GroupTrending> getGroupTrending(Long groupId) {
		String key = RedisKeys.getGroupTrendingKey(groupId);
		Map<String, String> results = jedis.hgetall(key);
		List<GroupTrending> trending = new ArrayList<GroupDataSource.GroupTrending>();
		for (Map.Entry<String, String> entry : results.entrySet()) {
			GroupTrending t = new GroupTrending();
			t.venueId = Long.valueOf(entry.getKey());
			t.userVenueValue = Long.valueOf(entry.getValue());
			t.placeImage = VenueDataSource.getGooglePlaceImageForTrendingList(t.venueId);
			trending.add(t);
		}
		return trending;
	}

	public static void flagGroupBuzz(Long userId, Long groupId, Long buzzId) throws SQLException {
		Databases.insert("INSERT INTO flagged_buzz " +
				"(user_id, group_id, buzz_id) " +
				"VALUES (?, ?, ?)",
				userId, groupId, buzzId);
	}

	public static void flagGroup(Long userId, Long groupId) throws SQLException {
		Databases.insert("INSERT INTO flagged_groups " +
				"(user_id, group_id) " +
				"VALUES (?, ?)",
				userId, groupId);
	}

	public static long addBuzz(Long groupId, Buzz b) {
		String buzzIdKey = RedisKeys.getGroupBuzzIdPrefix(groupId);
		Long buzzId = jedis.incr(buzzIdKey) - 1;
		b.buzzId = buzzId;
		String buzzKey = RedisKeys.getGroupBuzzKey(groupId);
		jedis.hset(buzzKey, buzzId.toString(), b);
		if (b.buzzType == BuzzType.IMAGE) {
			String imageKey = RedisKeys.getGroupBuzzImagesKey(groupId);
			String imagesIdKey = RedisKeys.getGroupBuzzImagesIdPrefix(groupId);
			Long id = jedis.incr(imagesIdKey) - 1;
			jedis.hset(imageKey, id.toString(), b.buzzText);
		}
		return buzzId;
	}

	public static Long getLastBuzzId(Long groupId) {
		String buzzIdKey = RedisKeys.getGroupBuzzIdPrefix(groupId);
		String val = jedis.get(buzzIdKey);
		if (val == null) {
			return null;
		}
		return Long.valueOf(val);
	}

	public static List<Buzz> getAllNewGroupBuzz(Long groupId, Long lastBuzzSeen) {
		ArrayList<Buzz> buzzes = new ArrayList<Buzz>();
		Long last = getLastBuzzId(groupId);

		//If no buzz, return empty
		if (last == null) {
			return buzzes;
		}

		//iF at last buzz, return empty
		if (lastBuzzSeen.equals(last)) {
			return buzzes;
		}

		Long start, end;

		start = Math.max(0, lastBuzzSeen + 1);
		end = last;

		int c = 0;
		String[] fields = new String[(int) (end - start + 1)];
		for (Long i = start; i <= end; i++) {
			fields[c++] = i.toString();
		}

		String key = RedisKeys.getGroupBuzzKey(groupId);
		List<String> vals = jedis.hmget(key, fields);
		for (String s : vals) {
			if (s == null) {
				continue;
			}
			Buzz b = gson.fromJson(s, Buzz.class);
			buzzes.add(b);
		}

		return buzzes;
	}

	public static List<Buzz> getBuzz(Long groupId, Long lastBuzzSeen, long count, Boolean latest) {
		ArrayList<Buzz> buzzes = new ArrayList<Buzz>();
		Long last = getLastBuzzId(groupId);

		//If no buzz, return empty
		if (last == null) {
			return buzzes;
		}

		//iF at last buzz, return empty
		if (lastBuzzSeen.equals(last)) {
			return buzzes;
		}

		Long start, end;

		if (lastBuzzSeen < 0) {
			start = Math.max(0, last - count + 1);
			end = last;
		} else {
			start = latest ? lastBuzzSeen + 1 : Math.max(0, lastBuzzSeen - count + 1);
			end = latest ? Math.max(last, lastBuzzSeen + count) : Math.max(0, lastBuzzSeen - 1);
		}

		int c = 0;
		String[] fields = new String[(int) (end - start + 1)];
		for (Long i = start; i <= end; i++) {
			fields[c++] = i.toString();
		}

		String key = RedisKeys.getGroupBuzzKey(groupId);
		List<String> vals = jedis.hmget(key, fields);
		for (String s : vals) {
			if (s == null) {
				continue;
			}
			Buzz b = gson.fromJson(s, Buzz.class);
			buzzes.add(b);
		}

		return buzzes;
	}

	public static boolean updateUserGroupLastBuzzAccessTimestamp(Long userId, Long groupId, Long timeStamp) {
		String groupBuzzLastAccessTimeStampKey = RedisKeys.getUserGroupBuzzLastAccessTimeStampKey(userId);
		return jedis.hset(groupBuzzLastAccessTimeStampKey, groupId.toString(), timeStamp);
	}

	public static String getUserGroupLastBuzzAccessTimestamp(Long userId, Long groupId) {
		String groupBuzzLastAccessTimeStampKey = RedisKeys.getUserGroupBuzzLastAccessTimeStampKey(userId);
		return jedis.hget(groupBuzzLastAccessTimeStampKey, groupId.toString());
	}

	public static boolean removeUserGroupLastBuzzAccessTimestamp(Long userId, Long groupId) {
		String groupBuzzLastAccessTimeStampKey = RedisKeys.getUserGroupBuzzLastAccessTimeStampKey(userId);
		return jedis.hdel(groupBuzzLastAccessTimeStampKey, groupId.toString());
	}

	@GeneratePojoBuilder
	public static class ClientBuzzImageData implements Comparable<ClientBuzzImageData> {
		public Long buzzId;
		public Long buzzTs;
		public String buzzText;

		@Override
		public int compareTo(ClientBuzzImageData o) {
			return o.buzzId.compareTo(this.buzzId);
		}
	}

	public static List<ClientBuzzImageData> getBuzzImages(Long groupId, Long start, int count) {
		List<ClientBuzzImageData> images = new ArrayList<ClientBuzzImageData>();

		String[] fields = new String[count];
		int c = 0;
		for (Long i = start; i < start + count; i++) {
			fields[c++] = i.toString();
		}

		String key = RedisKeys.getGroupBuzzImagesKey(groupId);
		List<String> results = jedis.hmget(key, fields);

		for (Long i = start; i < results.size(); i++) {
			String r = results.get(i.intValue());
			if (r != null) {
				ClientBuzzImageData buzz = new ClientBuzzImageData();
				buzz.buzzId = i;
				buzz.buzzText = r;
				images.add(buzz);
			}
		}
		return images;
	}

	public static void updateMemberBuzzNotifications(User sender, Group group) {
		Set<Long> members = GroupDataSource.getGroupMembers(group.id);
		members.remove(sender.id);
		if (members.size() > 0) {
			String[] memberStrings = new String[members.size()];
			int i = 0;
			for (Long member : members) {
				memberStrings[i++] = member.toString();
			}
			String notificationKey = RedisKeys.getUserGroupBuzzNotificationSetKey();
			jedis.sadd(notificationKey, memberStrings);
		}
	}
}
