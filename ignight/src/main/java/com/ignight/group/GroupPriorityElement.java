package com.ignight.group;

public class GroupPriorityElement implements Comparable<GroupPriorityElement> {
	public Long groupId;
	public Long cityId;
	public int noOfGroupMembers = 0;

	public GroupPriorityElement(Long groupId, Long cityId, int trendingScore) {
		super();
		this.groupId = groupId;
		this.cityId = cityId;
	}

	public GroupPriorityElement(long groupId, Long cityId) {
		super();
		this.groupId = groupId;
		this.cityId = cityId;
	}

	public void increment() {
		noOfGroupMembers++;
	}

	public void decrement() {
		noOfGroupMembers--;
	}

	@Override
	public boolean equals(Object o) {
		GroupPriorityElement v = (GroupPriorityElement) o;
		if (!groupId.equals(v.groupId))
			return false;
		if (!cityId.equals(v.cityId))
			return false;
		return true;
	}

	@Override
	public int compareTo(GroupPriorityElement o) {
		if (this.noOfGroupMembers < o.noOfGroupMembers) {
			return -1;
		} else if (this.noOfGroupMembers > o.noOfGroupMembers) {
			return 1;
		} else {
			return 0;
		}
	}

	@Override
	public String toString() {
		return "GroupPriorityElement [groupId=" + groupId + ", noOfGroupMembers=" + noOfGroupMembers + "]";
	}

}
