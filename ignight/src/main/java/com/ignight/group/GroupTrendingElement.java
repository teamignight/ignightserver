package com.ignight.group;

public class GroupTrendingElement implements Comparable<GroupTrendingElement> {
	public Long groupId;
	public int trendingScore = 0;

	public GroupTrendingElement(Long groupId, int trendingScore) {
		super();
		this.groupId = groupId;
	}

	public GroupTrendingElement() {
		super();
		this.groupId = -1L;
	}

	public GroupTrendingElement(long groupId) {
		super();
		this.groupId = groupId;
	}

	public void increment() {
		trendingScore++;
	}

	public void decrement() {
		trendingScore--;
	}

	@Override
	public boolean equals(Object o) {
		GroupTrendingElement v = (GroupTrendingElement) o;
		return groupId.equals(v.groupId);
	}

	@Override
	public int compareTo(GroupTrendingElement o) {
		if (this.trendingScore < o.trendingScore) {
			return -1;
		} else if (this.trendingScore > o.trendingScore) {
			return 1;
		} else {
			return 0;
		}
	}

	@Override
	public String toString() {
		return "GroupTrendingElement [groupId=" + groupId + ", noOfGroupMembers=" + trendingScore + "]";
	}

}
