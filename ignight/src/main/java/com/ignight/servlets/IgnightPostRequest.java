package com.ignight.servlets;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import mediautil.gen.Log;
import mediautil.image.jpeg.AbstractImageInfo;
import mediautil.image.jpeg.Entry;
import mediautil.image.jpeg.Exif;
import mediautil.image.jpeg.LLJTran;
import mediautil.image.jpeg.LLJTranException;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IgnightPostRequest extends IgnightRequest {
	private Logger log = LoggerFactory.getLogger(getClass());
	public JSONObject msg;
	public BufferedImage uploadedImage;
	public Integer rotationOperation;

	public IgnightPostRequest(HttpServletRequest servletRequest) throws Exception {
		processRequest(servletRequest);
	}

	@Override
	protected void processRequest(HttpServletRequest servletRequest) throws Exception {
		this.msg = getMsgBody(servletRequest);
		if (msg.has("type")) {
			this.type = msg.getString("type");
		}

		if (ServletFileUpload.isMultipartContent(servletRequest)) {
			getUploadedImage(servletRequest);
		}
	}

	private JSONObject getMsgBody(HttpServletRequest request) throws Exception {
		JSONObject msg = null;
		String requestContentType = request.getContentType();

		if (requestContentType == null) {
			throw new Exception("No Content Type Set!");
		}

		if (requestContentType.equals("application/json; charset=utf-8")) {
			msg = getBody(request);
		} else if (ServletFileUpload.isMultipartContent(request)) {
			Part jsonBodyPart = request.getPart("json_body");
			log.info("REQUEST: " + jsonBodyPart);
			Scanner scanner = new Scanner(jsonBodyPart.getInputStream());
			String jsonBodyString = scanner.nextLine();
			msg = (new JSONObject(jsonBodyString)).getJSONObject("msg");
			scanner.close();
		} else {
			throw new Exception("Unrecognized Content type: " + requestContentType);
		}

		if (msg == null) {
			throw new IllegalArgumentException("Msg was null in request: " + request);
		}
		return msg;
	}

	private JSONObject getBody(HttpServletRequest request) throws Exception {
		StringBuffer jb = new StringBuffer();
		JSONObject body;
		String line = null;
		BufferedReader reader = request.getReader();
		while ((line = reader.readLine()) != null) {
			jb.append(line);
		}

		body = new JSONObject(jb.toString());
		JSONObject msg = body.getJSONObject("msg");
		if (msg == null) {
			return null;
		}

		return msg;
	}

	private void getUploadedImage(HttpServletRequest request) throws Exception {
		InputStream imgInputStream = request.getPart("userImage").getInputStream();
		ByteArrayOutputStream baos = createByteArrayOutputStreamFromInputStream(imgInputStream);
		InputStream is1 = new ByteArrayInputStream(baos.toByteArray());
		InputStream is2 = new ByteArrayInputStream(baos.toByteArray());
		int operation = getOperationToCorrectOrientation(is2);
		BufferedImage img = ImageIO.read(is1);
		this.uploadedImage = img;
		this.rotationOperation = operation;
	}

	private ByteArrayOutputStream createByteArrayOutputStreamFromInputStream(InputStream is) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len;
		while ((len = is.read(buffer)) > -1) {
			baos.write(buffer, 0, len);
		}
		baos.flush();
		return baos;
	}

	private int getOperationToCorrectOrientation(InputStream is) throws LLJTranException, IOException {
		Log.debugLevel = Log.LEVEL_DEBUG;

		LLJTran ll = new LLJTran(is);
		ll.read(LLJTran.READ_INFO, true);
		AbstractImageInfo<?> imageInfo = ll.getImageInfo();

		int orientation = 1;
		if (imageInfo instanceof mediautil.image.jpeg.Exif) {
			Exif exif = (Exif) imageInfo;

			Entry orientationTag = exif.getTagValue(Exif.ORIENTATION, true);
			if (orientationTag != null)
				orientation = (Integer) orientationTag.getValue(0);
			int operation = Exif.opToCorrectOrientation[orientation];

			is.close();
			ll.freeMemory();
			return operation;
		}
		is.close();
		ll.freeMemory();
		return 0;

	}

}
