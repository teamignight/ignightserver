package com.ignight.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import net.karneim.pojobuilder.GeneratePojoBuilder;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.cloudsearchdomain.model.ContentType;
import com.google.api.client.http.HttpStatusCodes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ResponseUtil {
	private static Logger log = LoggerFactory.getLogger(ResponseUtil.class);
	protected static Gson gson = new GsonBuilder().disableHtmlEscaping().create();

	public static void sendResponse(HttpServletResponse response, String res) throws IOException {
		response.setContentType("application/json; charset=utf-8");
		PrintWriter out = response.getWriter();
		out.println(res);
		out.close();
	}

	public static void sendResponse(HttpServletResponse response, Object msg) throws IOException {
		response.setContentType(ContentType.Applicationjson.toString());
		PrintWriter out = response.getWriter();
		response.setStatus(HttpStatusCodes.STATUS_CODE_OK);
		out.println(gson.toJson(new ResponseMessageBuilder().withMsg(msg).build()));
		out.close();
	}

	@GeneratePojoBuilder
	public static class ResponseMessage {
		public String failureReason;
		public Object msg;
	}

	public static void sendUnknownErrorResponse(HttpServletResponse response) throws IOException, JSONException {
		response.setContentType("application/json; charset=utf-8");
		PrintWriter out = response.getWriter();
		JSONObject obj = new JSONObject();
		obj.put("res", false);
		obj.put("reason", "Error occurred on server.");
		String res = obj.toString();
		out.println(res);
		out.close();
	}

	public static void sendErrorResponse(HttpServletResponse response, String text) {
		try {
			response.setContentType("application/json; charset=utf-8");
			Map<String, Object> resp = new HashMap<String, Object>();
			resp.put("res", false);
			resp.put("reason", text);
			PrintWriter out = response.getWriter();
			out.println(gson.toJson(resp));
			out.close();
		} catch (IOException e) {
			log.error("Failed to write back to client", e);
		}
	}
	
	public static void sendServerBlockedResponse(HttpServletResponse response, String text) {
		try {
			response.setContentType("application/json; charset=utf-8");
			Map<String, Object> resp = new HashMap<String, Object>();
			resp.put("res", false);
			resp.put("reason", text);
			PrintWriter out = response.getWriter();
			out.println(gson.toJson(resp));
			out.close();
		} catch (IOException e) {
			log.error("Failed to write back to client", e);
		}
	}
}
