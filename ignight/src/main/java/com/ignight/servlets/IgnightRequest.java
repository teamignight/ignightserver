package com.ignight.servlets;

import javax.servlet.http.HttpServletRequest;

public abstract class IgnightRequest {
	public String type;

	protected abstract void processRequest(HttpServletRequest servletRequest) throws Exception;
}
