package com.ignight.servlets.data;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ignight.servlets.IgnightPostRequest;
import com.ignight.servlets.ResponseUtil;

@WebServlet(asyncSupported = true, value = "/data")
public class DataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DataServletHandler handler = null;
	private static Logger log = LoggerFactory.getLogger(DataServlet.class);

	public DataServlet() {
		super();
		handler = new DataServletHandler();
		log.info("Starting DataServlet");
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		try {
			IgnightPostRequest postRequest = new IgnightPostRequest(request);
			String type = postRequest.type;
			JSONObject postMsg = postRequest.msg;

			String res = null;
			if (type.equals("clear")) {
				res = handler.clearDailyData(postMsg);
			} else if (type.equals("clearAll")) {
				res = handler.clearAll(postMsg);
			} else if (type.equals("reset")) {
				res = handler.reset(postMsg);
			} else {
				log.error("Got unknown request: {}", type);
			}

			if (res != null) {
				ResponseUtil.sendResponse(response, res);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			try {
				ResponseUtil.sendUnknownErrorResponse(response);
			} catch (JSONException e1) {
				log.error(e1.getMessage(), e1);
			}
		}
	}
}
