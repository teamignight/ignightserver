package com.ignight.servlets.data;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.buzz.Buzz;
import com.google.gson.JsonObject;
import com.ignight.aws.AwsMessageSender;
import com.ignight.data.DataSource;
import com.ignight.dna.CalcVenueDna;
import com.ignight.dna.TrendingBarCalculator.BarColor;
import com.ignight.dna.TrendingValues;
import com.ignight.dna.TrendingVenue;
import com.ignight.dna.elements.Age;
import com.ignight.dna.elements.Atmosphere;
import com.ignight.dna.elements.City;
import com.ignight.dna.elements.Gender;
import com.ignight.dna.elements.Music;
import com.ignight.dna.elements.SpendingLimit;
import com.ignight.group.Group;
import com.ignight.group.GroupDataSource;
import com.ignight.servlets.BaseServletHandler;
import com.ignight.servlets.admin.AggregateVenueDna;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.user.RedisKeys;
import com.ignight.user.User;
import com.ignight.user.UserDna;
import com.ignight.venue.VenueActivity;
import com.ignight.venue.VenueDataSource;
import com.ignight.venue.VenueTrending;
import com.stats.Stats;
import com.util.EmailSender;
import com.util.IgnightProperties;

import databases.Databases;
import databases.JedisConnection;
import databases.Resources;

public class DataServletHandler extends BaseServletHandler {
	private static final Logger log = LoggerFactory.getLogger(DataServletHandler.class);
	private static final AwsMessageSender awsQueue = AwsMessageSender.getInstance();
	public static final JedisConnection jedis = JedisConnection.getInstance();
	private static final ExecutorService executors = Executors.newFixedThreadPool(1);
	private static final IgnightProperties prop = IgnightProperties.getInstance();

	private String clearDailyDataSecretKey = "spduankzu7edb1eumkpd";

	public String getStats() {
		Stats stats = Stats.getInstance();
		return stats.getStats();
	}

	public String getUserInfo(JsonObject obj) throws SQLException {
		User user = null;
		if (obj.has("userId")) {
			long userId = obj.get("userId").getAsLong();
			user = UserDataSource.getCachedUser(userId);
		} else if (obj.has("username")) {
			String username = obj.get("username").getAsString();
			user = UserDataSource.getUser(username);
		}
		if (user == null) {
			return "No user with data: " + obj;
		}
		boolean cached = true;
		if (obj.has("cached")) {
			cached = obj.get("cached").getAsBoolean();
		}
		UserDna dna = null;
		if (cached) {
			dna = UserDataSource.getCachedUserDna(user.id);
		} else {
			dna = UserDataSource.getUserDna(user.id);
		}

		UserDnaData data = new UserDnaData();
		data.userId = dna.userId;
		data.username = user.username;
		data.email = user.email;
		data.gender = Gender.values()[user.genderId.intValue()];
		data.notifyofGroupInvites = user.notifyOfGroupInvites;
		data.city = City.values()[user.cityId.intValue()];
		if (dna != null) {
			data.age = Age.values()[dna.ageId.intValue()];
			data.spendingLimit = SpendingLimit.values()[dna.spendingLimit.intValue()];
			for (Long m : dna.musicIds) {
				if (m >= 0) {
					data.music.add(Music.values()[m.intValue()]);
				}
			}
			for (Long a : dna.atmosphereIds) {
				if (a >= 0) {
					data.atmosphere.add(Atmosphere.values()[a.intValue()]);
				}
			}
		}

		return gson.toJson(data);
	}

	public static class UserDnaData {
		public long userId;
		public String username;
		public String email;
		public Gender gender;
		public boolean notifyofGroupInvites = true;
		public City city;
		public Age age;
		public SpendingLimit spendingLimit;
		public List<Music> music = new ArrayList<Music>();
		public List<Atmosphere> atmosphere = new ArrayList<Atmosphere>();
	}

	public String getVenueTrending(JsonObject obj) throws SQLException {
		Long venueId = null;
		if (obj.has("venueId")) {
			venueId = obj.get("venueId").getAsLong();
		} else if (obj.has("venueName")) {
			String name = obj.get("venueName").getAsString();
			venueId = VenueDataSource.getVenueId(name);
		}
		if (venueId == null) {
			return "No venue with data: " + obj;
		}
		VenueTrending trending = VenueDataSource.getVenueTrending(venueId);
		TrendingValues tV = trending.trendingValues;
		TrendingVenueData data = new TrendingVenueData();
		if (tV.ageId >= 0) {
			data.age = Age.values()[tV.ageId.intValue()];
		}
		if (tV.spendingId >= 0) {
			data.spendingLimit = SpendingLimit.values()[tV.spendingId.intValue()];
		}
		if (tV.musicId >= 0) {
			data.music = Music.values()[tV.musicId.intValue()];
		}
		if (tV.atmosphereId >= 0) {
			data.atmosphere = Atmosphere.values()[tV.atmosphereId.intValue()];
		}
		for (long g : trending.trendingGroups) {
			Group group = GroupDataSource.getGroupInCache(g);
			if (group != null) {
				data.trendingGroups.add(group.name);
			} else {
				data.errors.add("No Group Found for id: " + g);
			}
		}
		return gson.toJson(data);
	}

	public static class TrendingVenueData {
		public Age age;
		public SpendingLimit spendingLimit;
		public Music music;
		public Atmosphere atmosphere;
		public List<String> trendingGroups = new ArrayList<String>();
		public List<String> errors = new ArrayList<String>();
	}

	public String getUserTrendingVenues(JsonObject obj) throws SQLException {
		User user = null;
		if (obj.has("userId")) {
			long userId = obj.get("userId").getAsLong();
			user = UserDataSource.getCachedUser(userId);
		} else if (obj.has("username")) {
			String username = obj.get("username").getAsString();
			user = UserDataSource.getUser(username);
		}
		if (user == null) {
			return "No user with data: " + obj;
		}
		List<TrendingVenue> trendingVenues = UserDataSource.calcTrendingVenues(user.id);
		return gson.toJson(getUserTrendingVenues(trendingVenues));
	}

	private List<UserTrendingVenue> getUserTrendingVenues(List<TrendingVenue> venues) {
		List<UserTrendingVenue> userVenues = new ArrayList<DataServletHandler.UserTrendingVenue>();
		for (TrendingVenue tv : venues) {
			UserTrendingVenue uv = new UserTrendingVenue();
			uv.venueName = tv.venueInfo.name;
			uv.dnaValue = tv.getDnaValue();
			uv.barColor = tv.getBarColor();
			uv.baseDnaValue = tv.unweightedDnaValue;
			userVenues.add(uv);
		}
		return userVenues;
	}

	public static class UserTrendingVenue {
		public String venueName;
		public double dnaValue;
		public BarColor barColor;
		public double baseDnaValue;
	}

	public String gettingAggregatedVenueDna(JsonObject obj) throws SQLException {
		Long venueId = null;
		if (obj.has("venueId")) {
			venueId = obj.get("venueId").getAsLong();
		} else if (obj.has("venueName")) {
			String name = obj.get("venueName").getAsString();
			venueId = VenueDataSource.getVenueId(name);
		}
		if (venueId == null) {
			return "No venue with data: " + obj;
		}
		String key = RedisKeys.getVenueActivityKey(venueId);
		Set<String> activity = jedis.smembers(key);
		VenueActivity venueActivity = new VenueActivity(venueId);
		for (String a : activity) {
			long val = Long.valueOf(a);
			if (val < 0) {
				venueActivity.downvotes.add(Math.abs(val));
			} else {
				venueActivity.upvotes.add(val);
			}
		}

		AggregateVenueDna agg = new AggregateVenueDna(venueActivity);
		CalcVenueDna calcVenueDna = agg.getCalcVenueDna();
		AggregatedVenueDna aggregatedVenueDna = new AggregatedVenueDna(agg, calcVenueDna);
		return gson.toJson(aggregatedVenueDna);
	}

	public static class AggregatedVenueDna {
		public AggregateVenueDna aggregated;
		public CalcVenueDna calcVenueDna;

		public AggregatedVenueDna(AggregateVenueDna aggregated, CalcVenueDna calcVenueDna) {
			this.aggregated = aggregated;
			this.calcVenueDna = calcVenueDna;
		}
	}

	/*
	 * Return 0: Archive not in progress and has not been done today -- Allow both redis clear and archive to happen 
	 * Return 1: Archive not in progress and archives have been performed today -- Allow redis clear but do not allow archiving
	 * Return 2: Archive in progress (other processes should not perform archiving and redis should not be cleared)
	 */
	public String clearDailyData(JSONObject request) throws JSONException, SQLException {
		String secretKey = getClearDailyDataSecretKey(request);
		Long cityId = getCityId(request);
		Boolean force = request.has("force") ? request.getBoolean("force") : false;
		return clearDailyData(secretKey, cityId, force);
	}

	public String clearDailyData(JsonObject request) {
		String secretKey = getClearDailyDataSecretKey(request);
		Long cityId = request.get("cityId").getAsLong();
		Boolean force = request.has("force") ? request.get("force").getAsBoolean() : false;
		return clearDailyData(secretKey, cityId, force);
	}

	private String clearDailyData(String secretKey, Long cityId, boolean force) {
		if (!secretKey.equals(clearDailyDataSecretKey)) {
			return createFailureResponeWithText("Wrong secret key");
		}

		int archiveDailyDataOutput = archiveDailyData(cityId, force);

		try {
			String from = "team@ignight.com";
			String to = "team@ignight.com";
			String subject = null;
			String body = null;
			if (IgnightProperties.getInstance().getEnv().equals("prod")) {
				if (archiveDailyDataOutput == 0 || force) { //Only clear daily data in production if data has not been cleared for day
					awsQueue.sendResetMessage();
					clearDailyDataRedis();
					subject = "[Prod] Clear Daily Data Status For " + City.getCity(cityId) + " [COMPLETE]";
					body = "Clear Daily Data Status. Operation was successfully completed";
				} else {
					subject = "[Prod] Clear Daily Data Status For " + City.getCity(cityId) + " [INCOMPLETE]";
					if (archiveDailyDataOutput == 1) {
						body = "Clear Daily Data Status. Operation was not completed because data has already been cleared for the day.";
					} else {
						body = "Clear Daily Data Status. Operation was not completed because archive is in progress.";
					}
				}
			} else {// For development, you can clear daily data for debugging purposes as long as archiving not in progress
				if (archiveDailyDataOutput != 2 || force) {
					awsQueue.sendResetMessage();
					clearDailyDataRedis();
					subject = "[Dev] Clear Daily Data Status For " + City.getCity(cityId) + " [COMPLETE]";
					body = "Clear Daily Data Status. Operation was successfully completed";
				} else {
					subject = "[Dev] Clear Daily Data Status For " + City.getCity(cityId) + " [INCOMPLETE]";
					body = "Clear Daily Data Status. Operation was not completed because archive is in progress.";
				}
			}
			Stats stats = Stats.getInstance();
			stats.clear();
			if (subject != null && body != null) {
				executors.execute(new EmailSender(from, to, subject, body));
			}
		} catch (Exception e) {
			log.error("Failed to send email", e);
		}

		return createSuccessResponse();
	}

	protected String clearAll(JSONObject request) throws JSONException, SQLException {
		String secretKey = getClearDailyDataSecretKey(request);
		if (!secretKey.equals(clearDailyDataSecretKey)) {
			return createFailureResponeWithText("Wrong secret key");
		}
		clearAll();
		return createSuccessResponse();
	}

	private void clearAll() throws SQLException {
		clearAllSql();
		clearAllRedis();
	}

	private void clearAllSql() throws SQLException {
		Databases.update("DROP TABLE IF EXISTS user_images");
		Databases.update("DROP TABLE IF EXISTS user_dna");
		Databases.update("DROP TABLE IF EXISTS user_push");
		Databases.update("DROP TABLE IF EXISTS venue_additions");
		Databases.update("DROP TABLE IF EXISTS venue_errors");
		Databases.update("DROP TABLE IF EXISTS group_invites");
		Databases.update("DROP TABLE IF EXISTS group_members");
		Databases.update("DROP TABLE IF EXISTS groups");
		Databases.update("DROP TABLE IF EXISTS users");
	}

	private int archiveDailyData(Long cityId, boolean force) {
		Date date = new Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000);

		try {
			int archivesCaptured = archivesCapturedForToday(cityId, date);

			if (archivesCaptured == 0 || force) {
				Set<Long> userIds = UserDataSource.getListOfUserIdsForCity(cityId);
				archiveUserActivity(userIds, cityId, date);

				Set<Long> venueIds = VenueDataSource.getListOfVenueIdsForCity(cityId);
				archiveVenueBuzz(venueIds, cityId);
				archiveVenueCrowdStats(venueIds, cityId, date);

				Set<Long> groupIds = GroupDataSource.getListOfGroupIdsForCity(cityId);
				archiveGroupBuzz(groupIds, cityId);

				updateLastArchiveDate(cityId, date);
			}
			return archivesCaptured;
		} catch (SQLException e) {
			log.error("Failed to archive daily data", e);
			return -1;
		}
	}

	private static void updateLastArchiveDate(Long cityId, Date date) throws SQLException {
		log.info("Preparing to update last archive date to {} for cityId:{}", date, cityId);
		Connection conn = null;
		PreparedStatement ps = null;
		String sql = "UPDATE city_archive_status SET archive_in_progress = 0, last_archive_date = '" + date + "' where city_id = " + cityId;

		try {
			conn = Databases.dataSourceArchiving.getConnection();
			ps = conn.prepareStatement(sql);
			ps.execute();

			DataSource.setArchiveInProgressStatus(cityId, 0);

		} finally {
			Resources.close(conn, ps);
			log.info("Finished updating last archive date to {} for cityId:{}", date, cityId);
		}

	}

	/*
	 * Return 0: Archive not in progress and has not been done today -- Allow both redis clear and archive to happen 
	 * Return 1: Archive not in progress and archives have been performed today -- Allow redis clear but do not allow archiving
	 * Return 2: Archive in progress (other processes should not perform archiving and redis should not be cleared)
	 */
	private static int archivesCapturedForToday(Long cityId, Date date) throws SQLException {
		log.info("Checking if data has been archived for cityId: {} on {}", cityId, date.toString());
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs;
		String sql = "Select archive_in_progress, last_archive_date from city_archive_status where city_id=" + cityId;
		int output = 0;
		try {
			conn = Databases.dataSourceArchiving.getConnection();
			ps = conn.prepareStatement(sql);

			rs = ps.executeQuery();
			int i = 0;
			if (rs.next()) {
				i = rs.getInt("archive_in_progress");

				if (i == 0) {
					Resources.close(ps);
					sql = "Select last_archive_date from city_archive_status where city_id=" + cityId;
					ps = conn.prepareStatement(sql);
					rs = ps.executeQuery();

					if (rs.next()) {
						Date lastArchiveDate = new Date(0);
						lastArchiveDate = rs.getDate("last_archive_date");
						if (areDatesSameDay(date, lastArchiveDate))
							output = 1;
					}
				} else {
					output = 2;
				}

			} else {
				Resources.close(ps);
				sql = "Insert INTO city_archive_status (city_id, archive_in_progress) VALUES(?, ?) "
						+ "ON DUPLICATE KEY UPDATE "
						+ "archive_in_progress = ?";

				ps = conn.prepareStatement(sql);
				ps.setObject(1, cityId);
				ps.setObject(2, 1);
				ps.setObject(3, 1);
				ps.execute();
			}

		} finally {
			Resources.close(conn, ps);
			log.info("Finished checking if data has been archived for cityId: {} on {}. Result = {}", cityId, date.toString(), output);
		}
		if (output < 2) {
			DataSource.setArchiveInProgressStatus(cityId, 0);
		} else {
			DataSource.setArchiveInProgressStatus(cityId, 1);
		}
		return output;
	}

	private static boolean areDatesSameDay(Date d1, Date d2) {
		Calendar cal1 = new GregorianCalendar();
		cal1.setTime(d1);

		Calendar cal2 = new GregorianCalendar();
		cal2.setTime(d2);

		if (cal1.get(Calendar.YEAR) != cal2.get(Calendar.YEAR))
			return false;
		if (cal1.get(Calendar.DAY_OF_YEAR) != cal2.get(Calendar.DAY_OF_YEAR))
			return false;
		return true;

	}

	private static void archiveUserActivity(Set<Long> userIds, Long cityId, Date date) throws SQLException {
		log.info("Preparing to archive user activity for cityId: {} on {}", cityId, date.toString());
		Connection conn = null;
		PreparedStatement ps = null;
		String sql = "INSERT INTO user_votes " +
				"(city_id, user_id, venue_id, vote_type, date, day_of_week) VALUES (?, ?, ?, ?, ?, ?) "
				+ "ON DUPLICATE KEY UPDATE "
				+ "vote_type = ?, "
				+ "day_of_week = ?";

		try {
			conn = Databases.dataSourceArchiving.getConnection();
			ps = conn.prepareStatement(sql);

			for (Long userId : userIds) {
				Set<Long> userActivity = UserDataSource.getUserActivity(userId);

				for (Long activity : userActivity) {

					ps.setObject(1, cityId);
					ps.setObject(2, userId);
					ps.setObject(3, Math.abs(activity));
					ps.setObject(4, Long.signum(activity));
					ps.setObject(5, date);

					Calendar cal = new GregorianCalendar();
					cal.setTime(date);
					ps.setObject(6, cal.get(Calendar.DAY_OF_WEEK));
					ps.setObject(7, Long.signum(activity));
					ps.setObject(8, cal.get(Calendar.DAY_OF_WEEK));
					ps.addBatch();
				}
			}
			ps.executeBatch();

		} finally {
			Resources.close(conn, ps);
			log.info("Finished action to archive user activity for cityId: {} on {}", cityId, date.toString());
		}
	}

	private static void archiveVenueBuzz(Set<Long> venueIds, Long cityId) throws SQLException {
		log.info("Preparing to archive venue buzz for cityId: {}", cityId);
		Connection conn = null;
		PreparedStatement ps = null;
		String sql = "INSERT INTO venue_buzz " +
				"(buzz_id, user_id, city_id, venue_id, buzz_type, buzz_text, date, day_of_week) VALUES (?, ?, ?, ?, ?, ?, ?, ?) "
				+ "ON DUPLICATE KEY UPDATE "
				+ "user_id = ?, "
				+ "buzz_type = ?, "
				+ "buzz_text = ?, "
				+ "day_of_week = ?";

		try {
			conn = Databases.dataSourceArchiving.getConnection();
			ps = conn.prepareStatement(sql);

			for (Long venueId : venueIds) {
				List<Buzz> venueBuzz = VenueDataSource.getAllVenueBuzz(venueId);

				for (Buzz buzz : venueBuzz) {
					ps.setObject(1, buzz.buzzId);
					ps.setObject(2, buzz.userId);
					ps.setObject(3, cityId);
					ps.setObject(4, venueId);
					ps.setObject(5, buzz.buzzType.toString());
					ps.setObject(6, buzz.buzzText);

					Date buzzDate = new Date(buzz.buzzDateStamp);

					ps.setObject(7, buzzDate);
					Calendar cal = new GregorianCalendar();
					cal.setTime(buzzDate);
					ps.setObject(8, cal.get(Calendar.DAY_OF_WEEK));

					ps.setObject(9, buzz.userId);
					ps.setObject(10, buzz.buzzType.toString());
					ps.setObject(11, buzz.buzzText);
					ps.setObject(12, cal.get(Calendar.DAY_OF_WEEK));
					ps.addBatch();
				}
			}
			ps.executeBatch();

		} finally {
			Resources.close(conn, ps);
			log.info("Finished archiving venue buzz for for cityId: {}", cityId);
		}
	}

	private static void archiveVenueCrowdStats(Set<Long> venueIds1, Long cityId, Date date) throws SQLException {
		log.info("Preparing to archive venue crowd stats for cityId: {} on {}", cityId, date.toString());
		Connection conn = null;
		PreparedStatement ps = null;
		String sql = "INSERT INTO venue_crowd_stats " +
				"(venue_id, city_id, age_bucket_mode, atmosphere_type_mode, music_type_mode, avg_spending_limit, "
				+ "trending_group_id_1, trending_group_id_2, trending_group_id_3,"
				+ " trending_group_id_4,  trending_group_id_5, date, day_of_week) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "
				+ "ON DUPLICATE KEY UPDATE "
				+ "age_bucket_mode = ?, "
				+ "atmosphere_type_mode = ?, "
				+ "music_type_mode = ?, "
				+ "avg_spending_limit = ?,"
				+ "trending_group_id_1 = ?, "
				+ "trending_group_id_2 = ?, "
				+ "trending_group_id_3 = ?, "
				+ "trending_group_id_4 = ?, "
				+ "trending_group_id_5 = ?, "
				+ "day_of_week = ?";

		Set<Long> venueIds = new HashSet<Long>();
		for (int i = 0; i < 50; i++) {
			venueIds.add((long) i);
		}

		try {
			conn = Databases.dataSourceArchiving.getConnection();
			ps = conn.prepareStatement(sql);

			for (Long venueId : venueIds) {
				VenueTrending venueDna = VenueDataSource.getVenueTrending(venueId);

				ps.setObject(1, venueId);
				ps.setObject(2, cityId);
				ps.setObject(3, venueDna.trendingValues.ageId);
				ps.setObject(4, venueDna.trendingValues.atmosphereId);
				ps.setObject(5, venueDna.trendingValues.musicId);
				ps.setObject(6, venueDna.trendingValues.spendingId);

				int noOfTrendingGroups = venueDna.trendingGroups.size();

				for (int i = 0; i < 5; i++) {
					if (i >= noOfTrendingGroups) {
						ps.setObject(i + 7, -1);
						ps.setObject(i + 18, -1);
					} else {
						ps.setObject(i + 7, venueDna.trendingGroups.get(i));
						ps.setObject(i + 18, venueDna.trendingGroups.get(i));
					}

				}

				ps.setObject(12, date);
				Calendar cal = new GregorianCalendar();
				cal.setTime(date);
				ps.setObject(13, cal.get(Calendar.DAY_OF_WEEK));
				ps.setObject(14, venueDna.trendingValues.ageId);
				ps.setObject(15, venueDna.trendingValues.atmosphereId);
				ps.setObject(16, venueDna.trendingValues.musicId);
				ps.setObject(17, venueDna.trendingValues.spendingId);
				ps.setObject(23, cal.get(Calendar.DAY_OF_WEEK));
				ps.addBatch();
			}
			ps.executeBatch();

		} finally {
			Resources.close(conn, ps);
			log.info("Finished action to archive venue crowd stats for cityId: {} on {}", cityId, date.toString());
		}
	}

	private static void archiveGroupBuzz(Set<Long> groupIds, Long cityId) throws SQLException {
		log.info("Preparing to archive group buzz for cityId: {}", cityId);
		Connection conn = null;
		PreparedStatement ps = null;
		String sql = "INSERT INTO group_buzz " +
				"(buzz_id, user_id, city_id, group_id, buzz_type, buzz_text, date, day_of_week) VALUES (?, ?, ?, ?, ?, ?, ?, ?) "
				+ "ON DUPLICATE KEY UPDATE "
				+ "user_id = ?, "
				+ "buzz_type = ?, "
				+ "buzz_text = ?, "
				+ "day_of_week = ?";

		try {
			conn = Databases.dataSourceArchiving.getConnection();
			ps = conn.prepareStatement(sql);

			for (Long groupId : groupIds) {
				Long lastArchivedBuzzId = GroupDataSource.getLastArchivedBuzzIdForGroup(cityId, groupId);
				List<Buzz> groupBuzz = GroupDataSource.getAllNewGroupBuzz(groupId, lastArchivedBuzzId);

				for (Buzz buzz : groupBuzz) {
					ps.setObject(1, buzz.buzzId);
					ps.setObject(2, buzz.userId);
					ps.setObject(3, cityId);
					ps.setObject(4, groupId);
					ps.setObject(5, buzz.buzzType.toString());
					ps.setObject(6, buzz.buzzText);

					Date buzzDate = new Date(buzz.buzzDateStamp);
					ps.setObject(7, buzzDate);
					Calendar cal = new GregorianCalendar();
					cal.setTime(buzzDate);
					ps.setObject(8, cal.get(Calendar.DAY_OF_WEEK));

					ps.setObject(9, buzz.userId);
					ps.setObject(10, buzz.buzzType.toString());
					ps.setObject(11, buzz.buzzText);
					ps.setObject(12, cal.get(Calendar.DAY_OF_WEEK));
					ps.addBatch();
				}
			}
			ps.executeBatch();

		} finally {
			Resources.close(conn, ps);
			log.info("Finished action to archive group buzz for cityId: {}", cityId);
		}
	}

	private void clearDailyDataRedis() {
		clearUserActivity();
		clearUserTrending();
		clearUserBuzzListening();
		clearVenueDna();
		clearVenueActivity();
		//clearVenueBuzz();
		clearGroupTrending();
		clearGroupBuzz();
		clearUserGreenVenues();
	}

	private void clearAllRedis() {
		Set<String> keys = jedis.keys("*");
		deleteKeys(keys);
	}

	private void clearUserActivity() {
		Set<String> keys = jedis.keys(RedisKeys.USER_ACTIVITY_SET_KEY_PREFIX + "*");
		deleteKeys(keys);
	}

	private void clearUserTrending() {
		Set<String> keys = jedis.keys(RedisKeys.USER_TRENDING_VENUES_KEY_PREFIX + "*");
		deleteKeys(keys);
		keys = jedis.keys(RedisKeys.USER_TRENDING_BAR_COLOR_PREFIX + "*");
		deleteKeys(keys);
		keys = jedis.keys(RedisKeys.USER_TRENDING_BAR_REFRESH + "*");
		deleteKeys(keys);
	}

	private void clearUserGreenVenues(){
		Set<String> keys = jedis.keys(RedisKeys.USERS_GREEN_VENUES_HASH_KEY_PREFIX + "*");
		deleteKeys(keys);
	}
	
	
	private void clearUserBuzzListening() {
		Set<String> keys = jedis.keys(RedisKeys.USER_CURRENT_BUZZ_LISTENER_PREFIX + "*");
		deleteKeys(keys);
	}

	private void clearVenueDna() {
		Set<String> keys = jedis.keys(RedisKeys.VENUE_TRENDING_KEY_PREFIX + "*");
		deleteKeys(keys);
	}

	private void clearVenueActivity() {
		Set<String> keys = jedis.keys(RedisKeys.VENUE_ACTIVITY_KEY_PREFIX + "*");
		deleteKeys(keys);
	}

	private void clearVenueBuzz() {
		Set<String> keys = jedis.keys(RedisKeys.VENUE_BUZZ_PREFIX + "*");
		deleteKeys(keys);

		keys = jedis.keys(RedisKeys.VENUE_BUZZ_ID_PREFIX + "*");
		deleteKeys(keys);

		keys = jedis.keys(RedisKeys.VENUE_CURRENT_BUZZ_LISTENERS_PREFIX + "*");
		deleteKeys(keys);

		deleteKey(RedisKeys.VENUE_BUZZ_ACTIVE_SET);
	}

	public static void clearGroupBuzz() {
		Set<String> keys = jedis.keys(RedisKeys.GROUP_BUZZ_PREFIX + "*");

		for (String key : keys) {
			Long buzzSize = jedis.hlen(key);
			if (buzzSize > prop.getnNoOfGroupBuzzToKeep()) {
				String[] keySplit = key.split("::");
				String groupId = keySplit[1];

				String nextBuzzIdString = jedis.get(RedisKeys.getGroupBuzzIdPrefix(Long.valueOf(groupId)));
				long lastBuzzId = Long.valueOf(nextBuzzIdString) - 1;
				long firstBuzzIdToDelete = lastBuzzId - buzzSize + 1;
				long lastBuzzIdToDelete = lastBuzzId - prop.getnNoOfGroupBuzzToKeep();

				List<String> fields = new ArrayList<String>();
				for (long i = firstBuzzIdToDelete; i <= lastBuzzIdToDelete; i++) {
					fields.add(String.valueOf(i));
				}
				log.info("Clearing Buzz For Group: {}, from={}, to={}", groupId, firstBuzzIdToDelete, lastBuzzIdToDelete);
				jedis.hdel(key, (String[]) fields.toArray());
			}
		}
	}

	private void clearGroupTrending() {
		Set<String> keys = jedis.keys(RedisKeys.GROUP_TRENDING_KEY_PREFIX + "*");
		deleteKeys(keys);

		keys = jedis.keys(RedisKeys.GROUP_CURRENT_BUZZ_LISTENERS_PREFIX + "*");
		deleteKeys(keys);
	}

	private void deleteKeys(Set<String> keys) {
		for (String key : keys) {
			boolean del = jedis.del(key);
			if (del) {
				log.info("Deleted key : {}", key);
			} else {
				log.error("Failed to delete key: {}", key);
			}
		}
	}

	private void deleteKey(String key) {
		boolean del = jedis.del(key);
		if (del) {
			log.info("Deleted key : {}", key);
		} else {
			log.error("Failed to delete key: {}", key);
		}
	}

	protected String reset(JSONObject request) throws JSONException, SQLException {
		String secretKey = getClearDailyDataSecretKey(request);
		if (!secretKey.equals(clearDailyDataSecretKey)) {
			return createFailureResponeWithText("Wrong secret key");
		}

		reset();

		return createSuccessResponse();
	}

	public String reset(JsonObject request) throws SQLException {
		String secretKey = getClearDailyDataSecretKey(request);
		if (!secretKey.equals(clearDailyDataSecretKey)) {
			return createFailureResponeWithText("Wrong secret key");
		}

		reset();

		return createSuccessResponse();
	}

	private void reset() throws SQLException {
		clearAll();
		recreateSqlTables();
		VenueDataSource.loadAndSaveVenuesToRedis();
		AwsMessageSender.getInstance().sendClearMessage();
	}

	private void recreateSqlTables() throws SQLException {
		Databases.update("CREATE TABLE users ( " +
				"id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT," +
				"city_id INT UNSIGNED DEFAULT NULL, " +
				"email VARCHAR(255) NOT NULL, " +
				"username VARCHAR(32) NOT NULL," +
				"password VARCHAR(32) NOT NULL," +
				"gender_id INT UNSIGNED DEFAULT NULL," +
				"notify_of_group_invites BOOLEAN DEFAULT TRUE, " +
				"profile_picture_url TEXT DEFAULT NULL,created_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
				"modified_time TIMESTAMP ON UPDATE CURRENT_TIMESTAMP," +
				"deleted_time TIMESTAMP NULL DEFAULT NULL," +
				"UNIQUE (email, username)," +
				"PRIMARY KEY (id))");

		Databases.update("ALTER TABLE users AUTO_INCREMENT=1");

		Databases.update("CREATE TABLE user_dna " +
				"(user_id BIGINT UNSIGNED NOT NULL," +
				"city_id INT UNSIGNED DEFAULT NULL," +
				"age_id INT UNSIGNED NOT NULL," +
				"spending_limit INT UNSIGNED NOT NULL," +
				"music_choice_1 INT UNSIGNED NOT NULL," +
				"music_choice_2 INT DEFAULT -1," +
				"music_choice_3 INT DEFAULT -1," +
				"atmosphere_choice_1 INT UNSIGNED NOT NULL," +
				"atmosphere_choice_2 INT DEFAULT -1," +
				"atmosphere_choice_3 INT DEFAULT -1," +
				"created_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
				"modified_time TIMESTAMP ON UPDATE CURRENT_TIMESTAMP," +
				"FOREIGN KEY (user_id) REFERENCES users(id)," +
				"PRIMARY KEY (user_id))");

		Databases.update("CREATE TABLE user_images " +
				"(user_id BIGINT UNSIGNED NOT NULL," +
				"profile_picture_url TEXT DEFAULT NULL," +
				"chat_picture_url TEXT DEFAULT NULL," +
				"FOREIGN KEY (user_id) REFERENCES users(id)," +
				"PRIMARY KEY (user_id))");

		Databases.update("CREATE TABLE venue_errors " +
				"(id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT," +
				"user_id BIGINT UNSIGNED NOT NULL," +
				"venue_id BIGINT UNSIGNED NOT NULL," +
				"name TEXT NOT NULL," +
				"city_id INT UNSIGNED NOT NULL," +
				"address VARCHAR(255) DEFAULT NULL," +
				"number VARCHAR(32) DEFAULT NULL," +
				"url TEXT DEFAULT NULL," +
				"comments TEXT DEFAULT NULL," +
				"created_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
				"deleted_time TIMESTAMP NULL DEFAULT NULL," +
				"PRIMARY KEY (id, venue_id))");

		Databases.update("CREATE TABLE venue_additions " +
				"(id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT," +
				"user_id BIGINT UNSIGNED NOT NULL," +
				"name TEXT NOT NULL," +
				"city_id INT UNSIGNED NOT NULL," +
				"address VARCHAR(255) DEFAULT NULL," +
				"number VARCHAR(32) DEFAULT NULL," +
				"url TEXT DEFAULT NULL," +
				"comments TEXT DEFAULT NULL," +
				"created_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
				"deleted_time TIMESTAMP NULL DEFAULT NULL," +
				"PRIMARY KEY (id))");

		Databases.update("CREATE TABLE groups  " +
				"(id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT," +
				"name VARCHAR(128) NOT NULL," +
				"city_id INT UNSIGNED NOT NULL," +
				"admin_id BIGINT UNSIGNED DEFAULT NULL," +
				"description VARCHAR(255) NOT NULL," +
				"private BOOLEAN DEFAULT FALSE," +
				"created_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
				"deleted_time TIMESTAMP NULL DEFAULT NULL," +
				"CONSTRAINT uc_name UNIQUE (name, deleted_time)," +
				"PRIMARY KEY (id))");

		Databases.update("CREATE TABLE group_members " +
				"(user_id BIGINT UNSIGNED NOT NULL," +
				"group_id BIGINT UNSIGNED NOT NULL," +
				"FOREIGN KEY (user_id) REFERENCES users(id)," +
				"FOREIGN KEY (group_id) REFERENCES groups(id)," +
				"CONSTRAINT uc_member UNIQUE (user_id, group_id)," +
				"PRIMARY KEY (user_id, group_id))");

		Databases.update("CREATE TABLE group_invites " +
				"(user_id BIGINT UNSIGNED NOT NULL," +
				"invitee_id BIGINT UNSIGNED NOT NULL," +
				"group_id BIGINT UNSIGNED NOT NULL," +
				"FOREIGN KEY (user_id) REFERENCES users(id)," +
				"FOREIGN KEY (invitee_id) REFERENCES users(id)," +
				"CONSTRAINT uc_group_invite UNIQUE (user_id, group_id)," +
				"PRIMARY KEY (user_id, group_id))");
	}
}
