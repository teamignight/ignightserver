package com.ignight.servlets;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public abstract class BaseServletHandler {
	private static final Logger log = LoggerFactory.getLogger(ResponseUtil.class);

	protected static Gson gson = new GsonBuilder().disableHtmlEscaping().create();

	protected JSONArray getListOfValues(JSONObject request, String type, long max) throws JSONException {
		return request.getJSONArray(type);
	}

	protected String getIOSDeviceToken(JSONObject request) throws JSONException {
		return request.getString("iOSDeviceToken");
	}

	protected String getAndroidRegistrationId(JSONObject request) throws JSONException {
		return request.getString("androidRegistrationId");
	}

	protected String getIgnightMessageSubject(JSONObject request) throws JSONException {
		return request.getString("ignightMessageSubject");
	}

	protected String getIgnightMessageBody(JSONObject request) throws JSONException {
		return request.getString("ignightMessageBody");
	}

	protected long getUserId(JSONObject request) throws NumberFormatException, JSONException {
		return request.getLong("userId");
	}

	protected long getMemberId(JSONObject request) throws NumberFormatException, JSONException {
		return request.getLong("memberId");
	}

	protected long getUserId(Map<String, String[]> request) {
		return Long.valueOf(request.get("userId")[0]);
	}

	protected long getConnectTargetUserId(JSONObject request) throws NumberFormatException, JSONException {
		return request.getLong("connectTargetUserId");
	}

	protected long getConnectRequestorUserId(JSONObject request) throws NumberFormatException, JSONException {
		return request.getLong("connectRequestorUserId");
	}

	protected long getTargetUserId(JSONObject request) throws NumberFormatException, JSONException {
		return request.getLong("targetUserId");
	}

	protected String getConnectMessageText(JSONObject request) throws JSONException {
		return request.getString("connectMessageText");
	}

	protected String getConnectionId(JSONObject request) throws JSONException {
		return request.getString("connectionId");
	}

	protected long getUserIdToBlock(JSONObject request) throws NumberFormatException, JSONException {
		return request.getLong("userIdToBlock");
	}

	protected long getUserIdToUnBlock(JSONObject request) throws NumberFormatException, JSONException {
		return request.getLong("userIdToUnBlock");
	}

	protected String getUserName(Map<String, String[]> request) {
		return request.get("userName")[0].toLowerCase();
	}

	protected String getEmail(Map<String, String[]> request) {
		return request.get("email")[0].toLowerCase();
	}

	protected String getEmail(JSONObject request) throws JSONException {
		if (request.has("email")) {
			return request.getString("email").toLowerCase();
		}
		return null;
	}

	protected String getOldEmail(JSONObject request) throws JSONException {
		return request.getString("oldEmail").toLowerCase();
	}

	protected String getUserName(JSONObject request) throws JSONException {
		return request.getString("userName").toLowerCase();
	}

	protected String getFirstName(JSONObject request) throws JSONException {
		return request.getString("firstName");
	}

	protected String getLastName(JSONObject request) throws JSONException {
		return request.getString("lastName");
	}

	protected String getPicture(JSONObject request) throws JSONException {
		return request.getString("picture");
	}

	protected long getVenueId(JSONObject request) throws JSONException {
		return request.getLong("venueId");
	}

	protected long getVenueId(Map<String, String[]> request) {
		return Long.valueOf(request.get("venueId")[0]);
	}

	protected long getUserActivity(JSONObject request) throws JSONException {
		return request.getInt("userActivity");
	}

	protected long getBuzzId(JSONObject request) throws JSONException {
		return request.getLong("buzzId");
	}

	protected String getVenueCityId(JSONObject request) throws JSONException {
		return request.getString("venueCityId");
	}

	protected boolean getNotifyOfGroupInvites(JSONObject request) throws JSONException {
		return request.getBoolean("notifyOfGroupInvites");
	}

	protected boolean getDisplayNameInGroups(JSONObject request) throws JSONException {
		return request.getBoolean("displayNameInGroups");
	}

	protected String getClearDailyDataSecretKey(JSONObject request) throws JSONException {
		return request.getString("dailyDataSecretKey");
	}

	protected String getClearDailyDataSecretKey(JsonObject request) {
		return request.get("dailyDataSecretKey").getAsString();
	}

	protected String getPassword(JSONObject request) throws JSONException {
		return request.getString("password");
	}

	protected String getTempPassword(JSONObject request) throws JSONException {
		return request.getString("tempPassword");
	}

	protected boolean getIsTempPassword(JSONObject request) throws JSONException {
		if (request.has("isTempPassword"))
			return true;
		return false;
	}

	protected Long getAge(JSONObject request) throws JSONException {
		if (request.has("age")) {
			return request.getLong("age");
		}
		return null;
	}

	protected Long getGenderId(JSONObject request) throws JSONException {
		return request.getLong("genderId");
	}

	protected Long getCityId(JSONObject request) throws JSONException {
		return request.getLong("cityId");
	}

	protected Long getNewCityId(JSONObject request) throws JSONException {
		return request.getLong("newCityId");
	}

	protected Long getGroupId(JSONObject request) throws JSONException {
		return request.getLong("groupId");
	}

	protected String getGroupName(JSONObject request) throws JSONException {
		return request.getString("groupName");
	}

	protected String getGroupDescription(JSONObject request) throws JSONException {
		if (request.has("groupDescription"))
			return request.getString("groupDescription");
		return null;
	}

	protected String getGroupNameSearchString(JSONObject request) throws JSONException {
		return request.getString("groupNameSearchString");
	}

	protected String getSearchString(Map<String, String[]> request) {
		return (request.containsKey("search")) ? request.get("search")[0].toLowerCase() : null;
	}

	protected Boolean getIsPrivateGroup(JSONObject request) throws JSONException {
		return request.getBoolean("isPrivateGroup");
	}

	protected Boolean getIsGroupSearchable(JSONObject request) throws JSONException {
		if (request.has("isGroupSearchable"))
			return request.getBoolean("isGroupSearchable");
		return false;
	}

	protected String getExistingUserInfoJSON(JSONObject request) throws JSONException {
		return request.getString("existingUserInfo");
	}

	protected Long getPrivateGroupAdminId(JSONObject request) throws JSONException {
		return request.getLong("groupAdminId");
	}

	protected String getState(JSONObject request) throws JSONException {
		return request.getString("state");
	}

	protected String getVenueName(JSONObject request) throws JSONException {
		return request.getString("venueName");
	}

	protected String getAddress(JSONObject request) throws JSONException {
		return request.getString("address");
	}

	protected long getZip(JSONObject request) throws JSONException {
		return request.getLong("zip");
	}

	protected String getNumber(JSONObject request) throws JSONException {
		return request.getString("number");
	}

	protected String getUrl(JSONObject request) throws JSONException {
		return request.getString("url");
	}

	protected String getComment(JSONObject request) throws JSONException {
		return request.getString("comment");
	}

	protected long getNoOfBuzzElements(Map<String, String[]> request) {
		return (request.containsKey("noOfBuzzElements")) ? Long.valueOf(request.get("noOfBuzzElements")[0]) : 0;
	}

	protected String getBuzzText(JSONObject request) throws JSONException {
		return request.getString("buzzText");
	}

	protected String getBuzzType(JSONObject request) throws JSONException {
		return request.getString("buzzType");
	}

	protected Boolean getUserConnectionAvailabilityStatus(JSONObject request) throws JSONException {
		return request.getBoolean("userConnectionAvailabilityStatus");
	}

	protected int getTrendingFilter(JSONObject request) throws JSONException {
		return request.getInt("trendingFilter");
	}

	protected Integer getVenueStartIndex(JSONObject request) throws JSONException {
		return request.getInt("venueStartIndex");
	}

	protected Integer getTrendingVenueStartIndex(JSONObject request) throws JSONException {
		return request.getInt("trendingVenueStartIndex");
	}

	protected Integer getTrendingNoOfVenues(JSONObject request) throws JSONException {
		return request.getInt("trendingNoOfVenues");
	}

	protected String getNotificationId(JSONObject request) throws JSONException {
		return request.getString("notificationId");
	}

	/***************************
	 * Response Creators
	 * 
	 * @throws JSONException
	 **************************/

	public static String createSuccessResponse() {
		Map<String, Object> obj = new HashMap<String, Object>();
		obj.put("res", true);
		return gson.toJson(obj);
	}

	public static String createSuccessResponseWithText(String text) {
		Map<String, Object> obj = new HashMap<String, Object>();
		obj.put("res", true);
		obj.put("body", text);
		return gson.toJson(obj);
	}

	public static String createFailureResponeWithText(String text) {
		Map<String, Object> obj = new HashMap<String, Object>();
		obj.put("res", false);
		obj.put("reason", text);
		return gson.toJson(obj);
	}
	
	public static String createFailureResponeForBlockedCall(String text) {
		Map<String, Object> obj = new HashMap<String, Object>();
		obj.put("res", false);
		obj.put("reason", text);
		return gson.toJson(obj);
	}

	public static String createSuccessResponseWithBody(Object obj) {
		Map<String, Object> ret = new HashMap<String, Object>();
		ret.put("res", true);
		ret.put("body", obj);
		return gson.toJson(ret);
	}

	public static String createSuccessResponseWithJson(String json) {
		Map<String, Object> ret = new HashMap<String, Object>();
		ret.put("res", true);
		ret.put("body", json);
		return gson.toJson(ret);
	}
}
