package com.ignight.servlets.venue;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.buzz.Buzz;
import com.buzz.Buzz.BuzzType;
import com.buzz.ClientBuzzData;
import com.ignight.aws.AwsMessageSender;
import com.ignight.core.CoreData;
import com.ignight.group.Group;
import com.ignight.group.GroupDataSource;
import com.ignight.servlets.BaseServletHandler;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.user.UserServletHandler;
import com.ignight.user.User;
import com.ignight.user.UserImages;
import com.ignight.venue.Venue;
import com.ignight.venue.VenueDataSource;
import com.ignight.venue.VenueDataSource.ClientBuzzImageData;
import com.ignight.venue.VenueReport;
import com.ignight.venue.VenueTrending;
import com.push.PushUtil;

public class VenueServletHandler extends BaseServletHandler {
	private static final Logger log = LoggerFactory.getLogger(UserServletHandler.class);
	private static final AwsMessageSender awsQueue = AwsMessageSender.getInstance();

	/*
	 * GET REQUESTS
	 */

	public String getVenueDataForUser(User user, Venue venue) {
		return createVenueDataForUserResponse(user, venue);
	}

	private String createVenueDataForUserResponse(User user, Venue venue) {
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("venue", venue.toDataInMap());
		data.put("buzzActive", VenueDataSource.isVenueBuzzActive(venue.id));

		Map<String, Object> crowdStats = new HashMap<String, Object>();

		VenueTrending venueDna = VenueDataSource.getVenueTrending(venue.id);
		crowdStats.put("musicTypeMode", venueDna.trendingValues.musicId);
		crowdStats.put("atmosphereTypeMode", venueDna.trendingValues.atmosphereId);
		crowdStats.put("ageBucketMode", venueDna.trendingValues.ageId);
		crowdStats.put("avgSpendingLimit", venueDna.trendingValues.spendingId);
		data.put("venueStats", crowdStats);

		Set<Long> venueActivity = UserDataSource.getUserActivity(user.id);
		data.put("userInput", getVenueActivityForUser(venueActivity, venue.id));
		data.put("userBarColor", UserDataSource.getUserVenueBarColor(user.id, venue.id));

		List<Map<String, Object>> trendingGroups = new ArrayList<Map<String, Object>>();
		for (Long groupId : venueDna.trendingGroups) {
			Map<String, Object> group = new HashMap<String, Object>();
			group.put("groupId", groupId);
			Group g = GroupDataSource.getGroupInCache(groupId);
			Long groupSize = GroupDataSource.getGroupSize(groupId);
			if (g != null && groupSize > 0) {
				group.put("groupName", g.name);
				group.put("privateGroup", g.isPrivate);
				group.put("isMemberOfGroup", GroupDataSource.isMemberOfGroup(groupId, user.id));
				trendingGroups.add(group);
			}
		}
		data.put("venueEventInfo", VenueDataSource.getVenueEventInfo(venue.id));
		data.put("trendingGroups", trendingGroups);
		data.put("placeImage", VenueDataSource.getGooglePlaceImagesArray(venue.id));
		data.put("priceLevel", venue.priceLevel);
		data.put("hours", venue.hours);
		data.put("rating", venue.rating);

		return createSuccessResponseWithBody(data);
	}

	private long getVenueActivityForUser(Set<Long> venueActivity, Long venueId) {
		Long upvote = venueId;
		Long downvote = venueId * -1;
		if (venueActivity.contains(upvote)) {
			return 1;
		}
		if (venueActivity.contains(downvote)) {
			return -1;
		}
		return 0;
	}

	public String getLatestVenueBuzz(User user, Venue venue, Long lastBuzzId, Integer count) {
		return getBuzz(venue.id, user.id, lastBuzzId, count, true);
	}

	public String getVenueBuzz(User user, Venue venue, Long lastBuzzId, Integer count, Boolean latest) {
		return getBuzz(venue.id, user.id, lastBuzzId, count, latest);
	}

	private String getBuzz(Long venueId, Long userId, Long lastBuzzId, int count, boolean latest) {
		log.debug("Get Latest Venue Buzz - venueId = {}, lastBuzzId = {}, count = {}", venueId, lastBuzzId, count);

		User user = UserDataSource.getCachedUser(userId);
		if (user == null) {
			return createFailureResponeWithText("User does not exist.");
		}

		Venue venue = CoreData.getVenue(venueId);
		if (venue == null) {
			return createFailureResponeWithText("Venue does not exist.");
		}

		List<Buzz> buzzes = VenueDataSource.getBuzz(venueId, lastBuzzId, count, latest);

		List<ClientBuzzData> output = new ArrayList<ClientBuzzData>();
		for (Buzz b : buzzes) {
			ClientBuzzData out = new ClientBuzzData(b);
			User buzzUser = UserDataSource.getCachedUser(out.userId);
			if (buzzUser == null) {
				log.error("Buzz user does not exist: {}", out.userId);
				continue;
			}
			out.userName = buzzUser.username;
			UserImages images = UserDataSource.getCachedUserImages(out.userId);
			if (images != null) {
				out.chatPictureURL = images.chatPictureUrl;
			}
			output.add(out);
		}

		VenueDataSource.addUserAsListener(venueId, userId);

		Map<String, Object> result = new HashMap<String, Object>();
		result.put("buzz", output);

		return createSuccessResponseWithBody(result);
	}

	public String getVenueBuzzImages(Venue venue, Long buzzImageStartId, Long noOfBuzzElements) {
		List<ClientBuzzImageData> buzzImages = VenueDataSource.getBuzzImages(venue.id, buzzImageStartId + 1, noOfBuzzElements);
		Collections.reverse(buzzImages);

		Map<String, Object> output = new HashMap<String, Object>();
		output.put("buzz", buzzImages);
		return createSuccessResponseWithBody(output);
	}

	/*
	 * POST REQUEST
	 */

	public String setUserActivity(User user, Venue venue, Long activity) {
		final long venueId = venue.id;
		final Long userId = user.id;

		long val = VenueDataSource.getUserActivity(venueId, userId);
		UserDataSource.setUserActivity(userId, venueId, activity);
		VenueDataSource.setUserActivity(venueId, userId, activity);
		Set<Long> groups = UserDataSource.getUserGroups(userId);

		log.info("Updating User Activity: user = {}, venue = {}, activity = {}, prevUserActivity = {}", userId, venueId, activity, val);
		long delta = 0;
		if (val != activity) {
			delta = activity - val;
			awsQueue.sendVenueActivity(venueId, userId, delta);
			for (Long groupId : groups) {
				GroupDataSource.updateGroupTrending(groupId, venueId, delta);
			}
		}

		return createSuccessResponse();
	}

	public String reportVenueDetails(User user, VenueReport v) throws SQLException {
		VenueDataSource.saveVenueError(v, user.id);
		return createSuccessResponse();
	}

	public String requestVenueAddition(User user, VenueReport v) throws SQLException {
		VenueDataSource.saveVenueAddition(v, user.id);
		return createSuccessResponse();
	}

	public String flagVenueBuzz(Long userId, Long venueId, Long buzzId) throws SQLException {
		VenueDataSource.flagVenueBuzz(userId, venueId, buzzId);
		return createSuccessResponse();
	}

	public String handleAddBuzzRequest(User user, Venue venue, String buzzText) {
		Long userId = user.id;
		Long venueId = venue.id;

		Buzz buzz = new Buzz();
		buzz.buzzDateStamp = System.currentTimeMillis();
		buzz.userId = userId;
		buzz.buzzType = BuzzType.TEXT;
		buzz.buzzText = buzzText;
		buzz.buzzId = VenueDataSource.addBuzz(venueId, buzz);

		Map<String, Object> addedBuzz = new HashMap<String, Object>();
		addedBuzz.put("buzzId", buzz.buzzId);
		addedBuzz.put("buzz", buzz.buzzText);

		distributeBuzz(venueId, userId, buzz.buzzId);

		return createSuccessResponseWithBody(addedBuzz);
	}

	public String handleAddBuzzImageRequest(User user, Venue venue, String buzzUrl) {
		Long userId = user.id;
		Long venueId = venue.id;

		Buzz buzz = new Buzz();
		buzz.buzzDateStamp = System.currentTimeMillis();
		buzz.userId = userId;
		buzz.buzzType = BuzzType.IMAGE;

		//		BufferedImage scaledImage = ImageHandler.createScaledImage(image.image, ImageHandler.ImgType.IMG_BUZZ);

		//		buzz.buzzText = ImageHandler.uploadVenueBuzzImage(buzz, venueId, scaledImage, image.rotationOperation, venue.cityId);
		buzz.buzzText = buzzUrl;
		buzz.buzzId = VenueDataSource.addBuzz(venueId, buzz);

		Map<String, Object> addedBuzz = new HashMap<String, Object>();
		addedBuzz.put("buzzId", buzz.userId);
		addedBuzz.put("buzzUrl", buzz.buzzText);

		distributeBuzz(venueId, userId, buzz.buzzId);

		return createSuccessResponseWithBody(addedBuzz);
	}
	
	public String handleAddVenueEventRequest(Venue venue, String venueEventInfo) {
		VenueDataSource.addVenueEventInfo(venue.id, venueEventInfo);
		return createSuccessResponse();
	}

	private void distributeBuzz(Long venueId, Long userId, Long lastBuzzId) {
		Set<Long> listeners = VenueDataSource.getVenueBuzzListeners(venueId, userId);
		listeners.remove(userId);
		if (listeners.size() > 0) {
			PushUtil.sendNewVenueBuzz(listeners, venueId, lastBuzzId);
		}
	}
}
