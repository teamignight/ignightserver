package com.ignight.servlets.v2.user;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.util.Preconditions;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.user.UserServletHandler;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.servlets.v2.RequestUtil;
import com.ignight.user.User;
import com.ignight.user.UserDna;

@WebServlet(value = "/setUser/*")
public class SetUser extends BaseServlet {
	private static final long serialVersionUID = 1L;
	private UserServletHandler handler = new UserServletHandler();
	private static final Logger log = LoggerFactory.getLogger(SetUser.class);

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, JSONObject msg) throws Exception {
		Long cityId = RequestUtil.getLong(msg, "cityId");
		Long genderId = RequestUtil.getLong(msg, "genderId");
		Long ageId = RequestUtil.getLong(msg, "age");
		List<Long> mArray = RequestUtil.getLongArray(msg, "music");
		List<Long> aArray = RequestUtil.getLongArray(msg, "atmosphere");
		Long spendingLimit = RequestUtil.getLong(msg, "spendingLimit");

		Preconditions.checkNotNull(cityId, "cityId was null");
		Preconditions.checkNotNull(genderId, "genderId was null");
		Preconditions.checkNotNull(ageId, "age was null");
		Preconditions.checkNotNull(mArray, "music was null");
		Preconditions.checkArgument(mArray.size() > 0, "music was of size 0");
		Preconditions.checkNotNull(aArray, "atmosphere was null");
		Preconditions.checkArgument(aArray.size() > 0, "atmosphere array was of size 0");
		Preconditions.checkNotNull(spendingLimit, "spendingLimit was null");

		log.info("Getting Cached User: {}", id);
		User user = UserDataSource.getCachedUser(id);
		if (user == null) {
			throw new InvalidValueException("User Does not exists");
		}

		UserDna dna = new UserDna();
		dna.userId = id;
		dna.cityId = cityId;
		dna.ageId = ageId;
		dna.musicIds = convertToDnaList(mArray);
		dna.atmosphereIds = convertToDnaList(aArray);
		dna.spendingLimit = spendingLimit;

		return handler.setCompleteUserInfo(user, dna, cityId, genderId);
	}

	private List<Long> convertToDnaList(List<Long> array) {
		ArrayList<Long> prefs = new ArrayList<Long>();
		for (int i = 0; i < array.size(); i++) {
			prefs.add(array.get(i));
		}

		for (int i = array.size(); i < 3; i++) {
			prefs.add(-1l);
		}
		log.info("Setting List : {}", prefs);
		return prefs;
	}

	@Override
	protected String getMethodType() {
		return "PUT";
	}
}
