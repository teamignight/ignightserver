package com.ignight.servlets.v2.user;

import java.util.concurrent.ExecutorService;

import javax.servlet.AsyncContext;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ignight.servlets.image.ProfileImageUploader;

@WebServlet(asyncSupported = true, value = "/profile_image/*")
@MultipartConfig()
public class UpdateProfilePicture extends HttpServlet {
	private static final Logger log = LoggerFactory.getLogger(UpdateProfilePicture.class);
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
		if (ServletFileUpload.isMultipartContent(req)) {
			log.info("{} : uri={}", req.getMethod(), req.getRequestURI());
			AsyncContext ctx = req.startAsync();
			ExecutorService executor = (ExecutorService) req.getServletContext().getAttribute("imageExecutor");
			executor.execute(new ProfileImageUploader(ctx));
		} else {
			log.info("Tried to upload image, but upload is not multi-part");
		}
	}
}
