package com.ignight.servlets.v2.user;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ignight.servlets.BaseServletHandler;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.user.UserServletHandler;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.servlets.v2.RequestUtil;
import com.ignight.user.User;
import com.ignight.user.UserDna;

@WebServlet(value = "/updateUser/*")
public class UpdateUser extends BaseServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(UpdateUser.class);
	private UserServletHandler handler = new UserServletHandler();

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, JSONObject msg) throws Exception {
		User user = UserDataSource.getCachedUser(id);
		if (user == null) {
			throw new InvalidValueException("User does not exist");
		}

		String email = RequestUtil.getString(msg, "email");
		if (email != null) {
			handler.updateEmail(user, email.toLowerCase());
		}

		Boolean groupInvitePreference = RequestUtil.getBoolean(msg, "notifyOfGroupInvites");
		if (groupInvitePreference != null) {
			handler.updateUserGroupPreferences(user, groupInvitePreference);
		}

		UserDna dna = UserDataSource.getCachedUserDna(id);
		if (dna == null) {
			throw new InvalidValueException("User does not exist.");
		}

		Long ageId = RequestUtil.getLong(msg, "age");
		if (ageId != null) {
			handler.updateAge(user, dna, ageId);
		}

		Long spendingLimit = RequestUtil.getLong(msg, "spendingLimit");
		if (spendingLimit != null) {
			handler.updateSpendingLimitInDna(user, dna, spendingLimit);
		}

		List<Long> musicIds = convertToDnaList(RequestUtil.getLongArray(msg, "music"));
		if (musicIds != null) {
			handler.updateMusicInDna(user, dna, musicIds);
		}

		List<Long> atmosphereIds = convertToDnaList(RequestUtil.getLongArray(msg, "atmosphere"));
		if (atmosphereIds != null) {
			handler.updateAtmospheresInDna(user, dna, atmosphereIds);
		}

		return BaseServletHandler.createSuccessResponse();
	}

	private List<Long> convertToDnaList(List<Long> array) {
		if (array == null) {
			return null;
		}

		ArrayList<Long> prefs = new ArrayList<Long>();
		for (Long a : array) {
			prefs.add(a);
		}

		for (int i = array.size(); i < 3; i++) {
			prefs.add(-1l);
		}

		return prefs;
	}

	@Override
	protected String getMethodType() {
		return "POST";
	}
}
