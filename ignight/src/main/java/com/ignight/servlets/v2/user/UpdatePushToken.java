package com.ignight.servlets.v2.user;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.google.api.client.util.Preconditions;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.servlets.v2.RequestUtil;
import com.ignight.user.User;

@WebServlet(value = "/push_token/*")
public class UpdatePushToken extends BaseServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, JSONObject msg) throws Exception {
		Integer osId = RequestUtil.getInteger(msg, "osId");
		String token = RequestUtil.getString(msg, "token");

		Preconditions.checkNotNull(osId, "osId is null");
		Preconditions.checkNotNull(token, "token is null");

		User user = UserDataSource.getCachedUser(id);
		if (user == null) {
			throw new InvalidValueException("User does not exist.");
		}

		if (osId == OsType.IOS.ordinal()) {
			return userHandler.addIOSDeviceToken(user, token);
		} else if (osId == OsType.ANDROID.ordinal()) {
			return userHandler.addAndroidRegistrationId(user, token);
		} else {
			throw new InvalidValueException("Invaliid osId");
		}
	}

	public static enum OsType {
		IOS, ANDROID
	}

	@Override
	protected String getMethodType() {
		return "POST";
	}
}
