package com.ignight.servlets.v2;

import java.io.IOException;
import java.net.InetAddress;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;
import com.ignight.servlets.ResponseUtil;
import com.ignight.user.RedisKeys;

import databases.JedisConnection;

@WebServlet(value = "/ping")
public class PingServlet extends HttpServlet {
	private static final Logger log = LoggerFactory.getLogger(PingServlet.class);
	private static final long serialVersionUID = 1L;
	private static long healthCount = 0;
	public static final JedisConnection jedis = JedisConnection.getInstance();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (++healthCount <= 5) {
			log.info("Ping Test");
		}

		/*
		 * This is to register this instance
		 */
		if (healthCount % 6 == 0) {
			String hostName = InetAddress.getLocalHost().getHostName().replace('.', '-');
			String key = RedisKeys.APP_SERVERS_LIST + hostName;
			jedis.setex(key, String.valueOf(System.currentTimeMillis()), 60);
		}

		ResponseUtil.sendResponse(resp, Maps.newHashMap().toString());
	}
}
