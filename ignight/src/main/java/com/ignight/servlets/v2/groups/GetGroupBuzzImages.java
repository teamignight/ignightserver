package com.ignight.servlets.v2.groups;

import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.api.client.util.Preconditions;
import com.ignight.group.Group;
import com.ignight.group.GroupDataSource;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.servlets.v2.RequestUtil;
import com.ignight.servlets.v2.user.InvalidValueException;

@WebServlet(value = "/images/group/*")
public class GetGroupBuzzImages extends BaseServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, Map<String, String> parameters) throws Exception {
		Group group = GroupDataSource.getGroupInCache(id);
		if (group == null) {
			throw new InvalidValueException("Group does not exist.");
		}

		Long buzzImageStartId = RequestUtil.getLong(parameters, "buzzImageStartId");
		Preconditions.checkNotNull(buzzImageStartId, "buzzImageStartId is null");

		Integer noOfBuzzElements = RequestUtil.getInteger(parameters, "noOfBuzzImages");
		Preconditions.checkNotNull(noOfBuzzElements, "noOfBuzzElements is null");

		return groupHandler.getGroupBuzzImages(group, buzzImageStartId, noOfBuzzElements);
	}

	@Override
	protected String getMethodType() {
		return "GET";
	}

}
