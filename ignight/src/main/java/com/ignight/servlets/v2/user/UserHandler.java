package com.ignight.servlets.v2.user;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.repackaged.com.google.common.base.Preconditions;
import com.ignight.servlets.ResponseUtil;
import com.ignight.servlets.user.UserServletHandler;

@WebServlet(value = "/user/*")
public class UserHandler extends HttpServlet {
	private static Logger log = LoggerFactory.getLogger(UserHandler.class);
	private static final long serialVersionUID = 1L;
	private Long userId = null;
	private JSONObject msg = null;
	private UserServletHandler handler = new UserServletHandler();

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String path = req.getPathInfo();
		if (path == null) {
			Preconditions.checkArgument(!req.getMethod().equals("GET"), "Missing path parameter : userId");
		}
		else {
			String[] sp = path.split("/");
			try {
				userId = Long.valueOf(sp[sp.length - 1]);
			} catch (NumberFormatException e) {
				userId = null;
			}
		}
		super.service(req, resp);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			ServletContext context = getServletContext();
			RequestDispatcher dispatcher = context.getRequestDispatcher("/getUser" + req.getPathInfo());
			log.info("Dispatching to {}", "/getUser" + req.getPathInfo());
			dispatcher.forward(req, resp);
		} catch (Exception e) {
			log.error("Failed to get user information", e);
			ResponseUtil.sendErrorResponse(resp, e.getMessage());
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			ServletContext context = getServletContext();
			String dispatchPath = null;
			if (req.getPathInfo() == null) {
				dispatchPath = "/createUser";
			} else {
				dispatchPath = "/updateUser" + req.getPathInfo();
			}
			log.info("Dispatching to {}", dispatchPath);
			RequestDispatcher dispatcher = context.getRequestDispatcher(dispatchPath);
			dispatcher.forward(req, resp);
		} catch (Exception e) {
			log.error("Failed to set complete user information", e);
			ResponseUtil.sendErrorResponse(resp, e.getMessage());
		}
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			ServletContext context = getServletContext();
			RequestDispatcher dispatcher = context.getRequestDispatcher("/setUser" + req.getPathInfo());
			log.info("Fispatching to {}", "/setUser" + req.getPathInfo());
			dispatcher.forward(req, resp);
		} catch (Exception e) {
			log.error("Failed to update user", e);
			ResponseUtil.sendErrorResponse(resp, e.getMessage());
		}
	}
}
