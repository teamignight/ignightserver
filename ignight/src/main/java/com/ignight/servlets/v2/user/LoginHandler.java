package com.ignight.servlets.v2.user;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.google.api.client.repackaged.com.google.common.base.Preconditions;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.servlets.v2.RequestUtil;

@WebServlet(value = "/login")
public class LoginHandler extends BaseServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, JSONObject msg) throws Exception {
		String username = RequestUtil.getString(msg, "userName");
		String password = RequestUtil.getString(msg, "password");

		Preconditions.checkNotNull(username, "Username was null.");
		Preconditions.checkNotNull(password, "Password was null.");

		return userHandler.login(username.trim().toLowerCase(), password);
	}

	@Override
	protected String getMethodType() {
		return "POST";
	}
}
