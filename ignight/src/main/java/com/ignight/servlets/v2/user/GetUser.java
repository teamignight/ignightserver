package com.ignight.servlets.v2.user;

import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.user.User;

@WebServlet(value = "/getUser/*")
public class GetUser extends BaseServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, Map<String, String> parameters) throws Exception {
		User user = UserDataSource.getCachedUser(id);
		if (user == null) {
			throw new InvalidValueException("User does not exist.");
		}

		return userHandler.getUserInfo(user);
	}

	@Override
	protected String getMethodType() {
		return "GET";
	}

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, JSONObject msg) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}
