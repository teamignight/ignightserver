package com.ignight.servlets.v2.groups;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.google.api.client.util.Preconditions;
import com.ignight.group.Group;
import com.ignight.group.GroupDataSource;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.servlets.v2.RequestUtil;
import com.ignight.servlets.v2.user.InvalidValueException;

@WebServlet(value = "/buzz/flag/group/*")
public class FlagGroupBuzz extends BaseServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, JSONObject msg) throws Exception {
		
		Group group = GroupDataSource.getGroupInCache(id);
		if (group == null) {
			throw new InvalidValueException("Group does not exist.");
		}

		Long userId = RequestUtil.getLong(msg, "userId");
		Preconditions.checkNotNull(userId, "userId is null");
		
		Long buzzId = RequestUtil.getLong(msg, "buzzId");
		Preconditions.checkNotNull(buzzId, "buzzId is null");

		return groupHandler.flagGroupBuzz(userId, id, buzzId);
	}
	
	@Override
	protected String getMethodType() {
		return "POST";
	}

}
