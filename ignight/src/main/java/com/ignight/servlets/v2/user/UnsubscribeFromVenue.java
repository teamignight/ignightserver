package com.ignight.servlets.v2.user;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.google.api.client.util.Preconditions;
import com.ignight.core.CoreData;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.servlets.v2.RequestUtil;
import com.ignight.user.User;
import com.ignight.venue.Venue;

@WebServlet(value = "/unsubscribe/venue/*")
public class UnsubscribeFromVenue extends BaseServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, JSONObject msg) throws Exception {
		User user = UserDataSource.getCachedUser(id);
		if (user == null) {
			throw new InvalidValueException("User does not exist.");
		}

		Long venueId = RequestUtil.getLong(msg, "venueId");
		Preconditions.checkNotNull(venueId, "venueId is null");
		Venue venue = CoreData.getVenue(venueId);
		if (venue == null) {
			throw new InvalidValueException("Venue does not exist.");
		}

		return userHandler.unsubscribeFromVenue(user, venue);
	}

	@Override
	protected String getMethodType() {
		return "POST";
	}

}
