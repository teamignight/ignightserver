package com.ignight.servlets.v2.user.notifications;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.servlets.v2.user.InvalidValueException;
import com.ignight.user.User;

@WebServlet(value = "/user/notifications/clearinvites/*")
public class ClearGroupInvitesNotification extends BaseServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, JSONObject msg) throws Exception {
		User user = UserDataSource.getCachedUser(id);
		if (user == null) {
			throw new InvalidValueException("User does not exist.");
		}

		return userHandler.clearUserGroupInviteNotifications(user);
	}

	@Override
	protected String getMethodType() {
		return "POST";
	}
}
