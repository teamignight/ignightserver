package com.ignight.servlets.v2.user;

import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.v2.ApiException;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.user.User;

@WebServlet(value = "/forgot_password/*")
public class ForgotPassword extends BaseServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ForgotPassword.class);

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, Map<String, String> parameters) throws Exception {
		String[] uriSplit = req.getRequestURI().split("/");
		if (uriSplit.length == 4) {
			String username = uriSplit[uriSplit.length - 1];
			if (username == null || username.length() == 0) {
				throw new ApiException("Invalid API call");
			}
			username = username.toLowerCase().trim();
			User user = UserDataSource.getUser(username);
			if (user == null) {
				throw new InvalidValueException("Username not found.");
			} else {
				return userHandler.sendTempPassword(user, username);
			}
		}
		throw new ApiException("Invalid API call");
	}

	@Override
	public Long getUserId(HttpServletRequest req) {
		return null;
	}

	@Override
	protected String getMethodType() {
		return "GET";
	}
}
