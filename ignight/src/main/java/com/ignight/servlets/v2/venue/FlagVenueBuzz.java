package com.ignight.servlets.v2.venue;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.google.api.client.util.Preconditions;
import com.ignight.core.CoreData;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.servlets.v2.RequestUtil;
import com.ignight.servlets.v2.user.InvalidValueException;
import com.ignight.venue.Venue;

@WebServlet(value = "/buzz/flag/venue/*")
public class FlagVenueBuzz extends BaseServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, JSONObject msg) throws Exception {
		Venue venue = CoreData.getVenue(id);
		if (venue == null) {
			throw new InvalidValueException("Venue does not exist.");
		}

		Long userId = RequestUtil.getLong(msg, "userId");
		Preconditions.checkNotNull(userId, "userId is null");
		
		Long buzzId = RequestUtil.getLong(msg, "buzzId");
		Preconditions.checkNotNull(buzzId, "buzzId is null");

		return venueHandler.flagVenueBuzz(userId, id, buzzId);
	}

	@Override
	protected String getMethodType() {
		return "POST";
	}

}
