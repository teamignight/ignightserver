package com.ignight.servlets.v2.groups;

import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.api.client.util.Preconditions;
import com.ignight.group.Group;
import com.ignight.group.GroupDataSource;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.servlets.v2.RequestUtil;
import com.ignight.servlets.v2.user.InvalidValueException;
import com.ignight.user.User;

@WebServlet(value = "/buzz/group/*")
public class GetGroupBuzz extends BaseServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, Map<String, String> parameters) throws Exception {
		Group group = GroupDataSource.getGroupInCache(id);
		if (group == null) {
			throw new InvalidValueException("Group does not exist.");
		}

		Long userId = RequestUtil.getLong(parameters, "userId");
		Preconditions.checkNotNull(userId, "userId is null");

		User user = UserDataSource.getCachedUser(userId);
		if (user == null) {
			throw new InvalidValueException("User does not exist.");
		}

		Long lastBuzzId = RequestUtil.getLong(parameters, "lastBuzzId");
		Preconditions.checkNotNull(lastBuzzId, "lastBuzzId is null");

		Long count = RequestUtil.getLong(parameters, "count");
		Preconditions.checkNotNull(count, "count is null");

		Integer latest = RequestUtil.getInteger(parameters, "latest");
		Preconditions.checkNotNull(latest, "latest is null");

		return groupHandler.getGroupBuzz(group, user, count, lastBuzzId, latest > 0 ? true : false);
	}

	@Override
	protected String getMethodType() {
		return "GET";
	}

}
