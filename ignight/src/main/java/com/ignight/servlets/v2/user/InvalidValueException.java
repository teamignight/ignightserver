package com.ignight.servlets.v2.user;

public class InvalidValueException extends Exception {
	private static final long serialVersionUID = 1L;

	public InvalidValueException(String message) {
		super(message);
	}

	public InvalidValueException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
