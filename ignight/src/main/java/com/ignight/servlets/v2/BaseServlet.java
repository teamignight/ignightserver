package com.ignight.servlets.v2;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ignight.data.DataSource;
import com.ignight.servlets.ResponseUtil;
import com.ignight.servlets.group.GroupServletHandler;
import com.ignight.servlets.user.UserServletHandler;
import com.ignight.servlets.v2.user.InvalidValueException;
import com.ignight.servlets.venue.VenueServletHandler;
import com.util.IgnightProperties;

public abstract class BaseServlet extends HttpServlet {
	private final static Logger log = LoggerFactory.getLogger(BaseServlet.class);
	private static final long serialVersionUID = 1L;

	public UserServletHandler userHandler = new UserServletHandler();
	public GroupServletHandler groupHandler = new GroupServletHandler();
	public VenueServletHandler venueHandler = new VenueServletHandler();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
		long start = System.currentTimeMillis();
		Map<String, String> parameters = null;
		String response = null;

		try {
			Long id = getUserId(req);
			parameters = getPathParameters(req);

			if (getMethodType().toUpperCase().equals("GET")) {
				response = handle(req, resp, id, parameters);
				ResponseUtil.sendResponse(resp, response);
				logMessage(req, response, start, null);
			} else {
				response = "GET not supported";
				ResponseUtil.sendErrorResponse(resp, response);
			}
		} catch (InvalidValueException e) {
			logMessage(req, response, start, e);
			ResponseUtil.sendErrorResponse(resp, e.getMessage());
		} catch (Exception e) {
			logError(req, e.getMessage(), null, start, e);
			ResponseUtil.sendErrorResponse(resp, e.getMessage());
		}
	}

	public Long getUserId(HttpServletRequest req) {
		return RequestUtil.getIdInUri(req);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
		long start = System.currentTimeMillis();
		JSONObject msg = null;
		String response = null;
		try {
			Long id = getUserId(req);
			msg = getBody(req);

			if (getMethodType().toUpperCase().equals("POST")) {
				response = handle(req, resp, id, msg);
				ResponseUtil.sendResponse(resp, response);
				logMessage(req, response, msg, start, null);
			} else {
				response = "POST not supported";
				ResponseUtil.sendErrorResponse(resp, response);
			}
		} catch (Exception e) {
			ResponseUtil.sendErrorResponse(resp, e.getMessage());
			logError(req, e.getMessage(), msg, start, e);
		}
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		long start = System.currentTimeMillis();
		JSONObject msg = null;
		String response = null;
		try {
			Long id = getUserId(req);
			msg = getBody(req);

			if (getMethodType().toUpperCase().equals("PUT")) {
				response = handle(req, resp, id, msg);
				ResponseUtil.sendResponse(resp, response);
				logMessage(req, response, msg, start, null);
			} else {
				response = "PUT not supported";
				ResponseUtil.sendErrorResponse(resp, response);
			}
		} catch (Exception e) {
			ResponseUtil.sendErrorResponse(resp, e.getMessage());
			logError(req, e.getMessage(), msg, start, e);
		}
	}

	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, JSONObject msg) throws Exception {
		throw new ApiException("Post / Put Not Supported");
	}

	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, Map<String, String> parameters) throws Exception {
		throw new ApiException("Get Not Supported");
	}

	private Map<String, String> getPathParameters(HttpServletRequest req) throws Exception {
		Map<String, String[]> parameters = RequestUtil.getParameters(req);
		Map<String, String> params = new HashMap<String, String>();
		for (Map.Entry<String, String[]> entry : parameters.entrySet()) {
			params.put(entry.getKey(), entry.getValue()[0]);
		}
		return params;
	}

	private JSONObject getBody(HttpServletRequest req) throws Exception {
		JSONObject msg = RequestUtil.getBody(req);
		return msg;
	}

	private void logMessage(HttpServletRequest req, String response, long requestStart, Exception e) {
		try {
			String method = req.getMethod();
			String uri = req.getRequestURI();
			Object params = method.equals("GET") ? RequestUtil.getParams(req) : RequestUtil.getBody(req);
			String platform = RequestUtil.getPlatform(req);
			String userId = RequestUtil.getHeaderUserId(req);
			long elapsedTime = System.currentTimeMillis() - requestStart;
			log.info("{} : uId={}, platform={}, elapsedTime={} ms, uri={}, params={}, response={}",
					method, userId, platform, elapsedTime, uri, params, response, e);
		} catch (Exception e2) {
			log.error("Failed to log message", e2);
		}
	}

	private void logMessage(HttpServletRequest req, String response, JSONObject params, long requestStart, Exception e2) {
		try {
			String method = req.getMethod();
			String uri = req.getRequestURI();
			String platform = RequestUtil.getPlatform(req);
			String userId = RequestUtil.getHeaderUserId(req);
			long elapsedTime = System.currentTimeMillis() - requestStart;
			log.info("{} : uId={}, platform={}, elapsedTime={} ms, uri={}, params={}, response={}",
					method, userId, platform, elapsedTime, uri, params, response, e2);
		} catch (Exception e) {
			log.error("Failed to log message", e);
		}
	}

	private void logError(HttpServletRequest req, String error, JSONObject postParams, long requestStart, Exception e2) {
		try {
			String method = req.getMethod();
			String uri = req.getRequestURI();
			Object params = method.equals("GET") ? RequestUtil.getParams(req) : postParams;
			String platform = RequestUtil.getPlatform(req);
			String userId = RequestUtil.getHeaderUserId(req);
			long elapsedTime = System.currentTimeMillis() - requestStart;
			log.error("{} : uId={}, platform={}, elapsedTime={} ms, uri={}, params={}, response={}",
					method, userId, platform, elapsedTime, uri, params, error, e2);
		} catch (Exception e) {
			log.error("Failed to log Error", e);
		}
	}

	//Can add other conditions for blocking server calls here
	protected String blockServerCalls() throws SQLException {
		Long cityId = IgnightProperties.getInstance().getCityId();
		log.info("Checking if archiving in progress for cityId: {} on {}", cityId);
		if (DataSource.isArchiveInProgress(cityId))
			return "Server is being refreshed. Please try again shortly.";
		return null;
	}

	protected abstract String getMethodType();

}
