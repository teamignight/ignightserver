package com.ignight.servlets.v2.venue;

import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.api.client.util.Preconditions;
import com.ignight.core.CoreData;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.servlets.v2.RequestUtil;
import com.ignight.servlets.v2.user.InvalidValueException;
import com.ignight.venue.Venue;

@WebServlet(value = "/images/venue/*")
public class GetVenueImages extends BaseServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, Map<String, String> parameters) throws Exception {
		Venue venue = CoreData.getVenue(id);
		if (venue == null) {
			throw new InvalidValueException("Venue does not exist.");
		}

		Long buzzImageStartId = RequestUtil.getLong(parameters, "buzzImageStartId");
		Preconditions.checkNotNull(buzzImageStartId, "buzzImageStartId is null");

		Long noOfBuzzImages = RequestUtil.getLong(parameters, "noOfBuzzImages");
		Preconditions.checkNotNull(noOfBuzzImages, "noOfBuzzImages is null");
		return venueHandler.getVenueBuzzImages(venue, buzzImageStartId, noOfBuzzImages);
	}

	@Override
	protected String getMethodType() {
		return "GET";
	}

}
