package com.ignight.servlets.v2.user;

import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.api.client.util.Preconditions;
import com.ignight.group.Group;
import com.ignight.group.GroupDataSource;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.servlets.v2.RequestUtil;
import com.ignight.user.User;

@WebServlet(value = "/groups/users/search/*")
public class SearchUsers extends BaseServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, Map<String, String> parameters) throws Exception {
		User user = UserDataSource.getCachedUser(id);
		if (user == null) {
			throw new InvalidValueException("User does not exist.");
		}

		Long groupId = RequestUtil.getLong(parameters, "groupId");
		Preconditions.checkNotNull(groupId, "groupId was null");

		Group group = GroupDataSource.getGroupInCache(groupId);
		if (group == null) {
			throw new InvalidValueException("Group does not exist : " + groupId);
		}

		String searchString = RequestUtil.getString(parameters, "search");
		Preconditions.checkNotNull(searchString, "search was null");
		searchString = searchString.trim();

		Long offset = RequestUtil.getLongOrDefault(parameters, "offset", 0);
		Long limit = RequestUtil.getLongOrDefault(parameters, "limit", 20);

		return userHandler.handleSearchForUsers(user, group, searchString, offset, limit);
	}

	@Override
	protected String getMethodType() {
		return "GET";
	}
}
