package com.ignight.servlets.v2.user;

import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.util.Preconditions;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.servlets.v2.RequestUtil;
import com.ignight.user.User;

@WebServlet(value = "/trending/*")
public class CityTrendingList extends BaseServlet {
	private static final Logger log = LoggerFactory.getLogger(CityTrendingList.class);
	private static final long serialVersionUID = 1L;

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, Map<String, String> parameters) throws Exception {
		User user = UserDataSource.getCachedUser(id);
		if (user == null) {
			throw new InvalidValueException("User does not exist.");
		}

		TrendingRequest tR = new TrendingRequest();

		Integer count = RequestUtil.getInteger(parameters, "count");
		Preconditions.checkNotNull(count, "count is null");
		Preconditions.checkArgument(count >= 0, "Invalid count");
		tR.setCount(count);

		Double searchRadius = RequestUtil.getDouble(parameters, "search_radius");
		Preconditions.checkArgument(searchRadius == null || (searchRadius > 0.0), "Invalid search radius");
		tR.setSearchRadius(searchRadius);

		Double lat = RequestUtil.getDouble(parameters, "latitude");
		Double lon = RequestUtil.getDouble(parameters, "longitude");

		if (searchRadius != null) {
			Preconditions.checkArgument(lat != null, "Invalid latitude");
			tR.setLat(lat);
			Preconditions.checkArgument(lon != null, "Invalid longitude");
			tR.setLon(lon);
		}

		Boolean upvoteFilter = RequestUtil.getBoolean(parameters, "upvote_filter");
		tR.setUpvoteFilter(upvoteFilter);

		Long musicFilter = RequestUtil.getLong(parameters, "music_filter");
		Preconditions.checkArgument(musicFilter == null || (musicFilter >= 0 && musicFilter < 15), "Invalid music_filter");
		tR.setMusicFilter(musicFilter);

		String searchString = RequestUtil.getString(parameters, "search");
		if (searchString != null) {
			searchString = searchString.toLowerCase().trim();
		}
		tR.setSearchString(searchString);

		log.debug("TrendingRequest: {}", tR);

		Preconditions.checkArgument(tR.filterCount <= 1, "Too many filters being used");

		return userHandler.getUserTrendingList(user, tR);
	}

	public static class TrendingRequest {
		@Override
		public String toString() {
			return "TrendingRequest [count=" + count + ", searchRadius=" + searchRadius + ", lat=" + lat + ", lon=" + lon + ", upvoteFilter=" + upvoteFilter + ", musicFilter=" + musicFilter + ", searchString=" + searchString + ", filterCount=" + filterCount + "]";
		}

		private Integer count;
		private Double searchRadius;
		private Double lat;
		private Double lon;
		private Boolean upvoteFilter;
		private Long musicFilter;
		private String searchString;

		public int filterCount = 0;

		public Integer getCount() {
			return count;
		}

		public void setCount(Integer count) {
			this.count = count;
		}

		public Double getSearchRadius() {
			return searchRadius;
		}

		public void setSearchRadius(Double searchRadius) {
			this.searchRadius = searchRadius;
		}

		public Double getLat() {
			return lat;
		}

		public void setLat(Double lat) {
			this.lat = lat;
		}

		public Double getLon() {
			return lon;
		}

		public void setLon(Double lon) {
			this.lon = lon;
		}

		public Boolean getUpvoteFilter() {
			return upvoteFilter;
		}

		public void setUpvoteFilter(Boolean upvoteFilter) {
			this.upvoteFilter = upvoteFilter;
			if (upvoteFilter != null && upvoteFilter) {
				filterCount++;
			}
		}

		public Long getMusicFilter() {
			return musicFilter;
		}

		public void setMusicFilter(Long musicFilter) {
			this.musicFilter = musicFilter;
			if (musicFilter != null) {
				filterCount++;
			}
		}

		public String getSearchString() {
			return searchString;
		}

		public void setSearchString(String searchString) {
			this.searchString = searchString;
			if (searchString != null) {
				filterCount++;
			}
		}

	}

	@Override
	protected String getMethodType() {
		return "GET";
	}
}
