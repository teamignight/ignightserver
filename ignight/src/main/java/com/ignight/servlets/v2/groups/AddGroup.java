package com.ignight.servlets.v2.groups;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.google.api.client.util.Preconditions;
import com.ignight.group.GroupDataSource;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.servlets.v2.RequestUtil;
import com.ignight.servlets.v2.user.InvalidValueException;
import com.ignight.user.User;

@WebServlet(value = "/group/add/*")
public class AddGroup extends BaseServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, JSONObject msg) throws Exception {
		
		User user = UserDataSource.getCachedUser(id);
		if (user == null) {
			throw new InvalidValueException("User does not exist.");
		}

		Long cityId = RequestUtil.getLong(msg, "cityId");
		Preconditions.checkNotNull(cityId, "cityId is null");

		String groupName = RequestUtil.getString(msg, "groupName");
		Preconditions.checkNotNull(groupName, "groupName is null");
		Preconditions.checkArgument(!groupName.isEmpty(), "groupName can not be empty");

		String groupDescription = RequestUtil.getString(msg, "groupDescription");
		Preconditions.checkNotNull(groupDescription, "groupDescription is null");
		Preconditions.checkArgument(!groupDescription.isEmpty(), "groupDescription can not be empty");

		Boolean isPrivateGroup = RequestUtil.getBoolean(msg, "isPrivateGroup");
		Preconditions.checkNotNull(isPrivateGroup, "isPrivateGroup is null");

		boolean exists = GroupDataSource.doesGroupNameExist(groupName);
		if (exists) {
			throw new InvalidValueException("Group name already exists.");
		}

		return groupHandler.addGroup(user, cityId, groupName, groupDescription, isPrivateGroup);
	}

	@Override
	protected String getMethodType() {
		return "POST";
	}
}
