package com.ignight.servlets.v2.groups;

import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.servlets.v2.RequestUtil;
import com.ignight.servlets.v2.user.InvalidValueException;
import com.ignight.user.User;

@WebServlet(value = "/group/popular/*")
public class GetPopularGroups extends BaseServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, Map<String, String> parameters) throws Exception {
		User user = UserDataSource.getCachedUser(id);
		if (user == null) {
			throw new InvalidValueException("User does not exist.");
		}

		Long offset = RequestUtil.getLongOrDefault(parameters, "offset", 0);
		Long limit = RequestUtil.getLongOrDefault(parameters, "limit", 20);

		return groupHandler.handleGetPopularGroups(user, offset, limit);
	}

	@Override
	protected String getMethodType() {
		return "GET";
	}

}
