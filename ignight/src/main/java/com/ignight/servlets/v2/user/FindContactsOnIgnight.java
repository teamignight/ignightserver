package com.ignight.servlets.v2.user;

import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.user.UserServletHandler;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.user.User;

@WebServlet(value = "/findContactsOnIgnight/*")
public class FindContactsOnIgnight extends BaseServlet {
	private static final long serialVersionUID = 1L;
	private UserServletHandler handler = new UserServletHandler();
	private static Logger log = LoggerFactory.getLogger(FindContactsOnIgnight.class);

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, Map<String, String> parameters) throws Exception {
		log.info("In Get Contacts On Ignight");

		User user = UserDataSource.getCachedUser(id);
		if (user == null) {
			throw new InvalidValueException("User does not exist.");
		}
		
		return handler.findAddressBookContactsOnIgnight(id);
	}

	@Override
	protected String getMethodType() {
		return "GET";
	}
}
