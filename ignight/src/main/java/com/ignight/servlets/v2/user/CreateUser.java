package com.ignight.servlets.v2.user;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.util.Preconditions;
import com.ignight.servlets.user.UserServletHandler;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.servlets.v2.RequestUtil;

@WebServlet(value = "/createUser")
public class CreateUser extends BaseServlet {
	private static final long serialVersionUID = 1L;
	private UserServletHandler handler = new UserServletHandler();
	private static Logger log = LoggerFactory.getLogger(CreateUser.class);

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, JSONObject msg) throws Exception {
		log.info("In Create User Handle");

		String username = RequestUtil.getString(msg, "userName");
		String password = RequestUtil.getString(msg, "password");
		String email = RequestUtil.getString(msg, "email");

		Preconditions.checkNotNull(username, "Username cant be null");
		Preconditions.checkNotNull(password, "Password cant be null");
		Preconditions.checkNotNull(email, "Email cant be null");

		return handler.addNewUser(username.trim().toLowerCase(), password, email.trim().toLowerCase());
	}

	@Override
	protected String getMethodType() {
		return "POST";
	}
}
