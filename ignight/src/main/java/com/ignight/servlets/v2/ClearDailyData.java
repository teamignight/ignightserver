package com.ignight.servlets.v2;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.ignight.servlets.BaseServletHandler;
import com.ignight.servlets.data.DataServletHandler;

@WebServlet(value = "/data/clear")
public class ClearDailyData extends BaseServlet {
	private static final long serialVersionUID = 1L;
	private DataServletHandler handler = new DataServletHandler();

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, JSONObject msg) throws Exception {
		handler.clearDailyData(msg);
		return BaseServletHandler.createSuccessResponse();
	}

	@Override
	protected String getMethodType() {
		return "POST";
	}
}
