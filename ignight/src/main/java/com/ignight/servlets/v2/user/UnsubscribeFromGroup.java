package com.ignight.servlets.v2.user;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.google.api.client.util.Preconditions;
import com.ignight.group.Group;
import com.ignight.group.GroupDataSource;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.servlets.v2.RequestUtil;
import com.ignight.user.User;

@WebServlet(value = "/unsubscribe/group/*")
public class UnsubscribeFromGroup extends BaseServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, JSONObject msg) throws Exception {
		User user = UserDataSource.getCachedUser(id);
		if (user == null) {
			throw new InvalidValueException("User does not exist.");
		}

		Long groupId = RequestUtil.getLong(msg, "groupId");
		Preconditions.checkNotNull(groupId, "groupId is null");
		Group group = GroupDataSource.getGroupInCache(groupId);
		if (group == null) {
			throw new InvalidValueException("Group does not exist.");
		}

		return userHandler.unsubscribeFromGroup(user, group);
	}

	@Override
	protected String getMethodType() {
		return "POST";
	}

}
