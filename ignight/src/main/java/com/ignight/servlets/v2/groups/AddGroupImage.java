package com.ignight.servlets.v2.groups;

import java.util.concurrent.ExecutorService;

import javax.servlet.AsyncContext;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ignight.servlets.image.GroupBuzzImageUploader;

@WebServlet(asyncSupported = true, value = "/buzz/add/group/image/*")
@MultipartConfig
public class AddGroupImage extends HttpServlet {
	private static final Logger log = LoggerFactory.getLogger(AddGroupImage.class);
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
		if (ServletFileUpload.isMultipartContent(req)) {
			log.info("{} : uri={}", req.getMethod(), req.getRequestURI());
			AsyncContext ctx = req.startAsync();
			ExecutorService executor = (ExecutorService) req.getServletContext().getAttribute("imageExecutor");
			executor.execute(new GroupBuzzImageUploader(ctx));
		} else {
			log.info("Tried to upload image, but upload is not multi-part");
		}
	}
}
