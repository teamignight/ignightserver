package com.ignight.servlets.v2.venue;

import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.api.client.util.Preconditions;
import com.ignight.core.CoreData;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.servlets.v2.RequestUtil;
import com.ignight.servlets.v2.user.InvalidValueException;
import com.ignight.user.User;
import com.ignight.venue.Venue;

@WebServlet(value = "/buzz/venue/*")
public class GetVenueBuzz extends BaseServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, Map<String, String> parameters) throws Exception {
		Venue venue = CoreData.getVenue(id);
		if (venue == null) {
			throw new InvalidValueException("Venue does not exist.");
		}

		Long userId = RequestUtil.getLong(parameters, "userId");
		Preconditions.checkNotNull(userId, "userId is null");
		User user = UserDataSource.getCachedUser(userId);
		if (user == null) {
			throw new InvalidValueException("User does not exist.");
		}

		Integer count = RequestUtil.getInteger(parameters, "count");
		Preconditions.checkNotNull(count, "count is null");

		Long lastBuzzId = RequestUtil.getLong(parameters, "lastBuzzId");
		Preconditions.checkNotNull(lastBuzzId, "lastBuzzId is null");

		Integer latest = RequestUtil.getInteger(parameters, "latest");
		Preconditions.checkNotNull(latest, "latest is null");

		return venueHandler.getVenueBuzz(user, venue, lastBuzzId, count, latest > 0 ? true : false);
	}

	@Override
	protected String getMethodType() {
		return "GET";
	}

}
