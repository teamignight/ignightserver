package com.ignight.servlets.v2.venue;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.google.api.client.util.Preconditions;
import com.ignight.core.CoreData;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.servlets.v2.RequestUtil;
import com.ignight.servlets.v2.user.InvalidValueException;
import com.ignight.user.User;
import com.ignight.venue.Venue;
import com.util.GooglePlaceImagesUtils;

@WebServlet(value = "/venue/google/*")
public class FetchGooglePlacesVenueInfo extends BaseServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, JSONObject msg) throws Exception {


		//return GooglePlaceImagesUtils.fetchGooglePlacesData();
		return GooglePlaceImagesUtils.fetchDetailedGooglePlacesData();
		//return GooglePlaceImagesUtils.fetchGooglePlacesData();
		//return GooglePlaceImagesUtils.updateNumberOfImages();
		//return "Done Nothing";
		//return userHandler.sendSmsVerifcationCode("16466759997");
	}

	@Override
	protected String getMethodType() {
		return "POST";
	}

}
