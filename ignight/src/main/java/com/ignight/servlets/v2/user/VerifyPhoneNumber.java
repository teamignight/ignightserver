package com.ignight.servlets.v2.user;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.util.Preconditions;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.user.UserServletHandler;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.servlets.v2.RequestUtil;
import com.ignight.user.User;

@WebServlet(value = "/verifyPhoneNumber/*")
public class VerifyPhoneNumber extends BaseServlet {
	private static final long serialVersionUID = 1L;
	private UserServletHandler handler = new UserServletHandler();
	private static Logger log = LoggerFactory.getLogger(VerifyPhoneNumber.class);

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, JSONObject msg) throws Exception {
		log.info("In Verify Phone Number");

		String number = RequestUtil.getString(msg, "number");
		Preconditions.checkNotNull(number, "Phone number cant be null");
		
		String verificationCode = RequestUtil.getString(msg, "verificationCode");
		Preconditions.checkNotNull(verificationCode, "Verification code can't be null");
		
		User user = UserDataSource.getCachedUser(id);
		if (user == null) {
			throw new InvalidValueException("User does not exist.");
		}
		return handler.verifyPhoneNumber(user, number, verificationCode);
	}

	@Override
	protected String getMethodType() {
		return "POST";
	}
}
