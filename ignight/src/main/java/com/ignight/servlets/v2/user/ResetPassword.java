package com.ignight.servlets.v2.user;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.google.api.client.util.Preconditions;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.servlets.v2.RequestUtil;

@WebServlet(value = "/reset_password")
public class ResetPassword extends BaseServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, JSONObject msg) throws Exception {
		String username = RequestUtil.getString(msg, "userName");
		String password = RequestUtil.getString(msg, "password");
		String tempPassword = RequestUtil.getString(msg, "tempPassword");

		Preconditions.checkNotNull(username, "username is null");
		Preconditions.checkNotNull(password, "password is null");
		Preconditions.checkNotNull(tempPassword, "tempPassword is null");

		username = username.toLowerCase().trim();
		if (!UserDataSource.doesUsernameExist(username)) {
			throw new InvalidValueException("Username not found.");
		}

		return userHandler.resetPassword(username, password, tempPassword);
	}

	@Override
	protected String getMethodType() {
		return "POST";
	}

}
