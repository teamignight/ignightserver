package com.ignight.servlets.v2.groups;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.util.Preconditions;
import com.ignight.group.Group;
import com.ignight.group.GroupDataSource;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.servlets.v2.RequestUtil;
import com.ignight.servlets.v2.user.InvalidValueException;
import com.ignight.user.User;

@WebServlet(value = "/group/invite/*")
public class InviteUser extends BaseServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(InviteUser.class);

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, JSONObject msg) throws Exception {
		Group group = GroupDataSource.getGroupInCache(id);
		if (group == null) {
			throw new InvalidValueException("Group does not exist.");
		}

		Long userId = RequestUtil.getLong(msg, "userId");
		Preconditions.checkNotNull(userId, "userId is null");

		User user = UserDataSource.getCachedUser(userId);
		if (user == null) {
			throw new InvalidValueException("User does not exist.");
		}
		logger.info("Inviting User: {}", user.username);

		Long targetUserId = RequestUtil.getLong(msg, "targetUserId");
		Preconditions.checkNotNull(targetUserId, "targetUserId is null");

		User targetUser = UserDataSource.getCachedUser(targetUserId);
		if (targetUser == null) {
			throw new InvalidValueException("Target user does not exist.");
		}

		return groupHandler.inviteUser(group, user, targetUser);
	}

	@Override
	protected String getMethodType() {
		return "POST";
	}

}
