package com.ignight.servlets.v2.venue;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.google.api.client.util.Preconditions;
import com.ignight.core.CoreData;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.servlets.v2.RequestUtil;
import com.ignight.servlets.v2.user.InvalidValueException;
import com.ignight.user.User;
import com.ignight.venue.Venue;
import com.ignight.venue.VenueReport;

@WebServlet(value = "/venue/report/error/*")
public class ReportVenueError extends BaseServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, JSONObject msg) throws Exception {
		Venue venue = CoreData.getVenue(id);
		if (venue == null) {
			throw new InvalidValueException("Venue does not exist.");
		}

		Long userId = RequestUtil.getLong(msg, "userId");
		Preconditions.checkNotNull(userId, "userId is null");
		User user = UserDataSource.getCachedUser(userId);
		if (user == null) {
			throw new InvalidValueException("User does not exist.");
		}

		String venueName = RequestUtil.getString(msg, "venueName");
		Preconditions.checkNotNull(venueName, "venueName is null");

		Long cityId = RequestUtil.getLong(msg, "cityId");
		Preconditions.checkNotNull(cityId, "cityId is null");

		String address = RequestUtil.getString(msg, "address");
		Preconditions.checkNotNull(address, "address is null");

		String phoneNumber = RequestUtil.getString(msg, "number");
		Preconditions.checkNotNull(phoneNumber, "number is null");

		String url = RequestUtil.getString(msg, "url");
		Preconditions.checkNotNull(url, "url is null");

		String comment = RequestUtil.getString(msg, "comment");
		Preconditions.checkNotNull(comment, "comment is null");

		VenueReport v = new VenueReport(id, venueName, cityId, address, phoneNumber, url, comment);

		return venueHandler.reportVenueDetails(user, v);
	}

	@Override
	protected String getMethodType() {
		return "POST";
	}

}
