package com.ignight.servlets.v2.venue;

import java.util.concurrent.ExecutorService;

import javax.servlet.AsyncContext;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ignight.servlets.image.VenueBuzzImageUploader;

@MultipartConfig()
@WebServlet(asyncSupported = true, value = "/buzz/add/venue/image/*")
public class AddVenueImage extends HttpServlet {
	private static final Logger log = LoggerFactory.getLogger(AddVenueImage.class);
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
		if (ServletFileUpload.isMultipartContent(req)) {
			log.info("{} : uri={}", req.getMethod(), req.getRequestURI());
			AsyncContext ctx = req.startAsync();
			ExecutorService executor = (ExecutorService) req.getServletContext().getAttribute("imageExecutor");
			executor.execute(new VenueBuzzImageUploader(ctx));
		} else {
			log.info("Tried to upload image, but upload is not multi-part");
		}
		//		Venue venue = CoreData.venues.get(id);
		//		if (venue == null) {
		//			throw new InvalidValueException("Venue does not exist.");
		//		}
		//
		//		Long userId = RequestUtil.getLong(msg, "userId");
		//		Preconditions.checkNotNull(userId, "userId is null");
		//		User user = UserDataSource.getCachedUser(userId);
		//		if (user == null) {
		//			throw new InvalidValueException("User does not exist.");
		//		}
		//
		//		UploadedImage image = RequestUtil.getUploadedImage(req);
		//		return venueHandler.handleAddBuzzRequest(user, venue, image);
	}
}
