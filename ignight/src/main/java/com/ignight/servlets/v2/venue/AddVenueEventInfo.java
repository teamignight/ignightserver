package com.ignight.servlets.v2.venue;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.google.api.client.util.Preconditions;
import com.ignight.core.CoreData;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.servlets.v2.RequestUtil;
import com.ignight.servlets.v2.user.InvalidValueException;
import com.ignight.user.User;
import com.ignight.venue.Venue;

@WebServlet(value = "/venue/event/*")
public class AddVenueEventInfo extends BaseServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, JSONObject msg) throws Exception {
		Venue venue = CoreData.getVenue(id);
		if (venue == null) {
			throw new InvalidValueException("Venue does not exist.");
		}

		String venueEventInfo = RequestUtil.getString(msg, "venueEventInfo");
		Preconditions.checkNotNull(venueEventInfo, "venueEventInfo is null");

		return venueHandler.handleAddVenueEventRequest(venue, venueEventInfo);
	}

	@Override
	protected String getMethodType() {
		return "POST";
	}

}

