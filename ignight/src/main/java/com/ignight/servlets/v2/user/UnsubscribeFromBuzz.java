package com.ignight.servlets.v2.user;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.amazonaws.services.cloudfront_2012_03_15.model.InvalidArgumentException;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.user.User;

@WebServlet(value = "/unsubscribe/buzz/*")
public class UnsubscribeFromBuzz extends BaseServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, JSONObject msg) throws Exception {
		if (id < 0) {
			return null;
		}
		User user = UserDataSource.getCachedUser(id);
		if (user == null) {
			throw new InvalidArgumentException("User does not exist.");
		}

		return userHandler.unsubscribeFromBuzz(user.id);
	}

	@Override
	protected String getMethodType() {
		return "POST";
	}
}
