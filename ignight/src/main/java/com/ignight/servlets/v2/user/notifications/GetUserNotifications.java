package com.ignight.servlets.v2.user.notifications;

import javax.servlet.annotation.WebServlet;

import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.servlets.v2.user.InvalidValueException;
import com.ignight.user.User;

@WebServlet(value = "/user/notifications/*")
public class GetUserNotifications extends BaseServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected String handle(javax.servlet.http.HttpServletRequest req, javax.servlet.http.HttpServletResponse resp, Long id, java.util.Map<String, String> parameters) throws Exception {
		User user = UserDataSource.getCachedUser(id);
		if (user == null) {
			throw new InvalidValueException("User does not exist.");
		}
		return userHandler.getUserNotifications(user);
	};

	@Override
	protected String getMethodType() {
		return "GET";
	}
}
