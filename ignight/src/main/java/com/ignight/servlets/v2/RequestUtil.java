package com.ignight.servlets.v2;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.ignight.user.AddressBook;
import com.ignight.user.PhoneContact;

public class RequestUtil {
	private static final Logger log = LoggerFactory.getLogger(RequestUtil.class);
	private static final String PLATFORM_HEADER_KEY = "PLATFORM";
	private static final String USER_ID_HEADER_KEY = "USER_ID";

	public static class UploadedImage {
		public BufferedImage image;
		public Integer rotationOperation;
	}

	public static Long getIdInUri(HttpServletRequest req) {
		String path = req.getPathInfo();
		if (path == null) {
			//			Preconditions.checkArgument(req.getMethod().equals("GET"), "Missing path parameter : userId");
		}
		else {
			String[] sp = path.split("/");
			try {
				return Long.valueOf(sp[sp.length - 1]);
			} catch (NumberFormatException e) {
				throw e;
			}
		}
		return null;
	}

	public static UploadedImage getUploadedImage(HttpServletRequest request) throws Exception {
		InputStream imgInputStream = request.getPart("userImage").getInputStream();
		ByteArrayOutputStream baos = createByteArrayOutputStreamFromInputStream(imgInputStream);
		InputStream is1 = new ByteArrayInputStream(baos.toByteArray());
		InputStream is2 = new ByteArrayInputStream(baos.toByteArray());
		//		int operation = getOperationToCorrectOrientation(is2);
		BufferedImage img = ImageIO.read(is1);
		UploadedImage n = new UploadedImage();
		n.image = img;
		n.rotationOperation = 0;
		return n;
	}

	private static ByteArrayOutputStream createByteArrayOutputStreamFromInputStream(InputStream is) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len;
		while ((len = is.read(buffer)) > -1) {
			baos.write(buffer, 0, len);
		}
		baos.flush();
		return baos;
	}

	public static JSONObject getBody(HttpServletRequest request) throws Exception {
		JSONObject msg = null;
		if (ServletFileUpload.isMultipartContent(request)) {
			Part jsonBodyPart = request.getPart("json_body");
			Scanner scanner = new Scanner(jsonBodyPart.getInputStream());
			String jsonBodyString = scanner.nextLine();
			msg = (new JSONObject(jsonBodyString)).getJSONObject("msg");
			scanner.close();
		} else {
			StringBuffer jb = new StringBuffer();
			JSONObject body;
			String line = null;
			BufferedReader reader = request.getReader();
			while ((line = reader.readLine()) != null) {
				jb.append(line);
			}
			body = new JSONObject(jb.toString());
			msg = body.getJSONObject("msg");
			if (msg == null) {
				return null;
			}
		}
		return msg;
	}

	public static Map<String, String[]> getParameters(HttpServletRequest request) {
		return request.getParameterMap();
	}

	public static Map<String, String> getParams(HttpServletRequest request) {
		Map<String, String[]> parameters = RequestUtil.getParameters(request);
		Map<String, String> params = new HashMap<String, String>();
		for (Map.Entry<String, String[]> entry : parameters.entrySet()) {
			params.put(entry.getKey(), entry.getValue()[0]);
		}
		return params;
	}

	public static String getString(Map<String, String> request, String key) {
		if (checkExists(request, key)) {
			return request.get(key);
		}
		return null;
	}

	public static Long getLong(Map<String, String> request, String key) {
		if (checkExists(request, key)) {
			return Long.valueOf(request.get(key));
		}
		return null;
	}

	public static Long getLongOrDefault(Map<String, String> request, String key, long defaultVal) {
		Long val = getLong(request, key);
		if (val == null) {
			val = defaultVal;
		}
		return val;
	}

	public static Integer getInteger(Map<String, String> request, String key) {
		if (checkExists(request, key)) {
			return Integer.valueOf(request.get(key));
		}
		return null;
	}

	public static Double getDouble(Map<String, String> request, String key) {
		if (checkExists(request, key)) {
			return Double.valueOf(request.get(key));
		}
		return null;
	}

	public static Boolean getBoolean(Map<String, String> request, String key) {
		if (checkExists(request, key)) {
			String val = request.get(key);
			return Boolean.valueOf(val);
		} else {
			log.info("Key does not exist: {}", key);
		}
		return null;
	}

	public static String getString(JSONObject request, String key) {
		try {
			if (checkExists(request, key)) {
				return request.getString(key);
			}
			return null;
		} catch (JSONException e) {
			log.warn("Failed to process {}", request, e);
			return null;
		}
	}

	public static String getString(JsonObject request, String key) {
		if (checkExists(request, key)) {
			return request.get(key).getAsString();
		}
		return null;
	}

	public static Long getLong(JsonObject request, String key) {
		if (checkExists(request, key)) {
			return request.get(key).getAsLong();
		}
		return null;
	}

	public static Integer getInteger(JsonObject request, String key) {
		if (checkExists(request, key)) {
			return request.get(key).getAsInt();
		}
		return null;
	}

	public static Double getDouble(JsonObject request, String key) {
		if (checkExists(request, key)) {
			return request.get(key).getAsDouble();
		}
		return null;
	}

	public static Boolean getBoolean(JsonObject request, String key) {
		if (checkExists(request, key)) {
			return request.get(key).getAsBoolean();
		}
		return null;
	}

	public static <T> List<T> getArray(JsonObject request, String key) {
		return null;
	}

	public static Long getLong(JSONObject request, String key) {
		try {
			if (checkExists(request, key)) {
				return request.getLong(key);
			}
			return null;
		} catch (JSONException e) {
			log.warn("Failed to process {}", request, e);
			return null;
		}
	}

	public static Integer getInteger(JSONObject request, String key) {
		try {
			if (checkExists(request, key)) {
				return request.getInt(key);
			}
			return null;
		} catch (JSONException e) {
			log.warn("Failed to process {}", request, e);
			return null;
		}
	}

	public static Boolean getBoolean(JSONObject request, String key) {
		try {
			if (checkExists(request, key)) {
				return request.getBoolean(key);
			}
			return null;
		} catch (JSONException e) {
			log.warn("Failed to process {}", request, e);
			return null;
		}
	}

	public static List<Long> getLongArray(JSONObject request, String key) {
		try {
			if (checkExists(request, key)) {
				List<Long> returnArray = new ArrayList<Long>();
				JSONArray arr = request.getJSONArray(key);
				for (int i = 0; i < arr.length(); i++) {
					returnArray.add(arr.getLong(i));
				}
				return returnArray;
			}
			return null;
		} catch (JSONException e) {
			log.warn("Failed to process {}", request, e);
			return null;
		}
	}
	
	/*
	public static AddressBook getAddressBook(JSONObject request, String key) {
		AddressBook addressBook = new AddressBook();
		try {
			if (checkExists(request, key)) {
				JSONArray arr = request.getJSONArray(key);
				
				for (int i = 0; i < arr.length(); i++) {
					JSONObject contactJSON = arr.getJSONObject(i);
					String fName = contactJSON.getString("fname");
					String lName = contactJSON.getString("lname");
					JSONArray contactsJSONArray = contactJSON.getJSONArray("contacts");
					
					String[] numbers = new String[contactsJSONArray.length()];
					
					for (int j = 0; j < contactsJSONArray.length(); j++) {
						numbers[j] = contactsJSONArray.getString(j);						
					}
					
					PhoneContact currentContact = new PhoneContact(fName, lName, numbers);
					addressBook.contacts.add(currentContact);
				}
				return addressBook;
			}
			return null;
		} catch (JSONException e) {
			log.warn("Failed to process {}", request, e);
			return null;
		}
		
	}
	*/
	
	public static AddressBook getAddressBook(JSONObject request, String key) {
		AddressBook addressBook = new AddressBook();
		try {
			if (checkExists(request, key)) {
				JSONArray arr = request.getJSONArray(key);
				
				for (int i = 0; i < arr.length(); i++) {
					JSONObject contactJSON = arr.getJSONObject(i);
					String fName = contactJSON.getString("fname");
					String lName = contactJSON.getString("lname");
					
					ArrayList<String> nameList = new ArrayList<String>();
					nameList.add(0, fName);
					nameList.add(1, lName);
					
					JSONArray contactsJSONArray = contactJSON.getJSONArray("contacts");
				
					for (int j = 0; j < contactsJSONArray.length(); j++) {
						addressBook.contactsList.put(contactsJSONArray.getString(j), nameList);
					}
				}
				return addressBook;
			}
			return null;
		} catch (JSONException e) {
			log.warn("Failed to process {}", request, e);
			return null;
		}
		
	}

	public static String getPlatform(HttpServletRequest request) {
		String platform = request.getHeader(PLATFORM_HEADER_KEY);
		if (platform == null) {
			return "N/A";
		}
		return platform;
	}

	public static String getHeaderUserId(HttpServletRequest request) {
		String userId = request.getHeader(USER_ID_HEADER_KEY);
		if (userId == null) {
			return "N/A";
		}
		return userId;
	}

	private static boolean checkExists(JSONObject request, String key) {
		return request.has(key);
	}

	private static boolean checkExists(JsonObject request, String key) {
		return request.has(key);
	}

	private static boolean checkExists(Map<String, String> request, String key) {
		return request.containsKey(key);
	}
}
