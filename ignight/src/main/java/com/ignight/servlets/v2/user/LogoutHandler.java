package com.ignight.servlets.v2.user;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.ignight.servlets.BaseServletHandler;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.v2.BaseServlet;
import com.ignight.user.User;

@WebServlet(value = "/logout/*")
public class LogoutHandler extends BaseServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected String handle(HttpServletRequest req, HttpServletResponse resp, Long id, JSONObject msg) throws Exception {
		User user = UserDataSource.getCachedUser(id);
		if (user == null) {
			return BaseServletHandler.createFailureResponeWithText("User does not exist.");
		}

		return userHandler.logout(id);
	}

	@Override
	protected String getMethodType() {
		return "POST";
	}
}
