package com.ignight.servlets.admin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ignight.dna.CalcVenueDna;
import com.ignight.dna.DnaElementCount;
import com.ignight.dna.VenueDna;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.user.UserActivity;
import com.ignight.user.UserDna;
import com.ignight.venue.VenueActivity;
import com.util.MapUtil;

import databases.JedisConnection;

public class AggregateVenueDna {
	private VenueActivity activity;
	private static final JedisConnection jedis = JedisConnection.getInstance();
	private static final Logger log = LoggerFactory.getLogger(AggregateVenueDna.class);
	private static Integer MULTIPLIER = 1000;

	public Map<Long, Long> upvotedAgeIdCounter = new HashMap<Long, Long>();
	public Map<Long, Long> upvotedMusicIdCounter = new HashMap<Long, Long>();
	public Map<Long, Long> upvotedAtmosphereIdCounter = new HashMap<Long, Long>();
	public Map<Long, Long> upvotedApendingIdCounter = new HashMap<Long, Long>();
	public Long upvotedCounter = 0l;

	public Map<Long, Long> downvotedAgeIdCounter = new HashMap<Long, Long>();
	public Map<Long, Long> downvotedMusicIdCounter = new HashMap<Long, Long>();
	public Map<Long, Long> downvotedAtmosphereIdCounter = new HashMap<Long, Long>();
	public Map<Long, Long> downvotedSpendingIdCounter = new HashMap<Long, Long>();
	public Long downvotedCounter = 0l;

	public AggregateVenueDna(VenueActivity activity) {
		this.activity = activity;
	}

	public AggregateVenueDna(VenueActivity activity, Map<Long, UserActivity> userActivity) {
		this.activity = activity;
	}

	private UserActivity getUserActivity(Long userId) {
		UserActivity activity = UserDataSource.getUserActivityObj(userId);
		return activity;
	}

	public CalcVenueDna getCalcVenueDna() {
		if (activity == null) {
			return null;
		}
		clear();

		Integer activitySize = 0;
		for (Long user : activity.upvotes) {
			UserDna dna = UserDataSource.getCachedUserDna(user);
			UserActivity userActivity = getUserActivity(user);
			activitySize = userActivity.upvotes.size();
			if (dna != null) {
				applyToPart(upvotedAgeIdCounter, dna.ageId, MULTIPLIER / activitySize);
				applyToPart(upvotedApendingIdCounter, dna.spendingLimit, MULTIPLIER / activitySize);
				applyToPart(upvotedMusicIdCounter, dna.musicIds, activitySize);
				applyToPart(upvotedAtmosphereIdCounter, dna.atmosphereIds, activitySize);
				upvotedCounter += (MULTIPLIER / activitySize);
			}
		}
		VenueDna upvotedDna = getUpvotedVenueDna();

		for (Long user : activity.downvotes) {
			UserDna dna = UserDataSource.getCachedUserDna(user);
			UserActivity userActivity = getUserActivity(user);
			activitySize = userActivity.downvotes.size();
			if (dna != null) {
				applyToPart(downvotedAgeIdCounter, dna.ageId, MULTIPLIER / activitySize);
				applyToPart(downvotedSpendingIdCounter, dna.spendingLimit, MULTIPLIER / activitySize);
				applyToPart(downvotedMusicIdCounter, dna.musicIds, activitySize);
				applyToPart(downvotedAtmosphereIdCounter, dna.atmosphereIds, activitySize);
				downvotedCounter += (MULTIPLIER / activitySize);
			}
		}
		VenueDna downvotedDna = getDownvotedVenueDna();

		return new CalcVenueDna(upvotedDna, downvotedDna);
	}

	private void clear() {
		upvotedAgeIdCounter.clear();
		upvotedMusicIdCounter.clear();
		upvotedAtmosphereIdCounter.clear();
		upvotedApendingIdCounter.clear();
		upvotedCounter = 0l;

		downvotedAgeIdCounter.clear();
		downvotedMusicIdCounter.clear();
		downvotedAtmosphereIdCounter.clear();
		downvotedSpendingIdCounter.clear();
		downvotedCounter = 0l;
	}

	private VenueDna getUpvotedVenueDna() {
		VenueDna venueDna = new VenueDna(activity.id);
		venueDna.ageId = determineMax(upvotedAgeIdCounter);
		venueDna.spendingLimit = determineMax(upvotedApendingIdCounter);
		venueDna.atmospherePreferences = determineTop(upvotedAtmosphereIdCounter, 3);
		venueDna.musicPreferences = determineTop(upvotedMusicIdCounter, 3);
		venueDna.counter = upvotedCounter;
		return venueDna;
	}

	private VenueDna getDownvotedVenueDna() {
		VenueDna venueDna = new VenueDna(activity.id);
		venueDna.ageId = determineMax(downvotedAgeIdCounter);
		venueDna.spendingLimit = determineMax(downvotedSpendingIdCounter);
		venueDna.atmospherePreferences = determineTop(downvotedAtmosphereIdCounter, 3);
		venueDna.musicPreferences = determineTop(downvotedMusicIdCounter, 3);
		venueDna.counter = downvotedCounter;
		return venueDna;
	}

	private long determineMax(Map<Long, Long> values) {
		Map<Long, Long> sorted = MapUtil.sortByValue(values);
		for (Map.Entry<Long, Long> entry : sorted.entrySet()) {
			return entry.getKey();
		}
		return -1;
	}

	private List<DnaElementCount> determineTop(Map<Long, Long> values, int nValues) {
		List<DnaElementCount> topElements = new ArrayList<DnaElementCount>();
		Map<Long, Long> sorted = MapUtil.sortByValue(values);
		for (Map.Entry<Long, Long> entry : sorted.entrySet()) {
			topElements.add(new DnaElementCount(entry.getKey(), entry.getValue()));
			if (topElements.size() == nValues) {
				break;
			}
		}
		return topElements;
	}

	private static void applyToPart(Map<Long, Long> dnaPiece, List<Long> ids, int activitySize) {
		for (int i = 0; i < ids.size(); i++) {
			Long id = ids.get(i);
			if (id >= 0l) {
				applyToPart(dnaPiece, id, getPosScore(i) / activitySize);
			}
		}
	}

	private static int getPosScore(int pos) {
		if (pos == 0) {
			return MULTIPLIER * 3;
		} else if (pos == 1) {
			return MULTIPLIER * 2;
		} else if (pos == 2) {
			return MULTIPLIER * 1;
		}
		return 0;
	}

	private static void applyToPart(Map<Long, Long> dnaPiece, Long id, int score) {
		Long count = dnaPiece.get(id);
		if (count == null) {
			dnaPiece.put(id, (long) score);
		} else {
			dnaPiece.put(id, count + score);
		}
	}
}
