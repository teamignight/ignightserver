package com.ignight.servlets.admin;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin.AdminHandler;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.ignight.servlets.ResponseUtil;
import com.ignight.servlets.data.DataServletHandler;
import com.ignight.servlets.v2.RequestUtil;

@WebServlet(asyncSupported = true, value = "/admin")
public class AdminServlet extends HttpServlet {
	private final static Logger logger = LoggerFactory.getLogger(AdminServlet.class);
	private static final long serialVersionUID = 1L;
	private DataServletHandler handler = new DataServletHandler();
	private AdminHandler adminHandler = new AdminHandler();

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			JsonObject body = getBody(req);
			logger.info("Get Admin Request: uri={}, body={}", req.getRequestURI(), body);
			if (body.has("type")) {
				String type = body.get("type").getAsString();
				Object res = null;
				if (type.equals("reset")) { //VERY DANGEROUS!!!
					res = handler.reset(body);
				} else if (type.equals("clear")) {
					res = handler.clearDailyData(body);
				} else if (type.equals("userTrending")) {
					res = handler.getUserTrendingVenues(body);
				} else if (type.equals("aggregatedVenueDna")) {
					res = handler.gettingAggregatedVenueDna(body);
				} else if (type.equals("userInfo")) {
					res = handler.getUserInfo(body);
				} else if (type.equals("venueTrending")) {
					res = handler.getVenueTrending(body);
				} else if (type.equals("stats")) {
					res = handler.getStats();
				} else if (type.equals("updateTrendingCalcConfig")) {
					res = adminHandler.updateTrendingCalcConfig(body);
				}

				ResponseUtil.sendResponse(resp, res);
			}
		} catch (Exception e) {
			logger.error("Failed to process request", e);
		}
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			Map<String, String> body = RequestUtil.getParams(req);
			logger.info("Get Admin Request: uri={}, body={}", req.getRequestURI(), body);
			if (body.get("type") != null) {
				String type = body.get("type");
				Object res = null;
				if (type.equals("trendingCalcConfig")) {
					res = adminHandler.getTrendingCalcConfig();
				}

				ResponseUtil.sendResponse(resp, res);
			}
		} catch (Exception e) {
			logger.error("Failed to process request", e);
		}
	}

	private JsonObject getBody(HttpServletRequest request) throws JsonIOException, JsonSyntaxException, IOException {
		return new JsonParser().parse(request.getReader()).getAsJsonObject();
	}
}
