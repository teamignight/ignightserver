package com.ignight.servlets;

import javax.servlet.http.HttpServletRequest;

public interface AsyncServlets {
	public void handleGet(HttpServletRequest servletRequest) throws Exception;

	public void handlePost(HttpServletRequest servletRequest) throws Exception;
}
