package com.ignight.servlets.group;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.buzz.Buzz;
import com.buzz.Buzz.BuzzType;
import com.buzz.ClientBuzzData;
import com.ignight.aws.AwsMessageSender;
import com.ignight.core.CoreData;
import com.ignight.dna.TrendingValues;
import com.ignight.group.Group;
import com.ignight.group.GroupDataSource;
import com.ignight.group.GroupDataSource.GroupTrending;
import com.ignight.group.GroupDataSource.PopularGroups;
import com.ignight.server.responses.GroupSearches;
import com.ignight.servlets.BaseServletHandler;
import com.ignight.servlets.group.GroupServletHandler.GroupInfo.Subscription;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.user.GroupInvite;
import com.ignight.user.User;
import com.ignight.user.UserImages;
import com.ignight.venue.Venue;
import com.ignight.venue.VenueDataSource;
import com.push.PushUtil;
import com.stats.Stats;

import databases.Databases;
import databases.Transactable;

public class GroupServletHandler extends BaseServletHandler {
	private static final Logger log = LoggerFactory.getLogger(GroupServletHandler.class);
	private static final AwsMessageSender awsQueue = AwsMessageSender.getInstance();
	private static final Stats stats = Stats.getInstance();

	/*
	 * GET METHODS
	 */

	public String getGroupTrendingList(User user, Group group) {
		List<GroupTrending> trending = GroupDataSource.getGroupTrending(group.id);
		Collections.sort(trending);

		for (GroupTrending t : trending) {
			Venue v = CoreData.getVenue(t.venueId);
			t.venueName = v.name;
			t.latitude = v.latitude;
			t.longitude = v.longitude;
			t.userInput = VenueDataSource.getUserActivity(v.id, user.id);
			t.placeImage = VenueDataSource.getGooglePlaceImageForTrendingList(v.id);
		}

		return createSuccessResponseWithBody(trending);
	}

	public String searchVenuesInGroups(Group group, User user, String search, Long count) {
		List<GroupTrending> groupSearch = new ArrayList<GroupTrending>();
		for (int i = 0; i < CoreData.alphabeticalVenues.size(); i++) {
			Long venueId = CoreData.alphabeticalVenues.get(i);
			Venue v = CoreData.getVenue(venueId);
			String name = v.name.toLowerCase();
			if (name.contains(search.toLowerCase())) {
				GroupTrending gT = new GroupTrending();
				gT.venueId = venueId;
				gT.venueName = v.name;
				gT.longitude = v.longitude;
				gT.latitude = v.latitude;
				gT.userInput = VenueDataSource.getUserActivity(v.id, user.id);
				gT.userVenueValue = GroupDataSource.getGroupVenueValue(group.id, venueId);
				gT.placeImage = VenueDataSource.getGooglePlaceImageForTrendingList(v.id);
				groupSearch.add(gT);
			}
			if (groupSearch.size() >= count) {
				break;
			}
		}
		return createSuccessResponseWithBody(groupSearch);
	}

	public String handleGetListOfUsersInGroup(Group group) {
		List<GroupMember> outputArray = new ArrayList<GroupMember>();

		Set<Long> groupMembers = GroupDataSource.getGroupMembers(group.id);
		for (Long member : groupMembers) {
			User gMember = UserDataSource.getCachedUser(member);
			UserImages images = UserDataSource.getCachedUserImages(member);
			GroupMember groupMember = new GroupMember(
					gMember.id, gMember.username,
					(images != null) ? images.chatPictureUrl : null, gMember.number);
			outputArray.add(groupMember);
		}

		Collections.sort(outputArray);

		Map<String, Object> outputMap = new HashMap<String, Object>();
		outputMap.put("adminId", group.adminId);
		outputMap.put("groupMembers", outputArray);

		return createSuccessResponseWithBody(outputMap);
	}

	public static class GroupMember implements Comparable<GroupMember> {
		public Long userId;
		public String userName;
		public String chatPictureUrl;
		public String number;

		public GroupMember(Long userId, String userName, String url, String number) {
			this.userId = userId;
			this.userName = userName;
			this.chatPictureUrl = url;
			this.number = number;
		}

		@Override
		public int compareTo(GroupMember o) {
			return this.userName.compareTo(o.userName);
		}
	}

	public String handleSearchForGroups(
			User user,
			String searchString,
			long offset,
			long limit) throws SQLException {
		List<GroupSearches> groups = GroupDataSource.searchForGroups(searchString, offset, limit);
		Iterator<GroupSearches> iter = groups.iterator();
		while (iter.hasNext()) {
			GroupSearches group = iter.next();
			group.members = GroupDataSource.getGroupSize(group.id);
			if (group.members <= 0) {
				iter.remove();
			}
			group.isMember = GroupDataSource.isMemberOfGroup(group.id, user.id);
		}

		return createSuccessResponseWithBody(groups);
	}

	public String handleGetPopularGroups(User user, long offset, long limit) {
		List<PopularGroups> groups = GroupDataSource.getPopularGroups(user.id, offset, limit);
		return createSuccessResponseWithBody(groups);
	}

	public String getGroupInfo(User user, Group group) {
		Map<String, Object> groupInfo = new HashMap<String, Object>();
		groupInfo.put("name", group.name);
		groupInfo.put("id", group.id);
		groupInfo.put("description", group.description);
		groupInfo.put("public", !group.isPrivate);
		groupInfo.put("isMember", GroupDataSource.isMemberOfGroup(group.id, user.id));
		groupInfo.put("membersCount", GroupDataSource.getGroupSize(group.id));
		groupInfo.put("subscribed", GroupDataSource.isUserSubscribed(group.id, user.id));

		TrendingValues dna = GroupDataSource.getGroupDna(group.id);
		groupInfo.put("ageBucketMode", (dna != null) ? dna.ageId : -1);
		groupInfo.put("musicTypeMode", (dna != null) ? dna.musicId : -1);
		groupInfo.put("atmosphereTypeMode", (dna != null) ? dna.atmosphereId : -1);
		groupInfo.put("spendingLimitMode", (dna != null) ? dna.spendingId : -1);

		return createSuccessResponseWithBody(groupInfo);
	}

	public String getIncomingGroupRequests(User user) {
		List<GroupInvite> invites = UserDataSource.getInbox(user.id);
		Collections.sort(invites);

		ArrayList<Map<String, Object>> output = new ArrayList<Map<String, Object>>();
		for (GroupInvite g : invites) {
			Map<String, Object> groupInfo = new HashMap<String, Object>();

			User invitee = UserDataSource.getCachedUser(g.inviteeId);
			if (invitee == null) {
				log.error("User {} does not exist", g.inviteeId);
				continue;
			}

			Group group = GroupDataSource.getGroupInCache(g.groupId);
			if (group == null) {
				log.warn("Group {} Not in Cache, Maybe deleted?", g.groupId);
				continue;
			}
			groupInfo.put("groupId", g.groupId);
			groupInfo.put("groupName", group.name);
			groupInfo.put("timestamp", g.ts);
			groupInfo.put("userName", invitee.username);

			UserImages images = UserDataSource.getCachedUserImages(g.inviteeId);
			groupInfo.put("userPicture", (images != null) ? images.chatPictureUrl : null);

			output.add(groupInfo);
		}
		UserDataSource.updateGroupInviteNotification(user, false);
		return createSuccessResponseWithBody(output);
	}

	public String getGroupBuzz(Group group, User user, Long count, Long lastBuzzId, Boolean latest) {
		return getBuzz(group.id, user.id, lastBuzzId, count, latest);
	}

	private String getBuzz(Long groupId, Long userId, Long lastBuzzId, Long count, Boolean latest) {
		List<Buzz> buzzes = GroupDataSource.getBuzz(groupId, lastBuzzId, count, latest);

		List<ClientBuzzData> output = new ArrayList<ClientBuzzData>();
		for (Buzz b : buzzes) {
			ClientBuzzData out = new ClientBuzzData(b);
			User buzzUser = UserDataSource.getCachedUser(out.userId);
			if (buzzUser == null) {
				log.error("Buzz user does not exist: {}", out.userId);
				continue;
			}
			out.userName = buzzUser.username;
			UserImages images = UserDataSource.getCachedUserImages(out.userId);
			if (images != null) {
				out.chatPictureURL = images.chatPictureUrl;
			}
			output.add(out);
		}

		GroupDataSource.updateUserGroupLastBuzzAccessTimestamp(userId, groupId, System.currentTimeMillis());

		GroupDataSource.addUserAsListener(groupId, userId);

		Map<String, Object> result = new HashMap<String, Object>();
		result.put("buzz", output);

		return createSuccessResponseWithBody(result);
	}

	public String getGroupBuzzImages(Group group, Long buzzImageStartId, Integer noOfBuzzElements) {
		List<com.ignight.group.GroupDataSource.ClientBuzzImageData> buzzImages = GroupDataSource.getBuzzImages(group.id, buzzImageStartId + 1, noOfBuzzElements);
		Collections.reverse(buzzImages);

		Map<String, Object> output = new HashMap<String, Object>();
		output.put("buzz", buzzImages);
		return createSuccessResponseWithBody(output);
	}

	/*
	 * POST METHODS
	 */

	public String addGroup(final User user, final Long cityId, final String groupName, final String groupDescription, final Boolean isPrivateGroup) {
		final Group group = new Group();
		group.name = groupName;
		group.cityId = cityId;
		group.description = groupDescription;
		group.isPrivate = isPrivateGroup;
		group.adminId = user.id;

		group.id = Databases.transact(new Transactable<Long>() {
			@Override
			public Long transact(Connection connection) throws Exception {
				Long groupId = Databases.insert(connection, "INSERT INTO groups (name, city_id, admin_id, " +
						"description, private) VALUES (?, ?, ?, ?, ?)",
						group.name, group.cityId, group.adminId, group.description, group.isPrivate);
				Databases.insert(connection, "INSERT INTO group_members (user_id, group_id) VALUES (?, ?)",
						user.id, groupId);
				return groupId;
			}
		});

		GroupDataSource.addGroup(group, user.id);

		if (isPrivateGroup) {
			awsQueue.sendNewPrivateGroup(group.id, user.id);
		} else {
			awsQueue.sendNewPublicGroup(group.id, user.id);
		}

		stats.addNewGroup(group.id);
		return createSuccessResponseWithBody(group);
	}

	//need to remove last buzz access timestamp
	public String leaveGroup(final User user, final Group group) {
		final long userId = user.id;
		final long groupId = group.id;

		Databases.transact(new Transactable<Void>() {
			@Override
			public Void transact(Connection conn) throws Exception {
				Databases.delete(conn,
						"DELETE FROM group_members " +
								"WHERE group_id = ? AND user_id = ?",
						groupId, userId);
				GroupDataSource.removeMemberFromGroup(groupId, userId);
				UserDataSource.leaveGroup(userId, groupId);

				if (group.isPrivate && group.adminId.equals(userId)) {
					Long newAdminId = Databases.selectLong(conn,
							"SELECT user_id FROM group_members " +
									"WHERE group_id = ?", groupId);
					log.info("Private Group, Admin {} Leaving, NewAdminId: {}", userId, newAdminId);
					if (newAdminId != null) {
						Databases.update(conn,
								"UPDATE groups SET admin_id = ? " +
										"WHERE id = ?", userId, groupId);
						group.adminId = newAdminId;
						GroupDataSource.updateGroupInCache(group);
					}
				}

				if (GroupDataSource.getGroupSize(groupId) <= 0) {
					log.info("Deleting Group: {}", group);
					Databases.update(conn, "UPDATE groups SET deleted_time = NOW() " +
							"WHERE id = ?", group.id);
					GroupDataSource.deleteGroup(group);
				}

				Set<Long> activity = UserDataSource.getUserActivity(userId);
				for (Long a : activity) {
					GroupDataSource.updateGroupTrending(groupId, Math.abs(a), (a < 0) ? 1l : -1l);
				}

				return null;
			}
		});

		awsQueue.sendLeaveGroup(groupId, userId);
		GroupDataSource.removeUserGroupLastBuzzAccessTimestamp(userId, groupId);

		Map<String, Object> response = new HashMap<String, Object>();
		response.put("groupId", groupId);
		return createSuccessResponseWithBody(response);
	}

	public String joinGroup(User user, Group group) {
		final Long userId = user.id;
		final Long groupId = group.id;

		if (group.isPrivate) {
			log.error("User {} asking to join private group {}", userId, groupId);
			return createFailureResponeWithText("Cant join private group.");
		}

		Databases.transact(new Transactable<Void>() {
			@Override
			public Void transact(Connection connection) throws Exception {
				Databases.insert(connection, "INSERT INTO group_members (user_id, group_id) VALUES (?, ?)", userId, groupId);
				Databases.update(connection, "DELETE FROM group_invites WHERE user_id = ? AND group_id = ?", userId, groupId);
				GroupDataSource.joinedGroup(userId, groupId);
				return null;
			}
		});

		awsQueue.sendJoinGroup(groupId, userId);

		Map<String, Object> response = new HashMap<String, Object>();
		response.put("groupId", groupId);
		return createSuccessResponseWithBody(response);
	}

	public String respondToInvite(User user, Group group, final Boolean accept) {
		final Long userId = user.id;
		final Long groupId = group.id;

		Databases.transact(new Transactable<Void>() {
			@Override
			public Void transact(Connection connection) throws Exception {
				Databases.update(connection, "DELETE FROM group_invites WHERE user_id = ? AND group_id = ?",
						userId, groupId);
				if (accept)
					Databases.insert(connection, "INSERT INTO group_members (user_id, group_id) VALUES (?, ?)",
							userId, groupId);
				return null;
			}
		});

		GroupDataSource.respondToInvite(groupId, userId, accept);

		if (accept) {
			if (group.isPrivate) {
				GroupDataSource.registerUserForPush(group, user.id);
			}
			awsQueue.sendJoinGroup(groupId, userId);
		}

		Map<String, Object> response = new HashMap<String, Object>();
		response.put("groupId", groupId);

		return createSuccessResponseWithBody(response);
	}

	public String inviteUser(Group group, User user, User targetUser) throws SQLException {
		Long groupId = group.id;
		Long userId = user.id;
		Long targetUserId = targetUser.id;

		boolean invite = false;
		if (!GroupDataSource.isMemberOfGroup(groupId, targetUserId)) {
			long count = Databases.selectCount("SELECT COUNT(*) " +
					"FROM group_invites WHERE user_id = ? AND group_id= ? ",
					targetUserId, group.id);
			invite = count == 0;
			if (invite) {
				Databases.insert("INSERT INTO group_invites (user_id, invitee_id, group_id) " +
						"VALUES (?, ?, ?)", targetUserId, userId, groupId);
				GroupDataSource.inviteUserToGroup(groupId, userId, targetUserId);
			}
		}

		if (invite) {
			UserDataSource.updateGroupInviteNotification(targetUser, true);
			PushUtil.sendGroupInvitation(targetUser, user, group);
		}

		return createSuccessResponse();
	}

	public String addGroupBuzz(User user, Group group, String buzzText) {
		Long userId = user.id;
		Long groupId = group.id;

		Buzz buzz = new Buzz();
		buzz.buzzDateStamp = System.currentTimeMillis();
		buzz.userId = userId;
		buzz.buzzType = BuzzType.TEXT;
		buzz.buzzText = buzzText;
		buzz.buzzId = GroupDataSource.addBuzz(groupId, buzz);

		Map<String, Object> addedBuzz = new HashMap<String, Object>();
		addedBuzz.put("buzzId", buzz.buzzId);
		addedBuzz.put("buzz", buzz.buzzText);

		group.lastBuzzTimeStamp = buzz.buzzDateStamp;
		GroupDataSource.updateGroupInCache(group);

		GroupDataSource.updateUserGroupLastBuzzAccessTimestamp(userId, groupId, buzz.buzzDateStamp);

		distributeBuzz(group, user, buzz.buzzId);

		return createSuccessResponseWithBody(addedBuzz);
	}

	public String addGroupImageBuzz(User user, Group group, String url) {
		Long userId = user.id;
		Long groupId = group.id;

		Buzz buzz = new Buzz();
		buzz.buzzDateStamp = System.currentTimeMillis();
		buzz.userId = userId;
		buzz.buzzType = BuzzType.IMAGE;
		buzz.buzzText = url;
		buzz.buzzId = GroupDataSource.addBuzz(groupId, buzz);

		Map<String, Object> addedBuzz = new HashMap<String, Object>();
		addedBuzz.put("buzzId", buzz.buzzId);
		addedBuzz.put("buzzUrl", buzz.buzzText);

		group.lastBuzzTimeStamp = buzz.buzzDateStamp;
		GroupDataSource.updateGroupInCache(group);

		GroupDataSource.updateUserGroupLastBuzzAccessTimestamp(userId, groupId, buzz.buzzDateStamp);

		distributeBuzz(group, user, buzz.buzzId);

		return createSuccessResponseWithBody(addedBuzz);
	}

	public String flagGroupBuzz(Long userId, Long groupId, Long buzzId) throws SQLException {
		GroupDataSource.flagGroupBuzz(userId, groupId, buzzId);
		return createSuccessResponse();
	}

	public String flagGroup(Long userId, Long groupId) throws SQLException {
		GroupDataSource.flagGroup(userId, groupId);
		return createSuccessResponse();
	}

	public static class GroupInfo {
		public Long groupId;
		public Subscription status;

		public GroupInfo(Long groupId, Subscription status) {
			this.groupId = groupId;
			this.status = status;
		}

		public enum Subscription {
			REGISTERED, UNREGISTERED;
		}
	}

	public String registerUserForPush(User user, Group group) {
		GroupDataSource.registerUserForPush(group, user.id);
		return createSuccessResponseWithBody(new GroupInfo(group.id, Subscription.REGISTERED));
	}

	public String unregisterUserForPush(User user, Group group) {
		GroupDataSource.removeUserAsSubscriber(group.id, user.id);

		GroupDataSource.updateUserGroupLastBuzzAccessTimestamp(user.id, group.id, System.currentTimeMillis());

		return createSuccessResponseWithBody(new GroupInfo(group.id, Subscription.UNREGISTERED));
	}

	private void distributeBuzz(Group group, User user, Long lastBuzzId) {
		GroupDataSource.updateMemberBuzzNotifications(user, group);

		Set<Long> listeners = GroupDataSource.getGroupBuzzListeners(group.id, user.id);
		listeners.remove(user.id);
		if (listeners.size() > 0) {
			PushUtil.sendNewGroupBuzz(listeners, group, user, lastBuzzId);
		}

		Set<Long> subscribers = GroupDataSource.getGroupSubscribers(group.id);
		subscribers.removeAll(listeners);
		subscribers.remove(user.id);
		if (subscribers.size() > 0) {
			PushUtil.sendNewGroupBuzzAlerts(subscribers, group, user, lastBuzzId);
		}
	}
}
