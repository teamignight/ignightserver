package com.ignight.servlets.filter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ignight.data.DataSource;
import com.ignight.servlets.ResponseUtil;
import com.ignight.servlets.v2.RequestUtil;
import com.util.IgnightProperties;

@WebFilter(urlPatterns = "/*", asyncSupported = true)
public class HeaderFilter implements Filter {
	private static final Logger log = LoggerFactory.getLogger(HeaderFilter.class);
	private static final String IGNIGHT_TOKEN_KEY = "X-IGNIGHT-TOKEN";
	private static final String IGNIGHT_TOKEN = "E5DD71E0-710F-498D-A101-1E16B0617527";

	private static final ArrayList<String> whitelist = new ArrayList<String>();

	static {
		whitelist.add("ping");
		whitelist.add("data");
		whitelist.add("admin");
		whitelist.add("data/clear");
	}

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) arg0;
		String uri = req.getRequestURI();
		String[] split = uri.split("/");
		String whiteListCheck = "";
		for (int i = 2; i < split.length; i++) {
			if (!whiteListCheck.equals("")) {
				whiteListCheck += "/";
			}
			whiteListCheck += split[i];
		}

		if (whitelist.contains(whiteListCheck)) {
			chain.doFilter(arg0, arg1);
			return;
		}

		String blockServerCallsResponse;
		try {
			blockServerCallsResponse = blockServerCalls();
			if (blockServerCallsResponse != null) {
				log.info("Server is archiving, blocking call: {} : uId={}, " +
						"platform={}, uri={}",
						req.getMethod(),
						RequestUtil.getHeaderUserId(req),
						RequestUtil.getPlatform(req),
						uri);
				ResponseUtil.sendServerBlockedResponse((HttpServletResponse) arg1, blockServerCallsResponse);
				return;
			}
		} catch (SQLException e) {
			log.error("Error executing blocking action", e);
		}

		String token = req.getHeader(IGNIGHT_TOKEN_KEY);
		if (token != null) {
			if (token.equals(IGNIGHT_TOKEN)) {
				chain.doFilter(arg0, arg1);
			} else {
				log.info("Invalid token: " + token);
			}
		} else {
			log.info("No token provided");
		}
	}

	//Can add other conditions for blocking server calls here
	protected String blockServerCalls() throws SQLException {
		Long cityId = IgnightProperties.getInstance().getCityId();
		if (DataSource.isArchiveInProgress(cityId)) {
			return "Server is being refreshed. Please try again shortly.";
		}
		return null;
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

}
