package com.ignight.servlets.user;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.karneim.pojobuilder.GeneratePojoBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ignight.dna.CalcVenueDna;
import com.ignight.dna.TrendingBarCalculator;
import com.ignight.dna.TrendingVenue;
import com.ignight.group.GroupDataSource;
import com.ignight.imagehandling.ImageHandler;
import com.ignight.servlets.v2.user.InvalidValueException;
import com.ignight.user.AddressBook;
import com.ignight.user.GroupInvite;
import com.ignight.user.RedisKeys;
import com.ignight.user.User;
import com.ignight.user.UserActivity;
import com.ignight.user.UserDna;
import com.ignight.user.UserImages;
import com.ignight.venue.VenueDataSource;
import com.util.IgnightProperties;
import com.util.PasswordService;
import com.util.UserToVenueDna;

import databases.Databases;
import databases.DbException;
import databases.JedisConnection;
import databases.Resources;

public class UserDataSource {
	private static Logger log = LoggerFactory.getLogger(UserDataSource.class);
	private static Gson gson = new GsonBuilder().disableHtmlEscaping().create();

	public static final JedisConnection jedis = JedisConnection.getInstance();

	@GeneratePojoBuilder
	public static class UserNotifications {
		public boolean groupBuzz = false;
		public boolean inbox = false;
	}

	public static UserNotifications getUserNotifications(User user) {
		UserNotificationsBuilder builder = new UserNotificationsBuilder();

		String userIdString = user.id.toString();
		String notificationKey = RedisKeys.getUserGroupBuzzNotificationSetKey();
		builder.withGroupBuzz(jedis.sismember(notificationKey, userIdString));

		notificationKey = RedisKeys.getUserGroupInvitesNotificationsSetKey();
		builder.withInbox(jedis.sismember(notificationKey, userIdString));

		return builder.build();
	}

	public static UserNotifications clearGroupBuzzNotification(User user) {
		updateGroupBuzzNotification(user, false);
		return getUserNotifications(user);
	}

	public static void updateGroupBuzzNotification(User user, boolean add) {
		String notificationKey = RedisKeys.getUserGroupBuzzNotificationSetKey();
		if (add) {
			jedis.sadd(notificationKey, user.id.toString());
		} else {
			jedis.srem(notificationKey, user.id.toString());
		}
	}

	public static UserNotifications clearGroupInviteNotification(User user) {
		updateGroupInviteNotification(user, false);
		return getUserNotifications(user);
	}

	public static void updateGroupInviteNotification(User user, boolean add) {
		String notificationKey = RedisKeys.getUserGroupInvitesNotificationsSetKey();
		if (add) {
			jedis.sadd(notificationKey, user.id.toString());
		} else {
			jedis.srem(notificationKey, user.id.toString());
		}
	}

	public static long addUser(String email, String username, String password) throws NoSuchAlgorithmException, UnsupportedEncodingException, SQLException, InvalidValueException {
		if (doesEmailExist(email)) {
			throw new InvalidValueException("Email already exists.");
		}

		if (doesUsernameExist(username)) {
			throw new InvalidValueException("Username already exists.");
		}

		PasswordService passwordService = PasswordService.getInstance();
		String passwordDigest = passwordService.encrypt(password);
		return Databases.insert("INSERT INTO users (email, username, password) VALUES (?, ?, ?)", email.trim(), username.trim(), passwordDigest);
	}

	public static class UserSearch {
		public Long id;
		public String username;
		public String number;
	}

	public static Set<Long> getListOfUserIdsForCity(Long cityId) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		Set<Long> userIds = new HashSet<Long>();

		try {
			conn = Databases.dataSource.getConnection();
			ps = conn.prepareStatement(
					"SELECT id FROM users WHERE city_id = " + cityId);
			rs = ps.executeQuery();

			while (rs.next()) {
				Long nextUserId = rs.getLong("id");
				userIds.add(nextUserId);
			}

			return userIds;

		} finally {
			Resources.close(conn, ps, rs);
		}
	}

	public static List<UserSearch> searchForUsers(String search, long offset, long limit) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		List<UserSearch> usernames = new ArrayList<UserSearch>();
		try {
			conn = Databases.dataSource.getConnection();
			ps = conn.prepareStatement(
					"SELECT id, username, number FROM users " +
							"WHERE username LIKE '%" + search + "%' LIMIT ?, ?");
			ps.setObject(1, offset);
			ps.setObject(2, limit);
			rs = ps.executeQuery();
			while (rs.next()) {
				UserSearch username = new UserSearch();
				username.id = rs.getLong("id");
				username.username = rs.getString("username");
				username.number = rs.getString("number"); 
				usernames.add(username);
			}

			return usernames;
		} finally {
			Resources.close(conn, ps, rs);
		}
	}

	public static User getUser(String username) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			conn = Databases.dataSource.getConnection();
			ps = conn.prepareStatement(
					"SELECT id, city_id, email, username, password, gender_id, " +
							"profile_picture_url " +
							"FROM users WHERE username = ? ");
			ps.setObject(1, username);
			rs = ps.executeQuery();

			User user = null;
			if (rs.next()) {
				user = new User();
				user.id = rs.getLong("id");
				user.cityId = rs.getLong("city_id");
				user.email = rs.getString("email");
				user.username = rs.getString("username");
				user.setEncryptedPassword(rs.getString("password"));
				user.genderId = rs.getLong("gender_id");
				user.profilePictureUrl = rs.getString("profile_picture_url");
				return user;
			} else {
				return null;
			}
		} finally {
			Resources.close(conn, ps, rs);
		}
	}

	public static void loadAndSavedUsersToRedis() throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			conn = Databases.dataSource.getConnection();
			ps = conn.prepareStatement("SELECT * FROM users WHERE deleted_time IS NULL");
			rs = ps.executeQuery();

			List<User> users = new ArrayList<User>();
			while (rs.next()) {
				User u = new User();
				u.id = rs.getLong(1);
				User user = UserDataSource.getCachedUser(u.id);
				if (user != null) {
					continue;
				} else {
					log.error("User Not In DB: {}", u.id);
				}
				u.cityId = rs.getLong(2);
				u.email = rs.getString(3);
				u.username = rs.getString(4);
				//Dont want to store the password to redis
				u.genderId = rs.getLong(6);
				u.notifyOfGroupInvites = rs.getBoolean(7);
				u.profilePictureUrl = rs.getString(8);
				u.number = rs.getString(9);
				users.add(u);
			}

			log.info("Loaded {} Unsaved Users From SQL", users.size());

			for (User u : users) {
				String key = RedisKeys.getUserBucketKey(u.id);
				String obj = jedis.hget(key, u.id.toString());
				if (obj == null) {
					log.warn("Adding User To Cache: {} , {}", u.id, u.username);
					jedis.hset(key, u.id.toString(), u);
				}
			}
		} finally {
			Resources.close(conn, ps, rs);
		}
	}

	public static void loadAndSavedUserDnaToRedis() throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			conn = Databases.dataSource.getConnection();
			ps = conn.prepareStatement("SELECT * FROM user_dna ");
			rs = ps.executeQuery();

			List<UserDna> userDnas = new ArrayList<UserDna>();
			while (rs.next()) {
				UserDna u = new UserDna();
				u.userId = rs.getLong(1);
				u.cityId = rs.getLong(2);
				u.ageId = rs.getLong(3);
				u.spendingLimit = rs.getLong(4);
				u.musicIds = new ArrayList<Long>();
				u.musicIds.add(rs.getLong(5));
				u.musicIds.add(rs.getLong(6));
				u.musicIds.add(rs.getLong(7));
				u.atmosphereIds = new ArrayList<Long>();
				u.atmosphereIds.add(rs.getLong(8));
				u.atmosphereIds.add(rs.getLong(9));
				u.atmosphereIds.add(rs.getLong(10));
				userDnas.add(u);
			}

			log.info("Loaded {} UserDna's From SQL", userDnas.size());

			for (UserDna u : userDnas) {
				String key = RedisKeys.getUserDnaBucketKey(u.userId);
				String obj = jedis.hget(key, u.userId.toString());
				if (obj == null) {
					log.warn("Adding UserDna To Cache: {}", u.userId);
					jedis.hset(key, u.userId.toString(), u);
				}
			}
		} finally {
			Resources.close(conn, ps, rs);
		}
	}

	public static void loadAndSavedUserImagesToRedis() throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			conn = Databases.dataSource.getConnection();
			ps = conn.prepareStatement("SELECT * FROM user_images ");
			rs = ps.executeQuery();

			List<UserImages> userImages = new ArrayList<UserImages>();
			while (rs.next()) {
				UserImages u = new UserImages();
				u.userId = rs.getLong(1);
				u.profilePictureUrl = rs.getString(2);
				u.chatPictureUrl = rs.getString(3);
				userImages.add(u);
			}

			log.info("Loaded {} UserImages's From SQL", userImages.size());

			for (UserImages u : userImages) {
				String key = RedisKeys.getUserImagesKey(u.userId);
				String obj = jedis.hget(key, u.userId.toString());
				if (obj == null) {
					log.warn("Adding UserImages To Cache: {}", u.userId);
					jedis.hset(key, u.userId.toString(), u);
				}
			}
		} finally {
			Resources.close(conn, ps, rs);
		}
	}

	public static UserDna getUserDna(Long userId) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			conn = Databases.dataSource.getConnection();
			ps = conn.prepareStatement(
					"SELECT city_id, age_id, spending_limit, music_choice_1, music_choice_2, music_choice_3, " +
							"atmosphere_choice_1, atmosphere_choice_2, atmosphere_choice_3 " +
							"FROM user_dna WHERE user_id = ? ");
			ps.setObject(1, userId);
			rs = ps.executeQuery();

			UserDna dna = null;
			if (rs.next()) {
				dna = new UserDna();
				dna.userId = userId;
				dna.cityId = rs.getLong("city_id");
				dna.ageId = rs.getLong("age_id");
				dna.spendingLimit = rs.getLong("spending_limit");
				for (int i = 1; i <= 3; i++) {
					Long id = rs.getLong("music_choice_" + i);
					if (id != null) {
						dna.musicIds.add(id);
					}
					id = rs.getLong("atmosphere_choice_" + i);
					if (id != null) {
						dna.atmosphereIds.add(id);
					}
				}
				return dna;
			} else {
				return null;
			}
		} finally {
			Resources.close(conn, ps, rs);
		}
	}

	public static void updateCompleteUser(User user, UserDna dna) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			conn = Databases.dataSource.getConnection();
			conn.setAutoCommit(false);

			String sql = "UPDATE users SET city_id = ?, email = ?, username = ?, gender_id = ? WHERE id = ?";
			ps = conn.prepareStatement(sql);
			ps.setObject(1, user.cityId);
			ps.setObject(2, user.email);
			ps.setObject(3, user.username);
			ps.setObject(4, user.genderId);
			ps.setObject(5, user.id);
			int updated = ps.executeUpdate();
			if (updated == 0) {
				throw new SQLException("Failed to update users");
			}

			sql = "INSERT INTO user_dna " +
					"(user_id, city_id, age_id, spending_limit, music_choice_1, music_choice_2, music_choice_3, " +
					"atmosphere_choice_1, atmosphere_choice_2, atmosphere_choice_3) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			ps = conn.prepareStatement(sql);
			ps.setObject(1, user.id);
			ps.setObject(2, user.cityId);
			ps.setObject(3, dna.ageId);
			ps.setObject(4, dna.spendingLimit);
			ps.setObject(5, dna.musicIds.get(0));
			long val = dna.musicIds.get(1);
			ps.setObject(6, val >= 0 ? val : -1);
			val = dna.musicIds.get(2);
			ps.setObject(7, val >= 0 ? val : -1);
			ps.setObject(8, dna.atmosphereIds.get(0));
			val = dna.atmosphereIds.get(1);
			ps.setObject(9, val >= 0 ? val : -1);
			val = dna.atmosphereIds.get(2);
			ps.setObject(10, val >= 0 ? val : -1);
			updated = ps.executeUpdate();
			if (updated == 0) {
				throw new SQLException("Failed to inser dna for user: " + user.id);
			}
			conn.commit();
		} catch (SQLException e) {
			log.error("Failed to update user", e);
			Resources.rollback(conn);
			throw e;
		} catch (Exception e) {
			log.error("Failed to update user", e);
			Resources.rollback(conn);
			throw new DbException(e);
		} finally {
			Resources.close(conn, ps, rs);
		}
	}

	public static void updateUserPassword(String username, String password) throws SQLException {
		Databases.update("UPDATE users SET password = ? WHERE username = ?", password, username.toLowerCase());
	}

	public static void addGroupForUser(Long userId, Long groupId) {
		String key = RedisKeys.getUserGroupsKey(userId);
		jedis.sadd(key, groupId.toString());
	}

	public static void leaveGroup(Long userId, Long groupId) {
		String key = RedisKeys.getUserGroupsKey(userId);
		jedis.srem(key, groupId.toString());
	}

	public static void addInviteForUser(Long userId, GroupInvite invite) {
		String key = RedisKeys.getUserInboxKey(userId);
		jedis.hset(key, invite.groupId.toString(), invite);
	}

	public static void removeInviteForUser(Long userId, Long groupId) {
		String key = RedisKeys.getUserInboxKey(userId);
		jedis.hdel(key, groupId.toString());
	}

	public static List<GroupInvite> getInbox(Long userId) {
		String key = RedisKeys.getUserInboxKey(userId);
		Map<String, String> results = jedis.hgetall(key);
		List<GroupInvite> invites = new ArrayList<GroupInvite>();
		for (Map.Entry<String, String> entry : results.entrySet()) {
			GroupInvite invite = gson.fromJson(entry.getValue(), GroupInvite.class);
			invites.add(invite);
		}
		return invites;
	}

	public static Set<Long> getUserGroups(Long userId) {
		String key = RedisKeys.getUserGroupsKey(userId);
		Set<String> members = jedis.smembers(key);
		Set<Long> result = new HashSet<Long>();
		for (String member : members) {
			result.add(Long.valueOf(member));
		}
		return result;
	}

	public static void removeGroupForUser(Long groupId, Long userId) {
		leaveGroup(userId, groupId);
	}

	public static boolean isVenueTrending(Long userId, Long venueId) {
		String key = RedisKeys.getUserTrendingVenuesKey(userId);
		if (key != null) {
			String val = jedis.hget(key, venueId.toString());
			if (val != null) {
				return true;
			}
		}
		return false;
	}

	private static ArrayList<TrendingVenue> getUserTrendingDnaValues(Long userId) {
		ArrayList<TrendingVenue> venues = new ArrayList<TrendingVenue>();
		String key = RedisKeys.getVenueDnaKey();
		while (true) {
			if (key != null) {
				Map<String, String> data = jedis.hgetall(key);
				if (data != null) {
					//					UserActivity activity = getUserActivityObj(userId);
					//					UserDna dna = getCachedUserDna(userId);
					for (Map.Entry<String, String> entry : data.entrySet()) {
						Long venueId = Long.valueOf(entry.getKey());
						if (VenueDataSource.isVenueActive(venueId)) {
							CalcVenueDna calcVenueDna = gson.fromJson(entry.getValue(), CalcVenueDna.class);
							//							if (activity.upvotes.contains(venueId)) { //Upvoted
							//								calcVenueDna.removeUserDnaFromUpvote(dna, activity.upvotes.size());
							//							} else if (activity.downvotes.contains(-1 * venueId)) { //Downvoted
							//								calcVenueDna.addUserDnaFromDownvote(dna, activity.downvotes.size());
							//							}
							Double value = UserToVenueDna.getUserDnaValue(userId, calcVenueDna);
							if (value != null) {
								venues.add(new TrendingVenue(venueId, value, calcVenueDna.getBaseUserVenueScore(), true));
							}
						} else {
							jedis.hdel(key, venueId.toString());
						}
					}
					break;
				}
			}
		}
		return venues;
	}

	public static void saveTrendingVenueBarColors(Long userId, ArrayList<TrendingVenue> venues) {
		if (venues.size() < 0) {
			return;
		}
		String key = RedisKeys.getUserTrendingBarColorsKey(userId);
		log.debug("Deleting User Trenidng Bar Colors - Key: {}", key);
		jedis.del(key);
		Map<String, String> values = new HashMap<String, String>();
		if (venues.size() > 0) {
			Long barColor = venues.get(0).barColorId;
			Long colorCounter = 10000l;
			for (TrendingVenue tv : venues) {
				if (barColor.equals(tv.barColorId)) {
					colorCounter--;
				} else {
					colorCounter = 99999l;
					barColor = tv.barColorId;
				}
				String val = tv.barColorId + "." + colorCounter;
				values.put(tv.id.toString(), val);
			}
			log.debug("Saving User Trending Bar Colors - Key: {}", key);
			jedis.hmset(key, values);
		}
	}

	public static void updateUserBarRefreshKey(Long userId) {
		String key = RedisKeys.getUserTrendingBarRefreshKey(userId);
		Long currTs = System.currentTimeMillis();
		jedis.set(key, currTs.toString());
	}

	public static Long getUserVenueBarColor(Long userId, Long venueId) {
		String key = RedisKeys.getUserTrendingBarColorsKey(userId);
		String val = jedis.hget(key, venueId.toString());
		if (val == null) {
			return 4l;
		}
		return Double.valueOf(val).longValue();
	}

	public static ArrayList<TrendingVenue> getUserTrendingVenues(Long userId) {
		String key = RedisKeys.getUserTrendingBarRefreshKey(userId);
		String val = jedis.get(key);

		ArrayList<TrendingVenue> venues = new ArrayList<TrendingVenue>();
		if (val == null || (System.currentTimeMillis() - Long.valueOf(val)) > IgnightProperties.getInstance().getBarColorRefreshMillis()) {
			venues = calcTrendingVenues(userId);
		} else {
			venues = getCachedTrendingVenues(key, userId);
		}
		return venues;
	}

	public static ArrayList<TrendingVenue> calcTrendingVenues(Long userId) {
		ArrayList<TrendingVenue> venues = new ArrayList<TrendingVenue>();
		log.info("Recalcing Trending Values: user = {}", userId);
		venues = getUserTrendingDnaValues(userId);
		TrendingBarCalculator calculator = new TrendingBarCalculator(venues);
		calculator.applyBarColors();
		saveTrendingVenueBarColors(userId, venues);
		//TODO: Delete Bar Color Key if size is 0
		updateUserBarRefreshKey(userId);
		return venues;
	}

	public static ArrayList<TrendingVenue> getCachedTrendingVenues(String key, Long userId) {
		ArrayList<TrendingVenue> venues = new ArrayList<TrendingVenue>();
		log.info("Using Cached Bar Color Values: user = {}", userId);
		key = RedisKeys.getUserTrendingBarColorsKey(userId);
		if (key != null) {
			Map<String, String> data = jedis.hgetall(key);
			if (data != null) {
				for (Map.Entry<String, String> entry : data.entrySet()) {
					Long venueId = Long.valueOf(entry.getKey());
					Double value = Double.valueOf(entry.getValue());
					venues.add(new TrendingVenue(venueId, value, false));
				}
			}
		}
		return venues;
	}
	
	

	public static void setUserActivity(Long userId, Long venueId, Long activityValue) {
		String key = RedisKeys.getUserActivityKey(userId);
		Long oVId = -1 * venueId;
		jedis.srem(key, venueId.toString());
		jedis.srem(key, (oVId).toString());

		if (activityValue > 0) {
			jedis.sadd(key, venueId.toString());
		} else if (activityValue < 0) {
			jedis.sadd(key, oVId.toString());
		}
	}

	public static Set<Long> getUserActivity(Long userId) {
		String key = RedisKeys.getUserActivityKey(userId);
		Set<String> activity = jedis.smembers(key);
		Set<Long> venueActivity = new HashSet<Long>();
		if (activity != null) {
			for (String s : activity) {
				venueActivity.add(Long.valueOf(s));
			}
		}
		return venueActivity;
	}

	public static UserActivity getUserActivityObj(long userId) {
		Set<Long> activity = getUserActivity(userId);

		UserActivity userActivity = new UserActivity();
		for (Long a : activity) {
			if (a < 0) {
				userActivity.downvotes.add(-1 * a);
			} else {
				userActivity.upvotes.add(a);
			}
		}
		return userActivity;
	}

	public static Set<Long> getUserUpvotes(Long userId) {
		Set<Long> activity = getUserActivity(userId);
		Set<Long> upvotes = new HashSet<Long>();
		for (Long a : activity) {
			if (a > 0) {
				upvotes.add(a);
			}
		}
		return upvotes;
	}

	public static User getCachedUser(Long userId) {
		String data = jedis.hget(RedisKeys.getUserBucketKey(userId), userId.toString());
		if (data != null) {
			return gson.fromJson(data, User.class);
		}
		return null;
	}

	public static UserDna getCachedUserDna(Long userId) {
		String data = jedis.hget(RedisKeys.getUserDnaBucketKey(userId), userId.toString());
		if (data != null) {
			return gson.fromJson(data, UserDna.class);
		}
		return null;
	}

	public static UserImages getCachedUserImages(Long userId) {
		String key = RedisKeys.getUserImagesKey(userId);
		String data = jedis.hget(key, userId.toString());
		if (data != null) {
			return gson.fromJson(data, UserImages.class);
		}
		return null;
	}

	public static void saveUserImages(UserImages userImages) {
		String key = RedisKeys.getUserImagesKey(userImages.userId);
		jedis.hset(key, userImages.userId.toString(), userImages);
	}

	public static void saveDeviceTokenToCache(Long userId, String token) {
		String key = RedisKeys.getIosDeviceTokensKey(userId);
		jedis.hset(key, userId.toString(), token);
	}

	public static boolean isPushSet(Long userId) {
		return getIosTokenForUser(userId) != null || getAndroidRegistrationForUser(userId) != null;
	}

	public static String getIosTokenForUser(Long userId) {
		String key = RedisKeys.getIosDeviceTokensKey(userId);
		return jedis.hget(key, userId.toString());
	}

	public static void removeDeviceTokenForUser(Long userId) {
		String key = RedisKeys.getIosDeviceTokensKey(userId);
		jedis.hdel(key, userId.toString());
	}

	public static void saveAndroidRegistrationToCache(Long userId, String token) {
		String key = RedisKeys.getAndroidRegistartionKey(userId);
		jedis.hset(key, userId.toString(), token);
	}

	public static String getAndroidRegistrationForUser(Long userId) {
		String key = RedisKeys.getAndroidRegistartionKey(userId);
		return jedis.hget(key, userId.toString());
	}

	public static void removeAndroidRegistartionFromCache(Long userId) {
		String key = RedisKeys.getAndroidRegistartionKey(userId);
		jedis.hdel(key, userId.toString());
	}

	public static boolean doesUsernameExist(String username) {
		String key = RedisKeys.getUsernameKeyPrefix();
		return (jedis.hget(key, username) != null);
	}

	public static void removeEmailFromCache(String email) {
		jedis.srem(RedisKeys.getUserEmailKey(), email);
	}

	public static void addEmailToCache(String email) {
		jedis.sadd(RedisKeys.getUserEmailKey(), email);
	}

	public static boolean doesEmailExist(String email) {
		return jedis.sismember(RedisKeys.getUserEmailKey(), email);
	}

	public static void saveUserToRedis(User user) {
		String users_key = RedisKeys.getUserBucketKey(user.id);
		jedis.hset(users_key, user.id.toString(), user);
		jedis.sadd(RedisKeys.getUserEmailKey(), user.email.toLowerCase());
		jedis.hset(RedisKeys.getUsernameKeyPrefix(), user.username.toLowerCase(), user.id);
	}

	public static void saveUserDnaToRedis(UserDna dna) {
		String dna_key = RedisKeys.getUserDnaBucketKey(dna.userId);
		log.info("Saving User: {}, DNA: {}", dna.userId, dna);
		jedis.hset(dna_key, dna.userId.toString(), dna);
	}

	public static void saveTempPassword(String username, String tempPassword) {
		String key = RedisKeys.getUserTempPasswordKey(username);
		if (key == null) {
			log.error("Trying to save temporary password but userId was null");
			return;
		}
		jedis.setex(key, tempPassword, JedisConnection.SECS_IN_DAY);
	}

	public static String getTemporaryPassword(String username) {
		if (username == null) {
			return null;
		}
		String key = RedisKeys.getUserTempPasswordKey(username);
		return jedis.get(key);
	}
	
	public static void saveSmsVerificationCode(String username, String verificationCode) {
		String key = RedisKeys.getSmsVerificationCodeKey(username);
		if (key == null) {
			log.error("Trying to save verification key but username was null");
			return;
		}
		jedis.set(key, verificationCode);
	}
	
	public static String getSmsVerificationCode(String username) {
		if (username == null) {
			return null;
		}
		String key = RedisKeys.getSmsVerificationCodeKey(username);
		return jedis.get(key);
	}
	
	public static Boolean saveVerifiedPhoneNumberToCache(Long id, String number) {
		String key = RedisKeys.getPhoneNumberKey();
		return jedis.hset(key, number, id);
	}
	
	public static Long getUserIdForPhoneNumber(String number) {
		String key = RedisKeys.getPhoneNumberKey();
		String userId = jedis.hget(key, number);
		if(userId == null)
			return null;
		return Long.valueOf(userId);
	}
	
	public static Boolean deleteSmsVerificationCode(String username) {
		if (username == null) {
			return null;
		}
		String key = RedisKeys.getSmsVerificationCodeKey(username);
		return jedis.del(key);
	}

	public static Boolean saveAddressBookToCache(Long id, AddressBook addressBook) {
		String key = RedisKeys.getUserAddressBookKey(id);
		return jedis.hset(key, id.toString(), addressBook);
	}
	
	public static AddressBook getCachedAddressBook(Long id) {
		String data = jedis.hget(RedisKeys.getUserAddressBookKey(id), id.toString());
		if (data != null) {
			return gson.fromJson(data, AddressBook.class);
		}
		return null;
	}
	
	public static void removeUserFromListeningInBuzz(Long userId) {
		String userBuzzKey = RedisKeys.getUserCurrentBuzzListenerKey(userId);
		String val = jedis.get(userBuzzKey);
		if (val != null) {
			String gOrV = val.split("-")[0];
			Long id = Long.valueOf(val.split("-")[1]);
			if (gOrV.equalsIgnoreCase("G")) {
				GroupDataSource.removeUserAsListener(id, userId);
			} else {
				VenueDataSource.removeUserAsListener(id, userId);
			}
		}
	}

}
