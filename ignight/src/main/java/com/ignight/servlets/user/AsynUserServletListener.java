package com.ignight.servlets.user;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class AsynUserServletListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		ThreadPoolExecutor executor = (ThreadPoolExecutor) servletContextEvent.getServletContext().getAttribute(
				"userExecutor");
		executor.shutdown();
	}

	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		ExecutorService executor = Executors.newCachedThreadPool();
		servletContextEvent.getServletContext().setAttribute("userExecutor", executor);
	}

}
