package com.ignight.servlets.user;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.apache.commons.lang.RandomStringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.annotations.SerializedName;
import com.ignight.aws.AwsMessageSender;
import com.ignight.core.CoreData;
import com.ignight.dna.TrendingVenue;
import com.ignight.dna.filter.Filter;
import com.ignight.dna.filter.Filter.FilterType;
import com.ignight.group.Group;
import com.ignight.group.GroupDataSource;
import com.ignight.servlets.BaseServletHandler;
import com.ignight.servlets.user.UserDataSource.UserSearch;
import com.ignight.servlets.v2.ApiException;
import com.ignight.servlets.v2.user.CityTrendingList.TrendingRequest;
import com.ignight.servlets.v2.user.InvalidValueException;
import com.ignight.user.AddressBook;
import com.ignight.user.RedisKeys;
import com.ignight.user.SearchedUser;
import com.ignight.user.SearchedUser.GroupStatus;
import com.ignight.user.User;
import com.ignight.user.UserDna;
import com.ignight.user.UserImages;
import com.ignight.venue.Venue;
import com.ignight.venue.VenueDataSource;
import com.ignight.venue.VenueTrending;
import com.stats.Stats;
import com.util.Coordinate;
import com.util.DistanceCalculator;
import com.util.EmailSender;
import com.util.PasswordService;
import com.util.SMSVerification;

import databases.Databases;

public class UserServletHandler extends BaseServletHandler {
	private static final Logger log = LoggerFactory.getLogger(UserServletHandler.class);
	PasswordService passwordService = PasswordService.getInstance();
	private static final AwsMessageSender awsQueue = AwsMessageSender.getInstance();
	private Stats stats = Stats.getInstance();

	private static final ExecutorService executors = Executors.newFixedThreadPool(1);

	/* ���������������������� GET METHODS �������������������������������� */

	public String getUserNotifications(User user) {
		return createSuccessResponseWithBody(UserDataSource.getUserNotifications(user));
	}

	public String clearUserGroupBuzzNotification(User user) {
		return createSuccessResponseWithBody(UserDataSource.clearGroupBuzzNotification(user));
	}

	public String clearUserGroupInviteNotifications(User user) {
		return createSuccessResponseWithBody(UserDataSource.clearGroupInviteNotification(user));
	}

	public String sendTempPassword(User user, String username) throws AddressException, MessagingException, NoSuchAlgorithmException, UnsupportedEncodingException {
		String tempPassword = generateTempPassword();
		String passwordDigest = PasswordService.getInstance().encrypt(tempPassword);
		UserDataSource.saveTempPassword(username, passwordDigest);

		sendTempPasswordEmail(user.email, username, tempPassword);
		return createSuccessResponseWithText("Temporary password will be emailed shortly.");
	}

	private void sendTempPasswordEmail(String to, String userName, String tempPassword) throws AddressException, MessagingException {
		log.info("Preparing sendTempPasswordEmail to userName: {} with emailAddress: {}", userName, to);

		String subject = "Reset Your Password";
		String body = "Hi " + userName + ",\n \n"
				+ "We got a request to reset your IgNight password. Please complete the Reset "
				+ "Password screen with the Temporary Password below and enter a New Password "
				+ "within 24 hours. If you ignore this message, your password won't be changed. \n \n"
				+ "Your new temporary password is: " + tempPassword + "\n \n"
				+ "-The IgNight Team";

		executors.execute(new EmailSender(to, subject, body));
	}

	private String generateTempPassword() {
		String tempPassword = RandomStringUtils.randomAlphabetic(10);
		return tempPassword;
	}
	
	/*
	 * Hamidou
	 * TODO: ADD Amazon SMS Sending
	 */

	
	public String sendSmsVerifcationCode(String number) throws IOException, JSONException{
		String smsCode = generateSmsVerificationCode();
		
		String responseString = SMSVerification.sendSMSViaNexmo(number, smsCode);
		JSONObject responseJson = new JSONObject(responseString);
		JSONObject message = responseJson.getJSONArray("messages").getJSONObject(0);
		String status = message.getString("status");
		
		if(status.equals("0"))
			return createSuccessResponse();
		return createFailureResponeWithText("Unable to send sms, please try again.");
	}
	
	
	public String sendSmsVerifcationCode(User user, String number) throws IOException, JSONException{
		String smsCode = generateSmsVerificationCode();
		UserDataSource.saveSmsVerificationCode(user.username, smsCode);
		
		String responseString = SMSVerification.sendSMSViaNexmo(number, smsCode);
		JSONObject responseJson = new JSONObject(responseString);
		JSONObject message = responseJson.getJSONArray("messages").getJSONObject(0);
		String status = message.getString("status");
		
		if(status.equals("0"))
			return createSuccessResponse();
		return createFailureResponeWithText("Unable to send sms, please try again.");
	}
	
	
	public String verifyPhoneNumber(User user, String number, String verificationCode) throws InvalidValueException, SQLException{
		String username = user.username;
		
		String storedSmsVerificationCode = UserDataSource.getSmsVerificationCode(username);
		
		if (storedSmsVerificationCode == null) {
			throw new InvalidValueException("No sms verification code exists.");
		}
		if (!verificationCode.equals(storedSmsVerificationCode)) {
			throw new InvalidValueException("Incorrect sms verification code.");
		}
		
		UserDataSource.deleteSmsVerificationCode(username);
		user.number = number;
		UserDataSource.saveUserToRedis(user);
		Databases.update("UPDATE users SET number = ? WHERE username = ?", number, user.username.toLowerCase());	
		UserDataSource.saveVerifiedPhoneNumberToCache(user.id, number);
		return createSuccessResponse();
	}
	
	public String uploadAddressBook(User user, AddressBook addressBook){
		if(UserDataSource.saveAddressBookToCache(user.id, addressBook))
			return createSuccessResponse();
		return createFailureResponeWithText("Unable to store address book.");
	}
	
	private String generateSmsVerificationCode() {
		String smsVerificationCode = RandomStringUtils.randomNumeric(5);
		return smsVerificationCode;
	}

	public String getUserInfo(User user) throws Exception {
		UserDna dna = UserDataSource.getCachedUserDna(user.id);
		if (dna == null) {
			throw new ApiException("User profile not set.");
		}

		return generateLoginResponse(user, dna);
	}
	
	public String findAddressBookContactsOnIgnight(Long id){
		AddressBook addressBook = UserDataSource.getCachedAddressBook(id);
		if(addressBook == null)
			return createFailureResponeWithText("No address book available for user.");
		
		
		
		List<UserSearch> users = new ArrayList<UserSearch>();
		
		for(String number : addressBook.contactsList.keySet()){
			Long userId = UserDataSource.getUserIdForPhoneNumber(number);
			if(userId != null){
				User user = UserDataSource.getCachedUser(userId);
				if(user != null){
					UserSearch userSearch = new UserSearch();
					userSearch.id = user.id;
					userSearch.username = user.username;
					users.add(userSearch);
				}
					
			}
		}
		return createSuccessResponseWithBody(users);
	}

	public String getUserTrendingList(User user, TrendingRequest tR) {
		log.info("Got Trending Request:  {}", tR);

		List<TrendingVenue> trendingVenues = new ArrayList<TrendingVenue>();

		Filter searchFilter = new Filter();
		if (tR.getSearchRadius() != null) {
			searchFilter = new Filter(new Coordinate(tR.getLat(), tR.getLon()), tR.getSearchRadius());
		}

		trendingVenues = getTrendingVenuesList(user.id);
		filterOnRadius(trendingVenues, searchFilter);
		Boolean upvote = (tR.getUpvoteFilter() != null && tR.getUpvoteFilter());

		if (upvote) { //upvote filter
			filterUpvotedVenues(trendingVenues, user.id, searchFilter);
		} else if (tR.getMusicFilter() != null) { //Music filter
			filterOnMusicPreference(trendingVenues, user.id, tR.getMusicFilter(), searchFilter);
		} else if (tR.getSearchString() != null && !tR.getSearchString().isEmpty()) { //Search fitler
			filterOnSearch(trendingVenues, user.id, tR.getSearchString().toLowerCase());
		}

		if (tR.getMusicFilter() == null && !upvote) {
			if (trendingVenues.size() < tR.getCount()) {
				if (tR.getSearchString() != null && !tR.getSearchString().isEmpty()) {
					fillUpArray(trendingVenues, tR.getCount(), new Filter(tR.getSearchString()));
				} else {
					fillUpArray(trendingVenues, tR.getCount(), searchFilter);
				}
			}
		}

		Collections.sort(trendingVenues);

		if (trendingVenues.size() > tR.getCount()) {
			trendingVenues = trendingVenues.subList(0, tR.getCount());
		}

		if (!upvote) {
			fillUpUserInfo(user.id, trendingVenues);
		}

		return generateTrendingOutput(trendingVenues);
	}

	private void filterOnSearch(List<TrendingVenue> trendingVenues, Long userId, String searchString) {
		Iterator<TrendingVenue> trendingVenueIter = trendingVenues.iterator();
		while (trendingVenueIter.hasNext()) {
			TrendingVenue tv = trendingVenueIter.next();
			String tvName = tv.venueInfo.name.toLowerCase();
			if (!tvName.contains(searchString)) {
				trendingVenueIter.remove();
			}
		}
	}

	private void filterUpvotedVenues(List<TrendingVenue> trendingVenues, Long userId, Filter radius) {
		Set<Long> upvoted = UserDataSource.getUserUpvotes(userId);
		//Filter Trending Venues First
		Iterator<TrendingVenue> trendingVenueIter = trendingVenues.iterator();
		while (trendingVenueIter.hasNext()) {
			TrendingVenue tv = trendingVenueIter.next();
			if (upvoted.contains(tv.id)) {
				tv.userActivity = 1l;
				upvoted.remove(tv.id);
			} else {
				trendingVenueIter.remove();
			}
		}

		//Add remaining upvoted venues
		for (Long u : upvoted) {
			TrendingVenue tv = new TrendingVenue(u, 0.0, true);
			tv.userActivity = 1l;
			trendingVenues.add(tv);
		}

		filterOnRadius(trendingVenues, radius);
	}

	private void filterOnMusicPreference(List<TrendingVenue> trendingVenues, Long userId, Long musicPreference, Filter radius) {
		Iterator<TrendingVenue> trendingVenueIter = trendingVenues.iterator();
		while (trendingVenueIter.hasNext()) {
			TrendingVenue tv = trendingVenueIter.next();
			VenueTrending dna = VenueDataSource.getVenueTrending(tv.id);
			if (dna == null || !dna.trendingValues.musicId.equals(musicPreference)) {
				trendingVenueIter.remove();
			}
		}
		filterOnRadius(trendingVenues, radius);
	}

	public ArrayList<TrendingVenue> getTrendingVenuesList(Long userId) {
		ArrayList<TrendingVenue> trendingVenues = UserDataSource.getUserTrendingVenues(userId);
		return trendingVenues;
	}

	private void filterOnRadius(List<TrendingVenue> trendingVenues, Filter radius) {
		if (radius == null || radius.type == FilterType.NONE) {
			return;
		}
		Iterator<TrendingVenue> trendingVenueIter = trendingVenues.iterator();
		while (trendingVenueIter.hasNext()) {
			TrendingVenue tv = trendingVenueIter.next();
			if (!inRadius(tv.id, radius)) {
				trendingVenueIter.remove();
			}
		}
	}

	private void fillUpArray(List<TrendingVenue> trendingVenues, Integer totalSize, Filter filter) {
		Set<Long> trendingVenueIds = new HashSet<Long>();
		for (TrendingVenue tV : trendingVenues) {
			trendingVenueIds.add(tV.id);
		}
		for (int i = 0; i < CoreData.alphabeticalVenues.size(); i++) {
			if (trendingVenues.size() >= totalSize) {
				break;
			}
			Long venueId = CoreData.alphabeticalVenues.get(i);
			if (trendingVenueIds.contains(venueId)) {
				continue;
			}
			if (filter.type == FilterType.NONE) {
				trendingVenues.add(new TrendingVenue(venueId, 0.0d, true));
			} else if (filter.type == FilterType.SEARCH) {
				Venue v = CoreData.getVenue(venueId);
				String name = v.name.toLowerCase();
				if (name.contains(filter.searchString.toLowerCase())) {
					trendingVenues.add(new TrendingVenue(venueId, 0.0d, true));
				}
			} else if (filter.type == FilterType.RADIUS) {
				if (inRadius(venueId, filter)) {
					trendingVenues.add(new TrendingVenue(venueId, 0.0d, true));
				}
			}
		}
	}

	private void fillUpUserInfo(Long userId, List<TrendingVenue> trendingVenues) {
		Set<Long> venueActivity = UserDataSource.getUserActivity(userId);
		if (venueActivity.size() == 0) {
			return;
		}

		for (TrendingVenue tv : trendingVenues) {
			Long upvotedId = tv.id;
			Long downVotedId = tv.id * -1;
			if (venueActivity.contains(upvotedId)) {
				tv.userActivity = 1l;
			} else if (venueActivity.contains(downVotedId)) {
				tv.userActivity = -1l;
			}
		}
	}

	private boolean inRadius(Long venueId, Filter filter) {
		if (filter.radius == null ||
				filter.userCoordinate == null ||
				filter.radius == 0.0d ||
				filter.type != FilterType.RADIUS) {
			return false;
		}

		Venue v = CoreData.getVenue(venueId);
		if (v == null) {
			return false;
		}
		Coordinate venueLocation = v.getCoordinate();
		if (venueLocation == null) {
			return false;
		}

		return DistanceCalculator.getDistanceInMeters(filter.userCoordinate, venueLocation) < filter.radius;
	}

	private String generateTrendingOutput(List<TrendingVenue> trendingVenues) {
		List<Map<String, Object>> output = new ArrayList<Map<String, Object>>();
		for (TrendingVenue tv : trendingVenues) {
			Map<String, Object> venue = new HashMap<String, Object>();
			venue.put("venueId", tv.id);
			venue.put("venueName", tv.venueInfo.name);
			venue.put("placeImage", tv.placeImage);
			venue.put("userBarColor", tv.barColorId);
			venue.put("userInput", tv.userActivity);
			venue.put("longitude", tv.venueInfo.longitude);
			venue.put("latitude", tv.venueInfo.latitude);
			output.add(venue);
		}
		return createSuccessResponseWithBody(output);
	}

	/*
	 * groupId
	 * name
	 * adminId
	 * member count
	 * public / private
	 * buzzAvailable
	 */
	public String getUserGroups(User user) {
		Set<Long> userGroups = UserDataSource.getUserGroups(user.id);
		ArrayList<UserGroup> results = new ArrayList<UserGroup>();

		for (Long userGroup : userGroups) {
			Group g = GroupDataSource.getGroupInCache(userGroup);
			if (g == null) {
				UserDataSource.removeGroupForUser(userGroup, user.id);
				continue;
			}

			UserGroup group = new UserGroup();
			group.groupId = userGroup;
			group.name = g.name;
			group.adminId = g.adminId;
			group.isPrivate = g.isPrivate;
			group.members = GroupDataSource.getGroupSize(userGroup);
			group.lastBuzzTs = g.lastBuzzTimeStamp;
			String val = GroupDataSource.getUserGroupLastBuzzAccessTimestamp(user.id, userGroup);
			long groupBuzzLastAccessTimeStampForUser = val == null ? -1l : Long.valueOf(val);
			group.newGroupBuzzAvailable = groupBuzzLastAccessTimeStampForUser < g.lastBuzzTimeStamp;
			results.add(group);
		}

		Collections.sort(results);
		UserDataSource.updateGroupBuzzNotification(user, false);
		return createSuccessResponseWithBody(results);
	}

	public static class UserGroup implements Comparable<UserGroup> {
		public Long groupId;
		public String name;
		public Long adminId;
		@SerializedName("private")
		public Boolean isPrivate;
		public Long members;
		public Long lastBuzzTs = -1l;
		public Boolean newGroupBuzzAvailable;

		@Override
		public int compareTo(UserGroup o) {
			return o.lastBuzzTs.compareTo(this.lastBuzzTs);
			//			return this.name.toLowerCase().compareTo(o.name.toLowerCase());
		}
	}

	public String handleSearchForUsers(User user, Group group, String searchString, Long offset, Long limit) throws SQLException {
		return createSuccessResponseWithBody(searchForUsers(user.id, group.id, searchString, offset, limit));
	}

	private List<SearchedUser> searchForUsers(
			Long userId,
			Long groupId,
			String searchString,
			long offset,
			long limit) throws SQLException {
		List<UserSearch> searchedUsers = UserDataSource.searchForUsers(searchString, offset, limit);

		//id, userName, number
		AddressBook addressBook = UserDataSource.getCachedAddressBook(userId);
		
		List<SearchedUser> users = new ArrayList<SearchedUser>();
		Set<Long> members = GroupDataSource.getGroupMembers(groupId);
		Set<Long> invited = GroupDataSource.getInvitedUsers(groupId);
		
		for (UserSearch s : searchedUsers) {
			SearchedUser user = new SearchedUser(s);
			UserImages images = UserDataSource.getCachedUserImages(s.id);
			user.chatPictureURL = (images != null) ? images.chatPictureUrl : null;
			if (members.contains(user.userId)) {
				user.groupStatus = GroupStatus.MEMBER.ordinal();
			} else if (invited.contains(user.userId)) {
				user.groupStatus = GroupStatus.INVITED.ordinal();
			}
			
			if(s.number != null){
				if(addressBook != null){
					ArrayList<String> contactName = addressBook.contactsList.get(s.number);
					user.fName = contactName.get(0);
					user.lName = contactName.get(1);
				}		
			}
			users.add(user);
		}
		return users;
	}

	/* ���������������������� POST METHODS �������������������������������� */

	public String addNewUser(String username, String password, String email) throws NoSuchAlgorithmException, UnsupportedEncodingException, SQLException, InvalidValueException {
		log.info("In handle Add User: {}, {}, {}", username, password, email);
		long id = UserDataSource.addUser(email, username, password);
		User user = new User();
		user.email = email;
		user.username = username;
		user.id = id;

		// Save user to redis cache
		UserDataSource.saveUserToRedis(user);

		Map<String, Object> body = new HashMap<String, Object>();
		body.put("id", user.id);

		log.info("Sending Response: {}", body);
		stats.addNewUser(id);
		return createSuccessResponseWithBody(body);
	}

	public String login(String username, String password) throws NoSuchAlgorithmException, UnsupportedEncodingException, SQLException {
		String passwordDigest = passwordService.encrypt(password);
		return verifyLogin(username, passwordDigest);
	}

	private String verifyLogin(String username, String passwordDigest) throws SQLException {
		if (!UserDataSource.doesUsernameExist(username)) {
			return createFailureResponeWithText("Incorrect username and password combination.");
		}

		User user = UserDataSource.getUser(username);
		if (user == null) {
			return createFailureResponeWithText("Incorrect username and password combination.");
		}

		if (!user.getEncryptedPassword().equals(passwordDigest)) {
			return createFailureResponeWithText("Incorrect username and password combination.");
		}

		UserDna dna = UserDataSource.getUserDna(user.id);

		return generateLoginResponse(user, dna);
	}

	private String generateLoginResponse(User user, UserDna dna) {
		Map<String, Object> body = new HashMap<String, Object>();
		body.put("id", user.id);
		boolean isUserInfoSet = (dna != null && dna.validate());
		body.put("userInfoSet", isUserInfoSet);
		body.put("pushSet", UserDataSource.isPushSet(user.id));
		if (isUserInfoSet) {
			body.put("cityId", user.cityId);
			body.put("userName", user.username);
			UserImages uI = UserDataSource.getCachedUserImages(user.id);
			body.put("profilePictureURL", uI != null ? uI.profilePictureUrl : null);
			body.put("genderId", user.genderId);
			body.put("email", user.email);
			body.put("notifyOfGroupInvites", user.notifyOfGroupInvites);

			if (dna != null) {
				body.put("age", dna.ageId);
				body.put("music", dna.musicIds);
				body.put("atmosphere", dna.atmosphereIds);
				body.put("spending", dna.spendingLimit);
				body.put("dnaSet", true);
			} else {
				body.put("dnaSet", false);
			}
		}

		return createSuccessResponseWithBody(body);
	}

	public String setCompleteUserInfo(User user, UserDna newDna, Long cityId, Long genderId) throws SQLException {
		user.cityId = cityId;
		user.genderId = genderId;

		UserDataSource.updateCompleteUser(user, newDna);
		UserDataSource.saveUserToRedis(user);
		UserDataSource.saveUserDnaToRedis(newDna);
		awsQueue.sendNewUser(user.id);

		return generateLoginResponse(user, newDna);
	}

	public String resetPassword(String username, String password, String tempPassword) throws SQLException, NoSuchAlgorithmException, UnsupportedEncodingException, InvalidValueException {
		String tempPasswordDigest = passwordService.encrypt(tempPassword);
		String storedPswdDigest = UserDataSource.getTemporaryPassword(username);
		if (storedPswdDigest == null) {
			throw new InvalidValueException("No temporary password exists.");
		}

		if (!tempPasswordDigest.equals(storedPswdDigest)) {
			throw new InvalidValueException("Incorrect temporary password.");
		}

		String passwordDigest = passwordService.encrypt(password);
		Databases.update("UPDATE users SET password = ? WHERE username = ?", passwordDigest, username.toLowerCase());
		return verifyLogin(username, passwordDigest);
	}

	public String addIOSDeviceToken(User user, String token) {
		UserDataSource.saveDeviceTokenToCache(user.id, token);
		UserDataSource.saveUserToRedis(user);
		return createSuccessResponse();
	}

	public String addAndroidRegistrationId(User user, String token) {
		UserDataSource.saveAndroidRegistrationToCache(user.id, token);
		UserDataSource.saveUserToRedis(user);
		return createSuccessResponse();
	}

	public String updateEmail(User user, String newEmail) throws SQLException, InvalidValueException {
		if (user.email.equalsIgnoreCase(newEmail)) {
			return createSuccessResponse();
		}

		if (UserDataSource.doesEmailExist(newEmail)) {
			throw new InvalidValueException("Email already exists.");
		}

		Databases.update("UPDATE users SET email = ? WHERE id = ?", newEmail, user.id);

		String oldEmail = user.email;
		UserDataSource.removeEmailFromCache(oldEmail);
		user.email = newEmail;
		UserDataSource.saveUserToRedis(user);
		UserDataSource.addEmailToCache(newEmail);

		return createSuccessResponse();
	}

	public String updateAge(User user, UserDna dna, Long ageId) throws SQLException {
		Long userId = user.id;
		Databases.update("UPDATE user_dna SET age_id = ? WHERE user_id = ?", ageId, userId);
		dna.ageId = ageId;
		UserDataSource.saveUserDnaToRedis(dna);
		awsQueue.sendDnaChange(userId);
		return createSuccessResponse();
	}

	public String updateSpendingLimitInDna(User user, UserDna dna, Long spendingLimit) throws SQLException {
		Long userId = user.id;

		Databases.update("UPDATE user_dna SET spending_limit = ? WHERE user_id = ?", spendingLimit, userId);
		dna.spendingLimit = spendingLimit;
		UserDataSource.saveUserDnaToRedis(dna);
		awsQueue.sendDnaChange(userId);
		return createSuccessResponse();
	}

	public String updateUserGroupPreferences(User user, Boolean notifyOfGroupInvites) throws SQLException {
		Long userId = user.id;

		Databases.update("UPDATE users SET notify_of_group_invites = ? WHERE id = ?", notifyOfGroupInvites, userId);
		user.notifyOfGroupInvites = notifyOfGroupInvites;
		UserDataSource.saveUserToRedis(user);
		return createSuccessResponse();
	}

	public String updateMusicInDna(User user, UserDna dna, List<Long> musicIds) throws SQLException {
		Long userId = user.id;

		Databases.update("UPDATE user_dna " +
				"SET music_choice_1 = ?, music_choice_2 = ?, music_choice_3 = ? " +
				"WHERE user_id = ?", musicIds.get(0), musicIds.get(1), musicIds.get(2), userId);

		dna.musicIds = musicIds;
		UserDataSource.saveUserDnaToRedis(dna);
		awsQueue.sendDnaChange(userId);
		return createSuccessResponse();
	}

	public String updateAtmospheresInDna(User user, UserDna dna, List<Long> atmosphereIds) throws SQLException {
		Long userId = user.id;

		Databases.update("UPDATE user_dna " +
				"SET atmosphere_choice_1 = ?, atmosphere_choice_2 = ?, atmosphere_choice_3 = ? " +
				"WHERE user_id = ?", atmosphereIds.get(0), atmosphereIds.get(1), atmosphereIds.get(2), userId);

		dna.atmosphereIds = atmosphereIds;
		UserDataSource.saveUserDnaToRedis(dna);
		awsQueue.sendDnaChange(userId);
		return createSuccessResponse();
	}

	public String updateProfilePicture(Long userId, String profileUrl, String chatUrl) throws SQLException {
		UserImages userImages = UserDataSource.getCachedUserImages(userId);
		if (userImages == null) {
			userImages = new UserImages();
		}

		userImages.userId = userId;
		userImages.profilePictureUrl = profileUrl;
		userImages.chatPictureUrl = chatUrl;

		String sql = "INSERT INTO user_images (user_id, profile_picture_url, chat_picture_url) " +
				"VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE profile_picture_url = ?, chat_picture_url = ?";
		Databases.upsert(sql, userId, userImages.profilePictureUrl, userImages.chatPictureUrl,
				userImages.profilePictureUrl, userImages.chatPictureUrl);
		UserDataSource.saveUserImages(userImages);

		return createSuccessResponseWithText(userImages.profilePictureUrl);
	}

	public String unsubscribeFromVenue(User user, Venue venue) {
		VenueDataSource.removeUserAsListener(venue.id, user.id);
		return createSuccessResponse();
	}

	public String unsubscribeFromGroup(User user, Group group) {
		GroupDataSource.removeUserAsListener(group.id, user.id);
		return createSuccessResponse();
	}

	public String logout(Long userId) {
		UserDataSource.removeDeviceTokenForUser(userId);
		UserDataSource.removeAndroidRegistartionFromCache(userId);
		return createSuccessResponse();
	}

	public String unsubscribeFromBuzz(Long userId) {
		UserDataSource.removeUserFromListeningInBuzz(userId);
		return createSuccessResponse();
	}
}
