package com.ignight.servlets;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ignight.aws.AwsMessageSender;
import com.ignight.core.CoreData;
import com.ignight.data.DataSource;
import com.ignight.group.GroupDataSource;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.user.RedisKeys;
import com.ignight.venue.VenueDataSource;
import com.zaxxer.hikari.HikariDataSource;

import databases.DataSourceBuilder;
import databases.Databases;
import databases.JedisConnection;

public class StartupServlet extends HttpServlet {
	private static final Logger log = LoggerFactory.getLogger(StartupServlet.class);
	private static final long serialVersionUID = 1L;
	public static final JedisConnection jedis = JedisConnection.getInstance();

	@Override
	public void init() throws ServletException {
		/*
		 * Setup Mysql Connection
		 */
		try {
			log.info("Starting StartupServlet");
			Databases.dataSource = DataSourceBuilder.createDataSource();
			Databases.dataSourceArchiving = DataSourceBuilder.createDataSourceArchive();
		} catch (Exception e) {
			log.error("Failed to create MySQL Datasources", e);
		}

		try {
			log.info("Loading Archive Status");
			DataSource.loadAndSaveArchiveStatusToRedis();
			
			log.info("Loading Venues");
			VenueDataSource.loadAndSaveVenuesToRedis();
			log.info("Loading Number of Google Place Images");
			VenueDataSource.loadAndSavedNoOfGooglePlaceImagesToRedis(); //Pull no of Images from SQL and add to redis

			Set<String> keys = jedis.keys(RedisKeys.APP_SERVERS_LIST + "*");
			if (keys == null || keys.size() == 0) {
				log.info("Loading Users");
				UserDataSource.loadAndSavedUsersToRedis();
				log.info("Loading UserDna's");
				UserDataSource.loadAndSavedUserDnaToRedis();
				log.info("Loading UserImage's");
				UserDataSource.loadAndSavedUserImagesToRedis();
				log.info("Loading Group's");
				GroupDataSource.loadAndSaveGroupsToRedis();
				log.info("Loading Group Members");
				GroupDataSource.loadAndSaveGroupMembersToRedis();
				log.info("Loading Group Invitations");
				GroupDataSource.loadAndSaveGroupInvitesToRedis();
			} else {
				log.info("APP SERVERS: {}", keys);
			}
			String hostName = InetAddress.getLocalHost().getHostName().replace('.', '-');
			String key = RedisKeys.APP_SERVERS_LIST + hostName;
			jedis.setex(key, String.valueOf(System.currentTimeMillis()), 60);
		} catch (Exception e) {
			log.error("Failed to save data to Redis", e);
		}

		CoreData.alphabetizeVenues();
	}

	@Override
	public void destroy() {
		log.info("Cleaning up before shutdown..");
		AwsMessageSender awsMessageSender = AwsMessageSender.getInstance();
		awsMessageSender.shutdown();
		HikariDataSource dataSource = (HikariDataSource) Databases.dataSource;
		HikariDataSource dataSourceArchiving = (HikariDataSource) Databases.dataSourceArchiving;
		dataSource.close();
		dataSourceArchiving.close();

		String hostName;
		try {
			hostName = InetAddress.getLocalHost().getHostName().replace('.', '-');
			String key = RedisKeys.APP_SERVERS_LIST + hostName;
			jedis.del(key);
		} catch (UnknownHostException e) {
			log.error("Failed to get hostname", e);
		}

		JedisConnection.shutdown();
		super.destroy();
	}
}
