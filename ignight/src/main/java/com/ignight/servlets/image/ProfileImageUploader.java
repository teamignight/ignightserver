package com.ignight.servlets.image;

import java.io.InputStream;
import java.util.Map;

import javax.servlet.AsyncContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ignight.imagehandling.ImageHandler;
import com.ignight.imagehandling.ImageHandler.ImgType;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.user.UserServletHandler;
import com.ignight.servlets.v2.user.InvalidValueException;
import com.ignight.user.User;

public class ProfileImageUploader extends ImageUploader {
	private final static Logger log = LoggerFactory.getLogger(ProfileImageUploader.class);
	private static final UserServletHandler userHandler = new UserServletHandler();

	public ProfileImageUploader(AsyncContext ctx) {
		super(ctx);
	}

	@Override
	protected void checkId(Long id) throws InvalidValueException {
		User user = UserDataSource.getCachedUser(id);
		if (user == null) {
			throw new InvalidValueException("User does not exist.");
		}
	}

	@Override
	protected String processImages(User user, InputStream uploadStream) throws Exception {
		Map<ImgType, String> imageUrls = ImageHandler.uploadProfileImageI(id, uploadStream);
		return userHandler.updateProfilePicture(id, imageUrls.get(ImgType.IMG_PROFILE), imageUrls.get(ImgType.IMG_CHAT));
	}
}
