package com.ignight.servlets.image;

import java.io.InputStream;

import javax.servlet.AsyncContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.util.Preconditions;
import com.ignight.group.Group;
import com.ignight.group.GroupDataSource;
import com.ignight.imagehandling.ImageHandler;
import com.ignight.servlets.group.GroupServletHandler;
import com.ignight.servlets.v2.user.InvalidValueException;
import com.ignight.user.User;

public class GroupBuzzImageUploader extends ImageUploader {
	private static final Logger log = LoggerFactory.getLogger(GroupBuzzImageUploader.class);
	private static final GroupServletHandler groupHandler = new GroupServletHandler();
	private Group group;

	public GroupBuzzImageUploader(AsyncContext ctx) {
		super(ctx);
	}

	@Override
	protected void checkId(Long id) throws InvalidValueException {
		group = GroupDataSource.getGroupInCache(id);
		Preconditions.checkNotNull(group, "Group does not exist.");
	}

	@Override
	protected String processImages(User user, InputStream uploadStream) throws Exception {
		Preconditions.checkNotNull(user, "userId is null");
		String groupUrl = ImageHandler.uploadGroupBuzzImage(group, user, uploadStream);
		return groupHandler.addGroupImageBuzz(user, group, groupUrl);
	}
}
