package com.ignight.servlets.image;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebListener
public class ImageUploadListener implements ServletContextListener {
	private final Logger log = LoggerFactory.getLogger(ImageUploadListener.class);

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		log.info("Image Upload Listener Context destroyed");
		ThreadPoolExecutor executor = (ThreadPoolExecutor) servletContextEvent.getServletContext().getAttribute(
				"imageExecutor");
		executor.shutdown();
	}

	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		log.info("Image Upload Listener Context initialized");
		ExecutorService executor = Executors.newCachedThreadPool();
		servletContextEvent.getServletContext().setAttribute("imageExecutor", executor);
	}

}
