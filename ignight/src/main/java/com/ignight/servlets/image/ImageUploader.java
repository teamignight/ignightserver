package com.ignight.servlets.image;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Scanner;

import javax.servlet.AsyncContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ignight.servlets.ResponseUtil;
import com.ignight.servlets.user.UserDataSource;
import com.ignight.servlets.v2.RequestUtil;
import com.ignight.servlets.v2.user.InvalidValueException;
import com.ignight.user.User;

public abstract class ImageUploader implements Runnable {
	protected Logger log = LoggerFactory.getLogger(ImageUploader.class);

	private AsyncContext asyncContext;
	private HttpServletRequest request;
	private HttpServletResponse response;

	private JSONObject msg = null;
	private InputStream uploadStream = null;
	private User user = null;
	protected Long id;

	public ImageUploader(AsyncContext ctx) {
		this.asyncContext = ctx;
		this.request = (HttpServletRequest) ctx.getRequest();
		this.response = (HttpServletResponse) ctx.getResponse();
		this.id = RequestUtil.getIdInUri(request);
	}

	@Override
	public void run() {
		try {
			checkId(id);
			getRequestParts();
			ResponseUtil.sendResponse(response, processImages(user, uploadStream));
		} catch (Exception e) {
			log.error("Failed to upload images", e);
			ResponseUtil.sendErrorResponse(response, e.getMessage());
		} finally {
			if (uploadStream != null) {
				try {
					uploadStream.close();
				} catch (IOException e) {
				}
			}
		}
	}

	protected abstract void checkId(Long id) throws InvalidValueException;

	protected abstract String processImages(User user, InputStream uploadStream) throws Exception;

	private void getRequestParts() {
		DiskFileItemFactory factory = new DiskFileItemFactory();
		ServletContext servletContext = request.getServletContext();
		File reposityory = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
		factory.setRepository(reposityory);

		try {
			ServletFileUpload upload = new ServletFileUpload(factory);
			List<FileItem> items = upload.parseRequest(request);
			for (FileItem item : items) {
				if (item.isFormField()) {
					Scanner scanner = new Scanner(item.getInputStream());
					String jsonBodyString = scanner.nextLine();
					msg = (new JSONObject(jsonBodyString)).getJSONObject("msg");
					scanner.close();

					if (msg == null) {
						throw new InvalidValueException("Missing json_body");
					}

					Long userId = msg.getLong("userId");
					if (userId != null) {
						user = UserDataSource.getCachedUser(userId);
						if (user == null) {
							throw new InvalidValueException("User does not exist.");
						}
					}
				} else {
					uploadStream = item.getInputStream();
				}
			}

			if (uploadStream == null) {
				log.error("UploadStream is null, Size of FileItem: {}", items.size());
				ResponseUtil.sendErrorResponse(response, "Error on server");
				return;
			}
		} catch (Exception e) {
			log.error("Failed to parse request", e);
			ResponseUtil.sendErrorResponse(response, e.getMessage());
		}
	}
}
