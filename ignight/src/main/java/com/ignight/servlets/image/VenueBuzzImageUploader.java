package com.ignight.servlets.image;

import java.io.InputStream;

import javax.servlet.AsyncContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.util.Preconditions;
import com.ignight.core.CoreData;
import com.ignight.imagehandling.ImageHandler;
import com.ignight.servlets.v2.user.InvalidValueException;
import com.ignight.servlets.venue.VenueServletHandler;
import com.ignight.user.User;
import com.ignight.venue.Venue;

public class VenueBuzzImageUploader extends ImageUploader {
	private static final Logger log = LoggerFactory.getLogger(VenueBuzzImageUploader.class);
	private static final VenueServletHandler venueHandler = new VenueServletHandler();
	private Venue venue;

	public VenueBuzzImageUploader(AsyncContext ctx) {
		super(ctx);
	}

	@Override
	protected void checkId(Long id) throws InvalidValueException {
		venue = CoreData.getVenue(id);
		Preconditions.checkNotNull(venue, "Venue does not exist.");
	}

	@Override
	protected String processImages(User user, InputStream uploadStream) throws Exception {
		Preconditions.checkNotNull(user, "userId is null.");
		String venueUrl = ImageHandler.uploadVenueBuzzImage(venue, user, uploadStream);
		return venueHandler.handleAddBuzzImageRequest(user, venue, venueUrl);
	}

}
