package com.ignight.servlets;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IgnightGetRequest extends IgnightRequest {
	private static Logger log = LoggerFactory.getLogger(IgnightGetRequest.class);
	public Map<String, String[]> msg;

	public IgnightGetRequest(HttpServletRequest servletRequest) throws Exception {
		processRequest(servletRequest);
	}

	@Override
	protected void processRequest(HttpServletRequest servletRequest) throws Exception {
		this.msg = getParameters(servletRequest);
		this.type = getMsgType(msg);
	}

	protected Map<String, String[]> getParameters(HttpServletRequest request) throws Exception {
		Map<String, String[]> parameterMap = request.getParameterMap();
		if (parameterMap == null || parameterMap.isEmpty()) {
			throw new IllegalArgumentException("No Parameters In Request: " + request.toString());
		}
		return parameterMap;
	}

	protected String getMsgType(Map<String, String[]> msg) {
		String[] types = msg.get("type");
		if (types.length > 1) {
			throw new IllegalArgumentException("Multiple Types Found in request: " + msg);
		}
		if (types.length == 0) {
			throw new IllegalArgumentException("No Type Found in Request: " + msg);
		}
		return types[0];
	}
}
