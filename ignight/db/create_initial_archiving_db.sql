CREATE TABLE user_votes
(
id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
vote_type INT NOT NULL,
city_id INT NOT NULL,
user_id BIGINT UNSIGNED NOT NULL,
venue_id BIGINT UNSIGNED NOT NULL,
date DATE NOT NULL,
day_of_week INT NOT NULL,
PRIMARY KEY(id)
);

CREATE TABLE venue_buzz
(
buzz_id BIGINT UNSIGNED NOT NULL,
user_id BIGINT UNSIGNED NOT NULL,
city_id INT NOT NULL,
venue_id BIGINT UNSIGNED NOT NULL,
buzz_type TEXT NOT NULL,
buzz_text TEXT NOT NULL,
date DATE NOT NULL,
day_of_week INT NOT NULL,
PRIMARY KEY (buzz_id, city_id, venue_id)
);

CREATE TABLE venue_crowd_stats
(
venue_id BIGINT UNSIGNED NOT NULL,
city_id INT NOT NULL,
age_bucket_mode INT NOT NULL,
atmosphere_type_mode INT NOT NULL,
music_type_mode INT NOT NULL,
avg_spending_limit INT NOT NULL,
trending_group_id_1 INT,
trending_group_id_2 INT,
trending_group_id_3 INT,
trending_group_id_4 INT,
trending_group_id_5 INT,
date DATE NOT NULL,
day_of_week INT NOT NULL,
PRIMARY KEY (venue_id, city_id, date)
);

CREATE TABLE group_buzz
(
buzz_id BIGINT UNSIGNED NOT NULL,
user_id BIGINT UNSIGNED NOT NULL,
city_id INT NOT NULL,
group_id BIGINT UNSIGNED NOT NULL,
buzz_type TEXT NOT NULL,
buzz_text TEXT NOT NULL,
date DATE NOT NULL,
day_of_week INT NOT NULL,
PRIMARY KEY (buzz_id, city_id, group_id)
);

CREATE TABLE city_archive_status
(
city_id INT NOT NULL,
archive_in_progress INT NOT NULL,
last_archive_date DATE,
PRIMARY KEY (city_id)
);
