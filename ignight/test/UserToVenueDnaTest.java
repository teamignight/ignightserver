import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import util.Utils;

import com.ignight.dna.CalcVenueDna;
import com.ignight.dna.DnaElementCount;
import com.ignight.dna.VenueDna;
import com.ignight.dna.elements.Age;
import com.ignight.dna.elements.Atmosphere;
import com.ignight.dna.elements.Music;
import com.ignight.dna.elements.SpendingLimit;
import com.ignight.user.UserDna;
import com.util.Timer;
import com.util.UserToVenueDna;

public class UserToVenueDnaTest extends TestCase {
	private static final Logger logger = LoggerFactory.getLogger(UserToVenueDnaTest.class);
	UserDna userDna;

	public void setUp() throws Exception {
		userDna = new UserDna();
	}

	public void testSimpleTestFullLists() {
		List<Long> userPref = new ArrayList<Long>();
		userPref.add(Music.RAP.getId());
		userPref.add(Music.ROCK.getId());
		userPref.add(Music.CLASSIC_ROCK.getId());

		List<DnaElementCount> venuePref = new ArrayList<DnaElementCount>();
		venuePref.add(new DnaElementCount(Music.RAP.getId(), 100));
		venuePref.add(new DnaElementCount(Music.ROCK.getId(), 100));
		venuePref.add(new DnaElementCount(Music.CLASSIC_ROCK.getId(), 100));

		/*
		 * 0.5 vs 0.333 -> 0.333
		 * 0.35 vs 0.333 -> 0.333
		 * 0.15 vs 0.333 -> 0.15
		 * Total = 0.816
		 */

		long val = UserToVenueDna.calculatePreferencesCompatibility(userPref, venuePref);
		assertEquals(816, val);
	}

	public void testFullUserWithOneVenueTopPref() {
		List<Long> userPref = new ArrayList<Long>();
		userPref.add(Music.RAP.getId());
		userPref.add(Music.ROCK.getId());
		userPref.add(Music.CLASSIC_ROCK.getId());

		List<DnaElementCount> venuePref = new ArrayList<DnaElementCount>();
		venuePref.add(new DnaElementCount(Music.RAP.getId(), 100));

		/*
		 * 0.5 vs 1 -> 0.5
		 * 0.35 vs 0 -> 0
		 * 0.15 vs 0 -> 0
		 * Total = 0.5
		 */

		long val = UserToVenueDna.calculatePreferencesCompatibility(userPref, venuePref);
		assertEquals(500, val);
	}

	public void testFullUserWithOneVenueMiddlePref() {
		List<Long> userPref = new ArrayList<Long>();
		userPref.add(Music.RAP.getId());
		userPref.add(Music.ROCK.getId());
		userPref.add(Music.CLASSIC_ROCK.getId());

		List<DnaElementCount> venuePref = new ArrayList<DnaElementCount>();
		venuePref.add(new DnaElementCount(Music.ROCK.getId(), 100));

		/*
		 * 0.5 vs 0 -> 0
		 * 0.35 vs 1 -> 0.35
		 * 0.15 vs 0 -> 0
		 * Total = 0.35
		 */

		long val = UserToVenueDna.calculatePreferencesCompatibility(userPref, venuePref);
		assertEquals(350, val);
	}

	public void testFullUserWithOneVenueLastPref() {
		List<Long> userPref = new ArrayList<Long>();
		userPref.add(Music.RAP.getId());
		userPref.add(Music.ROCK.getId());
		userPref.add(Music.CLASSIC_ROCK.getId());

		List<DnaElementCount> venuePref = new ArrayList<DnaElementCount>();
		venuePref.add(new DnaElementCount(Music.CLASSIC_ROCK.getId(), 100));

		/*
		 * 0.5 vs 0 -> 0
		 * 0.35 vs 0 -> 0
		 * 0.15 vs 1 -> 0.15
		 * Total = 0.15
		 */

		long val = UserToVenueDna.calculatePreferencesCompatibility(userPref, venuePref);
		assertEquals(150, val);
	}

	public void testFullUserWithTwoVenueTopTwo() {
		List<Long> userPref = new ArrayList<Long>();
		userPref.add(Music.RAP.getId());
		userPref.add(Music.ROCK.getId());
		userPref.add(Music.CLASSIC_ROCK.getId());

		List<DnaElementCount> venuePref = new ArrayList<DnaElementCount>();
		venuePref.add(new DnaElementCount(Music.RAP.getId(), 100));
		venuePref.add(new DnaElementCount(Music.ROCK.getId(), 100));

		/*
		 * 0.5 vs 0.5 -> 0.5
		 * 0.35 vs 0.5 -> 0.35
		 * 0.15 vs 0 -> 0
		 * Total = 0.85
		 */

		long val = UserToVenueDna.calculatePreferencesCompatibility(userPref, venuePref);
		assertEquals(850, val);
	}

	public void testFullUserWithTwoVenueBottomTwo() {
		List<Long> userPref = new ArrayList<Long>();
		userPref.add(Music.RAP.getId());
		userPref.add(Music.ROCK.getId());
		userPref.add(Music.CLASSIC_ROCK.getId());

		List<DnaElementCount> venuePref = new ArrayList<DnaElementCount>();
		venuePref.add(new DnaElementCount(Music.ROCK.getId(), 900));
		venuePref.add(new DnaElementCount(Music.CLASSIC_ROCK.getId(), 100));

		/*
		 * 0.5 vs 0 -> 0
		 * 0.35 vs 0.9 -> 0.35
		 * 0.15 vs 0.1 -> 0.1
		 * Total = 0.45
		 */

		long val = UserToVenueDna.calculatePreferencesCompatibility(userPref, venuePref);
		assertEquals(450, val);
	}

	public void testFullUserWithTwoVenueEndPrefs() {
		List<Long> userPref = new ArrayList<Long>();
		userPref.add(Music.RAP.getId());
		userPref.add(Music.ROCK.getId());
		userPref.add(Music.CLASSIC_ROCK.getId());

		List<DnaElementCount> venuePref = new ArrayList<DnaElementCount>();
		venuePref.add(new DnaElementCount(Music.RAP.getId(), 900));
		venuePref.add(new DnaElementCount(Music.CLASSIC_ROCK.getId(), 100));

		/*
		 * 0.5 vs 0.9 -> 0.5
		 * 0.35 vs 0 -> 0
		 * 0.15 vs 0.1 -> 0.1
		 * Total = 0.6
		 */

		long val = UserToVenueDna.calculatePreferencesCompatibility(userPref, venuePref);
		assertEquals(600, val);
	}

	public void testOneUserWithFullVenueTop() {
		List<Long> userPref = new ArrayList<Long>();
		userPref.add(Music.RAP.getId());

		List<DnaElementCount> venuePref = new ArrayList<DnaElementCount>();
		venuePref.add(new DnaElementCount(Music.RAP.getId(), 300));
		venuePref.add(new DnaElementCount(Music.ROCK.getId(), 200));
		venuePref.add(new DnaElementCount(Music.CLASSIC_ROCK.getId(), 100));

		/*
		 * 1 vs 0.5 -> 0.5
		 * Total = 0.5
		 */

		long val = UserToVenueDna.calculatePreferencesCompatibility(userPref, venuePref);
		assertEquals(500, val);
	}

	public void testOneUserWithFullVenueMiddle() {
		List<Long> userPref = new ArrayList<Long>();
		userPref.add(Music.ROCK.getId());

		List<DnaElementCount> venuePref = new ArrayList<DnaElementCount>();
		venuePref.add(new DnaElementCount(Music.RAP.getId(), 300));
		venuePref.add(new DnaElementCount(Music.ROCK.getId(), 200));
		venuePref.add(new DnaElementCount(Music.CLASSIC_ROCK.getId(), 100));

		/*
		 * 1 vs 0.333 -> 0.333
		 * Total = 0.333
		 */

		long val = UserToVenueDna.calculatePreferencesCompatibility(userPref, venuePref);
		assertEquals(333, val);
	}

	public void testOneUserWithFullVenueBottom() {
		List<Long> userPref = new ArrayList<Long>();
		userPref.add(Music.CLASSIC_ROCK.getId());

		List<DnaElementCount> venuePref = new ArrayList<DnaElementCount>();
		venuePref.add(new DnaElementCount(Music.RAP.getId(), 300));
		venuePref.add(new DnaElementCount(Music.ROCK.getId(), 200));
		venuePref.add(new DnaElementCount(Music.CLASSIC_ROCK.getId(), 100));

		/*
		 * 1 vs 0.166 -> 0.166
		 * Total = 0.166
		 */

		long val = UserToVenueDna.calculatePreferencesCompatibility(userPref, venuePref);
		assertEquals(166, val);
	}

	public void testTwoUserWithFullVenueTopTwo() {
		List<Long> userPref = new ArrayList<Long>();
		userPref.add(Music.RAP.getId());
		userPref.add(Music.ROCK.getId());

		List<DnaElementCount> venuePref = new ArrayList<DnaElementCount>();
		venuePref.add(new DnaElementCount(Music.RAP.getId(), 300));
		venuePref.add(new DnaElementCount(Music.ROCK.getId(), 200));
		venuePref.add(new DnaElementCount(Music.CLASSIC_ROCK.getId(), 100));

		/*
		 * 0.65 vs 0.5 -> 0.5
		 * 0.35 vs 0.333 -> 0.333
		 * Total = 0.833
		 */

		long val = UserToVenueDna.calculatePreferencesCompatibility(userPref, venuePref);
		assertEquals(833, val);
	}

	public void testTwoUserWithFullVenueBottomTwo() {
		List<Long> userPref = new ArrayList<Long>();
		userPref.add(Music.ROCK.getId());
		userPref.add(Music.CLASSIC_ROCK.getId());

		List<DnaElementCount> venuePref = new ArrayList<DnaElementCount>();
		venuePref.add(new DnaElementCount(Music.RAP.getId(), 300));
		venuePref.add(new DnaElementCount(Music.ROCK.getId(), 200));
		venuePref.add(new DnaElementCount(Music.CLASSIC_ROCK.getId(), 100));

		/*
		 * 0.65 vs 0.333 -> 0.333
		 * 0.15 vs 0.166 -> 0.166
		 * Total = 0.499
		 */

		long val = UserToVenueDna.calculatePreferencesCompatibility(userPref, venuePref);
		assertEquals(499, val);
	}

	public void testTwoUserWithFullVenueEndPrefs() {
		List<Long> userPref = new ArrayList<Long>();
		userPref.add(Music.RAP.getId());
		userPref.add(Music.CLASSIC_ROCK.getId());

		List<DnaElementCount> venuePref = new ArrayList<DnaElementCount>();
		venuePref.add(new DnaElementCount(Music.RAP.getId(), 300));
		venuePref.add(new DnaElementCount(Music.ROCK.getId(), 200));
		venuePref.add(new DnaElementCount(Music.CLASSIC_ROCK.getId(), 100));

		/*
		 * 0.65 vs 0.5 -> 0.5
		 * 0.15 vs 0.166 -> 0.166
		 * Total = 0.666
		 */

		long val = UserToVenueDna.calculatePreferencesCompatibility(userPref, venuePref);
		assertEquals(666, val);
	}

	public void testOneUserWithOneVenueNone() {
		List<Long> userPref = new ArrayList<Long>();
		userPref.add(Music.RAP.getId());

		List<DnaElementCount> venuePref = new ArrayList<DnaElementCount>();
		venuePref.add(new DnaElementCount(Music.ROCK.getId(), 200));

		/*
		 * 1 vs 0 -> 0
		 * Total = 0
		 */

		long val = UserToVenueDna.calculatePreferencesCompatibility(userPref, venuePref);
		assertEquals(0, val);
	}

	public void testOneUserWithOneVenueFull() {
		List<Long> userPref = new ArrayList<Long>();
		userPref.add(Music.RAP.getId());

		List<DnaElementCount> venuePref = new ArrayList<DnaElementCount>();
		venuePref.add(new DnaElementCount(Music.RAP.getId(), 200));

		/*
		 * 1 vs 1 -> 1
		 * Total = 1
		 */

		long val = UserToVenueDna.calculatePreferencesCompatibility(userPref, venuePref);
		assertEquals(1000, val);
	}

	public void testOneUserWithTwoVenueTop() {
		List<Long> userPref = new ArrayList<Long>();
		userPref.add(Music.RAP.getId());

		List<DnaElementCount> venuePref = new ArrayList<DnaElementCount>();
		venuePref.add(new DnaElementCount(Music.RAP.getId(), 300));
		venuePref.add(new DnaElementCount(Music.ROCK.getId(), 200));

		/*
		 * 1 vs 0.6 -> 0.6
		 * Total = 0.6
		 */

		long val = UserToVenueDna.calculatePreferencesCompatibility(userPref, venuePref);
		assertEquals(600, val);
	}

	public void testOneUserWithTwoVenueBottom() {
		List<Long> userPref = new ArrayList<Long>();
		userPref.add(Music.RAP.getId());

		List<DnaElementCount> venuePref = new ArrayList<DnaElementCount>();
		venuePref.add(new DnaElementCount(Music.ROCK.getId(), 300));
		venuePref.add(new DnaElementCount(Music.RAP.getId(), 200));

		/*
		 * 1 vs 0.4 -> 0.4
		 * Total = 0.4
		 */

		long val = UserToVenueDna.calculatePreferencesCompatibility(userPref, venuePref);
		assertEquals(400, val);
	}

	public void testEmailThreadExample() {
		List<Long> userPref = new ArrayList<Long>();
		userPref.add(Music.RAP.getId());
		userPref.add(Music.ROCK.getId());

		List<DnaElementCount> venuePref = new ArrayList<DnaElementCount>();
		venuePref.add(new DnaElementCount(Music.ROCK.getId(), 200));
		venuePref.add(new DnaElementCount(Music.RAP.getId(), 100));

		/*
		 * 0.65 vs 0.333 -> 0.333
		 * 0.35 vs 0.666 -> 0.35
		 * Total = 0.683
		 */

		long val = UserToVenueDna.calculatePreferencesCompatibility(userPref, venuePref);

		assertEquals(683, val);
	}

	public void testAgeAndSpendingLimitCalculation() {
		long val = UserToVenueDna.calculateCompatibility(SpendingLimit.GETTING_BY.getId(), SpendingLimit.GETTING_BY.getId());
		assertEquals(1000, val);

		val = UserToVenueDna.calculateCompatibility(SpendingLimit.DOING_WELL.getId(), SpendingLimit.GETTING_BY.getId());
		assertEquals(500, val);

		val = UserToVenueDna.calculateCompatibility(SpendingLimit.GETTING_BY.getId(), SpendingLimit.DOING_WELL.getId());
		assertEquals(500, val);

		val = UserToVenueDna.calculateCompatibility(SpendingLimit.GETTING_BY.getId(), SpendingLimit.LIVING_THE_DREAM.getId());
		assertEquals(0, val);

		val = UserToVenueDna.calculateCompatibility(SpendingLimit.LIVING_THE_DREAM.getId(), SpendingLimit.GETTING_BY.getId());
		assertEquals(0, val);
	}

	/*
	 * Test Full Venue To Dna
	 */
	public void testUserToVenueDnaSimple() {
		UserDna userDna = Utils.createUserDna(
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR });

		VenueDna upvotedDna = Utils.createVenueDna(
				1, //Total User count
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new long[] { 500, 350, 150 }, //Count for each music preference
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR },
				new long[] { 500, 350, 150 }); //Count for each atmosphere preference
		VenueDna downvotedDna = new VenueDna(); //Empty

		CalcVenueDna venueDna = new CalcVenueDna(upvotedDna, downvotedDna);
		double val = UserToVenueDna.getUserDnaValue(userDna, venueDna);
		assertEquals(1.0, val);
	}

	public void testUserToVenueDnaSimple2() {
		UserDna userDna = Utils.createUserDna(
				Age._31_35,
				SpendingLimit.LIVING_THE_DREAM,
				new Music[] { Music.OLDIES, Music.ELECTRONIC, Music.LATIN },
				new Atmosphere[] { Atmosphere.SPORTS_BAR, Atmosphere.COCKTAIL_LOUNGE, Atmosphere.VIP_CLUB });

		VenueDna upvotedDna = Utils.createVenueDna(
				1, //Total User count
				Age._21_25,
				SpendingLimit.GETTING_BY,
				new Music[] { Music.ELECTRONIC, Music.OLDIES, Music.LATIN },
				new long[] { 3000, 2000, 1000 }, //Count for each music preference
				new Atmosphere[] { Atmosphere.LIVE_MUSIC, Atmosphere.SPORTS_BAR, Atmosphere.TECHNO_CLUB },
				new long[] { 3000, 2000, 1000 }); //Count for each atmosphere preference
		VenueDna downvotedDna = new VenueDna(); //Empty

		CalcVenueDna venueDna = new CalcVenueDna(upvotedDna, downvotedDna);
		double val = UserToVenueDna.getUserDnaValue(userDna, venueDna);
		assertEquals(.4831, val);
	}

	public void testUserToVenueDnaSimple3() {
		UserDna userDna = Utils.createUserDna(
				Age._31_35,
				SpendingLimit.LIVING_THE_DREAM,
				new Music[] { Music.OLDIES, Music.ELECTRONIC, Music.LATIN },
				new Atmosphere[] { Atmosphere.SPORTS_BAR, Atmosphere.COCKTAIL_LOUNGE, Atmosphere.VIP_CLUB });

		VenueDna upvotedDna = Utils.createVenueDna(
				1, //Total User count
				Age._31_35,
				SpendingLimit.GETTING_BY,
				new Music[] { Music.RNB, Music.OLDIES },
				new long[] { 3000, 2000 }, //Count for each music preference
				new Atmosphere[] { Atmosphere.VIP_CLUB },
				new long[] { 3000 }); //Count for each atmosphere preference
		VenueDna downvotedDna = new VenueDna(); //Empty

		CalcVenueDna venueDna = new CalcVenueDna(upvotedDna, downvotedDna);
		double val = UserToVenueDna.getUserDnaValue(userDna, venueDna);
		assertEquals(.38, val);
	}

	public void testUserToVenueDnaSimpleWithUserCount() {
		UserDna userDna = Utils.createUserDna(
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR });

		VenueDna upvotedDna = Utils.createVenueDna(
				5, //Total User count
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new long[] { 500, 350, 150 }, //Count for each music preference
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR },
				new long[] { 500, 350, 150 }); //Count for each atmosphere preference
		VenueDna downvotedDna = new VenueDna(); //Empty

		CalcVenueDna venueDna = new CalcVenueDna(upvotedDna, downvotedDna);
		double val = UserToVenueDna.getUserDnaValue(userDna, venueDna);
		assertEquals(5.0, val);
	}

	public void testUserToVenueDnaSimpleNoAgeMatch() {
		UserDna userDna = Utils.createUserDna(
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR });

		VenueDna upvotedDna = Utils.createVenueDna(
				1, //Total User count
				Age._31_35,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new long[] { 500, 350, 150 }, //Count for each music preference
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR },
				new long[] { 500, 350, 150 }); //Count for each atmosphere preference
		VenueDna downvotedDna = new VenueDna(); //Empty

		CalcVenueDna venueDna = new CalcVenueDna(upvotedDna, downvotedDna);
		double val = UserToVenueDna.getUserDnaValue(userDna, venueDna);
		assertEquals(0.85, val);
	}

	public void testUserToVenueDnaSimpleOneAwayAgeMatch() {
		UserDna userDna = Utils.createUserDna(
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR });

		VenueDna upvotedDna = Utils.createVenueDna(
				1, //Total User count
				Age._21_25,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new long[] { 500, 350, 150 }, //Count for each music preference
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR },
				new long[] { 500, 350, 150 }); //Count for each atmosphere preference
		VenueDna downvotedDna = new VenueDna(); //Empty

		CalcVenueDna venueDna = new CalcVenueDna(upvotedDna, downvotedDna);
		double val = UserToVenueDna.getUserDnaValue(userDna, venueDna);
		assertEquals(0.925, val);
	}

	public void testUserToVenueDnaSimpleNoSpendingMatch() {
		UserDna userDna = Utils.createUserDna(
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR });

		VenueDna upvotedDna = Utils.createVenueDna(
				1, //Total User count
				Age._18_20,
				SpendingLimit.LIVING_THE_DREAM,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new long[] { 500, 350, 150 }, //Count for each music preference
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR },
				new long[] { 500, 350, 150 }); //Count for each atmosphere preference
		VenueDna downvotedDna = new VenueDna(); //Empty

		CalcVenueDna venueDna = new CalcVenueDna(upvotedDna, downvotedDna);
		double val = UserToVenueDna.getUserDnaValue(userDna, venueDna);
		assertEquals(0.85, val);
	}

	public void testUserToVenueDnaSimpleOneAwaySpendingMatch() {
		UserDna userDna = Utils.createUserDna(
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR });

		VenueDna upvotedDna = Utils.createVenueDna(
				1, //Total User count
				Age._18_20,
				SpendingLimit.GETTING_BY,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new long[] { 500, 350, 150 }, //Count for each music preference
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR },
				new long[] { 500, 350, 150 }); //Count for each atmosphere preference
		VenueDna downvotedDna = new VenueDna(); //Empty

		CalcVenueDna venueDna = new CalcVenueDna(upvotedDna, downvotedDna);
		double val = UserToVenueDna.getUserDnaValue(userDna, venueDna);
		assertEquals(0.925, val);
	}

	public void testUserToVenueDnaSimpleNoMusicMatch() {
		UserDna userDna = Utils.createUserDna(
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR });

		VenueDna upvotedDna = Utils.createVenueDna(
				1, //Total User count
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.INDIE_ROCK },
				new long[] { 500 }, //Count for each music preference
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR },
				new long[] { 500, 350, 150 }); //Count for each atmosphere preference
		VenueDna downvotedDna = new VenueDna(); //Empty

		CalcVenueDna venueDna = new CalcVenueDna(upvotedDna, downvotedDna);
		double val = UserToVenueDna.getUserDnaValue(userDna, venueDna);
		assertEquals(0.5, val);
	}

	public void testUserToVenueDnaSimpleNoAtmosphereMatch() {
		UserDna userDna = Utils.createUserDna(
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR });

		VenueDna upvotedDna = Utils.createVenueDna(
				1, //Total User count
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new long[] { 500, 350, 150 }, //Count for each music preference
				new Atmosphere[] { Atmosphere.HIPSTER_LOUNGE },
				new long[] { 500 }); //Count for each atmosphere preference
		VenueDna downvotedDna = new VenueDna(); //Empty

		CalcVenueDna venueDna = new CalcVenueDna(upvotedDna, downvotedDna);
		double val = UserToVenueDna.getUserDnaValue(userDna, venueDna);
		assertEquals(0.8, val);
	}

	public void testUserToVenueDnaSimpleWithDownvote() {
		UserDna userDna = Utils.createUserDna(
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR });

		VenueDna upvotedDna = Utils.createVenueDna(
				1.5, //Total User count
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new long[] { 500, 350, 150 }, //Count for each music preference
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR },
				new long[] { 500, 350, 150 }); //Count for each atmosphere preference

		VenueDna downvotedDna = Utils.createVenueDna(
				1, //Total User count
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new long[] { 500, 350, 150 }, //Count for each music preference
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR },
				new long[] { 500, 350, 150 }); //Count for each atmosphere preference

		CalcVenueDna venueDna = new CalcVenueDna(upvotedDna, downvotedDna);
		double val = UserToVenueDna.getUserDnaValue(userDna, venueDna);
		assertEquals(0.5, val);
	}

	public void testUserToVenueDnaSimpleWithDownvoteUserCount() {
		UserDna userDna = Utils.createUserDna(
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR });

		VenueDna upvotedDna = Utils.createVenueDna(
				6, //Total User count
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new long[] { 500, 350, 150 }, //Count for each music preference
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR },
				new long[] { 500, 350, 150 }); //Count for each atmosphere preference

		VenueDna downvotedDna = Utils.createVenueDna(
				1, //Total User count
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new long[] { 500, 350, 150 }, //Count for each music preference
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR },
				new long[] { 500, 350, 150 }); //Count for each atmosphere preference

		CalcVenueDna venueDna = new CalcVenueDna(upvotedDna, downvotedDna);
		double val = UserToVenueDna.getUserDnaValue(userDna, venueDna);
		assertEquals(5.0, val);
	}

	public void testUserToVenueDnaSimpleWithDownvoteUserCountMoreDownvotes() {
		UserDna userDna = Utils.createUserDna(
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR });

		VenueDna upvotedDna = Utils.createVenueDna(
				1, //Total User count
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new long[] { 500, 350, 150 }, //Count for each music preference
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR },
				new long[] { 500, 350, 150 }); //Count for each atmosphere preference

		VenueDna downvotedDna = Utils.createVenueDna(
				6, //Total User count
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new long[] { 500, 350, 150 }, //Count for each music preference
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR },
				new long[] { 500, 350, 150 }); //Count for each atmosphere preference

		CalcVenueDna venueDna = new CalcVenueDna(upvotedDna, downvotedDna);
		double val = UserToVenueDna.getUserDnaValue(userDna, venueDna);
		assertEquals(-5.0, val);
	}

	public void testMikeExample() {
		UserDna userDna = Utils.createUserDna(
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR });

		VenueDna upvotedDna = Utils.createVenueDna(
				1, //Total User count
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new long[] { 3, 2, 1 }, //Count for each music preference
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR },
				new long[] { 3, 2, 1 }); //Count for each atmosphere preference
		VenueDna downvotedDna = new VenueDna(); //Empty

		CalcVenueDna venueDna = new CalcVenueDna(upvotedDna, downvotedDna);
		double val = UserToVenueDna.getUserDnaValue(userDna, venueDna);
		assertEquals(0.9881, val);
	}

	public void testMikeExample2() {
		UserDna userDna = Utils.createUserDna(
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR });

		VenueDna upvotedDna = Utils.createVenueDna(
				0.5, //Total User count
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new long[] { 3, 2, 1 }, //Count for each music preference
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR },
				new long[] { 3, 2, 1 }); //Count for each atmosphere preference
		VenueDna downvotedDna = new VenueDna(); //Empty

		CalcVenueDna venueDna = new CalcVenueDna(upvotedDna, downvotedDna);
		double val = UserToVenueDna.getUserDnaValue(userDna, venueDna);
		assertEquals(0.49405, val);
		assertEquals(0.9881, venueDna.getBaseUserVenueScore());
	}

	public void testSpeedTest() {
		UserDna userDna = Utils.createUserDna(
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR });

		VenueDna upvotedDna = Utils.createVenueDna(
				1, //Total User count
				Age._26_30,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new long[] { 500, 350, 150 }, //Count for each music preference
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR },
				new long[] { 500, 350, 150 }); //Count for each atmosphere preference

		VenueDna downvotedDna = Utils.createVenueDna(
				6, //Total User count
				Age._21_25,
				SpendingLimit.BROKE,
				new Music[] { Music.RAP, Music.ROCK, Music.COUNTRY },
				new long[] { 500, 350, 150 }, //Count for each music preference
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.DIVE_BAR, Atmosphere.GAY_BAR },
				new long[] { 500, 350, 150 }); //Count for each atmosphere preference
		CalcVenueDna venueDna = new CalcVenueDna(upvotedDna, downvotedDna);

		Timer timer = new Timer();
		timer.beginTime("SPEED");
		for (int i = 0; i < 300; i++) {
			double val = UserToVenueDna.getUserDnaValue(userDna, venueDna);
		}
		timer.endTime("SPEED");
		logger.info("Speed Test: {}", timer.print());
		//		assertEquals(-5.0, val);
	}

	/*
	 * 		List<Long> userPref = new ArrayList<Long>();
		userPref.add(Music.RAP.getId());
		userPref.add(Music.ROCK.getId());
		userPref.add(Music.CLASSIC_ROCK.getId());

		List<DnaElementCount> venuePref = new ArrayList<DnaElementCount>();
		venuePref.add(new DnaElementCount(Music.RAP.getId(), 100));
		venuePref.add(new DnaElementCount(Music.ROCK.getId(), 100));
		venuePref.add(new DnaElementCount(Music.CLASSIC_ROCK.getId(), 100));

		long val = UserToVenueDna.calculatePreferencesCompatibility(userPref, venuePref);
		assertEquals(816, val);
	 */

	public void testMikeTest() {
		UserDna userDna = Utils.createUserDna(
				Age._31_35,
				SpendingLimit.LIVING_THE_DREAM,
				new Music[] { Music.OLDIES, Music.ELECTRONIC, Music.LATIN },
				new Atmosphere[] { Atmosphere.SPORTS_BAR, Atmosphere.COCKTAIL_LOUNGE, Atmosphere.VIP_CLUB });

		VenueDna upvotedDna = Utils.createVenueDna(
				0.999, //Total User count
				Age._18_20,
				SpendingLimit.BROKE,
				new Music[] { Music.ELECTRONIC, Music.OLDIES, Music.RAP },
				new long[] { 1666, 1000, 1000 }, //Count for each music preference
				new Atmosphere[] { Atmosphere.SPORTS_BAR, Atmosphere.VIP_CLUB, Atmosphere.COCKTAIL_LOUNGE },
				new long[] { 1333, 1333, 1332 }); //Count for each atmosphere preference

		//		List<Long> userPref = new ArrayList<Long>();
		//		userPref.add(Music.OLDIES.getId());
		//		userPref.add(Music.ELECTRONIC.getId());
		//		userPref.add(Music.LATIN.getId());
		//
		//		List<DnaElementCount> venuePref = new ArrayList<DnaElementCount>();
		//		venuePref.add(new DnaElementCount(Music.ELECTRONIC.getId(), 1666));
		//		venuePref.add(new DnaElementCount(Music.OLDIES.getId(), 1000));
		//		venuePref.add(new DnaElementCount(Music.LATIN.getId(), 1000));
		//
		//		double val = UserToVenueDna.calculatePreferencesCompatibility(userPref, venuePref);
		//		assertEquals(0, val);

		VenueDna downvotedDna = Utils.createVenueDna(
				0.333, //Total User count
				Age._31_35,
				SpendingLimit.GETTING_BY,
				new Music[] { Music.RAP, Music.RNB, Music.OLDIES },
				new long[] { 1000, 666, 333 }, //Count for each music preference
				new Atmosphere[] { Atmosphere.COCKTAIL_LOUNGE, Atmosphere.GAY_BAR },
				new long[] { 1000, 666, }); //Count for each atmosphere preference
		CalcVenueDna venueDna = new CalcVenueDna(new VenueDna(), downvotedDna);

		double val = UserToVenueDna.getUserDnaValue(userDna, venueDna);
		assertEquals(-5.0, val);
	}
}
