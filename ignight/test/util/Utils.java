package util;

import com.ignight.dna.DnaElementCount;
import com.ignight.dna.VenueDna;
import com.ignight.dna.elements.Age;
import com.ignight.dna.elements.Atmosphere;
import com.ignight.dna.elements.Music;
import com.ignight.dna.elements.SpendingLimit;
import com.ignight.user.UserDna;

public class Utils {
	public static UserDna createUserDna(Age ageId, SpendingLimit spendingId, Music[] music, Atmosphere[] atmosphere) {
		UserDna dna = new UserDna().ageId(ageId.getId()).spendingLimit(spendingId.getId());
		for (int i = 0; i < music.length; i++) {
			dna.musicIds.add(music[i].getId());
		}
		for (int i = 0; i < atmosphere.length; i++) {
			dna.atmosphereIds.add(atmosphere[i].getId());
		}
		return dna;
	}

	public static VenueDna createVenueDna(double count, Age ageId, SpendingLimit spendingId, Music[] music, long[] musicCount, Atmosphere[] atmosphere, long[] atmosphereCount) {
		VenueDna dna = new VenueDna();
		dna.counter = (long) (count * 1000);
		dna.ageId = ageId.getId();
		dna.spendingLimit = spendingId.getId();
		for (int i = 0; i < music.length; i++) {
			dna.musicPreferences.add(new DnaElementCount(music[i].getId(), musicCount[i]));
		}
		for (int i = 0; i < atmosphere.length; i++) {
			dna.atmospherePreferences.add(new DnaElementCount(atmosphere[i].getId(), atmosphereCount[i]));
		}
		return dna;
	}
}
