import static com.ignight.dna.TrendingBarCalculator.BarColor.GREEN;
import static com.ignight.dna.TrendingBarCalculator.BarColor.ORANGE;
import static com.ignight.dna.TrendingBarCalculator.BarColor.RED;
import static com.ignight.dna.TrendingBarCalculator.BarColor.YELLOW;

import java.util.List;

import junit.framework.TestCase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ignight.dna.TrendingBarCalculator;
import com.ignight.dna.TrendingVenue;

public class TrendingBarCalculatorTest extends TestCase {
	private static final Logger logger = LoggerFactory.getLogger(TrendingBarCalculatorTest.class);
	TrendingBarCalculator calculator = new TrendingBarCalculator();

	public void setUp() throws Exception {
		calculator.clear();
		calculator.setCoefficeints(
				5, //Per color limits
				0.6, //Absolute Green Floor
				0.06); //Absolute Red Ceiling
	}

	public void testMeanStdDevCutoffs() {
		addVenue(0.5, null);
		addVenue(0.49, null);
		addVenue(0.59, null);
		addVenue(0.45, null);
		addVenue(0.49, null);
		addVenue(0.49, null);
		addVenue(0.6, null);
		addVenue(0.6, null);
		addVenue(0.6, null);
		addVenue(0.6, null);
		addVenue(0.4, null);
		addVenue(0.4, null);
		addVenue(0.4, null);
		addVenue(0.4, null);

		calculator.applyBarColors();
		assertEquals(0.5, getMean());
		assertEquals(0.083, getStdDev());
		assertEquals(0.5 - 0.083, getStdDevRedCutoff());
		assertEquals(0.5, getStdDevOrangeCutoff());
		assertEquals(0.584, getStdDevYellowCutoff()); //0.584 because of rounding
	}

	public void testUnweightedCutoff() {
		TrendingVenue tv = addVenue(0.61, 0.26); //addVenue (weightedDnaValue, baseDnaValue)
		calculator.applyBarColors();
		assertEquals(GREEN, tv.getBarColor());

		tv.setUnweightedDnaValue(0.25);
		calculator.applyBarColors();
		assertEquals(RED, tv.getBarColor());
	}

	public void testUnweightedCutoff2() {
		TrendingVenue tv = addVenue(0.9881, 0.9881); //addVenue (weightedDnaValue, baseDnaValue)
		TrendingVenue tv2 = addVenue(1.05, 0.15); //addVenue (weightedDnaValue, baseDnaValue)
		calculator.applyBarColors();

		assertEquals(GREEN, tv.getBarColor());
		assertEquals(RED, tv2.getBarColor());
	}

	public void testColorLimitsOnYellow() {
		TrendingVenue tv1 = addVenue(0.5, null);
		TrendingVenue tv2 = addVenue(0.4, null);
		TrendingVenue tv3 = addVenue(0.51, null);
		TrendingVenue tv4 = addVenue(0.45, null);
		TrendingVenue tv5 = addVenue(0.44, null);
		TrendingVenue tv6 = addVenue(0.49, null);

		calculator.applyBarColors();

		assertEquals(YELLOW, tv1.getBarColor());
		assertEquals(RED, tv2.getBarColor());
		assertEquals(GREEN, tv3.getBarColor());
		assertEquals(ORANGE, tv4.getBarColor());
		assertEquals(ORANGE, tv5.getBarColor());
		assertEquals(YELLOW, tv6.getBarColor());

		TrendingVenue tv7 = addVenue(0.49, null);
		TrendingVenue tv8 = addVenue(0.49, null);
		TrendingVenue tv9 = addVenue(0.49, null);
		TrendingVenue tv10 = addVenue(0.49, null);
		TrendingVenue tv11 = addVenue(0.49, null);

		calculator.applyBarColors();

		assertEquals(YELLOW, tv1.getBarColor());
		assertEquals(YELLOW, tv6.getBarColor());

		assertEquals(YELLOW, tv7.getBarColor());
		assertEquals(YELLOW, tv8.getBarColor());
		assertEquals(YELLOW, tv9.getBarColor());
		assertEquals(ORANGE, tv10.getBarColor());
		assertEquals(ORANGE, tv11.getBarColor());
	}

	public void testColorLimitsOnGreen() {
		TrendingVenue tv1 = addVenue(0.5, null);
		TrendingVenue tv2 = addVenue(0.49, null);
		TrendingVenue tv3 = addVenue(0.59, null);
		TrendingVenue tv4 = addVenue(0.45, null);
		TrendingVenue tv5 = addVenue(0.49, null);
		TrendingVenue tv6 = addVenue(0.49, null);

		calculator.applyBarColors();

		assertEquals(ORANGE, tv1.getBarColor());
		assertEquals(ORANGE, tv2.getBarColor());
		assertEquals(GREEN, tv3.getBarColor());
		assertEquals(RED, tv4.getBarColor());
		assertEquals(ORANGE, tv5.getBarColor());
		assertEquals(ORANGE, tv6.getBarColor());

		TrendingVenue tv7 = addVenue(0.6, null);
		TrendingVenue tv8 = addVenue(0.6, null);
		TrendingVenue tv9 = addVenue(0.6, null);
		TrendingVenue tv10 = addVenue(0.6, null);
		TrendingVenue tv11 = addVenue(0.4, null);
		TrendingVenue tv12 = addVenue(0.4, null);
		TrendingVenue tv13 = addVenue(0.4, null);
		TrendingVenue tv14 = addVenue(0.4, null);

		calculator.applyBarColors();

		assertEquals(ORANGE, tv1.getBarColor());
		assertEquals(ORANGE, tv2.getBarColor());
		assertEquals(GREEN, tv3.getBarColor());
		assertEquals(ORANGE, tv4.getBarColor());
		assertEquals(ORANGE, tv5.getBarColor());
		assertEquals(ORANGE, tv6.getBarColor());

		assertEquals(GREEN, tv7.getBarColor());
		assertEquals(GREEN, tv8.getBarColor());
		assertEquals(GREEN, tv9.getBarColor());
		assertEquals(GREEN, tv10.getBarColor());

		assertEquals(RED, tv11.getBarColor());
		assertEquals(RED, tv12.getBarColor());
		assertEquals(RED, tv13.getBarColor());
		assertEquals(RED, tv14.getBarColor());

		TrendingVenue tv20 = addVenue(0.6, null);
		calculator.applyBarColors();

		assertEquals(ORANGE, tv1.getBarColor());
		assertEquals(ORANGE, tv2.getBarColor());
		assertEquals(YELLOW, tv3.getBarColor());
		assertEquals(ORANGE, tv4.getBarColor());
		assertEquals(ORANGE, tv5.getBarColor());
		assertEquals(ORANGE, tv6.getBarColor());

		assertEquals(GREEN, tv7.getBarColor());
		assertEquals(GREEN, tv8.getBarColor());
		assertEquals(GREEN, tv9.getBarColor());
		assertEquals(GREEN, tv10.getBarColor());
		assertEquals(GREEN, tv20.getBarColor());

		assertEquals(RED, tv11.getBarColor());
		assertEquals(RED, tv12.getBarColor());
		assertEquals(RED, tv13.getBarColor());
		assertEquals(RED, tv14.getBarColor());
	}

	public void testAbsoluteLimits() {
		TrendingVenue tv = addVenue(0.060, null);
		calculator.applyBarColors();
		assertEquals(RED, tv.getBarColor());

		tv.setDnaValue(0.061);
		calculator.applyBarColors();
		assertEquals(ORANGE, tv.getBarColor());

		tv.setDnaValue(0.6);
		calculator.applyBarColors();
		assertEquals(GREEN, tv.getBarColor());
	}

	public void testBaseCase() {
		TrendingVenue bubCity = addVenue(0.73, 0.4);
		TrendingVenue twoAmigos = addVenue(0.225, 0.3);
		TrendingVenue halliganBar = addVenue(1000.0, 0.2);
		TrendingVenue twentyTwoThurty9 = addVenue(0.035, 0.2);
		TrendingVenue twentyFiveDegrees = addVenue(0.035, 0.9);

		//Apply Bar Colors and test
		calculator.applyBarColors();
		assertEquals(GREEN, bubCity.getBarColor());
		assertEquals(ORANGE, twoAmigos.getBarColor());
		assertEquals(RED, halliganBar.getBarColor());
		assertEquals(RED, twentyTwoThurty9.getBarColor());
		assertEquals(RED, twentyFiveDegrees.getBarColor());

		//Change Dna Value
		twentyTwoThurty9.setDnaValue(0.6);
		twentyFiveDegrees.setDnaValue(0.26);

		//Apply Bar Colors and test
		calculator.applyBarColors();
		assertEquals(GREEN, bubCity.getBarColor());
		assertEquals(ORANGE, twoAmigos.getBarColor());
		assertEquals(RED, halliganBar.getBarColor());
		assertEquals(RED, twentyTwoThurty9.getBarColor());
		assertEquals(ORANGE, twentyFiveDegrees.getBarColor());
	}

	public void testOrderingForRed() {
		TrendingVenue bubCity = addVenue(0.01, 0.4);
		TrendingVenue twoAmigos = addVenue(-0.4, 0.3);
		TrendingVenue halliganBar = addVenue(0.24, 0.2);
		TrendingVenue twentyTwoThurty9 = addVenue(-0.3, 0.2);
		TrendingVenue twentyFiveDegrees = addVenue(-0.01, 0.9);

		//Apply Bar Colors and test
		calculator.applyBarColors();
		assertEquals(RED, bubCity.getBarColor());
		assertEquals(RED, twoAmigos.getBarColor());
		assertEquals(RED, halliganBar.getBarColor());
		assertEquals(RED, twentyTwoThurty9.getBarColor());
		assertEquals(RED, twentyFiveDegrees.getBarColor());

		List<TrendingVenue> venues = calculator.getTrendingVenues();
		assertEquals(0.24, venues.get(0).getDnaValue());
		assertEquals(0.01, venues.get(1).getDnaValue());
		assertEquals(-0.01, venues.get(2).getDnaValue());
		assertEquals(-0.3, venues.get(3).getDnaValue());
		assertEquals(-0.4, venues.get(4).getDnaValue());
	}

	/*
	 * Helper Methods
	 */

	private TrendingVenue addVenue(Double val) {
		return addVenue(val, null);
	}

	private TrendingVenue addVenue(Double val, Double unweightedValue) {
		TrendingVenue tv = new TrendingVenue(val, unweightedValue);
		calculator.add(tv);
		return tv;
	}

	private double getMean() {
		return (int) (1000 * calculator.getMean()) / 1000.0;
	}

	private double getStdDev() {
		return (int) (1000 * calculator.getStdDev()) / 1000.0;
	}

	private double getStdDevRedCutoff() {
		return (int) (1000 * calculator.getStdDevRedCutoff()) / 1000.0;
	}

	private double getStdDevOrangeCutoff() {
		return (int) (1000 * calculator.getStdDevOrangeCutoff()) / 1000.0;
	}

	private double getStdDevYellowCutoff() {
		return (int) (1000 * calculator.getStdDevYellowCutoff()) / 1000.0;
	}
}
