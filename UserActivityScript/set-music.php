<?php 

    $page = 'login';
    $pageTitle = 'Select Music';
    $section = 'setup';

    include('includes/header.php'); 
    include('includes/top-bar.php'); 

    if (isset($_SESSION['error'])) {
    	$error = $_SESSION['error'];
    }

?>

<div id="main">

	<div class="steps clearfix">
		<ul>
			<li class="filled"></li>
			<li class="current"></li>
			<li></li>
			<li></li>
			<li></li>
		</ul>
		<div class="step-line"></div>
	</div>

	<?php 

		if (isset($error)) {
			echo '<p class="error">' . $error . '</p>';
		}
		unset($_SESSION['error']);

	?>

	<hr />

		
	<p>Please choose your top five favorite music genres in order.</p>

	<hr />

	<form class="login-form" action="functions/set-dna-handler.php?type=music" method="post">
	
		<?php

			$x = 1;
			while($x <= 5) {
		
				echo '<select name="music' . $x . '"><option value="null">Choose Music #' . $x . '</option>';
		
				foreach ($_SESSION['setup']['music'] as $k => $v) {
					echo '<option value="' . $k . '">' . $v . '</option>';
				}
		
				echo '</select>';
				$x ++;

			}
		
		?>
		
		<input type="submit" class="btn btn-success" value="Go To Next Step" />
	
	</form>

</div>

<?php include('includes/footer.php'); ?>
