<?php 

    $page = 'login';
    $pageTitle = 'Select Music';
    $section = 'setup';

    include('includes/header.php'); 
    include('includes/top-bar.php'); 

    if (isset($_SESSION['error'])) {
    	$error = $_SESSION['error'];
    }

?>

<div id="main">

	<div class="steps clearfix">
		<ul>
			<li class="filled"></li>
			<li class="filled"></li>
			<li class="filled"></li>
			<li class="current"></li>
			<li></li>
		</ul>
		<div class="step-line"></div>
	</div>

	<?php 

		if (isset($error)) {
			echo '<p class="error">' . $error . '</p>';
		}
		unset($_SESSION['error']);

	?>

	<hr />

		
	<p>Please choose your average spending preferences.</p>

	<hr />

	<form class="login-form" action="functions/set-dna-handler.php?type=spending" method="post">
		
		<select name="spendingLimit">
            <option value="null">Choose Spending Limit</option>
            <?php 
            	foreach ($_SESSION['setup']['spending'] as $k => $v) {
					echo '<option value="' . $k . '">' . $v . '</option>';
				}
            ?>
        </select>  
		
		<input type="submit" class="btn btn-success" value="Go To Next Step" />
	
	</form>

</div>

<?php include('includes/footer.php'); ?>
