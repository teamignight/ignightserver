<?php 

    include('includes/header.php'); 

    // Page variables

    $page = 'list';
    $pageTitle = 'Trending';
    $section = 'trending';

    // Load and calculate venues

    $venues = getTrendingList($user);

    include('includes/top-bar.php'); 
    include('includes/trending/search-bar.php');
    include('includes/trending/music-bar.php');

?>

<div id="main">

    <?php  

        // echo '<pre>';
        // print_r($user);
        // echo '</pre>';

    ?>
    

	
    <?php include('includes/trending/trending-list.php'); ?>

</div>

<?php include('includes/bottom-bar.php'); ?>

<?php include('includes/footer.php'); ?>