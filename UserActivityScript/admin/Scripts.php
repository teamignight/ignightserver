<?php 

    include('includes/header.php'); 

    // Page variables

    $page = 'login';
    $pageTitle = 'IgNight Admin';
    $section = 'admin';

    include('../includes/top-bar.php'); 

?>
        <div class="accordion-group">

            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#data-api">
                    Data API - GET
                </a>
            </div>
                    
                    <form class="admin clearfix" method="post" action="handlers/activityScript.php">
                        <label>Random Activity Script:</label>
                        <div class="input-append">
                            <input name="cityId" style="width: 210px" id="appendedInputButtons" placeholder="Enter cityId" type="text">
                            <input type="hidden" name="type" value="userList" />
                            <button class="btn" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                        </div>
                    </form>

            </div>


<?php include('includes/footer.php'); 