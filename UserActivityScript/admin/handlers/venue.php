<?php  

	require_once('../../classes/httpRequest.php');

	if (isset($_POST['venueName'])) {
		$_POST['venueName'] = $_POST['venueName'];
	}
	
	if ($_POST['method'] == 'get') {
		
		unset($_POST['method']);

		$type = $_POST['type'];
		unset($_POST['type']);

		if ($type == 'getInfo') {

			$data = array('venueId' => $_POST['venue1'], 'cityId' => $_POST['cityId']);

			unset($_POST);
			$_POST = $data;

		} else if($type == 'getVenueDataForUser') {
			$data = array('venueId' => $_POST['venueId'], 'userId' => $_POST['userId'],  'cityId' => $_POST['cityId'], 'buzzStartIndex' => $_POST['buzzStartIndex'],'noOfBuzzElements' => $_POST['noOfBuzzElements']);

			unset($_POST);
			$_POST = $data;

		}else if($type == 'getVenueBuzzChronological') {
			$data = array('venueId' => $_POST['venueId'], 'firstBuzzId' => $_POST['firstBuzzId'],  'noOfBuzzElements' => $_POST['noOfBuzzElements']);

			unset($_POST);
			$_POST = $data;

		} else if($type == 'setUserConnectionAvailabilityStatus') {
			$data = array('venueId' => $_POST['venueId'], 'userId' => $_POST['userId'],  'userConnectionAvailabilityStatus' => $_POST['userConnectionAvailabilityStatus']);

			unset($_POST);
			$_POST = $data; 

		} else if($type == 'sendConnectRequest') {
			$data = array('venueId' => $_POST['venueId'], 'userId' => $_POST['userId'], 'connectTargetUserId' => $_POST['connectTargetUserId']);

			unset($_POST);
			$_POST = $data; 

		}else if($type == 'acceptConnectRequest') {
			$data = array('venueId' => $_POST['venueId'], 'userId' => $_POST['userId'], 'connectRequestorUserId' => $_POST['connectRequestorUserId']);

			unset($_POST);
			$_POST = $data; 

		}else if($type == 'denyConnectRequest') {
			$data = array('venueId' => $_POST['venueId'], 'userId' => $_POST['userId'], 'connectRequestorUserId' => $_POST['connectRequestorUserId']);

			unset($_POST);
			$_POST = $data; 

		}else if($type == 'sendConnectMessage') {
			$data = array('venueId' => $_POST['venueId'], 'userId' => $_POST['userId'],  'connectionId' => $_POST['connectionId'], 'connectMessageText' => $_POST['connectMessageText']);

			unset($_POST);
			$_POST = $data; 

		}else if($type == 'blockAvailableConnectionUser') {
			$data = array('userId' => $_POST['userId'], 'userIdToBlock' => $_POST['userIdToBlock']);
			unset($_POST);
			$_POST = $data; 

		}else if($type == 'unBlockAvailableConnectionUser') {
			$data = array('userId' => $_POST['userId'], 'userIdToUnBlock' => $_POST['userIdToUnBlock']);
			unset($_POST);
			$_POST = $data; 

		}else if($type == 'getAvailableConnections') {
			$data = array('userId' => $_POST['userId'], 'venueId' => $_POST['venueId']);
			unset($_POST);
			$_POST = $data; 

		}else {

			$data = array('venueId' => $_POST['venueId'], 'cityId' => $_POST['cityId']);

			unset($_POST);
			$_POST = $data;

		}

		$get = new httpGet('venue', $type, $_POST);

		echo '<pre>';
		print_r($get);
		echo '</pre>';

		$response = json_decode(httpRequest::makeGetRequest($get));

	} else {

		unset($_POST['method']);

		$post = new httpPost('venue', $_POST);
		$response = httpRequest::makePostRequest($post);
	}

	

	if ($response) {

		echo '<pre>';
		print_r($response);
		echo '</pre>';
		
	} else {

		echo '<p class="error">No response form server</p>';

	}

	echo '<br /><br /><a href="../index.php">Back to admin Panel</a>'

?>