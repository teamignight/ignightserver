<?php  

	require_once('../../classes/httpRequest.php');
	
	if ($_POST['method'] == 'get') {

		unset($_POST['method']);

		$type = $_POST['type'];
		unset($_POST['type']);

		$get = new httpGet('user', $type, $_POST);

		echo '<pre>';
		print_r($get);
		echo '</pre>';

		$response = json_decode(httpRequest::makeGetRequest($get));

	} else {

		$img = array();

		unset($_POST['method']);

		if ($_POST['type'] == 'setDna') {

			$everything = array();
			$everything['type'] = $_POST['type'];
			$everything['spendingLimit'] = $_POST['spendingLimit'];
			$everything['userId'] = $_POST['userId'];

			$flatMusic = array();
			$flatAtmospheres = array();
			$flatVenues = array();

			foreach($_POST as $key => $value){
				if(strpos($key, 'music') !== false) {
					$flatMusic[] = (int) $value;
				}
			}

			foreach ($_POST as $key => $value) {
				if(strpos($key, 'atmosphere') !== false) { 
					$flatAtmospheres[] = (int) $value;
				}
			}

			foreach ($_POST as $key => $value) {
				if(strpos($key, 'venue') !== false) { 
					$flatVenues[] = (int) $value;
				}
			}

			$everything['music'] = $flatMusic;
			$everything['atmosphere'] = $flatAtmospheres;
			$everything['venue'] = $flatVenues;

			unset($_POST);
			$_POST = $everything;

		} elseif ($_POST['type'] == 'add') {
			$everything = array();
			$everything['type'] = $_POST['type'];
			$everything['userName'] = $_POST['userName'];
			$everything['password'] = $_POST['password'];
			$everything['email'] = $_POST['email'];

			unset($_POST);
			$_POST = $everything;

		} elseif($_POST['type'] == 'setCompleteUserInfo') {
			$everything = array();
			$everything['type'] = $_POST['type'];
			$everything['userId'] = $_POST['userId'];
			$everything['firstName'] = $_POST['firstName'];
			$everything['lastName'] = $_POST['lastName'];
			$everything['picture'] = $_POST['picture'];
			$everything['cityId'] = $_POST['cityId'];
			$everything['genderId'] = $_POST['genderId'];
			$everything['age'] = $_POST['age'];
			$everything['spendingLimit'] = $_POST['spendingLimit'];
			
			$flatMusic = array();
			$flatAtmospheres = array();
			$flatVenues = array();

			foreach($_POST as $key => $value){
				if(strpos($key, 'music') !== false) {
					$flatMusic[] = (int) $value;
				}
			}

			foreach ($_POST as $key => $value) {
				if(strpos($key, 'atmosphere') !== false) { 
					$flatAtmospheres[] = (int) $value;
				}
			}

			foreach ($_POST as $key => $value) {
				if(strpos($key, 'venue') !== false) { 
					$flatVenues[] = (int) $value;
				}
			}

			$everything['music'] = $flatMusic;
			$everything['atmosphere'] = $flatAtmospheres;
			$everything['venue'] = $flatVenues;
			

			unset($_POST);
			$_POST = $everything;
		}  elseif($_POST['type'] == 'addPublicUserGroup'){
			$everything = array();
			$everything['type'] = $_POST['type'];
			$everything['cityId'] = $_POST['cityId'];
			$everything['userId'] = $_POST['userId'];
			$everything['groupName'] = $_POST['groupName'];
			$everything['groupDescription'] = $_POST['groupDescription'];
			unset($_POST);
			$_POST = $everything;
			
		}  elseif($_POST['type'] == 'joinPublicUserGroup'){
			$everything = array();
			$everything['type'] = $_POST['type'];
			$everything['cityId'] = $_POST['cityId'];
			$everything['userId'] = $_POST['userId'];
			$everything['groupId'] = $_POST['groupId'];
			unset($_POST);
			$_POST = $everything;
			
		}  elseif($_POST['type'] == 'leavePublicUserGroup'){
			$everything = array();
			$everything['type'] = $_POST['type'];
			$everything['cityId'] = $_POST['cityId'];
			$everything['userId'] = $_POST['userId'];
			$everything['groupId'] = $_POST['groupId'];
			unset($_POST);
			$_POST = $everything;
			
		}  elseif($_POST['type'] == 'addPrivateUserGroup'){
			$everything = array();
			$everything['type'] = $_POST['type'];
			$everything['cityId'] = $_POST['cityId'];
			$everything['userId'] = $_POST['userId'];
			$everything['groupName'] = $_POST['groupName'];
			$everything['isGroupSearchable'] = $_POST['isGroupSearchable'];
			$everything['groupDescription'] = $_POST['groupDescription'];
			unset($_POST);
			$_POST = $everything;

		}  elseif($_POST['type'] == 'joinPrivateUserGroup'){
			$everything = array();
			$everything['type'] = $_POST['type'];
			$everything['cityId'] = $_POST['cityId'];
			$everything['userId'] = $_POST['userId'];
			$everything['groupId'] = $_POST['groupId'];
			unset($_POST);
			$_POST = $everything;
		
		}  elseif($_POST['type'] == 'leavePrivateUserGroup'){
			$everything = array();
			$everything['type'] = $_POST['type'];
			$everything['cityId'] = $_POST['cityId'];
			$everything['userId'] = $_POST['userId'];
			$everything['groupId'] = $_POST['groupId'];
			unset($_POST);
			$_POST = $everything;
			
		}  elseif($_POST['type'] == 'addGroupBuzz'){
			$everything = array();
			$everything['type'] = $_POST['type'];
			$everything['cityId'] = $_POST['cityId'];
			$everything['userId'] = $_POST['userId'];
			$everything['groupId'] = $_POST['groupId'];
			$everything['isPrivateGroup'] = $_POST['isPrivateGroup'];
			$everything['buzzText'] = $_POST['buzzText'];
			unset($_POST);
			$_POST = $everything;
		
		}  elseif($_POST['type'] == 'addUpvoteToGroupBuzz'){
			$everything = array();
			$everything['type'] = $_POST['type'];
			$everything['cityId'] = $_POST['cityId'];
			$everything['userId'] = $_POST['userId'];
			$everything['groupId'] = $_POST['groupId'];
			$everything['buzzId'] = $_POST['buzzId'];
			$everything['isPrivateGroup'] = $_POST['isPrivateGroup'];
			unset($_POST);
			$_POST = $everything;
		
		}  elseif($_POST['type'] == 'removeUpvoteFromGroupBuzz'){
			$everything = array();
			$everything['type'] = $_POST['type'];
			$everything['cityId'] = $_POST['cityId'];
			$everything['userId'] = $_POST['userId'];
			$everything['groupId'] = $_POST['groupId'];
			$everything['buzzId'] = $_POST['buzzId'];
			$everything['isPrivateGroup'] = $_POST['isPrivateGroup'];
			unset($_POST);
			$_POST = $everything;	
		
		}  elseif($_POST['type'] == 'sendPrivateUserGroupMembershipRequestToUser'){
			$everything = array();
			$everything['type'] = $_POST['type'];
			$everything['cityId'] = $_POST['cityId'];
			$everything['userId'] = $_POST['userId'];
			$everything['groupId'] = $_POST['groupId'];
			$everything['groupTargetUserId'] = $_POST['groupTargetUserId'];
			unset($_POST);
			$_POST = $everything;	
		
		}  elseif($_POST['type'] == 'sendPrivateUserGroupMembershipRequestToGroup'){
			$everything = array();
			$everything['type'] = $_POST['type'];
			$everything['cityId'] = $_POST['cityId'];
			$everything['userId'] = $_POST['userId'];
			$everything['groupId'] = $_POST['groupId'];
			unset($_POST);
			$_POST = $everything;	
					
		}  elseif($_POST['type'] == 'denyPrivateUserGroupMembershipRequestFromUser'){
			$everything = array();
			$everything['type'] = $_POST['type'];
			$everything['cityId'] = $_POST['cityId'];
			$everything['userId'] = $_POST['userId'];
			$everything['groupId'] = $_POST['groupId'];
			unset($_POST);
			$_POST = $everything;	

		}  elseif($_POST['type'] == 'denyPrivateUserGroupMembershipRequestFromGroup'){
			$everything = array();
			$everything['type'] = $_POST['type'];
			$everything['cityId'] = $_POST['cityId'];
			$everything['userId'] = $_POST['userId'];
			$everything['groupId'] = $_POST['groupId'];
			unset($_POST);
			$_POST = $everything;	
			
		}  elseif($_POST['type'] == 'revertDenyPrivateUserGroupMembershipRequestFromUser'){
			$everything = array();
			$everything['type'] = $_POST['type'];
			$everything['cityId'] = $_POST['cityId'];
			$everything['userId'] = $_POST['userId'];
			$everything['groupId'] = $_POST['groupId'];
			unset($_POST);
			$_POST = $everything;	
		
		}  elseif($_POST['type'] == 'revertDenyPrivateUserGroupMembershipRequestFromGroup'){
			$everything = array();
			$everything['type'] = $_POST['type'];
			$everything['cityId'] = $_POST['cityId'];
			$everything['userId'] = $_POST['userId'];
			$everything['groupId'] = $_POST['groupId'];
			unset($_POST);
			$_POST = $everything;	
		
		}  elseif($_POST['type'] == 'acceptPrivateUserGroupMembershipRequestFromUser'){
			$everything = array();
			$everything['type'] = $_POST['type'];
			$everything['cityId'] = $_POST['cityId'];
			$everything['userId'] = $_POST['userId'];
			$everything['groupId'] = $_POST['groupId'];
			unset($_POST);
			$_POST = $everything;		

		}  elseif($_POST['type'] == 'acceptPrivateUserGroupMembershipRequestFromGroup'){
			$everything = array();
			$everything['type'] = $_POST['type'];
			$everything['cityId'] = $_POST['cityId'];
			$everything['userId'] = $_POST['userId'];
			$everything['groupId'] = $_POST['groupId'];
			unset($_POST);
			$_POST = $everything;			
					
		}elseif($_POST['type'] == 'setUserInfo') {
			$everything = array();
			$everything['type'] = $_POST['type'];
			$everything['userId'] = $_POST['userId'];
			$everything['firstName'] = $_POST['firstName'];
			$everything['lastName'] = $_POST['lastName'];
			$everything['picture'] = $_POST['picture'];
			$everything['cityId'] = $_POST['cityId'];
			$everything['genderId'] = $_POST['genderId'];
			$everything['age'] = $_POST['age'];

			unset($_POST);
			$_POST = $everything;
		} elseif($_POST['type'] == 'updateSpendingLimit'){
			$everything = array();
			$everything['type'] = $_POST['type'];
			$everything['spendingLimit'] = $_POST['spendingLimit'];
			$everything['userId'] = $_POST['userId'];
			unset($_POST);
			$_POST = $everything;

		} elseif($_POST['type'] == 'updateMusic') {
			$everything = array();
			$flatMusic = array();

			foreach($_POST as $key => $value){
				if(strpos($key, 'music') !== false) {
					$flatMusic[] = (int) $value;
				}
			}

			$everything['type'] = $_POST['type'];
			$everything['userId'] = $_POST['userId'];
			$everything['music'] = $flatMusic;
			unset($_POST);
			$_POST = $everything;

		} elseif($_POST['type'] == 'updateAtmospheres') {
			$everything = array();
			$flatAtmospheres = array();

			foreach ($_POST as $key => $value) {
				if(strpos($key, 'atmosphere') !== false) { 
					$flatAtmospheres[] = (int) $value;
				}
			}

			$everything['type'] = $_POST['type'];
			$everything['userId'] = $_POST['userId'];
			$everything['atmosphere'] = $flatAtmospheres;
			unset($_POST);
			$_POST = $everything;

		} elseif($_POST['type'] == 'updateVenues') {
			$everything = array();
			$flatVenues = array();

			foreach ($_POST as $key => $value) {
				if(strpos($key, 'venue') !== false) { 
					$flatVenues[] = (int) $value;
				}
			}

			$everything['type'] = $_POST['type'];
			$everything['userId'] = $_POST['userId'];
			$everything['venue'] = $flatVenues;
			unset($_POST);
			$_POST = $everything;
		}



		$post = new httpPost('user', $_POST);

		print_r($post);
		$response = httpRequest::makePostRequest($post);
		
	}
	
	if ($response) {

		echo '<pre>';
		//echo 'before response';
		print_r($response);
		//echo 'after response';
		echo '</pre>';
		
	} else {

		echo '<p class="error">No response form server</p>';

	}

	echo '<br /><br /><a href="../index.php">Back to admin Panel</a>'

?>