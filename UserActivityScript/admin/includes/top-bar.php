
<div class="top-bar">
	<span class="page-title">
		<?php 
			if ($page != 'map' && $page != 'venue') {
				echo $pageTitle; 
			} else {
				echo '<a href="index.php" class="top-link"><i class="icon-arrow-left"></i>Back</a>';
			}
			
		?>
	</span>
	<?php if ($page != 'login') { ?>
		<a href="profile.php?id=0" class="btn <?php if ($page == 'profile' && $currentUser->id == $_GET['id']) { echo 'active'; } ?>"><i class="icon-user"></i></a>
	<?php } ?>
</div>