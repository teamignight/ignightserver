<?php

	require_once('../../classes/httpRequest.php');

	if ($_GET['id'] == 'null') {
		exit;
	} 

    $data = array('cityId' => $_GET['id']);
    $getVenueChoices = new httpGet('data', 'venueList', $data);
    $getVenueChoicesResponse = json_decode(httpRequest::makeGetRequest($getVenueChoices));

    $venueChoices = json_decode($getVenueChoicesResponse->body);

    $x = 1;
    while($x <= $_GET['count']) {

        echo '<select name="venue' . $x . '"><option value="null">Choose venue #' . $x . '</option>';

        foreach ($venueChoices as $venueChoice) {
            echo '<option value="' . $venueChoice->id . '">' . $venueChoice->name . '</option>';
        }

        echo '</select>';
        $x ++;

    }

?>