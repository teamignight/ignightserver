<?php

	class Comment {

		// Class properties

		static $index = 0;

		// Instance properties

		public $id;
		public $userId;
		public $venueId;
		public $content;
		public $votes;
		// public $timestamp;

		// Constructor

		public function __construct() {
	        $this->id = self::$index;
	        self::$index ++;    
	    }

		// Setters & Getters

	    public function getId() {
	    	return $this->$id;
	    }

		public function getUserId() {
			return $this->userId;
		}
		public function setUserId($userId) {
			$this->userId = $userId;
		}

		public function getVenueId() {
			return $this->venueId;
		}
		public function setVenueId($venueId) {
			$this->venueId = $venueId;
		}

		public function getContent() {
			return $this->content;
		}
		public function setContent($content) {
			$this->content = $content;
		}

		public function getVotes() {
			return $this->votes;
		}
		public function setVotes($votes) {
			$this->votes = $votes;
		}

		public function getTimestamp() {
			return $this->timestamp;
		}
		public function setTimestamp($timestamp) {
			$this->timestamp = $timestamp;
		}

		// Instance functions

		public function getDnaClass($dna) {

			if ($dna > 80) {
		 		$dnaClass = 'high';
		 	} else if ($dna > 60) {
		 		$dnaClass = 'med-high';
		 	} else if ($dna > 40) {
		 		$dnaClass = 'med';
		 	} else if ($dna > 10) {
		 		$dnaClass = 'low';
		 	} else {
		 		$dnaClass = '';
		 	}

		 	return $dnaClass;
		}

		public function addVote() {
			$votes = $this->getVotes();
			$votes ++;
			return $votes;
		}

	}

?>