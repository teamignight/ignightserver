<?php

	class Dna {

		// Instance properties

		public $topMusic = array();
		public $topAtmospheres = array();
		public $topVenues = array();
		public $age;
		public $spending;

		// Setters & Getters

		public function getTopMusic() {
			return $this->topMusic;
		}
		public function setTopMusic($topMusic) {
			foreach ($topMusic as $music) {
				$this->topMusic[] = $music;
			}

		}

		public function getTopAtmospheres() {
			return $this->topAtmospheres;
		}
		public function setTopAtmospheres($topAtmospheres) {
			foreach ($topAtmospheres as $atmosphere) {
				$this->topAtmospheres[] = $atmosphere;
			}
		}

		public function getTopVenues() {
			return $this->topVenues;
		}
		public function setTopVenues($topVenues) {
			foreach ($topVenues as $venue) {
				$this->topVenues[] = $venue;
			}
		}

		public function getAge() {
			return $this->age;
		}
		public function setAge($age) {
			$this->age = $age;
		}

		public function getSpending() {
			return $this->spending;
		}
		public function setSpending($spending) {
			$this->spending = $spending;
		}

	}

?>
