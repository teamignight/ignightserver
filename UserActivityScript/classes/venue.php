<?php

	class Venue {

		// Class properties

		static $index = 0;

		// Instance properties

		public $id;
		public $name;
		public $votes = array();
		public $userValue;
		public $address;
		public $city;
		public $state;
		public $zip;
		public $phone;
		public $url;
		public $lon;
		public $lat;
		public $venueUserValue;
		public $venueDna;
		public $buzz = array();

		// Constructor

		public function __construct() {
	        $this->id = self::$index;
	        self::$index ++;    
	    }

		// Setters & Getters

		public function getId() {
			return $this->id;
		}
		public function setId($id) {
			$this->id = $id;
		}

		public function getName() {
			return $this->name;
		}
		public function setName($name) {
			$this->name = $name;
		}

		public function getVotes() {
			return $this->votes;
		}
		public function setVotes($votes) {
			$this->votes = $votes;
		}

		public function getUserValue() {
			return $this->userValue;
		}
		public function setUserValue($userValue) {
			$this->userValue = $userValue;
		}

		public function getAddress() {
			return $this->address;
		}
		public function setAddress($address) {
			$this->address = $address;
		}

		public function getCity() {
			return $this->city;
		}
		public function setCity($city) {
			$this->city = $city;
		}

		public function getState() {
			return $this->state;
		}
		public function setState($state) {
			$this->state = $state;
		}

		public function getZip() {
			return $this->zip;
		}
		public function setZip($zip) {
			$this->zip = $zip;
		}

		public function getPhone() {
			return $this->phone;
		}
		public function setPhone($phone) {
			$this->phone = $phone;
		}

		public function getUrl() {
			return $this->url;
		}
		public function setUrl($url) {
			$this->url = $url;
		}

		public function getLon() {
			return $this->lon;
		}
		public function setLon($lon) {
			$this->lon = $lon;
		}

		public function getLat() {
			return $this->lat;
		}
		public function setLat($lat) {
			$this->lat = $lat;
		}

		public function getVenueDna() {
			return $this->venueDna;
		}
		public function setVenueDna($venueDna) {
			$this->venueDna = array(
				'music' => $venueDna['music'],
				'atmosphere' => $venueDna['atmosphere'],
				'age' => $venueDna['age'],
				'spending' => $venueDna['spending']
			);
		}

		public function getBuzz() {
			return $this->buzz;
		}
		public function setBuzz($buzz) {
			$this->buzz = $buzz;
		}

		// Class functions

		private function calcDistance($currentLocation, $destination) {
			return rand(0, 15);
		}
		private function calcDna($userDna, $venueDna) {
			return rand(0, 100);
		}

		// Instance functions

		public function getUserValueClass($userValue) {

			if ($userValue > 80) {
		 		$userValueClass = 'high';
		 	} else if ($userValue > 60) {
		 		$userValueClass = 'med-high';
		 	} else if ($userValue > 40) {
		 		$userValueClass = 'med';
		 	} else if ($userValue > 10) {
		 		$userValueClass = 'low';
		 	} else {
		 		$userValueClass = '';
		 	}

		 	return $userValueClass;
		}

		public function addVote($userId, $vote) {
			$votes = $this->getVotes();
			$votes[] = array('userId' => $userId, 'vote' => $vote);
			return $votes;
		}

		static function initFromJson($json) {

			// echo '<pre>';
			// print_r($json);
			// echo '</pre>';

			$venue = new Venue;
			$venue->setName($json->venue->metaData->name);
			$venue->setId($json->venue->metaData->venueId);
			$venue->setLat($json->venue->metaData->latitude);
			$venue->setLon($json->venue->metaData->longitude);
			$venue->setCity($json->venue->metaData->city);
			$venue->setAddress($json->venue->address);
			$venue->setZip($json->venue->zip);
			$venue->setPhone($json->venue->phoneNumber);
			$venue->setUrl($json->venue->url);
			$venue->setUserValue($json->venueTrendingScoreForUser);

			if (isset($json->dailyVenueData->venueBuzz)) {
				$venue->setBuzz($json->dailyVenueData->venueBuzz);
			}

			// echo '<pre>';
			// print_r($venue);
			// echo '</pre>';

			return $venue;
		}

		static function getNameFromId($venueId, $cityId) {

			$data = array('cityId' => $cityId, 'venueId' => $venueId);
    		$getVenue = new httpGet('venue', 'getInfo', $data);
    		$getVenueResponse = json_decode(httpRequest::makeGetRequest($getVenue));

    		$venue = $getVenueResponse->body;

    		return $venue->metaData->name;

		}

	}

?>