<?php

	 define('URL', 'http://ec2-174-129-68-153.compute-1.amazonaws.com:8080/IgNight/');
	 define('URL', 'localhost:8080/ignight/');
	
	class httpRequest {

		// Instance properties

		public $url;
		public $servlet;
		public $type;
		public $getData;
		public $postData;
		public $opt;
	
		// Setters & Getters

		public function getUrl() {
			return $this->url;
		}
		public function setUrl($url) {
			$this->url = $url;
		}

		public function getServlet() {
			return $this->servlet;
		}
		public function setServlet($servlet) {
			$this->servlet = $servlet;
		}

		public function getType() {
			return $this->type;
		}
		public function setType($type) {
			$this->type = $type;
		}

		public function getGetData() {
			return $this->getData;
		}
		public function setGetData($getData) {
			// Expects array
			$this->getData = httpRequest::parseAsUrl($getData);
		}

		public function getPostData() {
			return $this->postData;
		}
		public function setPostData($postData) {
			// Expects array
		 	$this->postData = array('msg' => $postData);
		} 

		public function getOpt() {
			return $this->opt;
		}
		public function setOpt($opt) {
			// Expects array
			$this->opt = $opt;
		}

		// Class methods

		static function parseAsUrl($getData) {
			$url = '';
			$x = 0;
			foreach ($getData as $key => $value) {
				$url .= $key . '=' . $value;
				$x ++;
				if ($x != count($getData)) {
					$url .= '&';
				}
			}
			return $url;
		}

		static function makeGetRequest($httpRequest) {

			$data = 'type=' . $httpRequest->getType() . '&' . $httpRequest->getGetData();
			$getUrl = $httpRequest->getUrl() . $httpRequest->getServlet() . '?' . $data;

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $getUrl);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			$response = json_encode(curl_exec($ch)); 

			// echo '<pre>';
			// print_r(curl_getinfo($ch));
			// echo '</pre>';

			curl_close($ch);

			if ($response) {
				return json_decode($response);
			} else {
				echo "Error";
			}


		}

		static function makePostRequest($httpRequest) {

			$postUrl = $httpRequest->getUrl() . $httpRequest->getServlet();
			$data = $httpRequest->getPostData();
			$userImage = $data['userImage'];
			unset($data['userImage']);
			$data_string = json_encode($data);
			//$data_string["json_body"] = json_encode($data);
			//$data_string["userImage"] = $userImage;
			//$data_string["test"] = "Test";
			$opt = $httpRequest->getOpt();

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $postUrl);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $opt);                       
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);  

			$response = curl_exec($ch);

			// echo '<pre>';
			// print_r(curl_getinfo($ch));
			// echo '</pre>';

			curl_close($ch);

			if ($response) {
				return json_decode($response);
			} else {

			}
		
		}


	}

	class httpGet extends httpRequest {

		// Constructor

		public function __construct($servlet, $type, $getData) {
	        $this->setUrl(URL);
	        $this->setServlet($servlet);
	        $this->setType($type);
	        $this->setGetData($getData);
	    }

	}

	class httpPost extends httpRequest {

		// Constructor

		public function __construct($servlet, $postData) {
	        $this->setUrl(URL);
	        //$this->setOpt(array('Content-Type: multipart/form-data; charset=utf-8')); 
	        $this->setOpt(array('Content-Type: application/json; charset=utf-8')); 
	        $this->setServlet($servlet);
	        $this->setPostData($postData);
	    }


	}

?>
