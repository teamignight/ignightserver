<div class="music-bar btn-group">
    <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Want to hear something <strong>new</strong> tonight? <span class="caret"></span></a>
    <ul class="dropdown-menu">
        <li><a href="#"><i class="icon-music"></i> Rap</a></li>
        <li><a href="#"><i class="icon-music"></i> Indie Rock</a></li>
        <li><a href="#"><i class="icon-music"></i> Techno</a></li>
        <li><a href="#"><i class="icon-music"></i> R&amp;B</a></li>
        <li><a href="#"><i class="icon-music"></i> Latin</a></li>
        <li><a href="#"><i class="icon-music"></i> Rock</a></li>
        <li><a href="#"><i class="icon-music"></i> Jazz</a></li>
        <li><a href="#"><i class="icon-music"></i> Pop</a></li>
        <li><a href="#"><i class="icon-music"></i> Reggae</a></li>
    </ul>
</div>