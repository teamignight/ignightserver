<div class="search-bar">
	<form class="form-inline" action="process-search.php">
		<input type="text" name="search" placeholder="Enter Venue Name..." />
		<input type="hidden" name="music" value="" />
		<?php if ($page == 'list') { ?>
        	<a class="btn ajax" targetdiv="main" href="map.php"><i class="icon-globe"></i></a>
        <?php } else { ?>
			<a class="btn ajax" targetdiv="main" href="#"><i class="icon-screenshot"></i></a>
        <?php } ?>
	</form>
	
</div>