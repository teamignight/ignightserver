<ul class="buzz">
		<?php
			$commentCount = 0;
			foreach($venue->getBuzz() as $comment) {
				


				$commentUser = User::initWithId($comment->userId);

				$dna = getDnaForUsers($user->getId(), $commentUser->getId());
				$commentUserDnaClass = getDnaClass($dna);

				$commentUsername = $commentUser->getFname() . ' ' . $commentUser->getLname();

				$commentContent = $comment->buzzText;

				// To be added
				$commentVoted = false;
				$commentVotes = 0;

		?>



			<li class="<?php if ($user->getId() == $commentUser->getId()) { echo 'current'; }?>">
				<div class="clearfix">
					<a href="profile.php?id=<?php echo $commentUser->getId(); ?>" class="user <?php echo $commentUserDnaClass; ?> tt" data-toggle="tooltip" title="<?php echo $commentUsername; ?>">
						<i class="icon-white icon-user"></i>
					</a>
					<div class="content">
						<a href="#" class="voted <?php if ($commentVoted) { echo 'true'; }?>"></a>
						<?php echo  nl2br($commentContent); ?>
					</div>
				</div>
				<div class="votes">
					<?php 
						if ($commentVotes > 0) { echo 'This comment has ' . $commentVotes . ' votes.'; } 
					?>
				</div>
				
			</li>

		<?php 
				$commentCount ++;
				if ($commentCount == $limit) {
					break;
				} 
			}
		?>
	</ul>
