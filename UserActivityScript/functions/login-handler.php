<?php  
	
	if (isset($_COOKIE["PHPSESSID"])) {
        session_start();
    }

    $home = $_SESSION['home'];

	require_once('../classes/httpRequest.php');
	require_once('../classes/user.php');
	require_once('../classes/dna.php');

	if (isset($_GET['type']) && $_GET['type'] == 'newUser') {
		$login = new httpPost('user', array('type' => 'login', 'userName' => $_SESSION['userName'], 'password' => $_SESSION['password']));
		$response = httpRequest::makePostRequest($login);
	}else {
		$login = new httpPost('user', $_POST);
		$response = httpRequest::makePostRequest($login);
	}
	
	

	if ($response && $response->res == 1) {

		$data = array('userId' => $response->body->id);
		$getInfo = new httpGet('user', 'getInfo', $data);
		$response = json_decode(httpRequest::makeGetRequest($getInfo));

		$user = User::initFromJson($response->body);

		$_SESSION['user'] = $user;
		unset($_SESSION['error']);

		header('Location:' . $home . '/index.php');
		
	} else {

		if (isset($response->text)) {
			$_SESSION['error'] = $textponse->text;
		} else {
			$_SESSION['error'] = 'Authentication failed.';
		}

		header('Location:..' . $home . '/login.php');

	}

?>