<?php

function getTrendingList($user) {
    $data = array('userId' => $user->userId, 'cityId' => $user->cityId);
    $getVenues = new httpGet('user', 'trendingList', $data);
    $response = json_decode(httpRequest::makeGetRequest($getVenues));

    if ($response && $response->res == 1) {

        $venues = array();
        foreach(json_decode($response->body) as $venue) {

            $data = array('venueId' => $venue->venueId, 'cityId' => $user->cityId);
            $getVenue = new httpGet('venue', 'getInfo', $data);
            $response = json_decode(httpRequest::makeGetRequest($getVenue));

            if ($response && $response->res == 1) {

                 $venues[] = Venue::buildVenueFromJson($response->body);

            }
        
        }

    }
}
