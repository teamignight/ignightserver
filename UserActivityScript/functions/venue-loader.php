<?php

	require_once('../classes/httpRequest.php');

	$file = "csv/venues.csv";

	$f = fopen($file, "r");

	$holder = array();

	$i = 0;
	$x = -1;
	while ($record = fgetcsv($f)) {

		foreach($record as $field) {

			if ($i % 8 == 0) {
				$x++;
			}

			$field = str_replace('. ', '', trim(preg_replace('/\s+/', ' ', $field)));
        	$holder[$x][] = $field;
        	$i ++;
    	}
	}

	$trash = array_pop($holder);


	foreach ($holder as $venue) {

		$data['type'] = 'add';

		$data['venueName'] = $venue[0];
		$data['url'] = $venue[1];
		$data['address'] = $venue[2];
		$data['cityId'] = (int) $venue[3];
		$data['zip'] = (int) $venue[4];
		$data['lat'] = (float) $venue[5];
		$data['lon'] = (float) $venue[6];
		$data['number'] = $venue[7];

		$post = new httpPost('venue', $data);
		$response = httpRequest::makePostRequest($post);

		echo '<pre>';
		print_r($response);
		echo '</pre>';

	}

	fclose($f);
