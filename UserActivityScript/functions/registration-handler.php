<?php  
	
	if (isset($_COOKIE["PHPSESSID"])) {
        session_start();
    }

    $home = $_SESSION['home'];

	require_once('../classes/httpRequest.php');



	if ($_POST && $_POST['confirmpassword'] == $_POST['password']) {


		unset($_POST['confirmpassword']);


	} else {

		$_SESSION['error'] = 'Passwords must match!';
		header('Location:..' . $home . '/register.php');
		exit;

	}

	// Check if userName is already taken
	$data = array('userName' => $_POST['userName']);
	$checkUserName = new httpGet('user', 'new', $data);
	$checkUserNameResponse = json_decode(httpRequest::makeGetRequest($checkUserName));

	if ($checkUserNameResponse && $checkUserNameResponse->res == 1) {

		// Add new user to db
		$addNewUser = new httpPost('user', $_POST); 
		$addNewUserResponse = httpRequest::makePostRequest($addNewUser);

		if ($addNewUserResponse && $addNewUserResponse->res == 1) {

			// Get new user's info and set the session
			$data = array('userId' => $addNewUserResponse->body->id);
			$getUserInfo = new httpGet('user', 'getInfo', $data);
			$getUserInfoResponse = json_decode(httpRequest::makeGetRequest($getUserInfo));

			$_SESSION['user'] = $getUserInfoResponse->body;
			unset($_SESSION['error']);

			$_SESSION['password'] = $_POST['password'];
			$_SESSION['userName'] = $_POST['userName'];

			header('Location:..' . $home . '/set-profile.php');
		
		} else {

			if ($addNewUserResponse && isset($addNewUserResponse->text)) {
				$_SESSION['error'] = $addNewUserResponse->text;
			} else {
				$_SESSION['error'] = 'Please try again';
			}

			header('Location:..' . $home . '/register.php');

		}

	} else {

		if ($checkUserNameResponse && isset($checkUserNameResponse->text)) {
			$_SESSION['error'] = $checkUserNameResponse->text;
		} else {
			$_SESSION['error'] = 'Username is taken';
		}

		header('Location:..' . $home . '/register.php');

	}	

	

	

?>