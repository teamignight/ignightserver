<?php 

    $page = 'login';
    $pageTitle = 'Select Music';
    $section = 'setup';

    include('includes/header.php'); 
    include('includes/top-bar.php'); 

    if (isset($_SESSION['error'])) {
    	$error = $_SESSION['error'];
    }

?>

<div id="main">

	<div class="steps clearfix">
		<ul>
			<li class="filled"></li>
			<li class="filled"></li>
			<li class="filled"></li>
			<li class="filled"></li>
			<li class="current"></li>
		</ul>
		<div class="step-line"></div>
	</div>

	<?php 

		if (isset($error)) {
			echo '<p class="error">' . $error . '</p>';
		}
		unset($_SESSION['error']);

	?>

	<hr />

		
	<p>Please choose your top three favorite venue atmospheres in order.</p>

	<hr />

	<form class="login-form" action="functions/set-dna-handler.php?type=venues" method="post">
	
		<?php

			$get = new httpGet('user', 'getInfo', array('userId' => $_SESSION['user']->userId));
			$userHolder = json_decode(httpRequest::makeGetRequest($get));

			$user = $userHolder->body;

			$data = array('cityId' => $user->cityId);
		    $getVenueChoices = new httpGet('data', 'venueList', $data);
		    $getVenueChoicesResponse = json_decode(httpRequest::makeGetRequest($getVenueChoices));

		    $venueChoices = $getVenueChoicesResponse->body;

		    $x = 1;
		    while($x <= 3) {

		        echo '<select name="venue' . $x . '"><option value="null">Choose venue #' . $x . '</option>';

		        foreach ($venueChoices as $venueChoice) {
		            echo '<option value="' . $venueChoice->id . '">' . $venueChoice->name . '</option>';
		        }

		        echo '</select>';
		        $x ++;

		    }
		
		?>
		
		<input type="submit" class="btn btn-success" value="Go To Next Step" />
	
	</form>

</div>

<?php include('includes/footer.php'); ?>
