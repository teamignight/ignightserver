<?php 

	$page = 'map';
	$pageTitle = '';
	$section = 'trending';

    include('includes/header.php'); 
    include('includes/top-bar.php');
    include('includes/trending/search-bar.php'); 
    include('includes/trending/music-bar.php');

?>

<div id="main">
	
    <iframe width="300" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=1+Hacker+Way,+Menlo+Park,+CA&amp;aq=0&amp;oq=1+hacker+way&amp;sll=40.780541,-52.382812&amp;sspn=73.752701,173.583984&amp;ie=UTF8&amp;hq=&amp;hnear=1+Hacker+Way,+Menlo+Park,+California+94025&amp;t=m&amp;z=14&amp;ll=37.484116,-122.148244&amp;output=embed"></iframe>
</div>

<?php include('includes/bottom-bar.php'); ?>

<?php include('includes/footer.php'); ?>