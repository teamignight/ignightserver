<?php 
    
    $page = 'profile';
    $pageTitle = 'Profile';
    $section = 'profile';

    include('includes/header.php'); 
    include('includes/top-bar.php'); 

    if (isset($_GET['id'])) {

    	$user = User::initWithId($_GET['id']);

    } else {
    	$user = $_SESSION['user'];
    }

    $id = $user->getId();
    $name = $user->getFname() . ' ' . $user->getLname();
    $location = $_SESSION['setup']['cities'][$user->getCityId()];
    $status = $user->getStatus();

    $dna = $user->dna;
    $music = $dna->getTopMusic();
    $venues = $dna->getTopVenues();
    $atmospheres = $dna->getTopAtmospheres();
    $age = $dna->age;
    $spending = $dna->spending;

?>

<div id="main">

	<div class="user-card clearfix">
		<div class="user-pic">
			<img src="#" alt="<?php echo $name; ?>">
		</div>
		<div class="user-info">
			<div class="user-name"><?php echo $name; ?></div>
			<div class="user-loc"><?php echo $location; ?></div>
			<div class="user-status">
				<?php echo $status; ?>
			</div>
		</div>
	</div>

	<?php if ($id != $_SESSION['user']->getId()) {?>
		
		<a class="connect btn btn-success" href="chat.php?id=<?php echo $id; ?>" class="chat">Start a chat with <strong><?php echo $name; ?></strong></a>

	<?php } ?>

	<ul class="top-lists">
	
		<li class="header"><i class="icon icon-music"></i>Top Music Genres</li>
		<?php $x=1; foreach ($music as $genre) { ?>
			<li><span class="n"><?php echo $x . '.'; ?></span><?php echo $_SESSION['setup']['music'][$genre]; ?></li>
		<?php $x++; } ?>

		<li class="header"><i class="icon icon-map-marker"></i>Top Venues</li>
		<?php $x=1; foreach ($venues as $venue) { ?>
			<li><span class="n"><?php echo $x . '.'; ?></span><?php echo Venue::getNameFromId($venue, $user->getCityId()); ?></li>
		<?php $x++; } ?>

		<li class="header"><i class="icon icon-glass"></i>Top Atmospheres</li>
		<?php $x=1; foreach ($atmospheres as $atmosphere) { ?>
			<li><span class="n"><?php echo $x . '.'; ?></span><?php echo $_SESSION['setup']['atmosphere'][$atmosphere]; ?></li>
		<?php $x++; } ?>

	</ul>

	<div class="recent-activity">
		<hr />
		<div class="subtitle">
			<h5><i class="icon icon-comment"></i>Recent Activity</h5>
		</div>
		<ul class="buzz">
			<?php 

				foreach ($user->comments as $comment) {
					foreach($_SESSION['venues'] as $venue) {
						if ($comment->getVenueId() == $venue->getId()) {
							$commentVenue = $venue;
						} 
					}

					$commentVenueName = $commentVenue->getName();
					$commentContent = $comment->getContent();
					$commentVotes = $comment->getVotes();

			?>

				<li>
					<div class="clearfix">
						<a href="venue.php?id=<?php echo $commentVenue->getId(); ?>" class="user high tt" data-toggle="tooltip" title="<?php echo $commentVenueName; ?>">
							<i class="icon-white icon-map-marker"></i>
						</a>
						<div class="content">
							<?php echo $commentContent; ?>
						</div>
					</div>
					<div class="votes">
						<?php 
							if ($commentVotes > 0) { echo 'This comment has ' . $commentVotes . ' votes.'; } 
						?>
					</div>
					
				</li>

			<?php } ?>

		</ul>

	</div>



    
</div>

<?php include('includes/bottom-bar.php'); ?>

<?php include('includes/footer.php'); ?>