<?php 

    $page = 'login';
    $pageTitle = 'IgNight Login';
    $section = 'setup';

    include('includes/header.php'); 
    include('includes/top-bar.php'); 

    if (isset($_SESSION['error'])) {
    	$error = $_SESSION['error'];
    }

?>

<div id="main">
	
	<form class="login-form" method="post" action="functions/login-handler.php">

		<input type="hidden" name="type" value="login" />

		<?php 

			if (isset($error)) {
				echo '<p class="error">' . $error . '</p>';
				unset($_SESSION['error']);
			}
			

		?>

		<p class="centered">Log in with</p>
		<p class="centered sm-btns">
			<a class="btn btn-primary">Facebook</a>
			<a class="btn btn-info">Twitter</a>
		</p>
		<p class="centered">or</p>
		<hr />
		<label>Email</label>
		<input name="userName" type="text" value="" />
		<label>Password</label>
		<input name="password" type="password" value="" />
		
		<input class="btn btn-success full" type="submit" value="Login" />

		<p class="centered">Don't have an account?</p>

		<a href="register.php" class="btn btn-warning full">Register a New Account</a>
			
	</form>

</div>

<?php include('includes/footer.php'); ?>
