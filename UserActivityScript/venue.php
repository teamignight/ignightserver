<?php 

    // Page variables

    $page = 'venue';
    $pageTitle = '';
    $section = 'trending';

    include('includes/header.php'); 
    include('includes/top-bar.php'); 

?>

<div id="main">
	
    <?php include('includes/trending/venue-details.php'); ?>

</div>

<?php include('includes/bottom-bar.php'); ?>

<?php include('includes/footer.php'); ?>