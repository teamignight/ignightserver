#Useful Links
- http://docs.aws.amazon.com/opsworks/latest/userguide/workingsecurity-ssl.html
- http://info.ssl.com/Article.aspx?id=12149

#Notes
- CANT test the certs with AFNetworking for iOS on Simulator


#Step 1
openssl genrsa 2048 > privatekey.pem

#Step 2
openssl req -new -key privatekey.pem -out csr.pem
#The common name must be the same name as the CNAME of the load balancer exactly

#Step 3
openssl x509 -req -days 365 -in csr.pem -signkey privatekey.pem -out server.crt

#Once you've added the public and private key to the ELB you can verify using
openssl s_client -connect production.chicago.ignight.com:443 -CAfile server.crt
#This should return a 'Verify return code: 0 (ok)' for Success

#Step 4 : Creates the .cer file that iOS Needs
openssl x509 -in server.crt -out iOSProdClient.cer -outform der

#TODO: Add Steps For Android
