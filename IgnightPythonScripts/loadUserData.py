#!/usr/local/bin/python

import redis
import csv
import requests
import json

import loadDataToSetupUser

r = redis.StrictRedis(host='localhost', port=6379, db=0)

keys = r.keys("users::*")
print keys
for key in keys:
    print "Deleting key: " + key
    r.delete(key)
    r.set("nextUserID", 0)

url = "http://localhost:8080/IgNight/user"
headers = {'content-type':"application/json; charset=utf-8"}

with open('chicagoSampleUserList.csv', 'rb') as csvfile:
    csvInfo = csv.reader(csvfile, delimiter=',')
    for row in csvInfo:
        userName = row[0]
        password = row[1]
        city = row[2]
        gender = row[4]
        age = row[5]
        music1 = row[6]
        music2 = row[7]
        music3 = row[8]
        music4 = row[9]
        music5 = row[10]
        venue1 = row[11]
        venue2 = row[12]
        venue3 = row[13]
        atm1 = row[14]
        atm2 = row[15]
        atm3 = row[16]
        spending = row[17]
        jsonMsg = {}
        msg = {}
        msg["type"] = "add"
        msg["userName"] = userName
        msg["password"] = password
        msg["venueCityId"] = loadDataToSetupUser.cityList[city.lower()]
        msg["genderId"] = loadDataToSetupUser.genderList[gender.lower()]
        jsonMsg["msg"] = msg
        r = requests.post(url, data=json.dumps(jsonMsg), headers=headers)
        res = json.loads(r.text)
        if res["res"] == True:
            print "Successful for user: " + user
        else:
            print "Failed user: " + user

        print "................."






