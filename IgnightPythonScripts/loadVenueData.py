#!/usr/local/bin/python

#import redis
import MySQLdb
import csv
import requests
import json

#import loadDataToSetupUser

db = MySQLdb.connect("localhost", "ignight", "ignight1234", "ignight_db")
c = db.cursor()
c.execute("set names utf8;")

#r = redis.StrictRedis(host='mes utf8;")localhost', port=6379, db=0)

#keys = r.keys("venues::*")
#print keys
#for key in keys:
#    print "Deleting key: " + key
#    r.delete(key)
#    r.set("nextVenueID", 0)

#nextVenueID = r.get("nextVenueID")
#if nextVenueID is None:
#    r.set("nextVenueID", 0)

#purl = "http://localhost:8080/ignight/venue"
#headers = {'content-type':"application/json; charset=utf-8"}

sql_insert = "INSERT INTO venues (name, city_id, address, number, latitude, longitude, url) VALUES (\"%s\", %s, \"%s\", \"%s\", %s, %s, \"%s\")"

with open('IgNight_Chicago_Venue_Data_Final.csv', 'rb') as csvfile:
    venueInfo = csv.reader(csvfile, delimiter=',')
    for row in venueInfo:
        name = row[0].replace("'", "\'")
        address = row[1].replace("'", "\'")
        city = row[2];
        #state = row[3]
        #zip = row[4]
        lat = row[5]
        lon = row[6]
        url = row[8].replace("'", "\'")
        phone = row[9].replace("'", "\'")
        
        if (lat == "" or lon == ""):
            continue;

        try :
            insert = (sql_insert % (name, 0, address, phone, lat, lon, url))
            c.execute(insert)
        except MySQLdb.ProgrammingError, e:
            print "The following query failed: %s, for reason: %s" % (insert, e)


db.commit()
db.close()
       
#        jsonMsg = {}
#        msg = {}
#        msg["type"] = "add"
#        msg["venueName"] = name
#        msg["address"] = address
#        msg["cityId"] = loadDataToSetupUser.cityList[city.lower()]
#        msg["zip"] = zip
#        msg["number"] = phone
#        msg["state"] = state
#        msg["lat"] = lat
#        msg["lon"] = lon
#        msg["url"] = url
#        jsonMsg["msg"] = msg
#        r = requests.post(purl, data=json.dumps(jsonMsg), headers=headers)
#        print r.text
#        print "...................."

