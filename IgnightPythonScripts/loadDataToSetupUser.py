#!/usr/local/bin/python

import redis
import csv
import requests
import json

r = redis.StrictRedis(host='localhost', port=6379, db=0)

headers = {'content-type':"application/json; charset=utf-8"}
dataUrl = "http://localhost:8080/ignight/data"

payload = {}
msg = {}
msg["type"] = "setupNewUser"
payload["msg"] = msg

cityList = {}
genderList = {}
musicList = {}
atmosphereList = {}
cityVenueList = {}

r = requests.get(dataUrl + "?type=setupNewUser", data=json.dumps(payload), headers=headers)
print r
res = json.loads(r.text)
print res
body = res['body']

genderList = body['gender']
musicList = body['music']
atmosphereList = body['atmosphere']
cityList = body['city']
print "GOT CITY LIST: " , cityList

for name, id in cityList.iteritems():
    msg["type"] = "venueList"
    msg["cityId"] = id
    payload["msg"] = msg
    r = requests.get(dataUrl+"?type=venueList&cityId=" + str(id),data=json.dumps(payload), headers=headers)
    res = json.loads(r.text)
    venueList = res['body']
    cityVenueList[id] = venueList

